//
//  singlePostVcHeaderView.swift
//  Qkopy
//
//  Created by Lincy George on 29/04/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit

@objc protocol singleTvcHeaderDelegate {
    func bookmarkTapped(source: UIView)
}
class singlePostVcHeaderView: UIView {

    @IBOutlet weak var profilePicImageView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var bookmarkButtonOutlet: UIButton!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBAction func bookmarkButtonTapped(_ sender: UIButton) {
        if Constants.clientSpecificConstantsStructName.showBookmarkBtn {
            self.bookmarkButtonOutlet.isSelected = !self.bookmarkButtonOutlet.isSelected
            //optionsWhenTappedOnChevronBtn(tag: sender.tag)
            headerDelegate?.bookmarkTapped(source: self.bookmarkButtonOutlet)
        }
    }
    weak var gotoUserDelegate: gotoUserProfileDelegate?
    weak var headerDelegate: singleTvcHeaderDelegate?
    var verifyImageView = UIImageView()
    var profileImageName: String = ""
    override func awakeFromNib() {
        if #available(iOS 13.0, *) {
            nameLabel.textColor = UIColor.label
            self.backgroundColor = UIColor.systemBackground
        } else {
            nameLabel.textColor = UIColor.black
            self.backgroundColor = UIColor.white
        }
        profilePicImageView.layer.cornerRadius = (profilePicImageView.frame.size.width) / 2
        profilePicImageView.clipsToBounds = true
        profilePicImageView.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))
        
        verifyImageView.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "verifyImage"))
        verifyImageView.frame = CGRect(x:profilePicImageView.frame.origin.x + profilePicImageView.frame.width - 20, y: profilePicImageView.frame.origin.y + profilePicImageView.frame.height - 20, width: 20, height: 20)
        self.addSubview(verifyImageView)
        let imageTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped))
        profilePicImageView?.isUserInteractionEnabled = true
        profilePicImageView?.addGestureRecognizer(imageTapGestureRecognizer)
        let nameLabelTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.nameLabelTapped))
        nameLabel?.isUserInteractionEnabled = true
        nameLabel?.addGestureRecognizer(nameLabelTapGestureRecognizer)
        let dateLabelTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dateLabelTapped))
        dateLabel?.isUserInteractionEnabled = true
        dateLabel?.addGestureRecognizer(dateLabelTapGestureRecognizer)
        settingFonts()
        if !Constants.clientSpecificConstantsStructName.showBookmarkBtn {
            bookmarkButtonOutlet.isHidden = true
        }
        
    }
    
    func settingFonts() {
        nameLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        dateLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
    }
    
    //MARK: - Targets
    //go to user profile on tap of dp
    @objc func imageTapped() {
        gotoUserDelegate?.tappedToOpenProfileFromSinglePostView!()
    }
    
    //go to user profile on tap of date label
    @objc func dateLabelTapped() {
        gotoUserDelegate?.tappedToOpenProfileFromSinglePostView!()
    }
    
    //go to user profile on tap of name label
    @objc func nameLabelTapped() {
        gotoUserDelegate?.tappedToOpenProfileFromSinglePostView!()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
