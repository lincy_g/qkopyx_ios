//
//  emptyBgImageView.swift
//  Qkopy
//
//  Created by Lincy George on 01/07/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit

class emptyBgImageView: UIView {
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var descLabelBottomConstraint: NSLayoutConstraint!
    
    var title: String? {
        get { return descLabel?.text }
        set { descLabel.text = newValue }
    }
    
    var image: UIImage? {
        get { return bgImageView?.image }
        set { bgImageView.image = newValue }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        if #available(iOS 13.0, *) {
            self.backgroundColor = UIColor.systemGray5
            descLabel.textColor = UIColor.secondaryLabel
        } else {
            self.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
            descLabel.textColor = UIColor.gray
        }
        if Constants.defaults.value(forKey: "contactsAccessPermitted") as? String == "false" {
            bgImageView.contentMode = .bottom
        } else {
            bgImageView.contentMode = .scaleAspectFit
            descLabelBottomConstraint.constant = UIScreen.main.bounds.height/4
        }
        descLabel.numberOfLines = 0
        descLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        descLabel.textAlignment = .center
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
    }
    @objc func userChangedTextSize(notification: NSNotification) {
        descLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
    }
}

extension UIView {
    /** Loads instance from nib with the same name. */
    func loadNib() -> UIView {
        let bundle = Bundle(for: type(of: self))
        let nibName = type(of: self).description().components(separatedBy: ".").last!
        let nib = UINib(nibName: nibName, bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as! UIView
    }
}
