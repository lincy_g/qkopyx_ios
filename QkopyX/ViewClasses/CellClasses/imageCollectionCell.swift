//
//  feedbackCell.swift
//  Qkopy
//
//  Created by Qkopy team on 24/11/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
@objc protocol imageCollectionDelegate {
    func closeTapped(index: Int)
}
@objc protocol imageTappedDelegate {
    func imgVwTapped(index: Int)
}
class imageCollectionCell: UICollectionViewCell {
    weak var delegate: imageCollectionDelegate?
    weak var imageDelegate: imageTappedDelegate?
    var index: Int?
    var textLabel: UILabel?
    @IBOutlet weak var imgvw: UIImageView!
    
    @IBOutlet weak var closeBtn: UIButton!
    @IBAction func closeBtnAction(_ sender: Any) {
        delegate?.closeTapped(index: index ?? 0)
    }
    
    override func awakeFromNib() {
        textLabel?.isHidden = true
        closeBtn.frame = CGRect(x: imgvw.frame.width - 30, y: 0, width: 30, height: 30)
        closeBtn.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        closeBtn.layer.cornerRadius = closeBtn.frame.size.width/2
        closeBtn.clipsToBounds = true
        closeBtn.layer.borderColor = UIColor.darkGray.cgColor
        closeBtn.layer.borderWidth = 0.5
        closeBtn.backgroundColor = UIColor.white
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped))
        imgvw.isUserInteractionEnabled = true
        imgvw.addGestureRecognizer(tap)
        imgvw.contentMode = .scaleAspectFill
        imgvw.clipsToBounds = true
        contentView.layer.cornerRadius = 6
        contentView.layer.masksToBounds = true
        let btnImg = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "closeImg"))
        closeBtn.setImage(btnImg, for: .normal)
    }
    
    @objc func imageTapped() {
        imageDelegate?.imgVwTapped(index: index ?? 0)
    }
}
