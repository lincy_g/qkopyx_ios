//
//  StatusFeedTableViewCell.swift
//  Qkopy
//
//  Created by Qkopy team on 14/03/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import URLEmbeddedView
import youtube_ios_player_helper
import AVFoundation

@objc protocol statusDelegateForClose {
    func closeTapped(cell: StatusFeedTableViewCell, pid: String)
}
@objc protocol bookmarkDelegate {
    func reloadTable(indexPath: IndexPath)
}
@objc protocol statusDelegateForChevron {
    func chevronTapped(cell: StatusFeedTableViewCell, desc: String, pid: String, time: String, privacy: String, location: String, locationName: String, color_code: String, postImageName: String, privacyID: String, customContacts: [String], viewsCount: String, isBookmark: Bool, attachments: [[String: Any]])
}

@objc protocol showMoreShowLessDelegate {
    func cellExpandOrReduce(pid: String, cell: StatusFeedTableViewCell)
}

@objc protocol gotoUserProfileDelegate {
    @objc optional func tappedToOpenProfileFromCell()
    @objc optional func tappedToOpenProfileFromSinglePostView()
}

@objc protocol singlePostVcDelegate {
    func openPostInSingleScreen(cell: StatusFeedTableViewCell)
}

@objc protocol openImageInFullScreenDelegate {
    func pushToVC(currentImagePath: String, imagePaths: [String], titleName: String, indexPath: IndexPath?)
}

@objc protocol openDocInWebviewDelegate {
    func openDoc(cell: StatusFeedTableViewCell, link: String, name: String)
}

@objc protocol openLikedListDelegate {
    func pushToLikedVC(likedArr: [String])
}

@objc protocol goToChatDelegate {
    func openParticularChatVC(userId: String, snippet: String)
}

@objc protocol setLikeStatusInParentVCDelegate {
    func updateLikePropertiesInDict(indexPath: IndexPath?, liked: Bool, likesArr: [String], likesArrCount: Int)
}
@objc class StatusFeedTableViewCell: UITableViewCell, CLLocationManagerDelegate, MKMapViewDelegate, NSLayoutManagerDelegate, YTPlayerViewDelegate, AVAudioPlayerDelegate {
    //constants
    var embeddedView: URLEmbeddedView?

    //variables
    var marker: GMSMarker?
    var pid: String?
    var time: String?
    weak var closeDelegate: statusDelegateForClose?
    weak var gotoUserDelegate: gotoUserProfileDelegate?
    weak var chevronDelegate: statusDelegateForChevron?
    weak var showMoreLessDelegate: showMoreShowLessDelegate?
    weak var fullScreenDelegate: openImageInFullScreenDelegate?
    weak var likedListDelegate: openLikedListDelegate?
    weak var updateLikesInDictDelegate: setLikeStatusInParentVCDelegate?
    weak var openInWebviewDelegate: openDocInWebviewDelegate?
    weak var chatVCDelegate: goToChatDelegate?
    weak var openSinglePostVcDelegate: singlePostVcDelegate?
    weak var bookmarkCellDelegate: bookmarkDelegate?

    var desc: String?
    var color_code: String?
    var locationName: String?
    var privacy: String?
    var previewView: UIView?
    var previewLinkURLString: String?
    var userID: String?
    var showPreview: String?
    var showMore: Bool?
    lazy var tappedOnStatusTV: UITapGestureRecognizer = {
        UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.textTapped))
    }()
    lazy var tappedOnLocTV: UITapGestureRecognizer = {
        UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.locTextTapped))
    }()
    lazy var tappedOnAttachment: UITapGestureRecognizer = {
        UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.attachmentTapped))
    }()
    var cellBackgroundView = UIView()
    var playerView: YTPlayerView?
    var playerIndicator: UIActivityIndicatorView?
    var entireHeight: CGFloat = 120
    var blackView: UIView?
    var postImageView: UIImageView?
    var imagesCollectionView: UICollectionView?
    var postImageName = ""
    var customContacts: [String] = []
    var privacyID: String?
    var viewsCount: String?
    var likesArr: Array<String> = []
    var likesArrCount = 0
    var likesCountButton = UIButton()
    var runLikeUnlikeApi = true
    var workItem: DispatchWorkItem?
    var firstCallPreviousState: Bool?
    var verifyImageView = UIImageView()
    var attachmentImageView: UIImageView?
    var attachmentNameLabel: UILabel?
    var attachments: [[String: Any]] = []
    var attachmentBaseView: UIView?
    var attachmentBackgroundView: UIView?
    var downloadImageView: UIImageView?
    var isDownloading: Bool?
    var circularProgressView: CircularProgress?
    var playRecImage: UIImage?
    var pauseRecImage: UIImage?
    var playPauseBtn: UIButton?
    var voicePlayerTextView: UITextView?
    var audioPlayer: AVAudioPlayer?
    var slider: UISlider?
    var timer: Timer?
    var progressLabel: UILabel?
    
    //outlets
    @IBOutlet weak var locationTextView: UITextView!
    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet var likeButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profilePicImgVw: UIImageView!
    @IBOutlet weak var closeBtn: UIButton!
    @IBOutlet weak var bookmark: UIButton!
    @IBOutlet weak var locationMarkerTextView: UITextView!
    @IBOutlet weak var greyedOutOverlay: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var coloredTextView: UITextView!
    @IBOutlet weak var viewsCountLabel: UILabel!
    @IBOutlet weak var chatBtnOutlet: UIButton!
    
    @IBOutlet var mapViewToDateLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationTVBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var statusTVBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var coloredTVBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var indicatorForPost: UIActivityIndicatorView!
    @IBOutlet weak var hashLabel: UILabel!
    @IBOutlet var hashLabelHeightConstant: NSLayoutConstraint!
    @IBOutlet var mapViewHeightConstraint: NSLayoutConstraint!
    //MARK: - Actions
    //close btn taaped- on pending to be posted feed cells
    @IBAction func closeBtnAction(_ sender: Any) {
        closeDelegate?.closeTapped(cell: self, pid: pid ?? "")
    }
    //MARK: - View methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        embeddedView = URLEmbeddedView()
        attachmentBackgroundView = UIView()
        attachmentBaseView = UIView()
        previewView = UIView()
        attachmentImageView = UIImageView()
        circularProgressView = CircularProgress(frame: CGRect(x: 10, y: 10, width: 50, height: 50))
        downloadImageView = UIImageView()
        playPauseBtn = UIButton()
        attachmentNameLabel = UILabel()
        postImageView = UIImageView()
        playerView = YTPlayerView()
        playerView?.delegate = self
        profilePicImgVw?.layer.cornerRadius = (profilePicImgVw?.frame.size.width)! / 2
        profilePicImgVw?.clipsToBounds = true
        profilePicImgVw?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))
        let layout = collageCollectionLayout()
        layout.delegate = self
        imagesCollectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: 100), collectionViewLayout: layout)
        imagesCollectionView?.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
        imagesCollectionView?.isScrollEnabled = false
        slider = UISlider()
        progressLabel = UILabel()
        
        likeButton.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "loveGrayImage"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        likeButton.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "loveRedImage")), for: .selected)
        bookmark.setImage(UIImage(named: "bookmarkOutline.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        bookmark.setImage(UIImage(named: "bookmarkSelected.png")?.withRenderingMode(.alwaysTemplate), for: .selected)
        hashLabel.textAlignment = .left
        hashLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        likeButton.addTarget(self, action: #selector(StatusFeedTableViewCell.handleLike), for: .touchUpInside)
        likeButton.imageView?.contentMode = .scaleAspectFit
        
        likesCountButton.frame = CGRect(x: 30, y: 0, width: viewsCountLabel.frame.width/3, height: viewsCountLabel.frame.height)
        likesCountButton.addTarget(self, action: #selector(StatusFeedTableViewCell.openLikesList), for: .touchUpInside)
        
        verifyImageView.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "verifyImage"))
        verifyImageView.frame = CGRect(x:profilePicImgVw.frame.origin.x + profilePicImgVw.frame.width - 20, y: profilePicImgVw.frame.origin.y + profilePicImgVw.frame.height - 20, width: 20, height: 20)
        
        playRecImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "playIcon"))
        pauseRecImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pauseIcon"))
        playPauseBtn?.tag = 0
        playPauseBtn?.setImage(playRecImage, for: .normal)
        playPauseBtn?.addTarget(self, action: #selector(self.recorderAction(sender:)), for: .touchUpInside)
        playPauseBtn?.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        
        progressLabel?.textColor = UIColor.gray
        progressLabel?.textAlignment = .center
        progressLabel?.text = "0.0"
        progressLabel?.textAlignment = .left
        progressLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        slider?.thumbTintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        slider?.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode"))
        //slider?.addTarget(self, action: #selector(self.endOfSliderDrag), for: .touchDragExit)
        slider?.addTarget(self, action: #selector(self.playbackSliderValueChanged), for: .touchDragInside)
        attachmentBaseView?.addSubview(slider!)
        attachmentBaseView?.addSubview(progressLabel!)
        
        shareButton.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "shareExternalImage"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        shareButton.addTarget(self, action: #selector(StatusFeedTableViewCell.shareButtonTapped(sender:)), for: .touchUpInside)
        shareButton.imageView?.contentMode = .scaleAspectFit
        
        chatBtnOutlet.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "replyIcon"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        chatBtnOutlet.addTarget(self, action: #selector(StatusFeedTableViewCell.chatButtonTapped(sender:)), for: .touchUpInside)
        chatBtnOutlet.frame.size = CGSize(width: 60, height: 60)
        chatBtnOutlet.imageView?.contentMode = .scaleAspectFit
        //shareButton.backgroundColor = UIColor.red
        viewsCountLabel.isUserInteractionEnabled = true
        viewsCountLabel.addSubview(likeButton)
        viewsCountLabel.addSubview(likesCountButton)
        viewsCountLabel.addSubview(shareButton)
        viewsCountLabel.addSubview(chatBtnOutlet)
        viewsCountLabel.bringSubviewToFront(likesCountButton)
        statusTextView.layoutManager.delegate = self
        
        //remove the below lines
        if !Constants.clientSpecificConstantsStructName.showChatAndLikeBtns {
            likeButton.isHidden = true
            likesCountButton.isHidden = true
            chatBtnOutlet.isHidden = true
        }
        if !Constants.clientSpecificConstantsStructName.showBookmarkBtn {
            bookmark.isHidden = true
        }
        coloredTextView.textColor = UIColor.white
        coloredTextView.layoutManager.delegate = self
        coloredTextView.tintColor = UIColor.white

        attachmentBaseView?.layer.cornerRadius = 10
        attachmentBaseView?.clipsToBounds = true
        attachmentBaseView?.layer.borderWidth = CGFloat(0.5)
        attachmentBaseView?.isUserInteractionEnabled = true
        attachmentBaseView?.layer.borderColor = UIColor.gray.cgColor

        attachmentImageView?.isUserInteractionEnabled = true
        attachmentImageView?.contentMode = .scaleAspectFill
        attachmentImageView?.clipsToBounds = true
        attachmentImageView?.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        
        downloadImageView?.isUserInteractionEnabled = true
        downloadImageView?.contentMode = .scaleAspectFit
        downloadImageView?.frame = CGRect(x: 10, y: 35, width: 60, height: 30)
        
        attachmentNameLabel?.isUserInteractionEnabled = true
        attachmentNameLabel?.contentMode = .scaleAspectFit
        attachmentNameLabel?.numberOfLines = 0
        
        statusTextView.dataDetectorTypes = [.phoneNumber]
        locationTextView.dataDetectorTypes = [.phoneNumber]
        coloredTextView.dataDetectorTypes = [.phoneNumber, .link]
        
        locationTextView.layoutManager.delegate = self
        
        tappedOnStatusTV.delegate = self
        tappedOnLocTV.delegate = self
        
        bookmark.tag = 0
        shareButton.tag = 1
        settingFonts()
        self.imagesCollectionView?.register(UINib(nibName: "imageCollectionCell", bundle: nil), forCellWithReuseIdentifier: Constants.nameOfIdentifiers.imageCollectionCellIdentifier)
        DispatchQueue.main.async {
            self.bookmark.addTarget(self, action: #selector(StatusFeedTableViewCell.bookmarkTapped(sender:)), for: .touchUpInside)
            self.statusTextView.isEditable = false
            self.coloredTextView.isEditable = false
            self.locationTextView.isEditable = false
            self.locationMarkerTextView.isEditable = false
            
            self.locationTextView.textContainerInset = UIEdgeInsets.init(top: 5, left: 6, bottom: 0, right: 0)
            self.locationMarkerTextView.contentInset = UIEdgeInsets.init(top: -2, left: -2, bottom: 0, right: 0)
            self.statusTextView.textContainerInset = UIEdgeInsets.init(top: 5, left: 6, bottom: 0, right: 0)
            self.coloredTextView.textContainerInset = UIEdgeInsets.init(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0)
            
            self.viewsCountLabel?.textAlignment = .right
            
            self.locationTextView.isScrollEnabled = false
            self.statusTextView.isScrollEnabled = false
            self.coloredTextView.isScrollEnabled = false
            self.locationMarkerTextView.isScrollEnabled = false
            self.entireHeight = Constants.defaults.value(forKey: "sizeOfLimitedCharacters") as! CGFloat
            self.coloredTextView.frame = CGRect(x: self.coloredTextView.frame.origin.x, y: self.coloredTextView.frame.origin.y, width: self.coloredTextView.frame.width, height: self.entireHeight)
            self.coloredTextView.centerVertically()
        }
        statusTextView.frame = CGRect(x: statusTextView.frame.origin.x, y: statusTextView.frame.origin.y, width: UIScreen.main.bounds.width, height: statusTextView.frame.height)
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func layoutSubviews() {
        DispatchQueue.main.async {
            self.chatBtnOutlet.center = CGPoint(x: self.viewsCountLabel.center.x + self.viewsCountLabel.frame.origin.x, y: self.viewsCountLabel.frame.height/2 - 15)
            self.nameLabel.backgroundColor = .clear
            self.dateLabel.backgroundColor = .clear
            self.viewsCountLabel.backgroundColor = .clear
            self.locationMarkerTextView.backgroundColor = .clear
            self.hashLabel.backgroundColor = .clear
            if #available(iOS 13.0, *) {
                self.nameLabel.textColor = .label
                self.postImageView?.backgroundColor = UIColor.systemBackground
                self.statusTextView.textColor = .label
                self.attachmentBackgroundView?.backgroundColor = .systemBackground
                self.attachmentBaseView?.backgroundColor = .systemBackground
                self.locationTextView.textColor = .label
                self.likeButton.tintColor = .label
                self.bookmark.tintColor = .label
                self.chatBtnOutlet.tintColor = .label
                self.shareButton.tintColor = .label
                self.locationTextView.backgroundColor = .systemBackground
                self.locationMarkerTextView.backgroundColor = .systemBackground
                self.statusTextView.backgroundColor = .systemBackground
                self.likesCountButton.setTitleColor(UIColor.label, for: .normal)
                self.embeddedView?.textProvider[.title].fontColor = .label
                self.embeddedView?.textProvider[.description].fontColor = .label
                self.embeddedView?.textProvider[.noDataTitle].fontColor = .label
                self.embeddedView?.textProvider[.domain].fontColor = .label
                self.cellBackgroundView.backgroundColor = UIColor.systemBackground
                self.indicatorForPost.backgroundColor = UIColor.systemBackground
                self.indicatorForPost.color = UIColor.label
                self.hashLabel.textColor = .systemGray2
            } else {
                self.nameLabel.textColor = UIColor.black
                self.postImageView?.backgroundColor = UIColor.white
                self.statusTextView.textColor = UIColor.black
                self.attachmentBackgroundView?.backgroundColor = UIColor.white
                self.attachmentBaseView?.backgroundColor = UIColor.white
                self.locationTextView.textColor = UIColor.black
                self.likeButton.tintColor = .black
                self.bookmark.tintColor = .black
                self.chatBtnOutlet.tintColor = .black
                self.shareButton.tintColor = .black
                self.locationTextView.backgroundColor = .white
                self.locationMarkerTextView.backgroundColor = .white
                self.statusTextView.backgroundColor = .white
                self.likesCountButton.setTitleColor(UIColor.black, for: .normal)
                self.embeddedView?.textProvider[.title].fontColor = .black
                self.embeddedView?.textProvider[.description].fontColor = .black
                self.embeddedView?.textProvider[.noDataTitle].fontColor = .black
                self.embeddedView?.textProvider[.domain].fontColor = .black
                self.cellBackgroundView.backgroundColor = UIColor.white
                self.hashLabel.textColor = .lightGray
            }
        }
        if  gotoUserDelegate != nil {
            let imageTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.imageTapped))
            profilePicImgVw?.isUserInteractionEnabled = true
            profilePicImgVw?.addGestureRecognizer(imageTapGestureRecognizer)
            let nameLabelTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.nameLabelTapped))
            nameLabel?.isUserInteractionEnabled = true
            nameLabel?.addGestureRecognizer(nameLabelTapGestureRecognizer)
            let dateLabelTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.dateLabelTapped))
            dateLabel?.isUserInteractionEnabled = true
            dateLabel?.addGestureRecognizer(dateLabelTapGestureRecognizer)
        }
        //shareButton.frame = CGRect(x: viewsCountLabel.frame.width - (likeButton.frame.width * 3) , y: likeButton.frame.origin.y, width: likeButton.frame.width * 3, height: likeButton.frame.height)
        //shareButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: likeButton.frame.width * 2, bottom: 0, right: 0)
//        var likeOrLikestext = Constants.otherConsts.likesText
//        if self.likesArrCount == 1 {
//            likeOrLikestext = Constants.otherConsts.likeText
//        }
        
        attachmentBaseView?.addGestureRecognizer(tappedOnAttachment)
        
        likesCountButton.setTitle("\(likesArrCount)"/* + likeOrLikestext*/, for: .normal)
        if Constants.clientSpecificConstantsStructName.showChatAndLikeBtns {
            if likesArrCount > 0 {
                likesCountButton.isHidden = false
            } else {
                likesCountButton.isHidden = true
            }
        }
        likesCountButton.contentHorizontalAlignment = .left
        if !playerView!.isHidden {
            self.playerView?.webView?.isHidden = true
            self.playerView?.backgroundColor = UIColor.black
            self.blackView = UIView(frame: CGRect(x: 0, y: 0, width: self.playerView!.frame.width, height: self.playerView!.frame.height))
            self.blackView?.backgroundColor = UIColor.black
            self.playerIndicator = UIActivityIndicatorView()
            self.playerIndicator?.frame = CGRect(x: self.playerView!.frame.width/2 - 15, y: self.entireHeight/2 - 15, width: 30, height: 30)
            self.blackView?.addSubview(self.playerIndicator!)
            self.playerIndicator?.startAnimating()
            self.playerView?.addSubview(self.blackView!)
        }
        if postImageView?.image != nil {
            postImageView?.isUserInteractionEnabled = true
            let makeFullScreenTap = UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.makeFullScreen))
            postImageView?.addGestureRecognizer(makeFullScreenTap)
        } else {
            postImageView?.gestureRecognizers?.removeAll()
        }
        self.contentView.addSubview(cellBackgroundView)
        self.contentView.sendSubviewToBack(cellBackgroundView)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        embeddedView?.prepareViewsForReuse()
    }
    
    //MARK: - General
    //light grey border line separation creation as layer
    func createdLayer() -> CALayer {
        let upperBorder = CALayer()
        upperBorder.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode")).cgColor
        upperBorder.frame = CGRect(x: 8, y: 0, width: self.frame.width - 32, height: 0.5)
        return upperBorder
    }
    
    func settingFonts() {
        nameLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        hashLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        dateLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        statusTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        locationTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        coloredTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
        viewsCountLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        likesCountButton.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        attachmentNameLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
    }
    
    func optionsWhenTappedOnChevronBtn(tag: Int) {
        if statusTextView.isHidden && coloredTextView.isHidden {
            if marker != nil {
                chevronDelegate?.chevronTapped(cell: self, desc: desc ?? "", pid: pid ?? "", time: time ?? "", privacy: privacy ?? "Public", location: "\(String(describing: marker!.position.latitude)),\(String(describing: marker!.position.longitude))", locationName: locationName ?? "", color_code: color_code ?? "", postImageName: postImageName, privacyID: privacyID ?? "", customContacts: customContacts, viewsCount: viewsCount ?? "0", isBookmark: tag == 0 ? true:false, attachments: attachments)
                
            } else {
                chevronDelegate?.chevronTapped(cell: self, desc: desc ?? "", pid: pid ?? "", time: time ?? "", privacy: privacy ?? "Public", location: "", locationName: "", color_code: color_code ?? "", postImageName: postImageName, privacyID: privacyID ?? "", customContacts: customContacts, viewsCount: viewsCount ?? "0", isBookmark: tag == 0 ? true:false, attachments: attachments)
            }
        } else {
            if coloredTextView.isHidden {
                color_code = ""
            }
            chevronDelegate?.chevronTapped(cell: self, desc: desc ?? "", pid: pid ?? "", time: time ?? "", privacy: privacy ?? "Public", location: "", locationName: "", color_code: color_code ?? "", postImageName: postImageName, privacyID: privacyID ?? "", customContacts: customContacts, viewsCount: viewsCount ?? "0", isBookmark: tag == 0 ? true:false, attachments: attachments)
        }
    }
    
    //MARK: - Location methods
    //location coordinats from dictionary set to marker
    func updateLocation(location: CLLocationCoordinate2D) {
        mapView.clear()
        mapView.camera = GMSCameraPosition(target: location, zoom: 15, bearing: 0, viewingAngle: 0)
        marker = GMSMarker()
        marker?.position = location
        marker?.map = mapView
        mapView?.settings.setAllGesturesEnabled(false)
        mapView?.gestureRecognizers?.removeAll()
        if self.postImageName == "" {
            let mapGesture = UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.mapThumbnailTapped))
            mapView?.addGestureRecognizer(mapGesture)
        }
        let locatioMarkerGesture = UITapGestureRecognizer(target: self, action: #selector(StatusFeedTableViewCell.mapThumbnailTapped))
        locationMarkerTextView.addGestureRecognizer(locatioMarkerGesture)
    }
    
    //MARK: - Targets
    @objc func mapThumbnailTapped() {
        if marker != nil {
            Constants.sharedUtils.openMapForPlace(locationName: locationName, location: marker!.position)
        }
    }
    
    //go to user profile on tap of dp
    @objc func imageTapped() {
        gotoUserDelegate?.tappedToOpenProfileFromCell!()
    }
    
    //go to user profile on tap of date label
    @objc func dateLabelTapped() {
        gotoUserDelegate?.tappedToOpenProfileFromCell!()
    }
    
    //go to user profile on tap of name label
    @objc func nameLabelTapped() {
        gotoUserDelegate?.tappedToOpenProfileFromCell!()
    }
    
    @objc func openLikesList() {
        likedListDelegate?.pushToLikedVC(likedArr: likesArr)
    }
    
    //handle likes
    @objc func handleLike() {
        #if AUTH
        let initialLikedStatus = self.likeButton.isSelected
        let initialLikesArr = self.likesArr
        let initialLikesArrCount = self.likesArrCount
        var currentLikedStatus = self.likeButton.isSelected
        var currentLikesArr = self.likesArr
        var currentLikesArrCount = self.likesArrCount
        if let id = Constants.defaults.value(forKey: "myID") as? String, id != "", Constants.defaults.value(forKey: "LoggedIn") as? Bool == true {
            self.likeButton.isSelected = !self.likeButton.isSelected
            if self.likeButton.isSelected == true {
                self.likesArrCount = self.likesArrCount + 1
                if !self.likesArr.contains(id) {
                    self.likesArr.append(id)
                }
            } else {
                self.likesArrCount = self.likesArrCount - 1
                if self.likesArr.contains(id) {
                    if let i = self.likesArr.firstIndex(of: id) {
                        self.likesArr.remove(at: i)
                    }
                }
            }
            //                            var likeOrLikestext = Constants.otherConsts.likesText
            //                            if self.likesArrCount == 1 {
            //                                likeOrLikestext = Constants.otherConsts.likeText
            //                            }
            self.likesCountButton.setTitle("\(self.likesArrCount)"/* + likeOrLikestext*/, for: .normal)
            if Constants.clientSpecificConstantsStructName.showChatAndLikeBtns {
                if self.likesArrCount > 0 {
                    self.likesCountButton.isHidden = false
                } else {
                    self.likesCountButton.isHidden = true
                }
            }
            currentLikedStatus = self.likeButton.isSelected
            currentLikesArr = self.likesArr
            currentLikesArrCount = self.likesArrCount
            UIView.animate(withDuration: 0.1, animations: {
                self.likeButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)

            },completion: { [weak self] _ in
                UIView.animate(withDuration: 0.1) {
                    self?.likeButton.transform = CGAffineTransform.identity
                }
            })
            let indexpath = self.indexPath
            self.updateLikesInDictDelegate?.updateLikePropertiesInDict(indexPath: indexpath, liked: currentLikedStatus, likesArr: currentLikesArr, likesArrCount: currentLikesArrCount)
            if runLikeUnlikeApi {
                runLikeUnlikeApi = false
                firstCallPreviousState = initialLikedStatus
            }
            workItem?.cancel()
            workItem = DispatchWorkItem {
                if self.firstCallPreviousState != self.likeButton.isSelected {
                    var endPart: String?
                    if self.likeButton.isSelected {
                        endPart = Constants.apiCalls.likePost
                    } else {
                        endPart = Constants.apiCalls.unlikePost
                    }
                    let params = ["userId" : id, "postId": self.pid!] as [String: Any]
                    apiCall.handleApiRequest(endPart: endPart!, params: params, completion: {[weak self] (responseJson, errorDesc, timeout) in
                        self?.runLikeUnlikeApi = true
                        if timeout || errorDesc != "" {
                            self?.updateLikesInDictDelegate?.updateLikePropertiesInDict(indexPath: indexpath, liked: initialLikedStatus, likesArr: initialLikesArr, likesArrCount: initialLikesArrCount)
                        }
                    })
                } else {
                    self.runLikeUnlikeApi = true
                }
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: workItem!)
        } else {
            Constants.sharedUtils.authenticateMobileNumber()
        }
        #endif
    }
    
    //text view tapped to either expand or shrink
    //status textview tapped
    @objc func textTapped() {
        if showMore != nil {
            if showMore! {
                //showMore = false
            } else {
                showMore = true
            }
            openSinglePostVcDelegate?.openPostInSingleScreen(cell: self)
            //showMoreLessDelegate?.cellExpandOrReduce(pid: pid ?? "", cell: self)
        }
    }
    
    //location textview tapped
    @objc func locTextTapped() {
        if showMore != nil {
            if showMore! {
                //showMore = false
            } else {
                showMore = true
            }
            openSinglePostVcDelegate?.openPostInSingleScreen(cell: self)
            //showMoreLessDelegate?.cellExpandOrReduce(pid: pid!, cell: self)
        }
    }
    
    //attachments tapped
    @objc func attachmentTapped() {
        attachmentBaseView?.isUserInteractionEnabled = false
        if (attachments[0]["link"] as? String)?.contains("doc") == true || (attachments[0]["link"] as? String)?.contains("image") == true || (attachments[0]["link"] as? String)?.contains("aud") == true {
            openInWebviewDelegate?.openDoc(cell: self, link: attachments[0]["link"] as? String ?? "", name: attachments[0]["name"] as? String ?? "")
            attachmentBaseView?.isUserInteractionEnabled = true
        }
    }
    
    //bookmark on feed tapped
    @objc func bookmarkTapped(sender: UIButton) {
        if Constants.clientSpecificConstantsStructName.showBookmarkBtn {
            self.bookmark.isSelected = !self.bookmark.isSelected
            var bookmarkedList = Constants.defaults.value(forKey: "bookmarkedList") as? [String] ?? []
            if self.pid != "" && self.pid != nil {
                if let index = bookmarkedList.firstIndex(of: self.pid!) {
                    bookmarkedList.remove(at: index)
                } else {
                    bookmarkedList.append(self.pid!)
                }
                Constants.defaults.setValue(bookmarkedList, forKey: "bookmarkedList")
            }
            if self.indexPath != nil {
                bookmarkCellDelegate?.reloadTable(indexPath: self.indexPath!)
            }
            //optionsWhenTappedOnChevronBtn(tag: sender.tag)
        }
    }
    
    func updateDisplay(progress: Float, totalSize: String) {
        if let nonNiiProgressView = circularProgressView, self.attachmentImageView?.subviews.contains(nonNiiProgressView) == false {
            self.circularProgressView?.center = attachmentImageView!.center
            self.attachmentImageView?.addSubview(circularProgressView!)
            self.downloadImageView?.image = nil
            self.downloadImageView?.removeFromSuperview()
        }
        self.circularProgressView?.setProgressWithAnimation(value: progress)
        self.attachmentNameLabel?.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
    }
    //tapped on share
    @objc func shareButtonTapped(sender: UIButton) {
        optionsWhenTappedOnChevronBtn(tag: sender.tag)
    }
    
    //tapped on reply Button
    @objc func chatButtonTapped(sender: UIButton) {
        #if AUTH
        if let _ = Constants.defaults.value(forKey: "myID") as? String,  Constants.defaults.value(forKey: "LoggedIn") as? Bool == true {
            var type = ""
            if attachments.count > 0 {
                type = attachments[0]["type"] as? String ?? ""
            }
            let dateFromTime = (time ?? "").dateFromISO8601
            let newDateFormatter = DateFormatter()
            newDateFormatter.dateFormat = "dd-MM-yyyy 'at' h:mm a"
            newDateFormatter.locale = NSLocale.system
            let stringFormTime = newDateFormatter.string(from: dateFromTime!)
            let timeTextToAppend = "\n" + Constants.otherConsts.postedOnText + stringFormTime
            var textToSend = ""
            if desc != nil {
                textToSend = desc == "" ? type: desc!
                if desc!.count > 50 {
                    textToSend = textToSend.padding(toLength: 50, withPad: textToSend, startingAt: 0) + "..." + timeTextToAppend
                } else {
                    textToSend = textToSend + "..." + timeTextToAppend
                }
            }
            chatVCDelegate?.openParticularChatVC(userId: userID ?? "", snippet: textToSend)
        } else {
            Constants.sharedUtils.authenticateMobileNumber()
        }
        #endif
    }
    
    //tappedOnImage
    @objc func makeFullScreen() {
        var imagePaths: [String] = []
        for dict in attachments {
            let imgPath = dict["link"] as! String
            if !imagePaths.contains(imgPath) {
                imagePaths.append(imgPath)
            }
        }
        if attachments.count == 0 && postImageName != "" {
            imagePaths = [postImageName]
        }
        fullScreenDelegate?.pushToVC(currentImagePath: postImageName, imagePaths: imagePaths, titleName: nameLabel.text ?? "", indexPath: nil)
    }
    
    @objc func recorderAction(sender: UIButton) {
        switch playPauseBtn!.tag {
        case 1:
            playPauseBtn?.tag = 2
            audioPlayer?.pause()
            //play btn
            playPauseBtn?.setImage(playRecImage, for: .normal)
        case 2:
            playPauseBtn?.tag = 1
            audioPlayer?.play()
            //pause btn
            playPauseBtn?.setImage(pauseRecImage, for: .normal)
        default:
            //pause btn
            playPauseBtn?.tag = 1
            playSound(name: attachments[0]["link"] as? String ?? "")
            playPauseBtn?.setImage(pauseRecImage, for: .normal)
        }
    }
    
    @objc func playbackSliderValueChanged(sender:UISlider) {
        if let time = TimeInterval(exactly: sender.value), audioPlayer != nil {
            audioPlayer?.currentTime = time
            progressLabel?.text = "\(Double(round(100*audioPlayer!.currentTime)/100))"
        }
    }
    
    @objc func updateSlider() {
        if audioPlayer?.isPlaying == true && slider != nil && audioPlayer != nil {
            slider!.value = Float(audioPlayer!.currentTime)
            progressLabel?.text = "\(Double(round(100*audioPlayer!.currentTime)/100))"
            print("Changing works")
        }
    }

    func playSound(name: String) {
        let url = URL(fileURLWithPath: ControllerUtils.shared.getBaseForCacheLocal(with: name) ?? "")
        
        do {
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } else {
                // Set category with options (iOS 9+) setCategory(_:options:)
                AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:withOptions:error:"), with: AVAudioSession.Category.playback, with:  [])
                
                // Set category without options (<= iOS 9) setCategory(_:)
                AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:error:"), with: AVAudioSession.Category.playback)
            }
            try AVAudioSession.sharedInstance().setActive(true)
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            //player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            audioPlayer = try AVAudioPlayer(contentsOf: url)
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            audioPlayer?.delegate = self
            if slider?.value == 0.0 {
                slider?.value = 0.0
            }
            slider?.maximumValue = Float(audioPlayer!.duration)
            timer = Timer.scheduledTimer(timeInterval: 0.0001, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
            guard let player = audioPlayer else { return }
            
            player.play()
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    //MARK: Delegates
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let touchPoint = touch.location(in: statusTextView)
        if previewView != nil {
            if statusTextView.textContainer.exclusionPaths.count > 0 && !statusTextView.isHidden {
                if CGRect(x: 0, y: statusTextView.frame.height - 130, width: statusTextView.frame.width, height: 130).contains(touchPoint) {
                    return false
                }
            }
        }
        return true
    }
    
    //layout delegates
    func layoutManager(_ layoutManager: NSLayoutManager, paragraphSpacingBeforeGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 0
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, paragraphSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 4
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 4
    }
    
    //ytplayer delegates
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerIndicator?.removeFromSuperview()
        //videoThumbnailImgVw?.removeFromSuperview()
        blackView?.removeFromSuperview()
        blackView = nil
        playerIndicator = nil
        playerView.webView?.isHidden = false
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)))
        } catch {
            // Set category with options (iOS 9+) setCategory(_:options:)
            AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:withOptions:error:"), with: AVAudioSession.Category.playback, with:  [])
            
            // Set category without options (<= iOS 9) setCategory(_:)
            AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:error:"), with: AVAudioSession.Category.playback)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        playPauseBtn?.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "playIcon")), for: .normal)
        playPauseBtn?.tag = 0
        slider?.value = Float(player.currentTime)
    }
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
    }
}
extension UIResponder {
    
    func next<T: UIResponder>(_ type: T.Type) -> T? {
        return next as? T ?? next?.next(type)
    }
}
extension UITableViewCell {
    
    var tableView: UITableView? {
        return next(UITableView.self)
    }
    
    var indexPath: IndexPath? {
        return tableView?.indexPath(for: self)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}
