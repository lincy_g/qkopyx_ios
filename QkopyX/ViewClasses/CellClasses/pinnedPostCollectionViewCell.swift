//
//  pinnedPostCollectionViewCell.swift
//  QkopyX
//
//  Created by Lincy George on 07/05/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import UIKit
import GoogleMaps
class pinnedPostCollectionViewCell: UICollectionViewCell {
    var postDict: PostObject?
    var indexpath: IndexPath!
    var postImageView: UIImageView?
    var attachmentNameLabel: UILabel?
    var marker: GMSMarker?
    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet weak var locationTextView: UITextView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var locationMarkerTextView: UITextView!
    @IBOutlet weak var coloredTextView: UITextView!
    
    override func awakeFromNib() {
        postImageView = UIImageView()
        coloredTextView.textColor = UIColor.white
        attachmentNameLabel = UILabel()
        postImageView?.contentMode = .scaleAspectFill
        attachmentNameLabel?.contentMode = .scaleAspectFit
        attachmentNameLabel?.numberOfLines = 0
        statusTextView.isEditable = false
        statusTextView.isSelectable = false
        locationTextView.isEditable = false
        locationTextView.isSelectable = false
        locationMarkerTextView.isEditable = false
        locationMarkerTextView.isSelectable = false
        settingFonts()
    }
    func settingFonts() {
        statusTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        locationTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        locationMarkerTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        coloredTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        attachmentNameLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
    }
    override func layoutSubviews() {
        coloredTextView.textColor = .white
        if #available(iOS 13.0, *) {
           self.statusTextView.textColor = .label
           self.locationTextView.textColor = .label
           self.locationTextView.backgroundColor = .systemBackground
           self.statusTextView.backgroundColor = .systemBackground
       } else {
           self.statusTextView.textColor = UIColor.black
           self.locationTextView.textColor = UIColor.black
           self.locationTextView.backgroundColor = .white
           self.statusTextView.backgroundColor = .white
       }
    }
    
    //location coordinats from dictionary set to marker
    func updateLocation(location: CLLocationCoordinate2D) {
        mapView.clear()
        mapView.camera = GMSCameraPosition(target: location, zoom: 15, bearing: 0, viewingAngle: 0)
        marker = GMSMarker()
        marker?.position = location
        marker?.map = mapView
        mapView?.settings.setAllGesturesEnabled(false)
        mapView?.gestureRecognizers?.removeAll()
    }
}
