//
//  singlePostTvcCell.swift
//  Qkopy
//
//  Created by Lincy George on 30/04/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit
import youtube_ios_player_helper
import URLEmbeddedView
import AVKit
import MapKit
import GoogleMaps
class singlePostTvcCell: UITableViewCell, YTPlayerViewDelegate, NSLayoutManagerDelegate {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var txtVw: UITextView!
    @IBOutlet weak var imgVw: UIImageView!
    
    let locationManager = CLLocationManager()
    
    var embeddedView: URLEmbeddedView?
    var playerView: YTPlayerView?
    var previewView: UIView?
    var attachmentImageView: UIImageView?
    var attachmentBaseView: UIView?
    var attachmentBackgroundView: UIView?
    var playerIndicator: UIActivityIndicatorView?
    var blackView: UIView?
    var previewLinkURLString: String?
    var marker: GMSMarker?
    var locationName: String?
    var downloadImageView: UIImageView?
    var isDownloading: Bool?
    var circularProgressView: CircularProgress?
    var attachmentNameLabel: UILabel?
    var timer: Timer?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtVw.isEditable = false
        embeddedView = URLEmbeddedView()
        attachmentBackgroundView = UIView()
        attachmentBaseView = UIView()
        previewView = UIView()
        attachmentImageView = UIImageView()
        downloadImageView = UIImageView()
        attachmentNameLabel = UILabel()
        playerView = YTPlayerView()
        circularProgressView = CircularProgress(frame: CGRect(x: 10, y: 10, width: 50, height: 50))
        playerView?.delegate = self
        txtVw.layoutManager.delegate = self
        txtVw.textContainerInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        if #available(iOS 13.0, *) {
            attachmentBaseView?.backgroundColor = UIColor.systemBackground
            attachmentNameLabel?.backgroundColor = UIColor.systemBackground
        } else {
            attachmentBaseView?.backgroundColor = UIColor.white
            attachmentNameLabel?.backgroundColor = UIColor.white
        }
        attachmentBaseView?.layer.borderColor = UIColor.gray.cgColor
        attachmentBaseView?.layer.cornerRadius = 10
        attachmentBaseView?.clipsToBounds = true
        attachmentBaseView?.layer.borderWidth = CGFloat(0.5)
        attachmentNameLabel?.isUserInteractionEnabled = true
        attachmentNameLabel?.contentMode = .scaleAspectFit
        attachmentNameLabel?.numberOfLines = 0
        attachmentImageView?.clipsToBounds = true
        settingFonts()
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
    }

    override func layoutSubviews() {
        if !playerView!.isHidden {
            self.playerView?.webView?.isHidden = true
            self.playerView?.backgroundColor = UIColor.black
            self.blackView = UIView(frame: CGRect(x: 0, y: 0, width: self.playerView!.frame.width, height: self.playerView!.frame.height))
            self.blackView?.backgroundColor = UIColor.black
            self.playerIndicator = UIActivityIndicatorView()
            self.playerIndicator?.frame = CGRect(x: self.playerView!.frame.width/2 - 15, y: 250, width: 30, height: 30)
            if playerIndicator != nil && blackView != nil {
                self.playerIndicator?.center = self.blackView!.center
            }
            self.blackView?.addSubview(self.playerIndicator!)
            self.playerIndicator?.startAnimating()
            self.playerView?.addSubview(self.blackView!)
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    //location coordinats from dictionary set to marker
    func updateLocation(location: CLLocationCoordinate2D) {
        mapView.clear()
        mapView.camera = GMSCameraPosition(target: location, zoom: 15, bearing: 0, viewingAngle: 0)
        marker = GMSMarker()
        marker?.position = location
        marker?.map = mapView
        mapView?.settings.setAllGesturesEnabled(false)
        mapView?.gestureRecognizers?.removeAll()
        let mapGesture = UITapGestureRecognizer(target: self, action: #selector(self.mapThumbnailTapped))
        mapView?.addGestureRecognizer(mapGesture)
        if txtVw.tag == 1 {
            let locatioMarkerGesture = UITapGestureRecognizer(target: self, action: #selector(self.mapThumbnailTapped))
            txtVw.addGestureRecognizer(locatioMarkerGesture)
        }
    }
    
    //MARK: - Targets
    @objc func mapThumbnailTapped() {
        if marker != nil {
            openMapForPlace(location: marker!.position)
        }
    }

    func openMapForPlace(location: CLLocationCoordinate2D) {
        
        let latitude: CLLocationDegrees = location.latitude
        let longitude: CLLocationDegrees = location.longitude
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = locationName ?? appDelegate.fetchFromPlist(plistName: "constants", key: "sharedLocText") as String
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            let locationNameAddedPercentage = locationName!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "\(latitude),\(longitude)"
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?q=\(locationNameAddedPercentage),\(latitude),\(longitude)")!)
        } else {
            mapItem.openInMaps(launchOptions: options)
        }
    }
    //ytplayer delegates
    func playerViewDidBecomeReady(_ playerView: YTPlayerView) {
        playerIndicator?.removeFromSuperview()
        //videoThumbnailImgVw?.removeFromSuperview()
        blackView?.removeFromSuperview()
        blackView = nil
        playerIndicator = nil
        playerView.webView?.isHidden = false
        do {
            if #available(iOS 10.0, *) {
                try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            } else {
                // Set category with options (iOS 9+) setCategory(_:options:)
                AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:withOptions:error:"), with: AVAudioSession.Category.playback, with:  [])
                
                // Set category without options (<= iOS 9) setCategory(_:)
                AVAudioSession.sharedInstance().perform(NSSelectorFromString("setCategory:error:"), with: AVAudioSession.Category.playback)
            }
        } catch {
            //
        }
    }
    func updateDisplay(progress: Float, totalSize: String) {
        if let nonNiiProgressView = circularProgressView, self.attachmentImageView?.subviews.contains(nonNiiProgressView) == false {
            self.circularProgressView?.center = attachmentImageView!.center
            self.attachmentImageView?.addSubview(circularProgressView!)
            self.downloadImageView?.image = nil
            self.downloadImageView?.removeFromSuperview()
        }
        self.circularProgressView?.setProgressWithAnimation(value: progress)
        self.attachmentNameLabel?.text = String(format: "%.1f%% of %@", progress * 100, totalSize)
    }

    func settingFonts() {
        attachmentNameLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        if tag == 0 {
            txtVw.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
        } else {
            txtVw.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        }
    }
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
    }
    //layout delegates
    func layoutManager(_ layoutManager: NSLayoutManager, paragraphSpacingBeforeGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 0
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, paragraphSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 4
    }
    
    func layoutManager(_ layoutManager: NSLayoutManager, lineSpacingAfterGlyphAt glyphIndex: Int, withProposedLineFragmentRect rect: CGRect) -> CGFloat {
        return 4
    }
}
