//
//  refreshControl.swift
//  Qkopy
//
//  Created by Lincy George on 17/06/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit

class customRefreshControl: UIRefreshControl {
    
    override init(frame: CGRect) {
        super.init()
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        var str: NSAttributedString?
        if #available(iOS 11.0, *) {
            if #available(iOS 13.0, *) {
                str = NSAttributedString(string: Constants.otherConsts.refreshOnPullText, attributes: [NSAttributedString.Key.foregroundColor : UIColor.label])

            } else {
                str = NSAttributedString(string: Constants.otherConsts.refreshOnPullText, attributes: [NSAttributedString.Key.foregroundColor : UIColor.black])
            }
        } else {
            self.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
            self.tintColor = UIColor.white
            str = NSAttributedString(string: Constants.otherConsts.refreshOnPullText, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        }
        self.attributedTitle = str
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
