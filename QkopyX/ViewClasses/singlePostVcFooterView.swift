//
//  singlePostVcFooterView.swift
//  Qkopy
//
//  Created by Lincy George on 30/04/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit
@objc protocol shareExternalFromFooterDelegate {
    func sharePostToExternal(source: UIView)
}

class singlePostVcFooterView: UIView {

    //variables
    var ownProfile: Bool?
    
    var likesArr: [String] = []
    var likesArrCount: Int = 0
    var runLikeUnlikeApi = true
    var firstCallPreviousState: Bool?
    var workItem: DispatchWorkItem?
    var pid: String?
    var indexPath: IndexPath?
    var currentDict: PostObject?
    weak var shareExternalDelegate: shareExternalFromFooterDelegate?
    weak var likedListDelegate: openLikedListDelegate?
    weak var likeDelegate: setLikeStatusInParentVCDelegate?
    weak var chatVCDelegate: goToChatDelegate?
    
    @IBOutlet weak var likeBtnOutlet: UIButton!
    
    @IBOutlet weak var chatBtnOutlet: UIButton!
    
    @IBOutlet weak var likesCountBtnOutlet: UIButton!
    @IBOutlet weak var shareBtnOutlet: UIButton!
    @IBOutlet weak var viewsCountLabel: UILabel!
    @IBAction func likeBtnAction(_ sender: UIButton) {
        #if AUTH
        let initialLikedStatus = self.likeBtnOutlet.isSelected
        let initialLikesArr = self.likesArr
        let initialLikesArrCount = self.likesArrCount
        var currentLikedStatus = self.likeBtnOutlet.isSelected
        var currentLikesArr = self.likesArr
        var currentLikesArrCount = self.likesArrCount
        if let id = UserDefaults.standard.value(forKey: "myID") as? String {
            self.likeBtnOutlet.isSelected = !self.likeBtnOutlet.isSelected
            if self.likeBtnOutlet.isSelected == true {
                self.likesArrCount = self.likesArrCount + 1
                if !self.likesArr.contains(id) {
                    self.likesArr.append(id)
                }
            } else {
                self.likesArrCount = self.likesArrCount - 1
                if self.likesArr.contains(id) {
                    if let i = self.likesArr.firstIndex(of: id) {
                        self.likesArr.remove(at: i)
                    }
                }
            }
            //                            var likeOrLikestext = appDelegate.fetchFromPlist(plistName: "constants", key:  "likesText")
            //                            if self.likesArrCount == 1 {
            //                                likeOrLikestext = appDelegate.fetchFromPlist(plistName: "constants", key: "likeText")
            //                            }
            self.likesCountBtnOutlet.setTitle("\(self.likesArrCount)"/* + likeOrLikestext*/, for: .normal)
            if self.likesArrCount > 0 {
                self.likesCountBtnOutlet.isHidden = false
            } else {
                self.likesCountBtnOutlet.isHidden = true
            }
            currentLikedStatus = self.likeBtnOutlet.isSelected
            currentLikesArr = self.likesArr
            currentLikesArrCount = self.likesArrCount
            UIView.animate(withDuration: 0.1, animations: {
                self.likeBtnOutlet.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                
            },completion: { _ in
                UIView.animate(withDuration: 0.1) {
                    self.likeBtnOutlet.transform = CGAffineTransform.identity
                }
            })
            let indexpath = self.indexPath
            self.likeDelegate?.updateLikePropertiesInDict(indexPath: indexpath, liked: currentLikedStatus, likesArr: currentLikesArr, likesArrCount: currentLikesArrCount)
            if runLikeUnlikeApi {
                runLikeUnlikeApi = false
                firstCallPreviousState = initialLikedStatus
            }
            workItem?.cancel()
            workItem = DispatchWorkItem {
                if self.firstCallPreviousState != self.likeBtnOutlet.isSelected {
                    var endPart: String?
                    if self.likeBtnOutlet.isSelected {
                        endPart = appDelegate.fetchFromPlist(plistName: "constants", key: "likeURL")
                    } else {
                        endPart = appDelegate.fetchFromPlist(plistName: "constants", key: "unlikeURL")
                    }
                    let params = ["userId" : id, "postId": self.pid!] as [String: Any]
                    apiCall.handleApiRequest(endPart: endPart!, params: params, completion: {[weak self] (responseJson, errorDesc, timeout) in
                        self?.runLikeUnlikeApi = true
                        if timeout || errorDesc != "" {
                            self?.likeDelegate?.updateLikePropertiesInDict(indexPath: indexpath, liked: initialLikedStatus, likesArr: initialLikesArr, likesArrCount: initialLikesArrCount)
                        }
                    })
                } else {
                    self.runLikeUnlikeApi = true
                }
            }
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 3, execute: workItem!)
        } else {
            Constants.sharedUtils.authenticateMobileNumber()
        }
        #endif
    }
    
    @IBAction func likesCountBtnAction(_ sender: UIButton) {
        likedListDelegate?.pushToLikedVC(likedArr: likesArr)
    }
    @IBAction func shareBtnAction(_ sender: UIButton) {
        shareExternalDelegate?.sharePostToExternal(source: self)
    }
    @IBAction func chatBtnAction(_ sender: UIButton) {
        #if AUTH
        if let _ = UserDefaults.standard.value(forKey: "myID") as? String {
            var type = ""
            if currentDict!.attachments.count > 0 {
                type = currentDict!.attachments[0]["type"] as? String ?? ""
            }
            let dateFromTime = (currentDict?.time ?? "").dateFromISO8601
            let newDateFormatter = DateFormatter()
            newDateFormatter.dateFormat = "dd-MM-yyyy 'at' h:mm a"
            newDateFormatter.locale = NSLocale.system
            let stringFormTime = newDateFormatter.string(from: dateFromTime!)
            let timeTextToAppend = "\n" + appDelegate.fetchFromPlist(plistName: "constants", key: "postedOnText") + stringFormTime
            var textToSend = ""
            if currentDict?.desc != nil {
                textToSend = currentDict!.desc == "" ? type: currentDict!.desc
                if currentDict!.desc.count > 50 {
                    textToSend = textToSend.padding(toLength: 50, withPad: textToSend, startingAt: 0) + "..." + timeTextToAppend
                } else {
                    textToSend = textToSend + "..." + timeTextToAppend
                }
            }
            chatVCDelegate?.openParticularChatVC(userId: currentDict?.id ?? "", snippet: textToSend)
        } else {
            Constants.sharedUtils.authenticateMobileNumber()
        }
        #endif
    }
    
    override func awakeFromNib() {
        settingFonts()
        likeBtnOutlet.backgroundColor = .clear
        chatBtnOutlet.backgroundColor = .clear
        likesCountBtnOutlet.backgroundColor = .clear
        viewsCountLabel.backgroundColor = .clear
        self.layer.addSublayer(createdLayer())
        if #available(iOS 13.0, *) {
            self.backgroundColor = UIColor.systemBackground
        } else {
            self.backgroundColor = UIColor.white
        }
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
    }
    
    
    override func layoutSubviews() {
        if ownProfile == true {
            viewsCountLabel.isHidden = false
            chatBtnOutlet.isHidden = true
            shareBtnOutlet.isHidden = true
        } else {
            viewsCountLabel.isHidden = true
            chatBtnOutlet.isHidden = false
            shareBtnOutlet.isHidden = false
        }
        likeBtnOutlet.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "loveGrayImage"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        likeBtnOutlet.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "loveRedImage")), for: .selected)
        shareBtnOutlet.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "shareExternalImage"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        chatBtnOutlet.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "replyIcon"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        
        likeBtnOutlet.imageView?.contentMode = .scaleAspectFit
        likesCountBtnOutlet.contentHorizontalAlignment = .left
        if #available(iOS 13.0, *) {
            self.likeBtnOutlet.tintColor = .label
            self.shareBtnOutlet.tintColor = .label
            self.chatBtnOutlet.tintColor = .label
            self.likesCountBtnOutlet.setTitleColor(UIColor.label, for: .normal)
        } else {
            self.likeBtnOutlet.tintColor = .black
            self.shareBtnOutlet.tintColor = .black
            self.chatBtnOutlet.tintColor = .black
            self.likesCountBtnOutlet.setTitleColor(UIColor.black, for: .normal)
        }
        if !Constants.clientSpecificConstantsStructName.showChatAndLikeBtns {
            likeBtnOutlet.isHidden = true
            likesCountBtnOutlet.isHidden = true
            chatBtnOutlet.isHidden = true
        }
        shareBtnOutlet.imageView?.contentMode = .scaleAspectFit
        chatBtnOutlet.imageView?.contentMode = .scaleAspectFit
        //shareButton.backgroundColor = UIColor.red
        viewsCountLabel.isUserInteractionEnabled = true
    }
    func settingFonts() {
        viewsCountLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        likesCountBtnOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
    }
    //light grey border line separation creation as layer
    func createdLayer() -> CALayer {
        let line = CALayer()
        line.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode")).cgColor
        line.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 0.5)
        return line
    }
    
    func optionsWhenTappedOnBookmarkBtn(tag: Int) {
        
        //shareExternalDelegate?.sharePostToExternal(source: self)
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
