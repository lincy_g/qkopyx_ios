//
//  Spinner.swift
//  Qkopy
//
//  Created by Lincy George on 08/07/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit
open class Spinner {
    internal static var activityIndicator: UIActivityIndicatorView?
    internal static var containerView: UIView?
    internal static var progressView: UIView?
    
    public static func start() {
        if let window = UIApplication.shared.keyWindow, activityIndicator == nil, containerView == nil, progressView == nil {
            activityIndicator = UIActivityIndicatorView()
            let frame = UIScreen.main.bounds
            let fixedCenter = CGPoint(x: frame.width/2, y: frame.height/2)
            containerView = UIView(frame: frame)
            containerView?.center = fixedCenter
            progressView = UIView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
            progressView?.center = fixedCenter
            progressView?.backgroundColor = UIColor(hex: 0x444444, alpha: 0.7)
            progressView?.clipsToBounds = true
            progressView?.layer.cornerRadius = 10
            
            activityIndicator?.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            activityIndicator?.style = .whiteLarge
            if progressView != nil {
                activityIndicator?.center = CGPoint(x: progressView!.bounds.width / 2, y: progressView!.bounds.height / 2)
            }
            
            if activityIndicator != nil {
                progressView?.addSubview(activityIndicator!)
            }
            if progressView != nil {
                containerView?.addSubview(progressView!)
            }
            if containerView != nil {
                window.addSubview(containerView!)
            }
            containerView?.isUserInteractionEnabled = false
            activityIndicator?.startAnimating()
        }
    }
    
    public static func stop() {
        activityIndicator?.stopAnimating()
        activityIndicator = nil
        containerView?.isUserInteractionEnabled = true
        containerView?.removeFromSuperview()
        containerView = nil
        progressView = nil
    }

    @objc public static func update() {
        if activityIndicator != nil {
            stop()
            start()
        }
    }
}
