//
//  statusCellExtension.swift
//  Qkopy
//
//  Created by Lincy George on 23/04/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit
import Foundation

extension StatusFeedTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate, layoutDelegate, imageTappedDelegate  {
    func setCollectionView(forRow row: Int) {
        imagesCollectionView?.delegate = self
        imagesCollectionView?.dataSource = self
        imagesCollectionView?.tag = row
        imagesCollectionView?.setContentOffset(imagesCollectionView!.contentOffset, animated:false) // Stops collection view if it was scrolling.
        DispatchQueue.main.async {
            self.imagesCollectionView?.reloadData()
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachments.count > 4 ? 4: attachments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.nameOfIdentifiers.imageCollectionCellIdentifier, for: indexPath)
        cell.contentView.layer.cornerRadius = 0
        cell.contentView.layer.masksToBounds = false
        if let annotateCell = cell as? imageCollectionCell {
            annotateCell.closeBtn.isHidden = true
            let postFileUrlString = attachments[indexPath.item]["link"] as? String ?? (" /posts/" + (attachments[indexPath.item]["name"] as? String ?? ""))
            let img = ControllerUtils.shared.getImage(index: imagesCollectionView == nil ? self.indexPath: IndexPath(row: imagesCollectionView!.tag, section: 0), isProfileImg: false, endPath: postFileUrlString)
            if img.images == nil {
                annotateCell.imgvw.image = img
            } else {
                annotateCell.imgvw.image = nil
            }
            if #available(iOS 13.0, *) {
                annotateCell.imgvw.backgroundColor = UIColor.systemGroupedBackground
            } else {
                annotateCell.imgvw.backgroundColor = UIColor.groupTableViewBackground
            }
            annotateCell.imgvw.isUserInteractionEnabled = true
            annotateCell.imageDelegate = self
            annotateCell.imgvw.alpha = 1
            if attachments.count > 4 && indexPath.row == 3 {
                if annotateCell.textLabel == nil {
                    annotateCell.textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: annotateCell.frame.size.width, height: annotateCell.frame.size.height))
                }
                annotateCell.isHidden = false
                annotateCell.textLabel?.alpha = 0.6
                annotateCell.textLabel?.text = "+\(attachments.count - 3)"
                annotateCell.textLabel?.backgroundColor = UIColor.white
                annotateCell.textLabel?.textColor = UIColor.black.darker()
                annotateCell.textLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
                annotateCell.textLabel?.textAlignment = .center
                annotateCell.imgvw.addSubview(annotateCell.textLabel!)
            } else {
                annotateCell.textLabel?.isHidden = true
                annotateCell.textLabel = nil
                annotateCell.textLabel?.removeFromSuperview()
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        //let postFileUrlString = attachments[indexPath.item]["link"] as? String ?? ""
        if attachments.count == 1 {
            return postImageView!.frame.height
        } else if attachments.count == 2 {
            return postImageView!.frame.height
        } else if attachments.count == 3 {
            switch indexPath.item {
            case 0:
                return postImageView!.frame.height
            case 1,2:
                return (postImageView!.frame.height)/2
            default:
                return postImageView!.frame.height
            }
        } else {
            return (postImageView!.frame.height)/2
        }
    }
    
    func imgVwTapped(index: Int) {
        if attachments.count == 1 {
            makeFullScreen()
        } else {
            openSinglePostVcDelegate?.openPostInSingleScreen(cell: self)
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        openSinglePostVcDelegate?.openPostInSingleScreen(cell: self)
//    }
}
