//
//  AppDelegate.swift
//  QkopyX
//
//  Created by Lincy George on 29/01/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//
import UIKit
import Fabric
import GoogleMaps
import GooglePlaces
import Crashlytics
import CoreData
import UserNotifications
import StoreKit
import Firebase

let appDelegate = UIApplication.shared.delegate as! AppDelegate
var savedContactRefDict: [String: DynamicKeyObject] = [:]
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, SKStoreProductViewControllerDelegate, MessagingDelegate {

    let reachability = Reachability()!
    var window: UIWindow?
    var launchOps: [UIApplication.LaunchOptionsKey: Any]?
    var fcmtoken: String?
    var notificationTapped: Bool = false
    var remoteConfig: RemoteConfig!
    #if AUTH
    var firebaseDb: Firestore?
    #endif
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                launchOps = launchOptions
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
         //   IQKeyboardManager.shared.enable = true
        }
        let protectedDataAvailable = UIApplication.shared.isProtectedDataAvailable
        if protectedDataAvailable {
            self.launchApp()
        } else {
            NSLog("protected_Data_NOT_Available")
            NotificationCenter.default.addObserver(self, selector: #selector(protectedDataAvailableNotification(notification:)), name: UIApplication.protectedDataDidBecomeAvailableNotification, object: nil)
        }
        return true

    }
    
    func handleNotification(tappedNotificationInfo: [AnyHashable: Any]) {
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = 0
            if let rootVC = self.window?.rootViewController {
                if let tabbarVC = rootVC as? UITabBarController ?? self.window?.rootViewController?.presentedViewController as? UITabBarController {
                    if let userVC = tabbarVC.children[1].children[0] as? UsersProfileTableViewController {
                        userVC.networkCall(isRefreshCall: true)
                    }
                }
            }
            if self.notificationTapped == true {
                self.openUserVC(id: Constants.clientSpecificConstantsStructName.clientId, withinApp: nil)
            }
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            }
        }
        /*always call queue instead of loading one by one - quick fix
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getDeliveredNotifications { (notifications) in
                let notificationsReversedList = notifications.reversed()
                let pidOfTapped = tappedNotificationInfo[AnyHashable("_id")] as? String ?? ""
                var alreadyAddedPidList: [String] = []
                for notification in notificationsReversedList {
                    let info = notification.request.content.userInfo
                    let pid = info[AnyHashable("_id")] as? String ?? ""
                    if !alreadyAddedPidList.contains(pid) {
                        alreadyAddedPidList.append(pid)
                    }
                    DispatchQueue.global(qos: .background).async {
                        self.createDictFromNotificationUserInfoAndPassToQueue(info: info)
                    }
                }
                if !alreadyAddedPidList.contains(pidOfTapped) {
                    DispatchQueue.global(qos: .background).async {
                        self.createDictFromNotificationUserInfoAndPassToQueue(info: tappedNotificationInfo)
                    }
                }
                UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            }
        }*/
    }
    
    func createDictFromNotificationUserInfoAndPassToQueue(info: [AnyHashable: Any]) {
        let desc = info[AnyHashable("post_desc")] as? String ?? ""
        let dataCode = desc.data(using: String.Encoding.utf8)!
        var messageString: String?
        if let msg = String(data: dataCode, encoding: String.Encoding.nonLossyASCII) {
            if msg == "" {
                messageString = desc
            } else {
                messageString = msg
            }
        }
        let loc = info[AnyHashable("post_loc")] as? String ?? ""
        let color_code = info[AnyHashable("bg_colour")] as? String ?? ""
        let time = info[AnyHashable("createdDate")] as? String ?? ""
        let pid = info[AnyHashable("_id")] as? String ?? ""
        let lastUpdatedServerTime = info[AnyHashable("updatedDate")] as? String ?? ""
        let largePost = info[AnyHashable("largePost")] as? String ?? "false"
        //let lastUpdatedTime = info[AnyHashable("updatedDate")] as? String ?? ""
        //let calendar = Calendar.current
        //let components = calendar.dateComponents([.hour], from: lastUpdatedTime.dateFromISO8601!, to: Date())
        let privacy = info[AnyHashable("privacy")] as? String ?? "Public"
        let likesArr = info[AnyHashable("postLike")] as? [String] ?? []
        let channelList = info[AnyHashable("channelList")] as? [String] ?? []
        var liked = false
        if let myId = Constants.defaults.value(forKey: "myID") as? String {
            liked = likesArr.contains(myId)
        }
        let showPreview = info[AnyHashable("showPreview")] as? String ?? "true"
        var attachments: [[String: Any]] = []
        if let attachmentString = info[AnyHashable("attachments")] as? String {
            attachments = attachmentString.parseJSONString as? [[String: Any]] ?? []
        }
        var postImageName: String?
        if let postImageUrlString = info["post_image"] {
            let fullUrlString = (Constants.otherLinks.mediaURL) + (postImageUrlString as! String)
            let imageUrl = URL(string: fullUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
            if imageUrl != nil && (postImageUrlString as! String) != "" {
                postImageName = imageUrl!.path
                let position = postImageName!.components(separatedBy: "/").count - 2
                if position >= 0 {
                    Constants.sharedUtils.saveFileDocumentDirectory(name: postImageName!.components(separatedBy: "/").last ?? "", directoryNamesString: postImageName!.replacingOccurrences(of: postImageName!.components(separatedBy: "/").last ?? "", with: ""), lastUpdatedDate: lastUpdatedServerTime.dateFromISO8601)
                }
            }
        }
        let id = info[AnyHashable("userId")] as? String ?? ""
        let postDict = PostObject(desc: messageString ?? desc, location: loc, time: time, pid: pid, privacy: privacy, showPreview: showPreview, colorCode: color_code, postFileName: postImageName ?? "", likesArray: likesArr, liked: liked, attachments: attachments, id: id, isPremium: false, channelList: channelList)
        if self.notificationTapped == true {
            self.openUserVC(id: Constants.clientSpecificConstantsStructName.clientId, withinApp: nil)
        }
        self.loadToParticularVCAndReload(dict: postDict, id: Constants.clientSpecificConstantsStructName.clientId, isLargePost: largePost)
    }
    
    func loadToParticularVCAndReload(dict: PostObject, id: String, isLargePost: String) {
        if isLargePost == "true" {
            let params = ["postId" : dict.pid] as [String : Any]
            apiCall.handleApiRequest(endPart: Constants.apiCalls.getPost, params: params, completion: {[weak self] (responseJson, errorDesc, timeout) in
                if responseJson != nil && errorDesc == "" {
                    let feed = ((responseJson as? NSDictionary ?? [:])["data"] as? NSDictionary ?? [:])["value"] as? NSDictionary ?? [:]
                    let desc = feed.value(forKey: "post_desc") as? String ?? ""
                    let dataCode = desc.data(using: String.Encoding.utf8)!
                    var messageString = desc
                    if let msg = String(data: dataCode, encoding: String.Encoding.nonLossyASCII), msg != "" {
                        messageString = msg
                    }
                    dict.desc = messageString
                }
                self?.loadDictToQueue(dict: dict, id: id)
            })
        } else {
            self.loadDictToQueue(dict: dict, id: id)
        }
    }
    
    func loadDictToQueue(dict: PostObject, id: String) {
        if let Arr = Constants.sharedUtils.loadCachedDataFromLocal(with: id) {
            if Arr.count > 0 {
                var feeds = Arr
                if feeds.filter({ $0.pid == dict.pid }).count == 0 {
                    feeds.insert(dict, at: 0)
                }
                Constants.sharedUtils.cacheDataToLocal(with: feeds, to: id)
            } else {
                var feeds: [PostObject] = []
                if feeds.filter({ $0.pid == dict.pid }).count == 0 {
                    feeds.insert(dict, at: 0)
                }
                Constants.sharedUtils.cacheDataToLocal(with: feeds, to: id)
            }
        } else {
            var feeds: [PostObject] = []
            if feeds.filter({ $0.pid == dict.pid }).count == 0 {
                feeds.insert(dict, at: 0)
            }
            Constants.sharedUtils.cacheDataToLocal(with: feeds, to: id)
        }
        DispatchQueue.main.async {
            if let rootVC = self.window?.rootViewController {
                if let tabbarVC = rootVC as? UITabBarController ?? self.window?.rootViewController?.presentedViewController as? UITabBarController {
                    if let userVC = tabbarVC.children[1].children[0] as? UsersProfileTableViewController {
                        if userVC.id == id {
                            if userVC.tableView != nil && userVC.arrayForFeed.filter({ $0.pid == dict.pid }).count == 0 {
                                userVC.arrayForFeed.insert(dict, at: 0)
                                userVC.heightArr = []
                                userVC.cellShowMore = [:]
                                userVC.tableView?.reloadData()
                            }
                        }
                    }
                }
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.content.userInfo[AnyHashable("type")] as? String == "post" {
            handleNotification(tappedNotificationInfo: notification.request.content.userInfo)
        }
        completionHandler([UNNotificationPresentationOptions.alert,UNNotificationPresentationOptions.sound])
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.content.userInfo[AnyHashable("type")] as? String == "post" {
            notificationTapped = true
            handleNotification(tappedNotificationInfo: response.notification.request.content.userInfo)
        }
        completionHandler()
    }


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("hre")
    }
    func application(_ application: UIApplication,didReceiveRemoteNotification notification: [AnyHashable : Any],fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if notification[AnyHashable("type")] as? String == "post" {
            if #available(iOS 10.0, *) {
                handleNotification(tappedNotificationInfo: notification)
            } else {
                handleNotification(tappedNotificationInfo: notification)
            }
        }
        completionHandler(UIBackgroundFetchResult.newData)
    }

    func launchApp() {
        window = UIWindow(frame: UIScreen.main.bounds)
        //Constants.defaults.set("false", forKey: "contactsAccessPermitted")
        if (Constants.defaults.value(forKey: "sizeOfLimitedCharacters") == nil) {
            let tempTextView = UITextView()
            tempTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
            tempTextView.textContainerInset = UIEdgeInsets.init(top: 50.0, left: 50.0, bottom: 50.0, right: 50.0)
            tempTextView.frame = self.window!.bounds
            var text = ""
            for _ in 1...160 {
                text = text + "x"
            }
            tempTextView.text = text
            tempTextView.sizeToFit()
            let sizeOfLimitedCharacters = tempTextView.frame.height
            Constants.defaults.set(sizeOfLimitedCharacters, forKey: "sizeOfLimitedCharacters")
        }
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        #if AUTH
        if let loggedIn = Constants.defaults.value(forKey: "LoggedIn") as? Bool, loggedIn == true, let id = Constants.defaults.value(forKey: "myID") as? String, id != "" {
            self.firebaseDb = Firestore.firestore()
            Constants.sharedUtils.setFireBaseUserData()
        }
        #endif
            //Messaging.messaging().shouldEstablishDirectChannel = true
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //TODO: Remove below line
        let selectedChannels = Constants.defaults.value(forKey: "selectedChannelIds") as? [String]
        if let _ = Constants.sharedUtils.loadCachedDataFromLocal(with: Constants.clientSpecificConstantsStructName.clientId), (selectedChannels == nil || (selectedChannels != nil && selectedChannels != [])) {
            if let refDict = Constants.sharedUtils.loadCachedDictionaryFromLocal(with: "savedContacts") {
                savedContactRefDict = refDict
            }
            let tabbarVC = mainStoryboard.instantiateViewController(withIdentifier: "tabbarVC") as! UITabBarController
            window?.rootViewController = tabbarVC
            window?.makeKeyAndVisible()
            tabbarVC.selectedIndex = 1
        } else {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let vc: GetStartedViewController = mainStoryboard.instantiateViewController(withIdentifier: "getStartedVc") as! GetStartedViewController
            window?.rootViewController = vc
            window?.makeKeyAndVisible()
        }
        //Constants.defaults.synchronize()
        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        GMSServices.provideAPIKey(Constants.clientSpecificConstantsStructName.gmsApiKey)
        GMSPlacesClient.provideAPIKey(Constants.clientSpecificConstantsStructName.gmsApiKey)
        if let id = Constants.defaults.value(forKey: "myID") as? String, id != "" {
            Crashlytics.sharedInstance().setUserIdentifier(id)
        }
        self.remoteConfig = RemoteConfig.remoteConfig()
        let remoteConfigSettings = RemoteConfigSettings()
        remoteConfigSettings.minimumFetchInterval = 0
        remoteConfig.configSettings = remoteConfigSettings
        let appDefaults: [String: NSObject] = [
            Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName : (Constants.defaults.value(forKey: Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName) as? NSDictionary ?? [:]) as NSObject]
        remoteConfig.setDefaults(appDefaults)
        self.fetchConfig()
        registerForPushNotifications(silent: Constants.defaults.value(forKey: "notificationFlag") as? Bool ?? false)
    }
    
    func fetchConfig() {
        let expirationDuration = 3600
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            if status == .success {
                print("Config fetched!")
                DispatchQueue.global(qos: .background).async {
                    self.remoteConfig.activate()
                    let dict = self.remoteConfig[Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName].jsonValue as? NSDictionary ?? [:]
                    if dict[Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.enableKey] as? Bool == false || dict[Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.enableKey] as? Bool == nil {
                        Constants.defaults.removeObject(forKey: Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName)
                    } else {
                        Constants.defaults.set(dict, forKey: Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName)
                    }
                }
            }
        }
    }
    
    func registerForPushNotifications(silent: Bool) {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = silent ? []: [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: silent ? []: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func loggedInSoEnable() {
        #if AUTH
        DispatchQueue.main.async {
            if let _ = Constants.defaults.value(forKey: "myID") as? String, Constants.defaults.value(forKey: "LoggedIn") as? Bool == true {
                self.firebaseDb = Firestore.firestore()
                self.sendingFcmTokenToServer()
                Constants.sharedUtils.setFireBaseUserData()
            }
        }
        #endif
    }
    
    func openUserVC(id: String, withinApp: Bool?) {
        DispatchQueue.main.async {
            var inUserVC: Bool = false
            if let topVC = UIApplication.topViewController() as? UsersProfileTableViewController {
                if topVC.id == id {
                    inUserVC = true
                }
            }
            if !inUserVC {
                if let window = self.window, let rootViewController = window.rootViewController {
                    var currentController = rootViewController
                    while let presentedController = currentController.presentedViewController {
                        currentController = presentedController
                    }
                    if !currentController.description.contains("TabBarController") {
                        currentController.dismiss(animated: false, completion: nil)
                        currentController = window.rootViewController!
                    }
                    if let rootVC = self.window?.rootViewController {
                        if let tabbarVC = rootVC as? UITabBarController ?? self.window?.rootViewController?.presentedViewController as? UITabBarController {
                            tabbarVC.selectedIndex = 1
                        } else if let childVCs = rootVC.presentedViewController?.children {
                            for child in childVCs {
                                if let tabbarVC = child as? UITabBarController {
                                    tabbarVC.selectedIndex = 1
                                }
                            }
                        }
                    }
                    if let tabbarVC = currentController as? UITabBarController ?? self.window?.rootViewController?.presentedViewController as? UITabBarController {
                        tabbarVC.selectedIndex = 1
                    }
                }
            }
        }
        notificationTapped = false
    }
    
    func versionInAppstore(completionHandler: @escaping (_ version: String) -> Void) {
        let stringUrl = "https://itunes.apple.com/lookup?bundleId=\(Bundle.main.bundleIdentifier!)"
        if let url = URL(string: stringUrl) {
            do {
                let data = try Data(contentsOf: url)
                if let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary {
                    if json["resultCount"] as? Int == 1 {
                        let version = (json.value(forKey: "results") as? [NSDictionary])!.first!.value(forKey: "version")! as? String ?? "0"
                        // version.pre
                        completionHandler(version)
                    }
                }
            } catch {
                completionHandler("0")
            }
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                DispatchQueue.main.async {
                    if Constants.defaults.value(forKey: "LoggedIn") as? Bool == true {
                        self.fcmtoken = result.token
                        self.sendingFcmTokenToServer()
                    }
                    Messaging.messaging().subscribe(toTopic: Constants.clientSpecificConstantsStructName.clientId) { error in
                         if let error = error {
                             print(error.localizedDescription)
                         } else {
                            print("Subscribed to \(Constants.clientSpecificConstantsStructName.clientId) topic")
                         }
                    }
                }
            }
        }
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("***\(fcmToken)")
        self.fcmtoken = fcmToken
        if Constants.defaults.value(forKey: "LoggedIn") as? Bool == true {
            sendingFcmTokenToServer()
        }
        DispatchQueue.main.async {
            Messaging.messaging().subscribe(toTopic: Constants.clientSpecificConstantsStructName.clientId) { error in
                if let error = error {
                    print(error.localizedDescription)
                } else {
                    print("Subscribed to \(Constants.clientSpecificConstantsStructName.clientId) topic")
                }
            }
        }
    }

    func sendingFcmTokenToServer() {
        #if AUTH
        if let id = Constants.defaults.value(forKey: "myID") as? String, id != "", Constants.defaults.value(forKey: "LoggedIn") as? Bool == true {
            let fcm = Constants.defaults.value(forKey: "fcmToken") as? String
            if fcm != fcmtoken {
                let deviceTypeInDefaults = Constants.defaults.value(forKey: "deviceType") as? String
                var params = ["userid" : id, "updateObj": ["notificationId": fcmtoken]] as [String : Any]
                if deviceTypeInDefaults != "ios" {
                    params = ["userid" : id, "updateObj": ["notificationId": fcmtoken, "device_type": "ios"]] as [String : Any]
                }
                apiCall.handleApiRequest(endPart: Constants.apiCalls.updateUser, params: params, completion: {[weak self] (responseJson, errorDesc, timeout) in
                    guard let strongSelf = self else {
                        return
                    }
                    if responseJson != nil && errorDesc == "" {
                        Constants.defaults.set(self?.fcmtoken ?? "", forKey: "fcmToken")
                        if deviceTypeInDefaults != "ios" {
                            Constants.defaults.set("ios", forKey: "deviceType")
                        }
                        if strongSelf.fcmtoken != nil {
                            strongSelf.firebaseDb!.collection(Constants.firebasePathConstants.users).document(id).updateData([
                                    "notificationId": strongSelf.fcmtoken!
                                ]) { (error) in
                            }
                        }
                        print("fcm token : \(self?.fcmtoken ?? "") sent as notificationID")
                    } else {
                        print(errorDesc)
                    }
                })
            }
        }
        #endif
    }
    //MARK: - Notifications
    //Constants.defaults nil at start - but has obtained the values
    @objc func protectedDataAvailableNotification(notification: NSNotification) {
        NSLog("protectedDataAvailableNotification")
        NotificationCenter.default.removeObserver(self, name: UIApplication.protectedDataDidBecomeAvailableNotification, object: nil)
        self.launchApp()
    }

    //get notified when there is network failur@objc e
    @objc func reachabilityChanged(note: NSNotification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            Spinner.stop()
            print("Network not reachable")
        }
    }
    
    func fetchFromPlist(plistName: String, key : String) -> String {
        var dict: NSDictionary?
        if let path = Bundle.main.path(forResource: plistName, ofType: "plist") {
            dict = NSDictionary(contentsOfFile: path)
            if let value = dict?.value(forKey: key) as? String {
                return value
            } else {
                return ""
            }
        }
        return ""
    }

    //get UIColor from hex
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: (NSCharacterSet.whitespacesAndNewlines as NSCharacterSet) as CharacterSet).uppercased()
        if (cString.hasPrefix("#")) {
            cString = String(cString[cString.index(after: cString.startIndex)...])
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
        
    //date components conversion
    func timeFromDateComponents(datecomponents: DateComponents, time: String, date: Date) -> String {
        var timeEdited: String?
        if ((datecomponents.day! + (-2 * datecomponents.day!)) >= 1) || ((datecomponents.month! + (-2 * datecomponents.month!)) >= 1) || ((datecomponents.year! + (-2 * datecomponents.year!)) >= 1) {
            let newDateFormatter = DateFormatter()
            newDateFormatter.dateFormat = "dd-MM-yyyy 'at' h:mm a"
            newDateFormatter.locale = NSLocale.system
            let stringFormTime = newDateFormatter.string(from: date)
            timeEdited = stringFormTime
            //time.padding(toLength: 22, withPad: time, startingAt: 0)
        } else if (datecomponents.hour! + (-2 * datecomponents.hour!)) >= 1 {
            if datecomponents.hour == -1 {
                timeEdited = "\((datecomponents.hour! + (-2 * datecomponents.hour!))) hour ago"
            } else {
                timeEdited = "\((datecomponents.hour! + (-2 * datecomponents.hour!))) hours ago"
            }
        } else if (datecomponents.minute! + (-2 * datecomponents.minute!)) >= 1 {
            if datecomponents.minute == -1 {
                timeEdited = "\((datecomponents.minute! + (-2 * datecomponents.minute!))) minute ago"
            } else {
                timeEdited = "\((datecomponents.minute! + (-2 * datecomponents.minute!))) minutes ago"
            }
        } else {
            timeEdited = "Just now"
        }
        return timeEdited!
    }
    
    func chatTimeFromDateComponents(date: Date) -> String {
        var timeEdited: String?
        let tempDateFormatter = DateFormatter()
        tempDateFormatter.dateFormat = "dd/MM/yyyy"
        tempDateFormatter.locale = NSLocale.system
        let tempStringDate = tempDateFormatter.string(from: date)
        let todaysStringDate = tempDateFormatter.string(from: Date())
        let yesterdaysStringDate = tempDateFormatter.string(from: NSCalendar.current.date(byAdding: .day, value: -1, to: Date())!)
            let newDateFormatter = DateFormatter()
        if tempStringDate == todaysStringDate {
            newDateFormatter.dateFormat = "'Today at' h:mm a"
        } else if tempStringDate == yesterdaysStringDate {
            newDateFormatter.dateFormat = "'Yesterday at' h:mm a"
        } else {
            newDateFormatter.dateFormat = "dd/MM/yyyy 'at' h:mm a"
        }
        newDateFormatter.locale = NSLocale.system
        let stringFormTime = newDateFormatter.string(from: date)
        timeEdited = stringFormTime
        return timeEdited!
    }
    
    func joinedDateString(date: Date) -> String {
        let tempDateFormatter = DateFormatter()
        tempDateFormatter.dateFormat = "MMMM dd, yyyy"
        return tempDateFormatter.string(from: date)
    }
    //cropping the profile image to a square image
    func cropImage(img: UIImage) -> UIImage{
        var sideLength: CGFloat?
        var valueX : CGFloat?
        var valueY: CGFloat?
        if img.cgImage!.height < img.cgImage!.width {
            sideLength = CGFloat(img.cgImage!.height)
            valueX = (CGFloat(img.cgImage!.width) - sideLength!) / 2
            valueY = 0
        } else {
            sideLength = CGFloat(img.cgImage!.width)
            valueX = 0
            valueY = (CGFloat(img.cgImage!.height) - sideLength!) / 2
        }
        let cropRect = CGRect(x: valueX!, y: valueY!, width: sideLength!, height: sideLength!)
        let imageRef = img.cgImage!.cropping(to: cropRect)
        let cropped : UIImage = UIImage(cgImage: imageRef!, scale: 0, orientation: img.imageOrientation)
        return cropped
    }
}
extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
extension String {
    
    var parseJSONString: Any? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            return try? JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }
}


