//
//  Constants.swift
//  Qkopy
//
//  Created by Lincy George on 27/05/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import Foundation
struct Constants {
    static let defaults = UserDefaults.standard
    static let sharedUtils = ControllerUtils.shared
    static let feedCellTitleBarHeight = 80.00
    static let feedCellBottomBarHeight = 33.00
    static let appSlogan = "Get Instant News & Updates"
    static let fromQkopyText = "from \n Qkopy"
    static let downloadAppText = "To like or to give reply privately, Sign up with Qkopy"
    static let titleForChannelsListing = "COVID-19 Categories"
    static let subscribedChannelsText = "COVID-19 Categories" //"Your Categories"
    static let helplineText = "Helpline"
    static let poweredByText = "Powered By"
    static let startText = "Start"
    static let loginToText = "Login to Qkopy"
    static let loginToMessage = "Please login to Qkopy for sending messsages to "
    static let messageText = "Message"
    static let bookmarksText = "Bookmarks"
    static let clientSpecificConstantsStructName = prdConstants.self
    static let setPincodeText = "Set your Pincode"

    enum Environment: String {
        case production = "https://apinew.qkopy.com/auth/"
        case test = "http://192.168.11.155:3000/auth/"
        case dev = "https://api.vstickr.com/auth/"
        case local = "http://192.168.31.70:8000/auth/"
    }

    struct segueIdentifiers {
        static let splashSegue = "toSplashVC"
    }
    
    struct sectionOrPageTitles {
        static let customiseQRDescription = "You can customise your QR by checking/unchecking the properties given below. Atleast one should be selected."
    }
    
    struct errorsWhileAuthenticating {
        static let heavyLoadAtServer = "Too many requests to server at this time. Please try again later."
        static let numberMissing = "Phone number not entered"
        static let invalidNumber = "Invalid phone number"
        static let notificationError = "Notification configuration issues. Please contact the Qkopy Support team"
        static let verificationError = "Verification failed"
        static let keychainError = "Keychain Access error"
        static let internalError = "Internal Error"
        static let codeInvalid = "Invalid verification code"
        static let codeNotCreated = "Verification code was not created"
        static let codeExpired = "SMS code has expired"
        static let codeNotSend = "SMS code cannot be send. Please contact the Qkopy Support team"
        static let otpDigitMissingError = "Please enter all 6 digits."
    }

    struct arrays {
        static let titleArrayForQROptions: [String] = ["Phone number", "Qkopy userName"]
        static let muteDurationArray = ["8 hours", "1 week", "1 year"]
        static let privacyArray = ["Me", "Contacts", "Public", "Custom", "Team"]
    }
    
    struct apiCalls {
        static let profile = "profile"
        static let mute = "mute"
        static let unmute = "unmute"
        static let likePost = "likePost"
        static let unlikePost = "unlikePost"
        static let getXFeeds = "getXFeeds"
        static let getFeeds = "getFeeds"
        static let getPost = "getPost"
        static let getChannels = "getChannels"
        static let updateUser = "updateUser"
        static let checkUserExistance = "checkUserExistance"
        static let fileupload = "fileupload"
        static let signup = "signup"
        static let chatNotify = "chatNotify"
        static let getBookmarks = "getBookmarks"
        static let getPinPosts = "getPinPosts"
        static let accountsToFollow = "accountsToFollow"
    }
    
    struct nameOfIdentifiers {
        static let feedCellIdentifier = "customFeedCellForStatus"
        static let contactTableCellIdentifier = "contactCell"
        static let imageFullScreenVcImageCellIdentifier = "dataCell"
        static let selectedUserCellIdentifier = "selectedUserCell"
        static let customisedQRCellIdentifier = "customiseQRCell"
        static let privacyDetailTableCellIdentifier = "settingsPrivacyCell"
        static let teamCellIdentifier = "teamCell"
        static let locationAddressCellIdentifier = "locationAddressCell"
        static let imageCollectionCellIdentifier = "imageCollectionCell"
        static let postPrivacyCellIdentifier = "postPrivacyCell"
        static let channelCellIdentifier = "channelCell"
        static let chatCellIdentifier = "chatCell"
        static let pinnedPostCollectionCellIdentifier = "pinnedPostCollectionCell"
    }
    
    struct firebasePathConstants {
        static let users = "users_plus"
        static let conversations = "conversations_plus"
    }

    struct urlRequestConstants {
        static let authKey = "naXw7ONz1zd92Y3b4dd363cyn4WpqA9o"
        static let contentType = "application/json"
        static let requestType = "POST"
        static let clientService = "frontend-client"
        static let timeoutIntervalForRequest = Double(25)
        static let timeoutIntervalForResource = Double(25)
    }
   
    struct otherConsts {
        static let welcomeText = "Welcome"
        static let getStarted = "Get Started"
        static let qkopyText = "Qkopy X"
        static let termsAndConditionsPreStatement = "By continuing you accept the "
        static let privacyPolicyText = "Privacy Policy"
        static let okText = "OK"
        static let cancelText = "Cancel"
        static let next = "Next"
        static let version = "Version"
        static let settingsText = "Settings"
        static let profileText = "Profile"
        static let usernamePlaceholder = "Name"
        static let verifiedUserText = "Verified Contact"
        static let unblockText = "Unblock"
        static let blockText = "Block"
        static let aboutText = "About"
        static let feedbackText = "Give Feedback"
        static let viewsText = " views"
        static let viewText = " view"
        static let chatOnText = "Turn on reply/messages"
        static let makeACallText = "Make a Call"
        static let noneText = "None"
        static let replyToWhatsappPretext = "Reply on post dated * from Kerala Space Park App:"
        static let makeACallMessage = "Do you want to contact \n ** \n *"
        static let contactFacilityBlockedText = "Call/Reply has been disabled by *."
        static let sharePageTitle = "Share"
        static let sharedViaText = "Shared via Qkopy"
        static let noPostsText = "No updates to display"
        static let aboutDefaultDescription = "Welcome to \(Constants.clientSpecificConstantsStructName.appName)"//"Hello!! I am here at Qkopy"
        static let seeMoreText = "...\n*Read More*"
        static let muteNotificationsText = "mute notifications"
        static let unmuteNotificationsText = "unmute notifications"
        static let showNotificationsText = "Show Notifications"
        static let shareExternalText = "Share to External"
        static let likeText = " like"
        static let shareText = "Share"
        static let shareThisAppText = "Share this App"
        static let likesText = " likes"
        static let unmuteText = "Unmute"
        static let muteText = "Mute"
        static let joinedOnText = "Joined on "
        static let sharedLocText = "Shared Location"
        static let refreshOnPullText = "Fetching new updates"
        static let deactivatedEmptyScreenText = "This contact has temporarily deactivated their account"
        static let noUpdatesEmptyScreenText = "No new updates"
        static let postedOnText = "Posted on "
        static let aboutMe = "About Me"
        static let descTextForProfileUpload = "Please provide your name and optional profile photo"
        static let deletePhotoText = "Delete Photo"
        static let settings = "Settings"
        static let initialStatus = "Make your updates here..."
        static let smsVerificationDescription = "By tapping 'Send Confirmation Code' Qkopy will send you an SMS to confirm your phone number. If you have not received any, please choose 'Try again' to reinitiate the SMS verification process."

        //temp texts
    }
    struct splashScreenConsts {
        static let usernameMissing = "Username not entered"
        
    }
    struct otherLinks {
        static let appstoreLink = "http://Qkopy.App"
        static let appstoreLinkWithoutHttp = "Qkopy.App"
        static let googleMapsLink = "https://maps.google.com/?q="
        static let mediaURL = "https://media.qkopy.com"
        static let qkopyPrivacyPolicyLink = "https://qkopy.com/x/privacy-policy.html"
    }
    
    struct alertTitles {
        static let error = "Error"
        static let accessDeniedText = "Access Denied"
    }
    
    struct alertMessages {
        static let updateAppText = "Please update to proceed "
        static let someErrorText = "Oops! Something went wrong. Please try again later."
        static let cameraAccessDenied = "Please allow Qkopy to access your camera through Settings."
        static let photosAccessDenied = "Please allow Qkopy to access your photos through Settings."
        static let profileWillBeVisibleAlertText = "Your profile will be visible to "
        static let oldVersionUsageText = " is using old version of Qkopy"
    }
    struct chatRelated {
        static let iTurnedOffMessageText = "You message is turned off. Please turn on from settings."
        static let iBlockedUserText = "Please unblock ** to send message."
        static let userTurnedOffMessage = "** has turned off message"
        static let noChatsMessage = "No messages yet."
        static let copyMessageText = "Copy Message"
        static let deletedMessageText = "This message was deleted"
        static let deleteMessageText = "Delete Message"
        static let messageText = "Message"
        static let newMessageText = "New Message"
        static let conversationDeleteText = "Selected conversation will be deleted"
        static let togglingChatText = "You are turning your reply/messages "
    }
    struct errorMessages {
        static let connectionLost = "Network not reachable"
        static let somethingWentWrong = "Something went wrong. Please try again later."
        static let unknownErrorText = "Unknown Error"
        static let fileFormatUnknownText = "Audio file is currently not supported in iOS"
        static let requestTimedOutText = "Please check your internet connectivity"
        static let pincodeUnavailable = "This pincode is not available for GoK Direct. Please enter a different pincode"
    }
}
