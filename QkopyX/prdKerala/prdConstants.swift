//
//  prdConstants.swift
//  QkopyX
//
//  Created by Lincy George on 20/03/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//
 
struct prdConstants {
    static let showPinnedPosts = false
    static let hasChannels = true
    static let pickFromPincode = true
    static let hasRemoteConfig = true
    static let showBookmarkBtn = false
    static let showChatAndLikeBtns = false
    static let clientId = "5b07bb5df7cdc5056eb936a0"
    static let appName = "GoK Direct"
    static let iTunesItemIdentifier = "1502436125"
    static let downloadAppText = "Hi,\nI am using GoK Direct App, to get instant messages from GoK Direct. Download GoK Direct App from http://qkopy.xyz/prdkerala"
    static let verifiedUsersWithoutUsernameDownloadAppText = "Shared via GoK Direct App. Download app from http://qkopy.xyz/prdkerala"
    static let gmsApiKey = "AIzaSyCyYo0Dn99qTcWqnW0rOk5O-FlBl0igMh0"
    //static let channelsUnderClientFetchingText = "Fetching latest categories under PRD"
    static let channelsForClientApiEndPart = ""
    static let helplineNumber = "1056"
    static let clientLogoName = "prdClientLogo.png"
    static let fromWhomText = "PRD Kerala"
    static let showQkopyXLogoInAbout = false
    static let aboutScreenDescription = "An initiative from Kerala Startup Mission and PRD Kerala."
    static let healthAlertTitle = "COVID-19 : WHO Health Alert"
    static let healthAlertMessage = "WHO Health Alert brings COVID-19 facts via WhatsApp"
    static let whoLinkToWhatsap = "http://bit.ly/who-covid19-whatsapp"
    
    struct remoteConfigIdentifiers {
        static let configKeyName = "gok_direct"
        static let labelNameKey = "menu_title_heat_map"
        static let urlToHitKey = "heat_map_url"
        static let enableKey = "enabled"
    }
}


// Naval - main changes

// 1. pincodeSetterViewController - commented font and constraint setting fuctions calling and done constraints from storyboard

// 2. changed GoogleService.plist api key
//from - AIzaSyCyYo0Dn99qTcWqnW0rOk5O-FlBl0igMh0
//to - AIzaSyAkQ8oz7mLcK_ZtjjTvUxPQBb_Gct__V1Y


