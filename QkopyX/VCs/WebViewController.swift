//
//  WebViewController.swift
//  QkopyX
//
//  Created by Lincy George on 23/03/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import UIKit
import WebKit
class WebViewController: UIViewController, WKUIDelegate {
    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlString = (appDelegate.remoteConfig[Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName].jsonValue as? NSDictionary ?? [:])[Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.urlToHitKey] as? String ?? ""
        if let url = URL(string:urlString) {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
