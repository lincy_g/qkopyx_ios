//
//  ImageFullScreenViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 15/06/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import GooglePlaces
@objc protocol editPicDelegate {
    func updateImage(image:UIImage?, imageName: String)
}

@objc protocol newEditTextDelegate {
    func aboutTextSet(text: String)
}

class ImageFullScreenViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {

    //Variables
    var image : UIImage?
    var name: String?
    var userName: String?
    var userPhoneNumber: String?
    var actualProfileName: String?
    var fromProfileVC: Bool = false
    var fullScreenImageView: UIImageView?
    var descTextView: UITextView?
    var titleLabel: UILabel?
    var about: String?
    var aboutView: UIView?
    var detailsView: UIView?
    var personalInfoTV: UITextView?
    var nameTV: UITextView?
    var utilsClass = ControllerUtils()
    var imageName = "cameraImage.JPG"
    var reachLabel: UILabel?
    var profileVerified: Bool?
    var qkopyId: String = ""
    var isPremium: Bool = false
    var unknownUser: Bool = false
    var joinedDateString: String?
    var location: [String: Any]?
 
    //outlets
    @IBOutlet var baseTable: UITableView!
    //MARK: - General Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setAllUserDetails()
        baseTableSetup()
        self.navigationController?.setDefaults(largeTitlesReq: false, title: Constants.otherConsts.profileText)
        fullScreenImageViewSetup()
        userDetails()
        aboutTitlelabelSetup()
        detailLabelSetup()
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    func setAllUserDetails() {
        let savedDetails = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails
        image = Constants.sharedUtils.getImage(isProfileImg: true, endPath: savedDetails?.imageName ?? "")
        about =  savedDetails?.about ?? Constants.otherConsts.aboutDefaultDescription
        name = Constants.otherConsts.profileText
        actualProfileName = savedDetails?.name ?? Constants.clientSpecificConstantsStructName.appName
        userName = savedDetails?.actualProfileName
        qkopyId = savedDetails?.qkopyId ?? ""
        location = savedDetails?.location
        joinedDateString = savedDetails?.createdDate
        profileVerified = savedDetails?.profileVerified ?? false
        userPhoneNumber = savedDetails?.number ?? ""
        isPremium = savedDetails?.isPremium ?? false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    //present alert
    func presentAlert(message: String) {
        DispatchQueue.main.async(execute: {
            Spinner.stop()
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
            let alertController = UIAlertController(title: Constants.alertTitles.error, message: message, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: Constants.otherConsts.okText, style: .cancel, handler: nil))
            UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        })
    }
    
    //boundary generator
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    //MARK: - Setting Views
    //navigation bar set up
    func titleBarSetting() {
    }
    
    //scroll view set up below as base
    func baseTableSetup() {
        baseTable.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        //baseTable.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
        baseTable.scrollsToTop = true
        baseTable.isScrollEnabled = true
        self.baseTable.contentInset = UIEdgeInsets.init(top: -1.0, left: 0.0, bottom: -1.0, right: 0.0)
    }
    
    //full view of image
    func fullScreenImageViewSetup() {
        fullScreenImageView = nil
        if image == nil {
            image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))
        } else if image != nil {
            if image!.size.height == 0.0 {
                image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))
            }
            
        }
        var y: CGFloat?
        fullScreenImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height - 104))
        fullScreenImageView?.image = image
        let height = (fullScreenImageView!.frame.width/image!.size.width) * image!.size.height
        y = 0
        
        fullScreenImageView?.frame = CGRect(x: fullScreenImageView!.frame.origin.x, y: y!, width: fullScreenImageView!.frame.width, height: height)
        if isPremium {
            let premiumImageView = UIImageView(image: UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "premiumImage")))
            premiumImageView.frame = CGRect(x: fullScreenImageView!.frame.width - 90, y: 10, width: 80, height: 20)
            premiumImageView.contentMode = .scaleAspectFit
            premiumImageView.alpha = 0.7
            fullScreenImageView?.addSubview(premiumImageView)
        }
    }
    
    //details of user
    func userDetails() {
        detailsView = UIView(frame: CGRect(x: fullScreenImageView!.frame.minX, y: 0, width: fullScreenImageView!.frame.width - 50, height: 70))
        detailsView?.backgroundColor = UIColor.clear
        nameTV = UITextView(frame: CGRect(x: 20, y: 0, width: detailsView!.frame.width - 40, height: 25))
        nameTV?.backgroundColor = UIColor.clear
        nameTV?.isUserInteractionEnabled = true
        nameTV?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        nameTV?.isEditable = false
        nameTV?.isScrollEnabled = false
        nameTV?.textContainerInset = UIEdgeInsets.init(top: 10, left: 0, bottom: 0, right: 0)
        self.detailsView?.addSubview(nameTV!)
        
        personalInfoTV = UITextView(frame: CGRect(x: 20, y: nameTV!.frame.minY + nameTV!.frame.height, width: detailsView!.frame.width - 40, height: 20))
        personalInfoTV?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        personalInfoTV?.backgroundColor = UIColor.clear
        personalInfoTV?.isUserInteractionEnabled = true
        personalInfoTV?.isEditable = false
        personalInfoTV?.textContainerInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 10, right: 0)
        personalInfoTV?.isScrollEnabled = false
        
        reachLabel = UILabel()
        reachLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        
        if #available(iOS 13.0, *) {
            nameTV?.textColor = UIColor.label
            reachLabel?.textColor = UIColor.label
        }
        self.detailsView?.addSubview(personalInfoTV!)
        self.detailsView?.sizeToFit()
        self.detailsView?.bringSubviewToFront(nameTV!)
    }
    
    //about heading
    func aboutTitlelabelSetup() {
        //aboutView = UIView(frame: CGRect(x: 0, y: fullScreenImageView!.frame.height + 10, width: fullScreenImageView!.frame.width, height: 90))
        aboutView = UIView(frame: CGRect(x: 0, y: 0, width: fullScreenImageView!.frame.width, height: self.view.frame.height - (fullScreenImageView!.frame.height + detailsView!.frame.height + 10)))
        self.aboutView?.backgroundColor = .clear
        titleLabel = UILabel(frame: CGRect(x: 25, y: 10, width: fullScreenImageView!.frame.width - 50, height: 30))
        titleLabel?.backgroundColor = .clear
        titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
        titleLabel?.text = "About"
        if #available(iOS 13.0, *) {
            titleLabel?.textColor = UIColor.label
        } else {
            titleLabel?.textColor = UIColor.darkGray
        }

        let lowerBorder = CALayer()
        lowerBorder.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode")).cgColor
        lowerBorder.frame = CGRect(x: 0, y: titleLabel!.frame.height, width: titleLabel!.frame.width, height: 0.5)
        titleLabel?.layer.addSublayer(lowerBorder)
        aboutView?.addSubview(titleLabel!)
    }
    
    //about - details of user
    func detailLabelSetup() {
        descTextView = UITextView(frame: CGRect(x: 20, y: titleLabel!.frame.minY + titleLabel!.frame.height + 10, width: fullScreenImageView!.frame.width - 40, height: 20))
        descTextView?.dataDetectorTypes = UIDataDetectorTypes.all
        descTextView?.isEditable = false
        descTextView?.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        descTextView?.backgroundColor = UIColor.clear
        descTextView?.isSelectable = true
        descTextView?.isScrollEnabled = false
        descTextView?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        
        aboutView?.addSubview(descTextView!)
    }
    
    func setFormatForText(str: String) -> NSMutableAttributedString {
        let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 20.0
        paragraphStyle.maximumLineHeight = 20.0
        paragraphStyle.minimumLineHeight = 20.0
        let mutStr = NSMutableAttributedString(string: str, attributes: [NSAttributedString.Key.paragraphStyle: paragraphStyle])
        mutStr.addAttribute(NSAttributedString.Key.font, value: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0), range: NSRange(location: 0, length: (str.count)))
        return mutStr
    }
    
    //MARK: - Delegates
    //tableview delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        if joinedDateString != nil || location != nil {
            return 3
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return nil
        }
        return ""
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 1.0
        }
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView.numberOfSections == 3 ? (section == 2): (section == 1) {
            return 1.0
        }
        return 10.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if fromProfileVC {
                if profileVerified == true {
                    return 4
                }
                return 3
            } else {
                if profileVerified == true {
                    return 3
                }
                return 2
            }
        case (joinedDateString == nil && location == nil) ? 1: 2:
            return 1
        case (joinedDateString == nil && location == nil) ? 3: 1:
            var returnNumber = joinedDateString != nil ? 1: 0
            returnNumber = returnNumber + (location != nil ? 1: 0)
            return returnNumber
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = baseTable.dequeueReusableCell(withIdentifier: Constants.nameOfIdentifiers.imageFullScreenVcImageCellIdentifier)!
        cell.accessoryType = .none
        if #available(iOS 13.0, *) {
            cell.imageView?.tintColor = UIColor.label
        } else {
            cell.imageView?.tintColor = UIColor.black
        }
        cell.textLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        switch indexPath.section {
        case 0:
            if profileVerified == true && indexPath.row == tableView.numberOfRows(inSection: 0) - 1 {
                let verifiedIconImageView = UIImageView(image: UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "verifyImage")))
                verifiedIconImageView.frame = CGRect(x: 25, y: 10, width: 20, height: 20)
                let verifiedLabel = UILabel(frame: CGRect(x: verifiedIconImageView.frame.origin.x + verifiedIconImageView.frame.width + 5, y: 0, width: cell.frame.width - (verifiedIconImageView.frame.origin.x + verifiedIconImageView.frame.width + 5), height: 40))
                verifiedLabel.text = Constants.otherConsts.verifiedUserText
                verifiedLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
                cell.addSubview(verifiedIconImageView)
                cell.addSubview(verifiedLabel)
                tableView.rowHeight = verifiedLabel.frame.height
            } else {
                switch indexPath.row {
                case 0:
                    if !cell.subviews.contains(fullScreenImageView!) {
                        cell.addSubview(fullScreenImageView!)
                    }
                    tableView.rowHeight = fullScreenImageView!.frame.height
                case 1:
                    if !cell.subviews.contains(detailsView!) {
                        cell.addSubview(detailsView!)
                    }
                    let str = userName ?? ""
                    let dataCode = str.data(using: String.Encoding.utf8)!
                    var nameString = str
                    if let name = String(data: dataCode, encoding: String.Encoding.nonLossyASCII) {
                        if name == "" {
                            nameString = str
                        } else {
                            nameString = name
                        }
                    }
                    if profileVerified == true && actualProfileName != nil && actualProfileName != "" && fromProfileVC != true {
                        nameTV?.text = actualProfileName ?? nameString
                    } else {
                        nameTV?.text = nameString
                    }
                    nameTV?.frame = CGRect(x: 20, y: 0, width: detailsView!.frame.width - 40, height: nameTV!.frame.height)
                    nameTV?.sizeToFit()
                    
                    var arrayOfDetailsInContacts = Constants.defaults.value(forKey: "immediateContactsSyncedResults") as? [String] ?? []
                    if let myFormattedNumber = Constants.defaults.value(forKey: "formattedMyNumber") as? String, myFormattedNumber != "", !arrayOfDetailsInContacts.contains(myFormattedNumber) {
                        arrayOfDetailsInContacts.append(myFormattedNumber)
                    }
                    if let myQkopyId = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.qkopyId, myQkopyId != "", myQkopyId != "@qkopy.com", !arrayOfDetailsInContacts.contains(myQkopyId) {
                        arrayOfDetailsInContacts.append(myQkopyId)
                    }
                    var tempDetailsArr: [String] = []
                    if unknownUser {
                        if userPhoneNumber != "" && userPhoneNumber != nil {
                            tempDetailsArr.append(userPhoneNumber!)
                        }
                        if qkopyId != "" {
                            tempDetailsArr.append(qkopyId + "@qkopy.com")
                        }
                    } else {
                        //commented for including qkopy contacts
                        //if arrayOfDetailsInContacts.contains(userPhoneNumber ?? "") &&
                        if userPhoneNumber != "" && userPhoneNumber != nil {
                            tempDetailsArr.append(userPhoneNumber!)
                        }
                        //}
                        //if arrayOfDetailsInContacts.contains(self.qkopyId) &&
                        if qkopyId != "" {
                            tempDetailsArr.append(qkopyId + "@qkopy.com")
                        }
                        //}
                    }
                    if tempDetailsArr.count == 0 {
                        personalInfoTV?.text = ""
                    } else {
                       // [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0)]
                        if profileVerified == true && actualProfileName != nil && actualProfileName != "" && fromProfileVC != true {
                            let joinedText = NSMutableAttributedString(string: tempDetailsArr.joined(separator: "\n"), attributes: [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)])
                            personalInfoTV?.attributedText = joinedText
                        } else {
                            personalInfoTV?.text = tempDetailsArr.joined(separator: "\n")
                        }
                    }
                    personalInfoTV?.sizeToFit()
                    if #available(iOS 13.0, *) {
                        personalInfoTV?.textColor = .label
                    }
                    personalInfoTV?.frame = CGRect(x: 20, y: nameTV!.frame.minY + nameTV!.frame.height , width: detailsView!.frame.width - 40, height: personalInfoTV!.frame.height)
                    detailsView?.frame = CGRect(x: fullScreenImageView!.frame.minX, y: 0, width: fullScreenImageView!.frame.width - 50, height: nameTV!.frame.height + personalInfoTV!.frame.height)
                    tableView.rowHeight = detailsView!.frame.height
                    
                    if !fromProfileVC {
                        let imgVw = UIImageView(image: UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "grayQRImage")))
                        imgVw.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
                        cell.accessoryView = imgVw
                    } else {
                        cell.accessoryView = nil
                    }
                case 2:
                    reachLabel?.numberOfLines = 0
                    reachLabel?.frame = CGRect(x: 25, y: 0, width: fullScreenImageView!.frame.width - 50, height: 44)
                    let userReach = Constants.defaults.value(forKey: "userReach") as? String ?? "..."
                    if userReach == "1" {
                        reachLabel?.text = "\(userReach) person has your contact in Qkopy"
                    } else {
                        reachLabel?.text = "\(userReach) people have your contact in Qkopy"
                    }
                    reachLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
                    if !cell.subviews.contains(reachLabel!) {
                        cell.addSubview(reachLabel!)
                    }
                    reachLabel?.sizeToFit()
                    reachLabel?.frame = CGRect(x: 25, y: 10, width: fullScreenImageView!.frame.width - 50, height: reachLabel!.frame.height)
                    tableView.rowHeight = reachLabel!.frame.height + 20
                default: break
                }
            }
        case (joinedDateString == nil && location == nil) ? 3: 1:
            switch(indexPath.row) {
            case joinedDateString != nil ? 0: 3:
                cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "joinedDate"))?.withRenderingMode(.alwaysTemplate)
                cell.textLabel?.text = Constants.otherConsts.joinedOnText + joinedDateString!
            case location != nil ? (joinedDateString != nil ? 1: 0): 3:
                cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "currentLocation"))?.withRenderingMode(.alwaysTemplate)
                cell.accessoryType = .disclosureIndicator
                cell.textLabel?.text = location!["name"] as? String
            default: break
            }
            tableView.rowHeight = 50
        case (joinedDateString == nil && location == nil) ? 1: 2:
            cell.imageView?.image = nil
            cell.textLabel?.text = ""
            if !cell.subviews.contains(aboutView!) {
                cell.addSubview(aboutView!)
            }
            let str = self.about ?? Constants.otherConsts.aboutDefaultDescription
            let dataCode = str.data(using: String.Encoding.utf8)!
            var messageString: String?
            if let msg = String(data: dataCode, encoding: String.Encoding.nonLossyASCII) {
                if msg == "" {
                    messageString = str
                } else {
                    messageString = msg
                }
            }
            descTextView?.attributedText = setFormatForText(str: messageString ?? str)
            if #available(iOS 13.0, *) {
                descTextView?.textColor = UIColor.label
            } else {
                descTextView?.textColor = UIColor.black
            }
            descTextView?.sizeToFit()
            descTextView?.frame = CGRect(x: 20, y: titleLabel!.frame.minY + titleLabel!.frame.height + 10, width: fullScreenImageView!.frame.width - 40, height: descTextView!.frame.height + 5)
            aboutView?.frame = CGRect(x: 0, y: 0, width: fullScreenImageView!.frame.width, height: descTextView!.frame.origin.y + descTextView!.frame.height)
            tableView.rowHeight = aboutView!.frame.height
        default:
            break
        }
        for subview in cell.subviews {
            if subview.description.lowercased().contains("separatorview") {
                subview.isHidden = false
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 0 && indexPath.row == 1 && !fromProfileVC {
            let generateVC = self.storyboard?.instantiateViewController(withIdentifier: "CustomQRTVC") as! CustomiseQRTableViewController
            generateVC.myName = nameTV?.text ?? ""
            generateVC.titleName = nameTV?.text ?? ""
            if unknownUser {
                if self.userPhoneNumber != "" && self.userPhoneNumber != nil {
                    generateVC.number = self.userPhoneNumber
                }
                if qkopyId != "" {
                    generateVC.qkopyIDText = self.qkopyId
                }
            } else {
                let arrayOfDetailsInContacts = Constants.defaults.value(forKey: "immediateContactsSyncedResults") as? [String] ?? []
                let qkopyContacts = Constants.defaults.value(forKey: "qkopyContacts") as? [String] ?? []
                if arrayOfDetailsInContacts == [] {
                    generateVC.number = self.userPhoneNumber
                    generateVC.qkopyIDText = self.qkopyId
                }
                if (arrayOfDetailsInContacts.contains(self.qkopyId) || qkopyContacts.contains(self.qkopyId)) && self.qkopyId != "" {
                    generateVC.qkopyIDText = self.qkopyId
                }
                if (arrayOfDetailsInContacts.contains(self.userPhoneNumber ?? "") || qkopyContacts.contains(self.userPhoneNumber ?? "")) && self.userPhoneNumber != nil && self.userPhoneNumber != "" {
                    generateVC.number = self.userPhoneNumber
                }
            }
            generateVC.contactsQR = true
            self.navigationController?.pushViewController(generateVC, animated: true)
        } else if indexPath.section == 1 && location != nil {
            if (joinedDateString != nil && indexPath.row == 1) || (joinedDateString == nil && indexPath.row == 0) {
                 if let coordinates = location!["coordinates"] as? [Double] {
                    let locationCoordinate = CLLocationCoordinate2D(latitude: coordinates[1], longitude: coordinates[0])
                    Constants.sharedUtils.openMapForPlace(locationName: location!["name"] as? String, location: locationCoordinate)
                }
            }
        }
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        nameTV?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        personalInfoTV?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
        descTextView?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        reachLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        self.baseTable.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
