//
//  OpenImageViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 23/03/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import UIKit

@objc protocol imageAfterCroppingDelegate {
    func imageAfterCropping(image: UIImage, index: Int)
}
class OpenImageViewController: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    var imagePaths : [String] = []
    var images: [UIImage] = []
    var currentTappedImageIndex = 0
    @IBOutlet var imgVw: UIImageView!
    @IBOutlet var baseScroll: UIScrollView!
    
    var navBar: UINavigationBar?
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var name: String?
    var navItem: UINavigationItem?
    var navBarFrame: CGRect?
    var uptoNavBarEndHeight: CGFloat!
    var screenHeight: CGFloat = 0
    var indexOfSelectedImageFromPostVc: Int?
    var preservedImgVwFrame: CGRect?
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
        } else {
            self.view.backgroundColor = UIColor.white
        }
        for imagePath in imagePaths {
            let img = Constants.sharedUtils.getImage(isProfileImg: false, endPath: imagePath)
            images.append(img)
        }
        baseScroll.minimumZoomScale = 1.0
        baseScroll.maximumZoomScale = 3.0
        self.navigationController?.hidesBarsOnTap = true
        self.navigationController?.barHideOnTapGestureRecognizer.numberOfTapsRequired = 1
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tappedOnImage(sender:)))
        baseScroll.addGestureRecognizer(tapGesture)
        baseScroll.delegate = self
        imgVw.frame = self.view.frame
        navBar = UINavigationBar(frame: navBarFrame!)
        self.view.addSubview(navBar!)
        if indexOfSelectedImageFromPostVc == nil {
            let panGesture = UIPanGestureRecognizer(target: self, action:#selector(OpenImageViewController.panGestureRecognizerHandler(sender:)))
            panGesture.delegate = self
            self.baseScroll.addGestureRecognizer(panGesture)
        }
//        if currentImage == nil {
//            currentImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))
//        } else if currentImage != nil {
//            if currentImage!.size.height == 0.0 {
//                currentImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))
//            }
//
//        }
        
        
        uptoNavBarEndHeight = navBar!.frame.origin.y + navBar!.frame.height
        screenHeight = (self.view.frame.height - uptoNavBarEndHeight)
        baseScroll.frame = CGRect(x: 0, y: uptoNavBarEndHeight, width: self.view.frame.width, height: screenHeight)
        imgVw.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: screenHeight)
        imgVw.image = images[currentTappedImageIndex]
        if imgVw.image?.images == nil {
            imgVw.contentMode = .scaleAspectFit
        } else {
            imgVw.contentMode = .center
        }
        imageZoomingFrame()
        preservedImgVwFrame = imgVw.frame
        baseScroll.addSubview(imgVw)
        let swipeLeftGesture = UISwipeGestureRecognizer(target: self, action: #selector(OpenImageViewController.swiped(sender:)))
        swipeLeftGesture.direction = .left
        swipeLeftGesture.delegate = self
        self.imgVw.addGestureRecognizer(swipeLeftGesture)
        let swipeRightGesture = UISwipeGestureRecognizer(target: self, action: #selector(OpenImageViewController.swiped(sender:)))
        swipeRightGesture.direction = .right
        swipeRightGesture.delegate = self
        self.imgVw.addGestureRecognizer(swipeRightGesture)
        
        let doubleTapGest = UITapGestureRecognizer(target: self, action: #selector(handleDoubleTapScrollView(recognizer:)))
        doubleTapGest.numberOfTapsRequired = 2
        doubleTapGest.delegate = self
        baseScroll.addGestureRecognizer(doubleTapGest)
        let shareBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.action, target: self, action: #selector(self.shareTapped))
        shareBtn.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        navItem = UINavigationItem(title: name ?? "Photo")
        navItem?.rightBarButtonItem = shareBtn
        let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.cancelTapped))
        navItem?.leftBarButtonItem = cancelBtn
        navBar?.items = [navItem!]
       // img
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.navigationController != nil {
            self.navigationController?.navigationBar.isHidden = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.navigationController != nil {
            self.navigationController?.navigationBar.isHidden = false
        }
    }
    
    func imageZoomingFrame() {
        let currentImage = images[currentTappedImageIndex]
        let height = (imgVw!.frame.width/currentImage.size.width) * currentImage.size.height
        var y: CGFloat = 0
        if height < screenHeight {
            y = (screenHeight - height)/2
            baseScroll.contentSize = CGSize(width: self.view.frame.width, height: screenHeight)
        } else {
            baseScroll.contentSize = CGSize(width: self.view.frame.width, height: height)
        }
        imgVw.isUserInteractionEnabled = true
        imgVw.frame = CGRect(x: imgVw.frame.origin.x, y: y, width: imgVw.frame.width, height: height)
        imgVw.isUserInteractionEnabled = true
    }

    func externalShare() {
//        let currentImage = images[currentTappedImageIndex]
//        let baseView = UIView()
//        baseView.backgroundColor = UIColor.white
//        let newImageView = UIImageView(image : currentImage)
//        let iconImgVw = UIImageView(image: UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "black_white_Qkopy")))
//
//        let leftLabel = UILabel(frame: CGRect(x: newImageView.frame.height*3/40, y: newImageView.frame.height, width: 30, height: 30)) //adjust frame to change position of water mark or text
//        leftLabel.text = " " + Constants.otherConsts.sharedViaText
//        leftLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: newImageView.frame.height/40)
//        leftLabel.backgroundColor = UIColor.white
//        leftLabel.textColor = UIColor.black
//        leftLabel.sizeToFit()
//        leftLabel.frame = CGRect(x: leftLabel.frame.origin.x, y: leftLabel.frame.origin.y, width: leftLabel.frame.width + 20, height: newImageView.frame.height/10)
//        let websiteLabel = UILabel()
//        websiteLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: newImageView.frame.height/40)
//        websiteLabel.text = Constants.otherLinks.appstoreLinkWithoutHttp
//        iconImgVw.frame = CGRect(x: 5, y: newImageView.frame.height, width: leftLabel.frame.height/2, height: leftLabel.frame.height)
//        leftLabel.frame = CGRect(x: iconImgVw.frame.origin.x + iconImgVw.frame.width, y: leftLabel.frame.origin.y, width: leftLabel.frame.width + 20, height: leftLabel.frame.height)
//        websiteLabel.frame = CGRect(x: leftLabel.frame.width + leftLabel.frame.origin.x, y: leftLabel.frame.origin.y, width: newImageView.frame.width - (leftLabel.frame.width + leftLabel.frame.origin.x) - leftLabel.frame.height/4, height: leftLabel.frame.height)
//        websiteLabel.textAlignment = .right
//        leftLabel.textAlignment = .left
//        iconImgVw.contentMode = .scaleAspectFit
//        baseView.addSubview(newImageView)
//        baseView.addSubview(leftLabel)
//        baseView.addSubview(websiteLabel)
//        baseView.addSubview(iconImgVw)
//        baseView.frame = CGRect(x: 0, y: 0, width: newImageView.frame.width, height: leftLabel.frame.origin.y + leftLabel.frame.height)
//        UIGraphicsBeginImageContextWithOptions(baseView.frame.size, false, 0.0)
//        baseView.layer.render(in: UIGraphicsGetCurrentContext()!)
//        let watermarkedImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext()
//        if let notNilImage = watermarkedImage {
//            let imageToShare = [notNilImage]
        let imageToShare = [images[currentTappedImageIndex]]
        let activityVC = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityVC.view.backgroundColor = UIColor.white
        activityVC.popoverPresentationController?.barButtonItem = navItem?.rightBarButtonItem
        activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
        }
        
        // exclude some activity types from the list (optional)
        //activityVC.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook, UIActivityType.al ]
        self.present(activityVC, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Targets
    @objc func handleDoubleTapScrollView(recognizer: UITapGestureRecognizer) {
        if baseScroll.zoomScale == 1 {
            baseScroll.zoom(to: zoomRectForScale(scale: baseScroll.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            baseScroll.setZoomScale(1, animated: true)
            if self.preservedImgVwFrame != nil {
                self.imgVw.frame = self.preservedImgVwFrame!
                if self.navBar?.isHidden == true {
                    self.imgVw.center = self.view.center
                }
            }
        }
    }

    @objc func panGestureRecognizerHandler(sender: UIPanGestureRecognizer) {
        if baseScroll.zoomScale > 1.0 {
            baseScroll.contentSize = CGSize(width: baseScroll.contentSize.width, height: baseScroll.contentSize.height + 2)
        }
        //self.navigationController?.view.backgroundColor = .red
        let touchPoint = sender.location(in: self.view?.window)
        var visibleRect = CGRect()
        visibleRect.origin = baseScroll.contentOffset
        visibleRect.size = baseScroll.bounds.size
        let theScale = 1.0 / baseScroll.zoomScale
        visibleRect.origin.x *= theScale
        visibleRect.origin.y *= theScale
        visibleRect.size.width *= theScale
        visibleRect.size.height *= theScale
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                if #available(iOS 13.0, *) {
                    self.view.backgroundColor = UIColor.systemBackground.withAlphaComponent(0.1)
                } else {
                    self.view.backgroundColor = UIColor.white.withAlphaComponent(0.1)
                }
                self.navBar?.isHidden = true
                if baseScroll.isZooming == false {
                    self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
                }
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                if baseScroll.zoomScale == 1.0 || (visibleRect.origin.x <= 0 && visibleRect.origin.y <= 0) {
                    if self.navigationController != nil {
                        self.navigationController?.navigationBar.isHidden = false
                        self.navigationController?.popViewController(animated: true)
                    } else {
                        self.navigationController?.isNavigationBarHidden = true
                        self.dismiss(animated: true, completion: nil)
                    }
                } else {
                    UIView.animate(withDuration: 0.3, animations: {
                        self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                        if #available(iOS 13.0, *) {
                            self.view.backgroundColor = UIColor.systemBackground
                        } else {
                            self.view.backgroundColor = UIColor.white
                        }
                        self.navBar?.isHidden = false
                    })
                }
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                    if #available(iOS 13.0, *) {
                        self.view.backgroundColor = UIColor.systemBackground
                    } else {
                        self.view.backgroundColor = UIColor.white
                    }
                    self.navBar?.isHidden = false
                })
            }
        }
    }
    
    @objc func doneTapped() {
        if self.navigationController != nil {
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    //images in black background
    @objc func tappedOnImage(sender: UITapGestureRecognizer) {
        if self.navBar?.isHidden == true {
            self.navigationController?.hidesBarsOnTap = false
            if #available(iOS 13.0, *) {
                self.baseScroll.backgroundColor = UIColor.systemBackground
                self.view.backgroundColor = UIColor.systemBackground
            } else {
                self.baseScroll.backgroundColor = UIColor.white
                self.view.backgroundColor = UIColor.white
            }
            if self.preservedImgVwFrame != nil {
                self.imgVw.frame = self.preservedImgVwFrame!
            } else {
                self.imgVw.contentMode = .scaleAspectFit
            }
            self.navBar?.isHidden = false
            self.navBar?.alpha = 1
            self.baseScroll.frame = CGRect(x: 0, y: uptoNavBarEndHeight, width: self.view.frame.width, height: screenHeight)
        } else {
            self.navigationController?.hidesBarsOnTap = true
            self.navigationController?.barHideOnTapGestureRecognizer.numberOfTapsRequired = 1
            self.baseScroll.backgroundColor = UIColor.black
            self.view.backgroundColor = UIColor.black
            self.navBar?.isHidden = true
            self.navBar?.alpha = 0
            self.baseScroll.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: screenHeight + uptoNavBarEndHeight)
            self.imgVw.center = self.view.center
        }
    }
    
    @objc func swiped(sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .right:
            if currentTappedImageIndex == 0 {
                baseScroll.alwaysBounceHorizontal = true
            } else {
                baseScroll.alwaysBounceHorizontal = false
            }
            currentTappedImageIndex = currentTappedImageIndex == 0 ? currentTappedImageIndex: (currentTappedImageIndex - 1)
        case .left:
            if currentTappedImageIndex == (images.count - 1) {
                baseScroll.alwaysBounceHorizontal = true
            } else {
                baseScroll.alwaysBounceHorizontal = false
            }
            currentTappedImageIndex = currentTappedImageIndex == (images.count - 1) ? currentTappedImageIndex: (currentTappedImageIndex + 1)
        default:
            break;
        }
        self.imgVw.image = images[currentTappedImageIndex]
        if imgVw.image?.images == nil {
            imgVw.contentMode = .scaleAspectFit
        } else {
            imgVw.contentMode = .center
        }
        imageZoomingFrame()
    }
    //MARK: - General Functions
    @objc func cancelTapped() {
        if self.navigationController != nil {
            self.navigationController?.navigationBar.isHidden = false
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func shareTapped() {
        externalShare()
    }
    
    //MARK: - Zoom functions
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = imgVw.frame.size.height / scale
        zoomRect.size.width  = imgVw.frame.size.width  / scale
        let newCenter = imgVw.convert(center, from: baseScroll)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    //which view should be zoomed
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imgVw
    }

    //MARK: - Delegates
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
