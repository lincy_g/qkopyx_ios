//
//  OpenAttachmentViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 23/06/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import UIKit
import WebKit
class OpenAttachmentViewController: UIViewController, WKUIDelegate, NSURLConnectionDataDelegate {

    var url: URL?
    var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let webView = WKWebView()
        webView.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: self.view.frame.height - 64)
        webView.scrollView.bounces = false
        //webView.scalesPageToFit = true
        webView.isUserInteractionEnabled = true
        webView.scrollView.showsHorizontalScrollIndicator = true
        webView.scrollView.showsVerticalScrollIndicator = true
        //webView.loadRequest(URLRequest(url: url!))
        if let data = try? Data(contentsOf: url!) {
            //if only pdf//
            webView.load(data, mimeType: "application/pdf", characterEncodingName: "", baseURL: url!.deletingLastPathComponent())
        } else {
            webView.load(URLRequest(url: url!))
        }
        webView.uiDelegate = self
        self.view.addSubview(webView)
        self.view.backgroundColor = webView.backgroundColor
        self.navigationController!.navigationBar.isTranslucent = false
        let shareBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.action, target: self, action: #selector(self.shareTapped))
        shareBtn.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        navigationItem.title = name ?? "Attachment"
        navigationItem.rightBarButtonItem = shareBtn
        webView.scrollView.contentSize = CGSize(width: webView.scrollView.contentSize.width, height: webView.scrollView.contentSize.height + 100)
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        } else {
            // Fallback on earlier versions
        }
        
        // Do any additional setup after loading the view.
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController!.navigationBar.isTranslucent = true
    }

    @objc func shareTapped() {
        var items: [Any]?
        if name != nil {
            if name!.components(separatedBy: ".").contains("pdf") == false && name!.components(separatedBy: ".").contains("PDF") == false {
                name = name! + ".pdf"
            }
        }
        let tempURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(name ?? ""))
        do {
            let data = try Data(contentsOf: url!)
            try data.write(to: tempURL)
            items = [tempURL]
        } catch {
        }
        if items == nil {
            items = [url!]
        }
        let activityVC = UIActivityViewController(activityItems: items!, applicationActivities: nil)
        activityVC.view.backgroundColor = UIColor.white
        activityVC.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem // so that iPads won't crash
        activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
            self.tabBarController?.tabBar.isUserInteractionEnabled = true
            do{
                try FileManager.default.removeItem(at: tempURL)
            } catch {
                print("not deleted")
            }
        }
        
        // exclude some activity types from the list (optional)
        //activityVC.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook, UIActivityType.al ]
        self.present(activityVC, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
