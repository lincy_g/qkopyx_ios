//
//  SettingsTableViewController.swift
//  QkopyX
//
//  Created by Lincy George on 04/02/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {

    var notificationToggle: UISwitch?
    var bookmarkListLabel: UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setDefaults(largeTitlesReq: false, title: Constants.otherConsts.settingsText)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            var i = 1
            if Constants.clientSpecificConstantsStructName.showBookmarkBtn {
                i += 1
            }
            if Constants.clientSpecificConstantsStructName.healthAlertTitle != "" {
                i += 1
            }
            if Constants.defaults.value(forKey: Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName) != nil {
                i += 1
            }
            return i
        default:
            return 2
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingsCell", for: indexPath)
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 0: 10:
                cell.textLabel?.text = Constants.bookmarksText
                let list = Constants.defaults.value(forKey: "bookmarkedList") as? [String] ?? []
                if bookmarkListLabel == nil {
                    bookmarkListLabel = UILabel()
                }
                bookmarkListLabel?.frame = CGRect(x: self.tableView.frame.width - 150, y: 0, width: 135, height: cell.frame.height)
                bookmarkListLabel?.textAlignment = .right
                var rightSideText: String?
                if list.count > 0 {
                    rightSideText = "\(list.count)"
                } else {
                    rightSideText = Constants.otherConsts.noneText
                }
                if rightSideText != Constants.otherConsts.noneText {
                    bookmarkListLabel?.frame = CGRect(x: tableView.frame.width - 150, y: 0, width: 120, height: cell.frame.height)
                    cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
                    cell.isUserInteractionEnabled = true
                    cell.textLabel?.isEnabled = true
                } else {
                    cell.accessoryType = UITableViewCell.AccessoryType.none
                    cell.isUserInteractionEnabled = false
                    cell.textLabel?.isEnabled = false
                }
                bookmarkListLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
                bookmarkListLabel?.text = "\(rightSideText!)"
                bookmarkListLabel?.textColor = UIColor.gray
                cell.addSubview(bookmarkListLabel!)
                cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "bookmarksSettingsIcon"))
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 1: 0:
                if Constants.clientSpecificConstantsStructName.hasChannels == true {
                    cell.textLabel?.text = Constants.subscribedChannelsText
                    cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
                    cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "categoryIcon"))
                } else {
                    cell.textLabel?.text = Constants.otherConsts.showNotificationsText
                    cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "showNotificationsImg"))
                    if notificationToggle == nil {
                        notificationToggle = UISwitch(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
                    }
                    notificationToggle?.addTarget(self, action: #selector(self.notificationToggleStateChanged(sender:)), for: UIControl.Event.valueChanged)
                    cell.accessoryType = UITableViewCell.AccessoryType.none
                    cell.accessoryView = notificationToggle
                    notificationToggle?.isOn = Constants.defaults.value(forKey: "notificationFlag") as? Bool ?? true
                }
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 2: 1:
                cell.textLabel?.text = Constants.clientSpecificConstantsStructName.healthAlertTitle.components(separatedBy: " : ")[1]
                cell.accessoryType = UITableViewCell.AccessoryType.none
                cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "whoAlertIcon"))
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 3: 2:
                cell.textLabel?.text = (appDelegate.remoteConfig[Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.configKeyName].jsonValue as? NSDictionary ?? [:])[Constants.clientSpecificConstantsStructName.remoteConfigIdentifiers.labelNameKey] as? String ?? ""
                cell.accessoryType = UITableViewCell.AccessoryType.none
                cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "heatMapIcon"))
            default:
                break;
            }
        case 1:
            switch indexPath.row {
            case 0:
                cell.textLabel?.text = Constants.otherConsts.shareThisAppText
                cell.accessoryType = UITableViewCell.AccessoryType.none
                cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "shareQkopyImg"))
            case 1:
                cell.textLabel?.text = Constants.otherConsts.aboutText
                cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
                cell.imageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "aboutImg"))
            default:
                break;
            }
        default: break;
        }
        // Configure the cell...
        if Constants.clientSpecificConstantsStructName.showBookmarkBtn == true && !(indexPath.section == 0 && indexPath.row == 0) {
            if cell.subviews.contains(bookmarkListLabel!) {
                let index = Int(cell.subviews.firstIndex(of: bookmarkListLabel!)!)
                cell.subviews[index].removeFromSuperview()
            }
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 0: 10:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "userProfileTVC") as! UsersProfileTableViewController
                vc.showOnlyBookmarkedItems = true
                self.navigationController?.pushViewController(vc, animated: true)
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 1: 0:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "channelsTVC") as! ChannelsTableViewController
                self.navigationController?.pushViewController(vc, animated: true)
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 2: 1:
                let alertController = UIAlertController(title: Constants.clientSpecificConstantsStructName.healthAlertTitle, message: Constants.clientSpecificConstantsStructName.healthAlertMessage, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constants.otherConsts.cancelText, style: .default, handler: nil))
                let startButton = UIAlertAction(title: Constants.startText, style:.default, handler: {(action) -> Void in
                    alertController.dismiss(animated: true, completion: nil)
                    if (UIApplication.shared.canOpenURL(URL(string:Constants.clientSpecificConstantsStructName.whoLinkToWhatsap)!)) {
                        UIApplication.shared.openURL(URL(string:
                            Constants.clientSpecificConstantsStructName.whoLinkToWhatsap)!)
                    }
                })
                alertController.addAction(startButton)
                self.present(alertController, animated: true, completion: nil)
            case Constants.clientSpecificConstantsStructName.showBookmarkBtn ? 3: 2:
                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "webVC") as! WebViewController
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                break;
            }
        case 1:
            switch indexPath.row {
            case 0:
                let textToShare = Constants.clientSpecificConstantsStructName.downloadAppText
                let objectsToShare = [textToShare] as [Any]
                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                activityVC.view.backgroundColor = UIColor.white
                activityVC.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
                activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
                }
                self.present(activityVC, animated: true, completion: nil)
            case 1:
                performSegue(withIdentifier: "toAboutVC", sender: self)
            default:
                break;
            }
        default: break;
        }
    }
    
    //MARK:- Targets
    @objc func notificationToggleStateChanged(sender: UISwitch) {
        Constants.defaults.set(notificationToggle!.isOn, forKey: "notificationFlag")
        appDelegate.registerForPushNotifications(silent: notificationToggle!.isOn)
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
