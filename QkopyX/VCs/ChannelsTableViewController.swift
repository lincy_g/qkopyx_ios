//
//  ChannelsTableViewController.swift
//  QkopyX
//
//  Created by Lincy George on 11/03/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import UIKit
import Firebase

class ChannelsTableViewController: UITableViewController {

    var networkCallOnProgress: Bool = false
    var arrayOfChannelsAndId: [[String: String]] = []
    var baseView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var descLabel = UILabel()
    var listOfChannels: [[String: [String]]] = []
    var selectedChannels: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = Constants.titleForChannelsListing
        titleBarSetting()
        refreshControlSetting()
        listOfChannels = Constants.defaults.value(forKey: "listOfChannels") as? [[String: [String]]] ?? []
        selectedChannels = Constants.defaults.value(forKey: "selectedChannelIds") as? [String] ?? []
        tableView?.decelerationRate = UIScrollView.DecelerationRate.normal
        baseView.frame = CGRect(x:0, y: navigationController?.navigationBar.frame.height ?? 64, width: self.view.frame.width, height: 100)
        activityIndicator.frame = CGRect(x: (self.view.frame.width/2) - 15, y: 5, width: 40, height: 40)
        activityIndicator.color = UIColor.gray
        baseView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        descLabel.frame = CGRect(x:20, y: 20, width: self.view.frame.width - 40, height: 0)
        descLabel.textAlignment = .center
        descLabel.textColor = UIColor.gray
        descLabel.numberOfLines = 0
        descLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        descLabel.text = ""//Constants.clientSpecificConstantsStructName.channelsUnderClientFetchingText
        descLabel.sizeToFit()
        descLabel.frame = CGRect(x:20, y: 20, width: self.view.frame.width - 40, height: descLabel.frame.height)
        self.baseView.frame.size = CGSize(width: self.baseView.frame.width, height: self.descLabel.frame.height + 40)
        baseView.addSubview(descLabel)
        tableView.tableHeaderView = baseView
        if self.tabBarController == nil {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constants.otherConsts.next, style: UIBarButtonItem.Style.done, target: self, action:  #selector(self.doneBtnTapped))
        } else {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(self.doneBtnTapped))
        }
        if selectedChannels != [] {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
        networkCall(endPart: Constants.apiCalls.getChannels, params: ["userId": Constants.clientSpecificConstantsStructName.clientId])
    }
    //MARK: - setting views
    //set title bar
    func titleBarSetting() {
        self.navigationController?.setDefaults(largeTitlesReq: false, title: Constants.titleForChannelsListing)
    }

    //refreshControl for table
    func refreshControlSetting() {
        self.refreshControl = UIRefreshControl(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        self.refreshControl?.backgroundColor = tableView.backgroundColor
        self.refreshControl?.tintColor = UIColor.gray
        let textWhileRefreshing: String = ""//Constants.clientSpecificConstantsStructName.channelsUnderClientFetchingText
        
        var str: NSAttributedString?
        if #available(iOS 13.0, *) {
            self.refreshControl?.backgroundColor = UIColor.systemGray5
            self.refreshControl?.tintColor = UIColor.label
            str = NSAttributedString(string: textWhileRefreshing, attributes: [NSAttributedString.Key.foregroundColor : UIColor.label])
        } else {
            self.refreshControl?.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
            self.refreshControl?.tintColor = UIColor.white
            str = NSAttributedString(string: textWhileRefreshing, attributes: [NSAttributedString.Key.foregroundColor : UIColor.gray])
        }
        self.refreshControl?.attributedTitle = str
        self.refreshControl?.addTarget(self, action: #selector(self.pulledToRefresh), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            if #available                                                                                                                                                                                                      (iOS 11.0, *) {
                tableView.addSubview(self.refreshControl!)
            } else {
                tableView.refreshControl = self.refreshControl
            }
        } else {
            tableView.addSubview(self.refreshControl!)
        }
    }
    //MARK: - Targets
    //refreshed by pulling the table
    @objc func pulledToRefresh() {
        self.view.endEditing(true)
        self.refreshControl?.backgroundColor = self.tableView.backgroundColor
        if !networkCallOnProgress {
            networkCall(endPart: Constants.apiCalls.getChannels, params: ["userId": Constants.clientSpecificConstantsStructName.clientId])
        } else {
            refreshControl?.endRefreshing()
        }
    }

    
    //MARK: - Network Calls
    func networkCall(endPart: String, params: [String: Any]) {
        networkCallOnProgress = true
        apiCall.handleApiRequest(endPart: endPart, params: params as [String: Any], completion: {[weak self] (responseJson, errorDesc, timeout) in
            guard let strongSelf = self else {
                return
            }
            if responseJson != nil && errorDesc == "" {
                let arr = ((responseJson as? NSDictionary ?? [:])["data"] as? NSDictionary ?? [:])["value"] as? [NSDictionary] ?? []
                
                var tempListOfChannels: [[String: [String]]] = []
                for dict in arr {
                    if let key = dict["_id"] as? String, let name = dict["name"] as? String, let desc = dict["description"] as? String {
                        let element: [String: [String]] = [key: [name, desc]]
                        if tempListOfChannels.filter({$0 == element}).count == 0 {
                            tempListOfChannels.append(element)
                        }
                    }
                }
                self?.listOfChannels = tempListOfChannels
                Constants.defaults.set(tempListOfChannels, forKey: "listOfChannels")
                DispatchQueue.main.async {
                    self?.descLabel.text = ""
                    self?.descLabel.sizeToFit()
                    self?.descLabel.frame = CGRect(x:20, y: 20, width: strongSelf.view.frame.width - 40, height: 0)
                    self?.baseView.frame.size = CGSize(width: strongSelf.baseView.frame.width, height: strongSelf.descLabel.frame.height + 40)
                    self?.activityIndicator.stopAnimating()
                    self?.activityIndicator.removeFromSuperview()
                    self?.tableView.reloadData()
                    self?.networkCallOnProgress = false
                    self?.refreshControl?.endRefreshing()
                }
            } else {
                self?.networkCallOnProgress = false
                DispatchQueue.main.async {
                    self?.refreshControl?.endRefreshing()
                    print(errorDesc)
                    self?.descLabel.text = errorDesc
                    self?.descLabel.sizeToFit()
                    self?.descLabel.frame = CGRect(x:20, y: 20, width: strongSelf.view.frame.width - 40, height: strongSelf.descLabel.frame.height)
                    self?.baseView.frame.size = CGSize(width: strongSelf.baseView.frame.width, height: strongSelf.descLabel.frame.height + 40)
                    self?.activityIndicator.stopAnimating()
                    self?.activityIndicator.removeFromSuperview()
                    self?.refreshControl?.endRefreshing()
                }
            }
        })
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return listOfChannels.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.nameOfIdentifiers.channelCellIdentifier)!
        cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        cell.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        cell.textLabel?.text = listOfChannels[indexPath.row].values.first![0]
        if listOfChannels[indexPath.row].values.first!.count > 1 {
            cell.detailTextLabel?.text = listOfChannels[indexPath.row].values.first![1]
            cell.detailTextLabel?.numberOfLines = 0
        }
        if selectedChannels.contains(listOfChannels[indexPath.row].keys.first!) {
            cell.accessoryType = .checkmark
            cell.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        } else {
            cell.accessoryType = .none
        }
        // Configure the cell...

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let key = listOfChannels[indexPath.row].keys.first {
            if let position = selectedChannels.firstIndex(of: key){
                selectedChannels.remove(at: position)
            } else {
                selectedChannels.append(key)
            }
        }
        tableView.reloadRows(at: [indexPath], with: .automatic)
        if selectedChannels != [] {
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    @objc func doneBtnTapped() {
        let previousSelectedChannels = Constants.defaults.value(forKey: "selectedChannelIds") as? [String] ?? []
        var allSubscribedChannels = Constants.defaults.value(forKey: "allSubscribedChannels") as? [String] ?? []
        if previousSelectedChannels.sorted() != selectedChannels.sorted() {
            let unsubscribeChannels = Array(Set(self.selectedChannels).symmetricDifference(previousSelectedChannels))
            for channel in self.selectedChannels {
                if !allSubscribedChannels.contains(channel) {
                    allSubscribedChannels.append(channel)
                }
                Messaging.messaging().subscribe(toTopic: channel) { error in
                     if let error = error {
                         print(error.localizedDescription)
                     } else {
                        print("Subscribed to \(channel) topic")
                     }
                }
            }
            for channel in unsubscribeChannels {
                if !self.selectedChannels.contains(channel) {
                    if let index = allSubscribedChannels.firstIndex(of: channel) {
                        allSubscribedChannels.remove(at: index)
                    }
                    Messaging.messaging().unsubscribe(fromTopic: channel) { error in
                         if let error = error {
                             print(error.localizedDescription)
                         } else {
                            print("Unsubscribed \(channel) topic")
                         }
                    }
                }
            }
            Constants.defaults.set(allSubscribedChannels, forKey: "allSubscribedChannels")
            Constants.defaults.set(selectedChannels, forKey: "selectedChannelIds")
            if self.tabBarController != nil {
                if let userVc = self.tabBarController?.viewControllers?[1].children[0] as? UsersProfileTableViewController {
                    userVc.changedSelectionOfCategoriesRefreshed = false
                    userVc.networkCall(isRefreshCall: true)
                }
            }
        }
        if self.tabBarController == nil {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! UITabBarController
            vc.selectedIndex = 1
            vc.modalPresentationStyle = .fullScreen
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popToRootViewController(animated: true)
            appDelegate.window?.rootViewController?.present(vc, animated: true, completion: nil)
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        descLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
    }

}
