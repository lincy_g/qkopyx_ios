//
//  AboutViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 24/03/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    
    //outlets
    var versionLabel = UILabel()
    var nameTV = UITextView()
    var imageVw = UIImageView()
    var poweredByLabel = UILabel()
    var qkopyXImageVw = UIImageView()
    
    //MARK: - Default methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setDefaults(largeTitlesReq: false, title: Constants.otherConsts.aboutText)
        
        imageVw.image = Constants.clientSpecificConstantsStructName.showQkopyXLogoInAbout ? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "qkopyXLogo")): UIImage(named: Constants.clientSpecificConstantsStructName.clientLogoName)
        qkopyXImageVw.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "xLogo"))
        let heightReducingNavBar = (self.view.frame.height - 44) as CGFloat
        imageVw.frame = CGRect(x: self.view.frame.width/2 - self.view.frame.width/5, y: heightReducingNavBar/2 - self.view.frame.height/5, width: (self.view.frame.width/5)*2, height: (self.view.frame.width/5)*2)
        nameTV.frame = CGRect(x: 40, y: imageVw.frame.minY + imageVw.frame.height + 20, width: self.view.frame.width - 80, height: 100)
        
        poweredByLabel.frame = CGRect(x: 40, y: nameTV.frame.minY + nameTV.frame.height + 20, width: self.view.frame.width - 80, height: 20)
        qkopyXImageVw.frame = CGRect(x: self.view.frame.width/2 - 50, y: poweredByLabel.frame.minY + poweredByLabel.frame.height + 0, width: 100, height: 30)
        qkopyXImageVw.contentMode = .scaleAspectFit
        versionLabel.frame = CGRect(x: 0, y: self.view.frame.height - 60, width: self.view.frame.width, height: 20)
        nameTV.text = Constants.clientSpecificConstantsStructName.aboutScreenDescription
        versionLabel.text = Constants.otherConsts.version + " " + (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0")
        nameTV.textAlignment = .center
        poweredByLabel.text = Constants.poweredByText
        poweredByLabel.textAlignment = .center
        nameTV.isScrollEnabled = false
        versionLabel.textAlignment = .center
        nameTV.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        versionLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        
        self.view.addSubview(imageVw)
        self.view.addSubview(versionLabel)
        self.view.addSubview(poweredByLabel)
        self.view.addSubview(qkopyXImageVw)
        self.view.addSubview(nameTV)
        imageVw.backgroundColor = .clear
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
            self.nameTV.textColor = .label
            versionLabel.textColor = .label
        } else {
            self.view.backgroundColor = .white
            nameTV.textColor = UIColor.black
            versionLabel.textColor = UIColor.black
        }
        // Do any additional setup after loading the view.
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
