//
//  ViewController.swift
//  QkopyX
//
//  Created by Lincy George on 29/01/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import UIKit

class GetStartedViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var logoLabel: UILabel!
    
    @IBOutlet weak var qkopySloganTextView: UITextView!
    
    @IBOutlet weak var getStartedBtn: UIButton!
    
    @IBOutlet weak var fromWhomTextLabel: UILabel!
    @IBOutlet weak var policyTextView: UITextView!
    @IBAction func getStartedBtnAction(_ sender: Any) {
        let selectedChannels = Constants.defaults.value(forKey: "selectedChannelIds") as? [String]
        let pincodeSet = Constants.defaults.value(forKey: "userSetPincode") as? String
        if Constants.clientSpecificConstantsStructName.pickFromPincode == true && (pincodeSet == nil || pincodeSet == "") {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "pincodeVC") as! PincodeSetterViewController
            let navController = UINavigationController(rootViewController: vc)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        } else if (Constants.clientSpecificConstantsStructName.hasChannels == true) && (selectedChannels == nil || selectedChannels == []) {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "channelsTVC") as! ChannelsTableViewController
            let navController = UINavigationController(rootViewController: vc)
            navController.modalPresentationStyle = .fullScreen
            self.present(navController, animated: true, completion: nil)
        } else {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! UITabBarController
            vc.selectedIndex = 1
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImageView.image = UIImage(named: Constants.clientSpecificConstantsStructName.clientLogoName)!
        getStartedBtn.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))
        getStartedBtn.setTitle(Constants.otherConsts.getStarted, for: .normal)
        getStartedBtn.setTitleColor(.white, for: .normal)
        fromWhomTextLabel.text = Constants.clientSpecificConstantsStructName.fromWhomText
        getStartedBtn.layer.cornerRadius = 10
        qkopySloganTextView.text = Constants.appSlogan
        qkopySloganTextView.isScrollEnabled = false
        logoLabel.text = Constants.clientSpecificConstantsStructName.appName
        
        let privacyPolicyLink = Constants.otherLinks.qkopyPrivacyPolicyLink
        let termsText = Constants.otherConsts.termsAndConditionsPreStatement + Constants.otherConsts.privacyPolicyText
        let str = NSMutableAttributedString(string: termsText)
        str.addAttribute(NSAttributedString.Key.font, value: UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0) , range: NSRange(location: 0, length: termsText.count - 1))
        str.addAttribute(NSAttributedString.Key.link, value: privacyPolicyLink, range: NSRange(location: termsText.count - Constants.otherConsts.privacyPolicyText.count, length: Constants.otherConsts.privacyPolicyText.count))
        if #available(iOS 13.0, *) {
            str.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.label, range: NSRange(location: 0, length: termsText.count - 1))
        } else {
            str.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.gray, range: NSRange(location: 0, length: termsText.count - 1))
        }
        let linkAttributes: [String: Any] = [
            NSAttributedString.Key.foregroundColor.rawValue: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode")),
            NSAttributedString.Key.underlineStyle.rawValue: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.font.rawValue: UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0) ]
        // textView is a UITextView
        policyTextView.linkTextAttributes = ControllerUtils.shared.convertToOptionalNSAttributedStringKeyDictionary(linkAttributes)
        policyTextView.attributedText = str
        policyTextView.isEditable = false
        policyTextView.textAlignment = .center
        policyTextView.isScrollEnabled = false
        // Do any additional setup after loading the view.
    }


}

