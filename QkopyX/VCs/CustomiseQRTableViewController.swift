//
//  CustomiseQRTableViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 16/07/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import UIKit
//import FirebaseDynamicLinks

class CustomiseQRTableViewController: UITableViewController {

    var showCells = false
    //Variables
    var myName: String?
    var qkopyIDText: String?
    var number: String?
    var titleName: String?
    var qrcodeImage: CIImage?
    var checkMarkArray: [Bool] = [true, false]
    var imgVw = UIImageView()
    var contactsQR = false
    var utilsClass = ControllerUtils()
    var arrayOfString: [String] = []
    
    //MARK: - Default functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        if let fromDefaults = Constants.defaults.value(forKey: "checkMarkArrayForQR") as? [Bool] {
            checkMarkArray =  fromDefaults
        }
        self.navigationController?.setDefaults(largeTitlesReq: false, title: titleName ?? "")
        if !contactsQR {
            let qkopyId = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.qkopyId
            if qkopyId != nil && qkopyId != "" {
                showCells = true
                createHeaderView()
            }
        }
        self.tableView.isScrollEnabled = false
        createQR()
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.inputAccessoryView?.isHidden = false
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var inputAccessoryView: UIView? {
        return nil
    }
    
    //MARK: - General Functions
    func createHeaderView() {
        let headerTV = UITextView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 100))
        headerTV.textAlignment = .center
        if #available(iOS 13.0, *) {
            headerTV.textColor = .label
        } else {
            headerTV.textColor = .darkGray
        }
        headerTV.backgroundColor = UIColor.clear
        headerTV.textContainerInset = UIEdgeInsets.init(top: 20, left: 20, bottom: 20, right: 20)
        headerTV.isScrollEnabled = false
        headerTV.isUserInteractionEnabled = false
        headerTV.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        headerTV.text = Constants.sectionOrPageTitles.customiseQRDescription
        headerTV.sizeToFit()
        self.tableView.tableHeaderView = headerTV
        //self.tableView.reloadData()
    }

    func createQR() {
        /* deep link ==> utilsClass.showProgressView(self.view, center: CGPoint(x: self.view.center.x, y: self.view.center.y - (self.navigationController?.navigationBar.frame.origin.y ?? 20)))*/
        let dataCode = myName!.data(using: String.Encoding.utf8)!
        var nameString = myName!
        if tableView.tableFooterView == nil {
            tableView.tableFooterView = UIView()
        }
        if tableView.tableFooterView?.subviews.contains(imgVw) == false {
            tableView.tableFooterView?.addSubview(imgVw)
        }
        var customiseQRArray = [true, false]
        if !contactsQR {
            customiseQRArray = Constants.defaults.value(forKey: "checkMarkArrayForQR") as? [Bool] ?? [true, false]
        } else {
            customiseQRArray = [true, true]
        }
        if let name = String(data: dataCode, encoding: String.Encoding.nonLossyASCII) {
            if name == "" {
                nameString = myName!
            } else {
                nameString = name
            }
        }
        let tableOccupiedHeight = (self.tableView.visibleCells.last ?? self.tableView).frame.origin.y + (self.tableView.visibleCells.last?.frame.height ?? 0)
        let gapBetweenCellAndFooter = self.tableView.tableFooterView!.frame.origin.y - tableOccupiedHeight
        let sidelengthOfQR = self.view.frame.width/1.5
        var y: CGFloat = 0
        if showCells {
            y = (self.tableView.frame.width - sidelengthOfQR)/2
        } else {
            y = (self.view.frame.height - sidelengthOfQR - gapBetweenCellAndFooter)/2 - 64
        }
        //nameString = nameString.stringByRemovingEmoji()
        if number != nil || qkopyIDText != nil {
            imgVw.frame = CGRect(x: (self.tableView.frame.width - sidelengthOfQR)/2, y: y, width: sidelengthOfQR, height: sidelengthOfQR)
            
            //
            //            text = nameString + "," + myNumber
            // commented below lines: once userid is implemented uncomment the below and comment the above line
            arrayOfString = []
            arrayOfString.append(nameString)
            if qkopyIDText != nil && qkopyIDText != "" && customiseQRArray[1] {
                arrayOfString.append(qkopyIDText! + "@qkopy.com")
            }
            if number != "" && number != nil && customiseQRArray[0] {
                arrayOfString.append(number!)
            }
            let text = arrayOfString.joined(separator: ",")
            let data = text.data(using: String.Encoding.utf8, allowLossyConversion: false)
            self.createQrFromUrl(dataSent: data)
            /* deep link ==>
            guard let link = URL(string: "http://qkopy.com/savecontact")?.appendingPathComponent(arrayOfString.joined(separator: ",")) else { return }
            let dynamicLinksDomain = "qkopy.page.link"
            let linkBuilder = DynamicLinkComponents(link: link, domain: dynamicLinksDomain)
            linkBuilder.iOSParameters = DynamicLinkIOSParameters(bundleID: "com.qkopy.qkopy")
            linkBuilder.iOSParameters?.appStoreID = Constants.appleStoreConsts.iTunesItemIdentifier
            linkBuilder.androidParameters = DynamicLinkAndroidParameters(packageName: "com.museon.qkopy")
            linkBuilder.options = DynamicLinkComponentsOptions()
            linkBuilder.options?.pathLength = .short
            linkBuilder.shorten() { url, warnings, error in
                guard let url = url, error == nil else {
                    guard let longDynamicLink = linkBuilder.url else { return }
                    self.createQrFromUrl(link: longDynamicLink)
                    return
                }
                self.createQrFromUrl(link: url)
            }*/
        } else {
            DispatchQueue.main.async(execute: { () -> Void in
                self.navigationController?.popViewController(animated: true)
                //let message = Constants.otherConsts.profileSettingText
                let message = Constants.alertMessages.someErrorText
                let alertController = UIAlertController(title: Constants.alertTitles.error, message: message, preferredStyle: .alert)
                let ok = UIAlertAction(title: Constants.otherConsts.okText, style:.cancel, handler: {(action) -> Void in
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(ok)
                self.present(alertController, animated: true, completion: nil)
                Spinner.stop()
            })
        }
    }
    
    func createQrFromUrl(/* deep link ==> link: URL*/dataSent: Data?) {
        /* -- deep link ==>
         let lastComponent = link.description.replacingOccurrences(of: Constants.otherLinks.firebaseDynamicLinkDomain, with: "")
        let newLink = URL(string: Constants.otherLinks.qkopyDomainForDynamicLink + lastComponent)?.appendingPathComponent(arrayOfString.joined(separator: ","))
        if let data = newLink?.absoluteString.data(using: String.Encoding.utf8, allowLossyConversion: false) {*/
        if let data = dataSent {
            let filter = CIFilter(name: "CIQRCodeGenerator")
        
            filter?.setValue(data, forKey: "inputMessage")
            filter?.setValue("H", forKey: "inputCorrectionLevel")
            qrcodeImage = filter?.outputImage
            let scaleX = imgVw.frame.size.width / qrcodeImage!.extent.size.width
            let scaleY = imgVw.frame.size.height / qrcodeImage!.extent.size.height
        
            let transformedImage = qrcodeImage!.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
            imgVw.image = UIImage(ciImage: transformedImage)
            let img = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "black_white_Qkopy"))
            let frontImgVw = UIImageView(frame: CGRect(x: imgVw.frame.width/2 - (imgVw.frame.width/7), y: imgVw.frame.height/2 - (imgVw.frame.height/7), width: imgVw.frame.width/3.5, height: imgVw.frame.width/3.5))
            //frontImgVw.layer.cornerRadius = frontImgVw.frame.size.width/2
            //frontImgVw.clipsToBounds = true
            frontImgVw.image = img
            imgVw.addSubview(frontImgVw)
        }
        /* deep link ==> Spinner.stop()*/
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if showCells {
            return Constants.arrays.titleArrayForQROptions.count
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.nameOfIdentifiers.customisedQRCellIdentifier, for: indexPath)
        cell.textLabel?.text = Constants.arrays.titleArrayForQROptions[indexPath.row]
        if checkMarkArray.count > 0 {
            if checkMarkArray[indexPath.row] {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        } else {
            cell.accessoryType = .checkmark
        }
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if checkMarkArray.count >= indexPath.row {
            checkMarkArray[indexPath.row] = !checkMarkArray[indexPath.row]
        }
        if !checkMarkArray.contains(true) {
            checkMarkArray[indexPath.row] = !checkMarkArray[indexPath.row]
        }
        Constants.defaults.set(checkMarkArray, forKey: "checkMarkArrayForQR")
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.createQR()
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
}
extension Character {
    fileprivate func isEmoji() -> Bool {
        return Character(UnicodeScalar(UInt32(0x1d000))!) <= self && self <= Character(UnicodeScalar(UInt32(0x1f77f))!)
            || Character(UnicodeScalar(UInt32(0x2100))!) <= self && self <= Character(UnicodeScalar(UInt32(0x26ff))!)
    }
}

extension String {
    func stringByRemovingEmoji() -> String {
        return String(self.filter { !$0.isEmoji() })
    }
}
