//
//  PincodeSetterViewController.swift
//  QkopyX
//
//  Created by Lincy George on 27/05/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import UIKit
import GooglePlaces
import Firebase
class PincodeSetterViewController: UIViewController, UITextFieldDelegate {
    //variables
    var pinEntered = ["","","","","",""]
    //outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var errorMessagelabel: UILabel!
    @IBOutlet weak var unknownPincodeOutlet: UIButton!
    @IBOutlet weak var sendCodeOutlet: UIButton!
    @IBOutlet weak var pinFieldOne: UITextField!
    @IBOutlet weak var pinFieldTwo: UITextField!
    @IBOutlet weak var pinFieldThree: UITextField!
    @IBOutlet weak var pinFieldFour: UITextField!
    @IBOutlet weak var pinFieldFive: UITextField!
    @IBOutlet weak var pinFieldSix: UITextField!
    
    @IBAction func unknownPincodeAction(_ sender: Any) {
        for subview in self.view.subviews {
            if subview.isKind(of: UITextField.self) {
                (subview as! UITextField).text = ""
            }
        }
        openAutoCompleteVc()
    }
    
    @IBAction func sendCodeAction(_ sender: Any) {
        let pincode = pinEntered.joined()
        errorMessagelabel.text = ""
        if pincode.count == 6 {
            if let pincodeAsInt = Int(pincode) {
                apiCall.handleApiRequest(endPart: Constants.apiCalls.accountsToFollow, params: ["pinCode" : pincodeAsInt], completion: {(responseJson, errorDesc, timeout) in
                    if responseJson != nil && errorDesc == "" {
                        let resultDict = responseJson as? NSDictionary ?? [:]
                        let dicts = (resultDict["data"]! as? NSDictionary ?? [:])["value"] as? [NSMutableDictionary] ?? []
                        if dicts.count > 0 {
                            DispatchQueue.global(qos: .background).async {
                                self.subscribeToAllPincodeRelatedChannels(pincode: pincode, dicts: dicts)
                            }
                            let selectedChannels = Constants.defaults.value(forKey: "selectedChannelIds") as? [String]
                            DispatchQueue.main.async {
                                if (Constants.clientSpecificConstantsStructName.hasChannels == true) && (selectedChannels == nil || selectedChannels == []) {
                                     let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "channelsTVC") as! ChannelsTableViewController
                                     let navController = UINavigationController(rootViewController: vc)
                                     navController.modalPresentationStyle = .fullScreen
                                     self.dismiss(animated: true, completion: nil)
                                     self.navigationController?.popToRootViewController(animated: true)
                                     appDelegate.window?.rootViewController?.present(navController, animated: true, completion: nil)
                                 } else {
                                    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabbarVC") as! UITabBarController
                                    vc.selectedIndex = 1
                                    vc.modalPresentationStyle = .fullScreen
                                    self.present(vc, animated: true, completion: nil)
                                }
                            }
                        } else {
                            self.refreshAfterError(message: errorDesc != "" ? errorDesc: Constants.errorMessages.pincodeUnavailable)
                        }
                    } else {
                        self.refreshAfterError(message: errorDesc != "" ? errorDesc: Constants.alertMessages.someErrorText)
                    }
                })
            } else {
                self.refreshAfterError(message: Constants.errorsWhileAuthenticating.codeInvalid)
            }
        } else {
           self.refreshAfterError(message: Constants.errorsWhileAuthenticating.otpDigitMissingError)
           return
        }
    }
    func openAutoCompleteVc() {
        refreshAfterError(message: "")
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        if #available(iOS 13.0, *) {
            autocompleteController.view.backgroundColor = .systemBackground
            autocompleteController.tableCellBackgroundColor = .systemBackground
        }
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt(GMSPlaceField.name.rawValue) |
          UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue))!
        autocompleteController.placeFields = fields
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .region
        filter.country = "IN"
        autocompleteController.autocompleteFilter = filter
        autocompleteController.modalPresentationStyle = .fullScreen
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func subscribeToAllPincodeRelatedChannels(pincode: String, dicts: [NSMutableDictionary]) {
        var allSubscribedChannels: [String] = []
        var allUserIds: [String] = []
        for dict in dicts {
            if let id = dict["_id"] as? String {
                var createdTime = dict["createdDate"] as? String
                if createdTime != "" && createdTime != nil {
                    let dateFromString = createdTime!.dateFromISO8601
                    if dateFromString != nil {
                        createdTime = appDelegate.joinedDateString(date: dateFromString!)
                    }
                }
                savedContactRefDict[id] = DynamicKeyObject(minimalDetails: MinimalContactDetails(name: dict["name"] as? String ?? "", imageName: dict["profile_pic"] as? String ?? "", profileVerified: dict["profileVerified"] as? Bool ?? false, isPremium: dict["isPremium"] as? Bool ?? false, muted: false, mutedTill: "", number: dict["number"] as? String ?? "", qkopyId: dict["userName"] as? String ?? "", actualProfileName:  dict["name"] as? String ?? "", about: dict["about"] as? String ?? "", createdDate: createdTime, location: dict["location"] as? [String: Any]))
                let _ = Constants.sharedUtils.getImage(isProfileImg: true, endPath: dict["profile_pic"] as? String ?? "")
                if !allUserIds.contains(id) {
                    allUserIds.append(id)
                }
                if id != Constants.clientSpecificConstantsStructName.clientId {
                    var idsToSubscribe: [String] = []
                    if let channels = dict["channels"] as? [NSDictionary] {
                        for channelDict in channels {
                            if let channelId = channelDict["_id"] as? String, !idsToSubscribe.contains(channelId) {
                                idsToSubscribe.append(channelId)
                            }
                        }
                    }
                    for channelId in idsToSubscribe {
                        Messaging.messaging().subscribe(toTopic: channelId) { error in
                             if let error = error {
                                 print(error.localizedDescription)
                             } else {
                                print("Subscribed to \(channelId) topic")
                             }
                        }
                    }
                    if allSubscribedChannels == [] {
                        allSubscribedChannels = idsToSubscribe
                    } else {
                        allSubscribedChannels = allSubscribedChannels + idsToSubscribe
                    }
                }
            }
            Constants.defaults.set(allUserIds, forKey: "allUserIds")
            Constants.defaults.set(allSubscribedChannels, forKey: "allSubscribedChannels")
            Constants.sharedUtils.cacheDictionaryToLocal(with: savedContactRefDict, to: "savedContacts")
        }
        Constants.defaults.set(pincode, forKey: "userSetPincode")
    }
    //MARK: - Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.errorMessagelabel.numberOfLines = 0
        errorMessagelabel.text = ""
        errorMessagelabel.sizeToFit()
        errorMessagelabel.textAlignment = .left
        self.navigationItem.title = Constants.setPincodeText
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
        } else {
            self.view.backgroundColor = .white
        }
        pinFieldOne.delegate = self
        pinFieldTwo.delegate = self
        pinFieldThree.delegate = self
        pinFieldFour.delegate = self
        pinFieldFive.delegate = self
        pinFieldSix.delegate = self
        
        pinFieldOne.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        pinFieldTwo.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        pinFieldThree.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        pinFieldFour.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        pinFieldFive.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        pinFieldSix.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        pinFieldOne.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        pinFieldTwo.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        pinFieldThree.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        pinFieldFour.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        pinFieldFive.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        pinFieldSix.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
    //    settingFonts()
    //    settingFrames()
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
        // Do any additional setup after loading the view.
        
        addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default

        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))

        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        pinFieldOne.inputAccessoryView = doneToolbar
        pinFieldTwo.inputAccessoryView = doneToolbar
        pinFieldThree.inputAccessoryView = doneToolbar
        pinFieldFour.inputAccessoryView = doneToolbar
        pinFieldFive.inputAccessoryView = doneToolbar
        pinFieldSix.inputAccessoryView = doneToolbar
    }

    @objc func doneButtonAction(){
        pinFieldOne.resignFirstResponder()
        pinFieldTwo.resignFirstResponder()
        pinFieldThree.resignFirstResponder()
        pinFieldFour.resignFirstResponder()
        pinFieldFive.resignFirstResponder()
        pinFieldSix.resignFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        pinFieldOne.resignFirstResponder()
        pinFieldTwo.resignFirstResponder()
        pinFieldThree.resignFirstResponder()
        pinFieldFour.resignFirstResponder()
        pinFieldFive.resignFirstResponder()
        pinFieldSix.resignFirstResponder()
        //*** to uncomment
        /*
        if !movingToTabbarVC {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            movingToTabbarVC = false
        }
     */
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//        if pinFieldSix.text != "" {
//            pinFieldSix.becomeFirstResponder()
//        } else {
//            pinFieldOne.becomeFirstResponder()
//        }
//    }
    //MARK: - Setting Views
    func settingFrames() {
        titleLabel.frame = CGRect(x: 0, y: 80, width: self.view.frame.width, height: 30)
        pinFieldOne.frame = CGRect(x: self.view.frame.width/2 - 145 , y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        pinFieldTwo.frame = CGRect(x: pinFieldOne.frame.minX + pinFieldOne.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        pinFieldThree.frame = CGRect(x: pinFieldTwo.frame.minX + pinFieldTwo.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        pinFieldFour.frame = CGRect(x: pinFieldThree.frame.minX + pinFieldThree.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        pinFieldFive.frame = CGRect(x: pinFieldFour.frame.minX + pinFieldFour.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        pinFieldSix.frame = CGRect(x: pinFieldFive.frame.minX + pinFieldFive.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        self.errorMessagelabel.numberOfLines = 0
        errorMessagelabel.sizeToFit()
        var height = errorMessagelabel.frame.height
        if errorMessagelabel.frame.height == 0 {
            height = 10
        }
        errorMessagelabel.frame = CGRect(x: pinFieldOne.frame.origin.x, y:  pinFieldOne.frame.minY + pinFieldOne.frame.height + 10, width: pinFieldSix.frame.minX + pinFieldSix.frame.width - pinFieldOne.frame.minX , height: height)
        unknownPincodeOutlet.frame = CGRect(x: errorMessagelabel.frame.origin.x, y:  errorMessagelabel.frame.minY + errorMessagelabel.frame.height + 10, width: errorMessagelabel.frame.width, height: 40)
        sendCodeOutlet.frame = CGRect(x: errorMessagelabel.frame.origin.x, y:  unknownPincodeOutlet.frame.origin.y + unknownPincodeOutlet.frame.height + 10, width: errorMessagelabel.frame.width, height: 40)
    }
    //MARK: - Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.text = "\u{200B}"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 2
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if !newString.contains("\u{200B}") {
            maxLength = 1
        }
        return newString.length <= maxLength
    }
    //MARK: - Targets
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if text!.count == 2 {
            switch textField{
            case pinFieldOne:
                pinEntered[0] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                pinFieldTwo.becomeFirstResponder()
            case pinFieldTwo:
                pinEntered[1] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                pinFieldThree.becomeFirstResponder()
            case pinFieldThree:
                pinEntered[2] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                pinFieldFour.becomeFirstResponder()
            case pinFieldFour:
                pinEntered[3] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                pinFieldFive.becomeFirstResponder()
            case pinFieldFive:
                pinEntered[4] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                pinFieldSix.becomeFirstResponder()
            case pinFieldSix:
                pinEntered[5] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                pinFieldSix.becomeFirstResponder()
            default:
                break
            }
        } else if (text!.count == 0 || text!.count == 1) {
            switch textField{
            case pinFieldOne:
                pinEntered[0] = ""
                pinFieldOne.text = "\u{200B}"
                pinFieldOne.becomeFirstResponder()
            case pinFieldTwo:
                pinEntered[1] = ""
                pinFieldOne.becomeFirstResponder()
            case pinFieldThree:
                pinEntered[2] = ""
                pinFieldTwo.becomeFirstResponder()
            case pinFieldFour:
                pinEntered[3] = ""
                pinFieldThree.becomeFirstResponder()
            case pinFieldFive:
                pinEntered[4] = ""
                pinFieldFour.becomeFirstResponder()
            case pinFieldSix:
                pinEntered[5] = ""
                pinFieldFive.becomeFirstResponder()
            default:
                break
            }
        }
    }
    func settingFonts() {
        titleLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        errorMessagelabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0)
        unknownPincodeOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        sendCodeOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        for sub in self.view.subviews {
            if let field = sub as? UITextField {
                field.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
            }
        }
    }
    
    func refreshAfterError(message: String) {
        DispatchQueue.main.async {
            Spinner.stop()
            self.errorMessagelabel.numberOfLines = 0
            self.sendCodeOutlet.isUserInteractionEnabled = true
            self.unknownPincodeOutlet.isUserInteractionEnabled = true
            self.errorMessagelabel.text = message
            self.errorMessagelabel.sizeToFit()
            self.settingFrames()
        }
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
        settingFrames()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PincodeSetterViewController: GMSAutocompleteViewControllerDelegate {

  // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let lat = place.coordinate.latitude
        let long = place.coordinate.longitude
        let location = CLLocation(latitude: lat, longitude: long)
        Spinner.start()
        self.sendCodeOutlet.isUserInteractionEnabled = false
        self.unknownPincodeOutlet.isUserInteractionEnabled = false
        DispatchQueue.main.async {
            viewController.dismiss(animated: true, completion: {
                let geocoder = CLGeocoder()
                geocoder.reverseGeocodeLocation(location) { (placemarks, error) in
                    if let error = error {
                        self.refreshAfterError(message: error.localizedDescription.components(separatedBy: ". ").count > 0 ?  error.localizedDescription.components(separatedBy: ". ")[0]: error.localizedDescription)
                        self.pinFieldSix.becomeFirstResponder()
                    } else if let placemark = placemarks?[0], let postalCode = placemark.postalCode {
                        let arr = postalCode.map { String($0) }
                        self.pinEntered = arr
                        self.pinFieldOne.text = arr[0]
                        self.pinFieldTwo.text = arr[1]
                        self.pinFieldThree.text = arr[2]
                        self.pinFieldFour.text = arr[3]
                        self.pinFieldFive.text = arr[4]
                        self.pinFieldSix.text = arr[5]
                        self.pinFieldSix.becomeFirstResponder()
                    }
                    Spinner.stop()
                    self.sendCodeOutlet.isUserInteractionEnabled = true
                    self.unknownPincodeOutlet.isUserInteractionEnabled = true
                }
            })
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        refreshAfterError(message: error.localizedDescription)
    }

    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }

    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }

    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
