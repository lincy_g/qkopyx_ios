//
//  UsersProfileTableViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 21/02/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
import MapKit
import CoreData
import URLEmbeddedView
import youtube_ios_player_helper
//import Firebase
//import FirebaseAnalytics

class UsersProfileTableViewController: UITableViewController, statusDelegateForChevron, showMoreShowLessDelegate, openImageInFullScreenDelegate, setLikeStatusInParentVCDelegate, openDocInWebviewDelegate, URLSessionDownloadDelegate, singlePostVcDelegate, gotoUserProfileDelegate, UITabBarControllerDelegate, goToChatDelegate, bookmarkDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    let totalHeightToLeaveForTitleAndBottomBarInCell = CGFloat(Constants.feedCellTitleBarHeight + Constants.feedCellBottomBarHeight)
    let downloadService = DownloadService()
    // Create downloadsSession here, to set self as delegate
    lazy var downloadsSession: URLSession = {
        //    let configuration = URLSessionConfiguration.default
        let configuration = URLSessionConfiguration.background(withIdentifier: "userBgSessionConfiguration")
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    let fixedHeight = Constants.defaults.value(forKey: "sizeOfLimitedCharacters") as? CGFloat ?? 200
    private let memoryCache: NSCache<NSString, UIImage> = {
        let cache = NSCache<NSString, UIImage>()
        cache.countLimit = 30
        return cache
    }()
    //Variables
    var bgImageView : UIImageView?
    var noUpdatesLabel: UILabel?
    var id = Constants.clientSpecificConstantsStructName.clientId
    var arrayForFeed: [PostObject] = []
    var scrollViewLastContentOffset: CGFloat = 0
    var sameTabTapped = false
    var loadedFromServer: Bool = false
    var loadingFirst: Bool?
    var endOfPosts : Bool?
    var networkCallOnProgress = false
    var firstTimeFeedLoad: Bool = true
    var heightArr: [CGFloat] = []
    var activityIndicator: UIActivityIndicatorView?
    var viewOverlay: UIView?
    var cellShowMore: [String: Bool] = [:]
    var cellIsDownloading: [String: Bool] = [:]
    var titleButtonForNav: UIButton?
    var userImageCropped: UIImage?
    var phoneNumber: String?
    var savedTabbar: UITabBarController?
    var startIndex: String?
    var verifiedImageView: UIImageView?
    var premiumImageView: UIImageView?
    var muteUnmuteButton:UIBarButtonItem?
    var muteButtonImage: UIImage?
    var unmuteButtonImage: UIImage?
    var utilsClass = ControllerUtils()
    var isUnsavedContact = false
    var goToMeVC: Bool = false
    var changedSelectionOfCategoriesRefreshed: Bool = false
    var showOnlyBookmarkedItems = false
    var pinnedPostsArr: [PostObject] = []
    //Outlets
    
    //MARK: - Default view methods
    override func viewDidLoad() {
        super.viewDidLoad()
        downloadService.downloadsSession = downloadsSession
//        var name = Constants.clientSpecificConstantsStructName.appName
//        if name.contains("\n\n") {
//            name = name.components(separatedBy: "\n\n")[0]
//        }
//        if name.count > 20 {
//            name = name.padding(toLength: 20, withPad: name, startingAt: 0)
//            name = name + "..."
//        }
        self.navigationController?.setDefaults(largeTitlesReq: false, title: showOnlyBookmarkedItems ? Constants.bookmarksText: Constants.clientSpecificConstantsStructName.appName)

        
        //loadTitleViewForNav()
        if #available(iOS 11.0, *) {
        } else {
            self.navigationController?.navigationBar.frame = CGRect(x: 0, y: UIApplication.shared.statusBarFrame.height, width: self.view.frame.width, height: self.navigationController!.navigationBar.frame.height)
        }
        self.navigationController?.setDefaults(largeTitlesReq: false)
        userTableViewSetting()
        refreshControlSetting()
        
        if showOnlyBookmarkedItems == false {
            if Constants.clientSpecificConstantsStructName.helplineNumber != "" {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constants.helplineText, style: UIBarButtonItem.Style.done, target: self, action: #selector(self.callHelpline))
            }
            if Constants.clientSpecificConstantsStructName.showChatAndLikeBtns == true {
                self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "replyNavIcon"), style: .done, target: self, action: #selector(self.chatBtnTapped))
                    //UIBarButtonItem(title: Constants.messageText, style: UIBarButtonItem.Style.done, target: self, action: #selector(self.chatBtnTapped))
            }
        }
        //rightBarButtonsSettingsIfSavedContact()
        tableView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(UsersProfileTableViewController.checkForReachability), name: ReachabilityChangedNotification, object: nil)
        DispatchQueue.main.async {
            UIApplication.shared.applicationIconBadgeNumber = 0
        }
        DispatchQueue.global(qos: .background).async {
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().removeAllDeliveredNotifications()
            }
        }
        do {
            try appDelegate.reachability.startNotifier()
        } catch {
            
        }
        tableView.register(UINib(nibName: "statusFeed", bundle: nil), forCellReuseIdentifier: Constants.nameOfIdentifiers.feedCellIdentifier)
        let savedDetails = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails
        let profileImage = Constants.sharedUtils.getImage(isProfileImg: true, endPath: savedDetails?.imageName ?? "")
        self.userImageCropped = appDelegate.cropImage(img: profileImage)
        dataLoadFromDefaults(firstLoad: true, timeout: false)
        OGDataProvider.shared.updateInterval = 10.days
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
        memoryCache.removeAllObjects()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let profileImage = Constants.sharedUtils.getImage(isProfileImg: true, endPath: savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.imageName ?? "")
        self.userImageCropped = appDelegate.cropImage(img: profileImage)
        sameTabTapped = true
        DispatchQueue.main.async {
            self.tabBarController?.delegate = self
            self.tabBarController?.tabBar.isHidden = false
            if Constants.defaults.value(forKey: "shouldReloadQueue") as? Bool == true {
                Constants.defaults.set(false, forKey: "shouldReloadQueue")
                self.tableView.reloadData()
                self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
            }
            if !self.showOnlyBookmarkedItems {
                let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
                self.tableView.contentInset = adjustForTabbarInsets
                self.tableView.scrollIndicatorInsets = adjustForTabbarInsets
            }
        }
        if appDelegate.notificationTapped == true {
            appDelegate.notificationTapped = false
            DispatchQueue.main.async {
                self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
            }
        }
        if !loadedFromServer && appDelegate.reachability.isReachable && loadingFirst != nil && !self.networkCallOnProgress {
            self.networkCall(isRefreshCall: false)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        loadingFirst = false
        if goToMeVC && self.savedTabbar != nil {
            savedTabbar?.selectedIndex = 2
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.delegate = nil
        sameTabTapped = false
    }
    //MARK: - Setting views
    //Basic field setting
    func setProfileFields(name: String, mob: String, imageName: String, arr: [NSMutableDictionary], arePinnedPosts: Bool) {
        DispatchQueue.main.async(execute: { () -> Void in
            self.tableView.tableFooterView = nil
            Spinner.stop()
            var image: UIImage?
            if !self.showOnlyBookmarkedItems && !arePinnedPosts {
                if imageName == "" {
                    image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))!
                } else {
                    image = Constants.sharedUtils.getImage(isProfileImg: true, endPath: imageName)
                }
                self.userImageCropped = appDelegate.cropImage(img: image!)
            }
        })
        if self.startIndex == "0" && !arePinnedPosts {
            self.firstTimeFeedLoad = false
        }
//        let allSubscribedChannels = Constants.defaults.value(forKey: "allSubscribedChannels") as? [String]
        if arr.count != 0 {
            var i = 0
            var tempArr: [PostObject] = []
            for feed in arr {
                let feed = Feed(feed: feed)
//                var canAddThisFeed = true
//                if Constants.clientSpecificConstantsStructName.pickFromPincode {
//                    if let common = allSubscribedChannels?.filter({ feed.channelList.contains($0) }) {
//                        if common.count == 0 {
//                            canAddThisFeed = false
//                        }
//                    }
//                }
//                if canAddThisFeed {
                    var postFileName: String?
                    if feed.attachments.count > 0 {
                        for attachment in feed.attachments {
                            if attachment["type"] as? String == "image" {
                                postFileName = saveImage(postFileUrlString: feed.attachments[0]["link"] as? String ?? "", lastUpdated: feed.lastUpdatedDate)
                            }
                            if (attachment["type"] as? String == "doc") {
                                if let thumbnailLink = attachment["thumbnail"] as? String {
                                    let _ = saveImage(postFileUrlString: thumbnailLink, lastUpdated: feed.lastUpdatedDate)
                                }
                            }
                        }
                    } else if feed.singlePostImageName != "" {
                        postFileName = saveImage(postFileUrlString: feed.singlePostImageName, lastUpdated: feed.lastUpdatedDate)
                    }
                    let dict = PostObject(desc: feed.desc, location: feed.location, time: feed.time, pid: feed.pid, privacy: feed.privacy, showPreview: feed.showPreview, colorCode: feed.colorCode, postFileName: postFileName ?? "", likesArray: feed.likesArray, liked: feed.liked, attachments: feed.attachments, id: feed.id, channelList: feed.channelList)
                    var loadedTableOnce = false
                    if startIndex == "0" || arePinnedPosts {
                        tempArr.append(dict)
                    } else if self.arrayForFeed.filter({ $0.pid == dict.pid }).count == 0 {
                        self.arrayForFeed.append(dict)
                    }
                    if i == arr.count - 1 && !loadedTableOnce {
                        DispatchQueue.main.async(execute: { () -> Void in
                            if arePinnedPosts {
                                Constants.sharedUtils.cacheDataToLocal(with: tempArr, to: "pinnedPostsDict")
                                self.loadCollectionViewInHeader()
                            } else {
                                if self.startIndex == "0" {
                                    self.arrayForFeed = []
                                    self.heightArr = []
                                    self.cellShowMore = [:]
                                    self.tableView.reloadData()
                                    self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
                                    self.arrayForFeed = tempArr
                                }
                                self.tableView.reloadData()
                                self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
                                Constants.sharedUtils.cacheDataToLocal(with: self.arrayForFeed, to: self.showOnlyBookmarkedItems == true ? "bookmarkedDict": self.id)
                                if self.tableView.tableFooterView != nil {
                                    self.tableView.tableFooterView = nil
                                }
                            }
                            loadedTableOnce = true
                            Spinner.stop()
                            self.refreshControl?.endRefreshing()
                            self.loaderInFooter(show: false)
                        })
                    }
 //               }
                i += 1
            }
        } else if arr.count == 0 && startIndex == "0" && !appDelegate.reachability.isReachable {
            DispatchQueue.main.async(execute: { () -> Void in
                Spinner.stop()
                self.activityIndicator?.stopAnimating()
                self.refreshControl?.endRefreshing()
                self.dataLoadFromDefaults(firstLoad: false, timeout: true)
            })
        } else if arr.count == 0 && self.endOfPosts == true {
            DispatchQueue.main.async(execute: { () -> Void in
                Spinner.stop()
                self.activityIndicator?.stopAnimating()
                self.refreshControl?.endRefreshing()
                if self.startIndex == "0" {
                    self.addBGtoTable(timeOut: false, deactivated: false)
                }
                Constants.sharedUtils.cacheDataToLocal(with: [], to: self.showOnlyBookmarkedItems == true ? "bookmarkedDict": self.id)
            })
        } else if arr.count == 0 {
            DispatchQueue.main.async(execute: { () -> Void in
                Spinner.stop()
                self.activityIndicator?.stopAnimating()
                self.refreshControl?.endRefreshing()
                if self.startIndex == "0" {
                    self.addBGtoTable(timeOut: false, deactivated: false)
                    Constants.sharedUtils.cacheDataToLocal(with: [], to: self.showOnlyBookmarkedItems == true ? "bookmarkedDict": self.id)
                }
            })
        }
        self.networkCallOnProgress = false
    }
    
    //download and save post image
    func saveImage(postFileUrlString: String, lastUpdated: Date?) -> String {
        let fullUrlString = Constants.otherLinks.mediaURL + postFileUrlString
        var postImageName: String?
        if let fileUrl = URL(string: fullUrlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? ""), postFileUrlString != "" {
            postImageName = fileUrl.path
            let position = postImageName!.components(separatedBy: "/").count - 2
            if position >= 0 {
                Constants.sharedUtils.saveFileDocumentDirectory(name: postImageName!.components(separatedBy: "/").last ?? "", directoryNamesString: postImageName!.replacingOccurrences(of: postImageName!.components(separatedBy: "/").last ?? "", with: ""), lastUpdatedDate: lastUpdated)
            }
        }
        return postImageName ?? ""
    }
    
    func loadTitleViewForNav() {
        DispatchQueue.main.async {
            self.titleButtonForNav =  UIButton(type: .custom)
            self.titleButtonForNav?.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
            self.titleButtonForNav?.addTarget(self, action: #selector(self.titleNameTapped), for: .touchUpInside)
            let descriptor = UIFont.systemFont(ofSize: UIFont.labelFontSize, weight: .semibold).fontDescriptor
            self.titleButtonForNav?.titleLabel?.font = UIFont(descriptor: descriptor, size: 0)
            var name = Constants.clientSpecificConstantsStructName.appName
            if name.contains("\n\n") {
                name = name.components(separatedBy: "\n\n")[0]
            }
            if name.count > 20 {
                name = name.padding(toLength: 20, withPad: name, startingAt: 0)
                name = name + "..."
            }
            self.titleButtonForNav?.setTitle(name, for: .normal)
            if #available(iOS 13.0, *) {
                self.titleButtonForNav?.setTitleColor(UIColor.label, for: .normal)
            } else {
                self.titleButtonForNav?.setTitleColor(UIColor.black, for: .normal)
            }
            self.titleButtonForNav?.titleLabel?.numberOfLines = 1
            self.titleButtonForNav?.titleLabel?.lineBreakMode = .byTruncatingTail
            self.navigationItem.titleView = self.titleButtonForNav
        }
    }
    
    //base table view framing
    func userTableViewSetting() {
        if #available(iOS 11.0, *) {
            tableView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        } else {
            tableView.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.minY + self.navigationController!.navigationBar.frame.height, width: self.view.frame.width, height: self.view.frame.height - (self.navigationController!.navigationBar.frame.minY + self.navigationController!.navigationBar.frame.height))
        }
        self.automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 13.0, *) {
            tableView.backgroundColor = UIColor.systemGray5
        } else {
            tableView.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
        }
        tableView.decelerationRate = UIScrollView.DecelerationRate.normal
    }
    
    //create cell BG
    func cellBG(height: CGFloat) -> UIView {
        let backgroundView : UIView = UIView(frame: CGRect(x: 0, y: 8, width: self.view.frame.size.width, height: height - 10))
        if #available(iOS 13.0, *) {
            backgroundView.layer.backgroundColor = UIColor.systemBackground.cgColor
        } else {
            backgroundView.layer.backgroundColor = UIColor.white.cgColor
        }
        backgroundView.layer.masksToBounds = false
        backgroundView.layer.cornerRadius = 2.0
        return backgroundView
    }
    
    //refreshControl
    func refreshControlSetting() {
        self.refreshControl = UIRefreshControl(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 50))
        var str: NSAttributedString?
        if #available(iOS 13.0, *) {
            self.refreshControl?.backgroundColor = UIColor.systemGray5
            self.refreshControl?.tintColor = UIColor.label
            str = NSAttributedString(string: Constants.otherConsts.refreshOnPullText, attributes: [NSAttributedString.Key.foregroundColor : UIColor.label])
        } else {
            self.refreshControl?.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
            self.refreshControl?.tintColor = UIColor.white
            str = NSAttributedString(string: Constants.otherConsts.refreshOnPullText, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        }
        self.refreshControl?.attributedTitle = str
        self.refreshControl?.addTarget(self, action: #selector(UsersProfileTableViewController.pulledToRefresh), for: UIControl.Event.valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = self.refreshControl
        } else {
            tableView.addSubview(self.refreshControl!)
        }
    }
    
    //When array empty - setting BG to the view
    func addBGtoTable(timeOut: Bool, deactivated: Bool) {
        if arrayForFeed.count == 0 || self.startIndex == "0" {
            if (self.arrayForFeed.count == 0) {
                DispatchQueue.main.async {
                    var screenView: UIView?
                    let frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.view.frame.height)
                    var image: UIImage?
                    var title: String?
                    if timeOut {
                        image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "timeoutImage"))
                        title = Constants.errorMessages.connectionLost
                    } else if deactivated {
                        image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "deactivatedProfileImage"))
                        title = Constants.otherConsts.deactivatedEmptyScreenText
                    } else if self.networkCallOnProgress {
                        self.loaderInFooter(show: true)
                        return
                    } else {
                        image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "queueBG"))
                        title = Constants.otherConsts.noUpdatesEmptyScreenText
                    }
                    if screenView == nil {
                        if let emptyBGView = emptyBgImageView(frame: frame).loadNib() as? emptyBgImageView {
                            self.activityIndicator?.stopAnimating()
                            self.activityIndicator = nil
                            emptyBGView.image = image
                            emptyBGView.title = title
                            emptyBGView.frame = frame
                            screenView = emptyBGView
                        }
                    }
                    if #available(iOS 13.0, *) {
                        self.tableView.backgroundColor = UIColor.systemGray5
                    } else {
                        self.tableView.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
                    }
                    self.tableView.tableFooterView = screenView
                }
            }
        }
    }
    
    func rightBarButtonsSettingsIfSavedContact() {
        let dotsImage = UIImage(named: "Menu_Dot")!.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: dotsImage, style: .done, target: self, action: #selector(self.sideMenuTapped))
//        muteButtonImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "muteIcon"))
//        unmuteButtonImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "unmuteIcon"))
//        muteUnmuteButton = UIBarButtonItem(image: muteButtonImage, style: .done, target: self, action: #selector(self.muteUnmuteOptionsTapped))
//        if isMuted {
//            muteUnmuteButton?.image = muteButtonImage
//        } else {
//            muteUnmuteButton?.image = unmuteButtonImage
//        }
//        self.navigationItem.rightBarButtonItems = [muteUnmuteButton!]
    }
        
    //enable and disable interaction
    func userInteractionForViews(interact: Bool) {
        self.navigationController?.navigationBar.isUserInteractionEnabled = interact
        self.tableView.isUserInteractionEnabled = interact
    }
    
    func loaderInFooter(show: Bool) {
        DispatchQueue.main.async {
            if show {
                self.activityIndicator = UIActivityIndicatorView(frame: CGRect(x: (self.view.frame.width/2) - 15, y: 5, width: 40, height: 40))
                self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50))
                self.tableView.tableFooterView?.addSubview(self.activityIndicator!)
                if #available(iOS 13.0, *) {
                    self.activityIndicator?.color = .label
                }
                self.activityIndicator?.startAnimating()
            } else if self.tableView.tableFooterView != nil {
                self.activityIndicator?.stopAnimating()
                self.tableView.tableFooterView?.subviews.last?.removeFromSuperview()
                self.activityIndicator = nil
                self.tableView.tableFooterView = nil
            }
        }
    }
    
    func loadCollectionViewInHeader() {
        if let pinnedPostsDict = Constants.sharedUtils.loadCachedDataFromLocal(with: "pinnedPostsDict"), pinnedPostsDict != [] {
            pinnedPostsArr = pinnedPostsDict
            DispatchQueue.main.async {
                let layout = UICollectionViewFlowLayout()
                layout.itemSize = CGSize(width: 150, height: 150)
                layout.scrollDirection = .horizontal
                let pinnedPostsCollectionView = UICollectionView(frame: CGRect(x: 0, y: 30, width: self.view.frame.width, height: 155), collectionViewLayout: layout)
                pinnedPostsCollectionView.register(UINib(nibName: Constants.nameOfIdentifiers.pinnedPostCollectionCellIdentifier, bundle: nil), forCellWithReuseIdentifier: Constants.nameOfIdentifiers.pinnedPostCollectionCellIdentifier)
                if #available(iOS 13.0, *) {
                    pinnedPostsCollectionView.backgroundColor = UIColor.systemGray5
                } else {
                    pinnedPostsCollectionView.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
                }
                let baseView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 185))
                let label = UILabel(frame: CGRect(x: 10, y: 0, width: 200, height: 30))
                label.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
                label.textColor = .gray
                label.text = "Pinned Posts"
                label.backgroundColor = .clear
                baseView.addSubview(label)
                baseView.addSubview(pinnedPostsCollectionView)
                pinnedPostsCollectionView.delegate = self
                pinnedPostsCollectionView.dataSource = self
                pinnedPostsCollectionView.reloadData()
                self.tableView.tableHeaderView = baseView
            }
        }
    }
    
    //for collection view
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pinnedPostsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.nameOfIdentifiers.pinnedPostCollectionCellIdentifier, for: indexPath) as! pinnedPostCollectionViewCell
        cell.postImageView?.image = nil
        cell.postImageView?.removeFromSuperview()
        cell.attachmentNameLabel?.removeFromSuperview()
        cell.indexpath = indexPath
        if #available(iOS 13.0, *) {
            cell.contentView.backgroundColor = UIColor.systemBackground
        } else {
            cell.contentView.backgroundColor = UIColor.white
        }
        if pinnedPostsArr.count > indexPath.row {
            let dict = pinnedPostsArr[indexPath.row]
            cell.postDict = dict
            var desc = dict.desc
            let moreString = NSAttributedString(string: Constants.otherConsts.seeMoreText.replacingOccurrences(of: "*", with: "").replacingOccurrences(of: "\n", with: ""), attributes: [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0) , NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.strokeWidth : -4.0])
            if desc.count > 30 {
                var endIndex = 30
                for i in 30...(desc.count - 1) {
                    let index = desc.index(desc.startIndex, offsetBy: i)
                    
                    if desc[index] == " " || desc[index] == "\n" {
                        endIndex = i
                        break
                    }
                }
                desc = String(desc[..<desc.index(desc.startIndex, offsetBy: endIndex)])
            }
            
            let attrText = NSMutableAttributedString(string: desc)
            if desc != "" {
                attrText.append(moreString)
            }
            if dict.location != "" && dict.location != "null,null" {
                if desc != "" {
                    cell.locationTextView.attributedText = attrText
                }
                var locationMarkerTextViewText = ""
                if dict.location.components(separatedBy: ",").count >= 3 {
                    let latitude = dict.location.components(separatedBy: ",")[0]
                    let longitude = dict.location.components(separatedBy: ",")[1]
                    var locationName = dict.location.replacingOccurrences(of: (latitude + "," + longitude + ","), with: "")
                    let locNameArr = locationName.components(separatedBy: " ")
                    if locNameArr.count > 2 {
                        locationName = locNameArr[0] + " " + locNameArr[1] + "..."
                    }
                    locationMarkerTextViewText = locationName
                } else {
                    var locationName = dict.location
                    let locNameArr = locationName.components(separatedBy: " ")
                    if locNameArr.count > 2 {
                        locationName = locNameArr[0] + " " + locNameArr[1] + "..."
                    }
                    locationMarkerTextViewText = locationName
                }
                let locAttachment = NSTextAttachment()
                let img = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "mapIconGray"))
                locAttachment.image = img
                locAttachment.bounds = CGRect(x: 0, y: cell.locationTextView.font!.descender, width: 17, height: 17)
                let locAttachmentStr = NSMutableAttributedString(attributedString: NSAttributedString(attachment: locAttachment))
                let locStr = NSAttributedString(string: locationMarkerTextViewText, attributes: [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0), NSAttributedString.Key.foregroundColor: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))])
                locAttachmentStr.append(locStr)
                cell.locationMarkerTextView.attributedText = locAttachmentStr
                cell.locationTextView.isHidden = false
                cell.locationTextView.sizeToFit()
                cell.locationMarkerTextView.isHidden = false
                cell.mapView.isHidden = false
                cell.statusTextView.isHidden = true
                cell.coloredTextView.isHidden = true
            } else if dict.colorCode != "" {
                cell.coloredTextView.attributedText = attrText
                cell.coloredTextView.textContainerInset = UIEdgeInsets(top: 30, left: 20, bottom: 30, right: 20)
                cell.coloredTextView.textAlignment = .center
                cell.coloredTextView.backgroundColor = appDelegate.hexStringToUIColor(hex: dict.colorCode)
                cell.coloredTextView.centerVertically()
                cell.locationTextView.isHidden = true
                cell.locationMarkerTextView.isHidden = true
                cell.mapView.isHidden = true
                cell.statusTextView.isHidden = true
                cell.coloredTextView.isHidden = false
            } else {
                cell.statusTextView.attributedText = attrText
                cell.statusTextView.sizeToFit()
                cell.locationTextView.isHidden = true
                cell.locationMarkerTextView.isHidden = true
                cell.mapView.isHidden = true
                cell.statusTextView.isHidden = false
                cell.coloredTextView.isHidden = true
            }
            if dict.attachments.count > 0 {
                switch (dict.attachments[0]["type"] as? String ?? "") {
                case "image":
                    let postImage = Constants.sharedUtils.getImage(isProfileImg: false, endPath: (dict.attachments[0]["link"] as? String ?? ""))
                    cell.postImageView?.frame = CGRect(x: 0, y: cell.mapView.isHidden ? cell.statusTextView.frame.height: 0, width: 150, height: cell.mapView.isHidden ? (150 - cell.statusTextView.frame.height): cell.mapView.frame.height)
                    cell.postImageView?.image = postImage
                    if postImage.images != nil && cell.postImageView != nil {
                        cell.postImageView?.contentMode = .center
                    } else {
                        cell.postImageView?.contentMode = .scaleAspectFill
                    }
                    cell.postImageView?.clipsToBounds = true
                    if cell.mapView.isHidden == false {
                        cell.mapView.addSubview(cell.postImageView!)
                        cell.mapView.frame.size = CGSize(width: cell.postImageView!.frame.width, height: cell.postImageView!.frame.height)
                    } else {
                        cell.statusTextView.addSubview(cell.postImageView!)
                        let path = UIBezierPath(rect: CGRect(x:0, y: cell.statusTextView.frame.height, width: cell.postImageView!.frame.width, height:cell.postImageView!.frame.height))
                        cell.statusTextView.textContainer.exclusionPaths = [path]
                    }
                case "doc":
                    let pdfAttachment = NSTextAttachment()
                    let pdfImg = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfDummyImage"))
                    pdfAttachment.image = pdfImg
                    pdfAttachment.bounds = CGRect(x: 0, y: cell.attachmentNameLabel!.font!.descender, width: 17, height: 17)
                    let pdfAttachmentStr = NSMutableAttributedString(attributedString: NSAttributedString(attachment: pdfAttachment))
                    pdfAttachmentStr.append(NSAttributedString(string: " " + "\n" + (dict.attachments[0]["name"] as? String ?? "")))
                    if desc == "" {
                        pdfAttachmentStr.append(moreString)
                    }
                    cell.attachmentNameLabel?.frame = CGRect(x: 10, y: cell.statusTextView.frame.height + 10, width: cell.frame.width - 20, height: 40)
                    cell.attachmentNameLabel?.attributedText = pdfAttachmentStr
                    cell.attachmentNameLabel?.sizeToFit()
                    cell.attachmentNameLabel?.frame = CGRect(x: 10, y: cell.attachmentNameLabel!.frame.origin.y, width: cell.frame.width - 20, height: cell.attachmentNameLabel!.frame.height)
                    if !cell.mapView.isHidden {
                        cell.mapView.addSubview(cell.attachmentNameLabel!)
                        cell.mapView.frame.size = CGSize(width: 150, height: cell.attachmentNameLabel!.frame.height)
                    } else {
                        cell.statusTextView.addSubview(cell.attachmentNameLabel!)
                        let path = UIBezierPath(rect: CGRect(x:0, y: cell.statusTextView.frame.height, width: 150, height: cell.attachmentNameLabel!.frame.height))
                        cell.statusTextView.textContainer.exclusionPaths = [path]
                    }
                default:
                    cell.attachmentNameLabel?.text = Constants.errorMessages.fileFormatUnknownText
                    cell.attachmentNameLabel?.sizeToFit()
                    cell.attachmentNameLabel?.frame = CGRect(x: 10, y: cell.statusTextView.frame.height + 10, width: cell.frame.width - 20, height: cell.attachmentNameLabel!.frame.height)
                    if !cell.mapView.isHidden {
                        cell.mapView.addSubview(cell.attachmentNameLabel!)
                        cell.mapView.frame.size = CGSize(width: 150, height: cell.attachmentNameLabel!.frame.height)
                    } else {
                        cell.statusTextView.addSubview(cell.attachmentNameLabel!)
                        let path = UIBezierPath(rect: CGRect(x:0, y: cell.statusTextView.frame.height, width: 150, height: cell.attachmentNameLabel!.frame.height))
                        cell.statusTextView.textContainer.exclusionPaths = [path]
                    }
                }
            }
            if dict.location != "" && dict.location != "null,null" && dict.attachments.count == 0 {
                let latitude = dict.location.components(separatedBy: ",")[0]
                let longitude = dict.location.components(separatedBy: ",")[1]
                let locationCoordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude:  Double(longitude)!)
                cell.updateLocation(location: locationCoordinate)
            }
        } else {
            // hide particular cell
        }
        // Configure the cell
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = pinnedPostsArr[indexPath.row]
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "singlePostVc") as! SinglePostTableViewController
        vc.currentPost = dict
        vc.indexPath = indexPath
        vc.userDetails = savedContactRefDict[dict.id ?? Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails
        vc.postOwnerName = vc.userDetails?.name ?? vc.userDetails?.actualProfileName ?? ""
        var image: UIImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))!
        if savedContactRefDict.keys.contains(dict.id ?? "") {
            image = Constants.sharedUtils.getImage(isProfileImg: true, endPath: vc.userDetails?.imageName ?? "")
        }
        vc.profilePicImage = appDelegate.cropImage(img: image)
        vc.savedTabbar = self.tabBarController
        vc.userId = self.id
        
        var time = dict.time
        //time = time.padding(toLength: 10, withPad: time, startingAt: 0)
        if time != "" {
            let dateFromString = time.dateFromISO8601
            if dateFromString != nil {
                let calendar = NSCalendar.current
                let datecomponents = calendar.dateComponents([.day, .month, .year, .hour, .minute, .second], from: Date(), to: dateFromString!)
                
                time = appDelegate.timeFromDateComponents(datecomponents: datecomponents, time: time, date: dateFromString!)
            } else if time != "" {
                time.removeLast(10)
            }
        }
        time = time + " |  "

        let dateStr = NSMutableAttributedString(string: time)
        let attributedText = settingNameAttributedLabelTextForCell(dateStr: dateStr, time: time, privacy: dict.privacy, userId: dict.id ?? "")
        vc.cellTime = dict.time
        vc.dateAttributedString = attributedText
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: 10, bottom: 5, right: 10)
    }
    
    //MARK: - Network Related
    //network call
    func networkCall(isRefreshCall: Bool) {
        self.getContactDetails(id: self.id, isRefreshCall: isRefreshCall, completion: {[weak self] (contactDict, deactivated, timeout) in
            guard let strongSelf = self else {
                return
            }
            if contactDict != [:] && contactDict != nil {
                DispatchQueue.global(qos: .background).async {
                    if self?.showOnlyBookmarkedItems == true {
                        let feeds = (contactDict!["data"]! as? NSDictionary ?? [:])["value"] as? [NSMutableDictionary] ?? []
                        self?.setProfileFields(name: "", mob: "", imageName: "", arr: feeds, arePinnedPosts: false)
                    } else {
                        if !Constants.clientSpecificConstantsStructName.pickFromPincode {
                            var name = (contactDict!["userDetails"] as! NSDictionary)["name"] as? String ?? ""
                            if name.contains("\n\n") {
                                name = name.components(separatedBy: "\n\n")[0]
                            }
                            var mobileNo = ""
                            var createdTime = (contactDict!["userDetails"] as! NSDictionary)["createdDate"] as? String
                            if createdTime != "" && createdTime != nil {
                                let dateFromString = createdTime!.dateFromISO8601
                                if dateFromString != nil {
                                    createdTime = appDelegate.joinedDateString(date: dateFromString!)
                                }
                            }
                            let location = (contactDict!["userDetails"] as! NSDictionary)["location"] as? [String: Any]
                            let arrayOfDetailsInContacts = Constants.defaults.value(forKey: "immediateContactsSyncedResults") as? [String] ?? []
                            let mobNumFromServer = (contactDict!["userDetails"] as! NSDictionary)["mobile"] as? String ?? ""
                            if (contactDict!["userDetails"] as? NSDictionary ?? [:])["contactPrivate"] as? Bool != true || (contactDict!["userDetails"] as? NSDictionary ?? [:])["userName"] as? String == "" || (contactDict!["userDetails"] as? NSDictionary ?? [:])["userName"] == nil || (mobNumFromServer != "" &&  arrayOfDetailsInContacts.contains(mobNumFromServer)) {
                                mobileNo = mobNumFromServer
                            } else {
                                mobileNo = ""
                            }
                            let urlString = Constants.otherLinks.mediaURL + ((contactDict!["userDetails"] as! NSDictionary)["profile_pic"] as? String ?? "")
                            let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
                            //let imageName = url?.path.components(separatedBy: "/").last
                            let imgName = url!.path
                            //Constants.sharedUtils.removeFileFromCache(appendName: (savedContactRefDict[self.id] as! [String])[1])
                            let position = imgName.components(separatedBy: "/").count - 2
                            if position >= 0 {
                                Constants.sharedUtils.saveFileDocumentDirectory(name: imgName.components(separatedBy: "/").last ?? "", directoryNamesString: imgName.replacingOccurrences(of: imgName.components(separatedBy: "/").last ?? "", with: ""), lastUpdatedDate: ((contactDict!["userDetails"] as! NSDictionary)["updatedDate"] as? String ?? "").dateFromISO8601)
                            }
                            var about = (contactDict!["userDetails"] as! NSDictionary)["about"] as? String ?? ""
                            let qkopyUsername = (contactDict!["userDetails"] as! NSDictionary)["userName"] as? String ?? ""
                            let profileVerified = (contactDict!["userDetails"] as? NSDictionary ?? [:])["profileVerified"] as? Bool ?? false
                            let isMuted = (contactDict!["userDetails"] as? NSDictionary ?? [:])["muteFlag"] as? Bool ?? false
                            if isMuted {
                                self?.muteUnmuteButton?.image = self?.muteButtonImage
                            } else {
                                self?.muteUnmuteButton?.image = self?.unmuteButtonImage
                            }
                            let isPremium = (contactDict!["userDetails"] as? NSDictionary ?? [:])["isPremium"] as? Bool ?? false
                            if isPremium == false {
                                DispatchQueue.main.async {
                                    self?.premiumImageView?.removeFromSuperview()
                                }
                            }
                            if about == "" {
                                about = Constants.otherConsts.aboutDefaultDescription
                            }
                            savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId] = DynamicKeyObject(minimalDetails: MinimalContactDetails(name: name, imageName: imgName, profileVerified: profileVerified, isPremium: isPremium, muted: isMuted, mutedTill: "", number: mobileNo, qkopyId: qkopyUsername, actualProfileName: name, about: about, createdDate: createdTime, location: location))
                            Constants.sharedUtils.cacheDictionaryToLocal(with: savedContactRefDict, to: "savedContacts")
                        }
                        if deactivated {
                            Spinner.stop()
                            self?.activityIndicator?.stopAnimating()
                            self?.refreshControl?.endRefreshing()
                            self?.arrayForFeed = []
                            self?.heightArr = []
                            self?.tableView.reloadData()
                            self?.firstTimeFeedLoad = false
                            self?.loadedFromServer = true
                            self?.addBGtoTable(timeOut: false, deactivated: true)
                        } else {
                            self?.setProfileFields(name: savedContactRefDict[strongSelf.id]?.minimalDetails?.name ?? "", mob: savedContactRefDict[strongSelf.id]?.minimalDetails?.number ?? "", imageName: savedContactRefDict[strongSelf.id]?.minimalDetails?.imageName ?? "", arr: contactDict!["feeds"] as! [NSMutableDictionary], arePinnedPosts: false)
                        }
                    }
                }
            } else {
                if deactivated {
                    DispatchQueue.main.async(execute: { () -> Void in
                        Spinner.stop()
                        self?.activityIndicator?.stopAnimating()
                        self?.refreshControl?.endRefreshing()
                        self?.arrayForFeed = []
                        self?.heightArr = []
                        self?.tableView.reloadData()
                        self?.firstTimeFeedLoad = false
                        self?.loadedFromServer = true
                        self?.addBGtoTable(timeOut: false, deactivated: true)
                    })
                } else if contactDict == nil {
                    DispatchQueue.main.async(execute: { () -> Void in
                        Spinner.stop()
                        self?.activityIndicator?.stopAnimating()
                        self?.refreshControl?.endRefreshing()
                        if self?.arrayForFeed.count == 0 {
                            self?.dataLoadFromDefaults(firstLoad: false, timeout: timeout)
                        }
                    })
                } else {
                    self?.setProfileFields(name: "", mob: "", imageName: "", arr: [], arePinnedPosts: false)
                }
            }
        })
    }
    
    //retrieve contact
    func getContactDetails(id:String, isRefreshCall: Bool, completion: @escaping (_ result: NSDictionary?, _ addDeactiveBG: Bool, _ timeout: Bool)->()) {
        if appDelegate.reachability.isReachable {
            networkCallOnProgress = true
            if firstTimeFeedLoad {
                startIndex = "0"
            } else {
                startIndex = arrayForFeed.last?.pid ?? "0"
            }
            if !isRefreshCall {
                loaderInFooter(show: true)
            }
            if isRefreshCall == true {
                self.startIndex = "0"
            }
            var uptoCount: Int?
            if startIndex == "0" || isRefreshCall == true {
                uptoCount = 6
            } else {
                uptoCount = 3
            }
            DispatchQueue.global(qos: .background).async {
                let selectedChannelIds = Constants.defaults.value(forKey: "selectedChannelIds") as? [String] ?? []
                var params = ["id" : id, "start_index": self.startIndex!, "count": uptoCount!, "channelList": selectedChannelIds] as [String : Any]
                
                print(params)
                
                if Constants.clientSpecificConstantsStructName.pickFromPincode {
                    let allSubscribedChannels = Constants.defaults.value(forKey: "allSubscribedChannels") as? [String] ?? []
                    let allUserIds = Constants.defaults.value(forKey: "allUserIds") as? [String] ?? []
                    params = ["userIds" : allUserIds, "channelList": allSubscribedChannels, "start_index": self.startIndex!, "count": uptoCount!] as [String : Any]
                }
                if self.showOnlyBookmarkedItems {
                    let bookmarkedList = Constants.defaults.value(forKey: "bookmarkedList") as? [String] ?? []
                    params = ["userId" : id, "bookmarkList": bookmarkedList] as [String : Any]
                }
                apiCall.handleApiRequest(endPart: self.showOnlyBookmarkedItems ? Constants.apiCalls.getBookmarks: (Constants.clientSpecificConstantsStructName.pickFromPincode ? Constants.apiCalls.getFeeds: Constants.apiCalls.getXFeeds), params: params, completion: {[weak self] (responseJson, errorDesc, timeout) in
                    guard let strongSelf = self else {
                        return
                    }
                    if strongSelf.changedSelectionOfCategoriesRefreshed == false {
                        strongSelf.changedSelectionOfCategoriesRefreshed = true
                    }
                    if strongSelf.startIndex == "0" {
                        strongSelf.endOfPosts = false
                    }
                    if responseJson != nil && errorDesc == "" {
                        if self?.showOnlyBookmarkedItems == true {
                            let resultDict = responseJson as? NSDictionary ?? [:]
                            self?.endOfPosts = true
                            self?.loadedFromServer = true
                            completion(resultDict, false, timeout)
                        } else {
                            var contactDetails = responseJson as? NSDictionary ?? [:]
                            contactDetails = (contactDetails["data"]! as? NSDictionary ?? [:])["value"] as? NSMutableDictionary ?? [:]
                            if self?.startIndex == "0" && Constants.clientSpecificConstantsStructName.showPinnedPosts {
                                self?.networkCallForPinnedPosts()
                            }
//                            if active == 0 {
//                                self?.endOfPosts = true
//                                self?.loadedFromServer = true
//                                completion(contactDetails, true, timeout)
//                            } else {
                                if uptoCount != (contactDetails["feeds"] as! [NSMutableDictionary]).count {
                                    self?.endOfPosts = true
                                } else {
                                    self?.endOfPosts = false
                                }
                                self?.loadedFromServer = true
                                completion(contactDetails, false, timeout)
//                            }
                        }
                    } else {
                        self?.loaderInFooter(show: false)
                        self?.networkCallOnProgress = false
                        DispatchQueue.main.async {
                            self?.refreshControl?.endRefreshing()
                            Spinner.stop()
                            completion(nil, false, timeout)
                        }
                    }
                })
            }
        } else {
            self.refreshControl?.endRefreshing()
            self.loaderInFooter(show: false)
            Spinner.stop()
            if self.arrayForFeed.count == 0 {
                self.dataLoadFromDefaults(firstLoad: false, timeout: true)
            }
        }
    }
    
    func networkCallForPinnedPosts() {
        apiCall.handleApiRequest(endPart: Constants.apiCalls.getPinPosts, params: ["userId" : id], completion: {[weak self] (responseJson, errorDesc, timeout) in
            guard let strongSelf = self else {
                return
            }
            if responseJson != nil && errorDesc == "" {
                let resultDict = responseJson as? NSDictionary ?? [:]
                let feeds = (resultDict["data"]! as? NSDictionary ?? [:])["value"] as? [NSMutableDictionary] ?? []
                if feeds.count > 0 {
                    strongSelf.setProfileFields(name: "", mob: "", imageName: "", arr: feeds, arePinnedPosts: true)
                } else {
                    Constants.sharedUtils.cacheDataToLocal(with: [], to: "pinnedPostsDict")
                    DispatchQueue.main.async {
                        strongSelf.tableView.tableHeaderView = nil
                    }
                }
            }
        })
    }
    
    //MARK: - General Methods
    //fetch from Constants.defaults
    func dataLoadFromDefaults(firstLoad:Bool, timeout: Bool) {
        if let Arr = Constants.sharedUtils.loadCachedDataFromLocal(with: self.showOnlyBookmarkedItems == true ? "bookmarkedDict": self.id) {
            if Arr.count > 0 {
                self.arrayForFeed = Arr
                self.loadCollectionViewInHeader()
                self.cellShowMore = [:]
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
                    if self.tableView.tableFooterView != nil {
                        self.tableView.tableFooterView = nil
                    }
                }
            } else {
                addBGtoTable(timeOut: timeout, deactivated: false)
            }
        } else {
            if firstLoad && appDelegate.reachability.isReachable && !timeout {
                //Constants.sharedUtils.showProgressView(self.view)
            } else if firstLoad && !appDelegate.reachability.isReachable && arrayForFeed == [] {
                addBGtoTable(timeOut: true, deactivated: false)
            } else {
                addBGtoTable(timeOut: timeout, deactivated: false)
            }
        }
        if firstLoad && appDelegate.reachability.isReachable {
            self.networkCall(isRefreshCall: false)
        }
    }
    
    //boundary string generation
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }

    //MARK: - Table view Cell Formation
    //Name Label appended with time and privacy icon. Make the text attributed
    func settingNameAttributedLabelTextForCell(dateStr: NSMutableAttributedString, time: String, privacy: String, userId: String) -> NSMutableAttributedString {
        dateStr.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.gray, range: NSRange(location: 0, length: time.count))
        dateStr.addAttribute(NSAttributedString.Key.font, value: UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0) , range: NSRange(location: 0, length: time.count))
        let attachment = NSTextAttachment()
        var nameForPrivacyIcon: String?
        switch (privacy) {
        case "Me":
            nameForPrivacyIcon = "mePrivacy.png"
            break
        case "Contacts":
            nameForPrivacyIcon = "contactsPrivacy.png"
            break
        case "Public":
            nameForPrivacyIcon = "publicPrivacy.png"
            break
        case "Custom":
            nameForPrivacyIcon = "custom_contacts.png"
            break
        default:
            nameForPrivacyIcon = "custom_contacts.png"
            break
        }
        let img = UIImage(named: nameForPrivacyIcon!)
        attachment.image = img
        attachment.bounds = CGRect(x: 0, y: -3, width: 15, height: 15)
        let attachmentString = NSAttributedString(attachment: attachment)
        dateStr.append(attachmentString)
        if savedContactRefDict[userId]?.minimalDetails?.isPremium == true {
            dateStr.append(NSAttributedString(string: "  "))
            let premiumAttachment = NSTextAttachment()
            let premiumImage = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "premiumStarImage"))
            premiumAttachment.image = premiumImage
            premiumAttachment.bounds = CGRect(x: 0, y: -3, width: 15, height: 15)
            let premiumAttachmentString = NSAttributedString(attachment: premiumAttachment)
            dateStr.append(premiumAttachmentString)
        }
        return dateStr
    }
    
    //the heightArr values - either inserted or removed based on updation
    func heighArrSettingOnCellLoad(isStatusTV: Bool, attachmentPosted: Bool, closeBtnHidden: Bool , index : Int, rowHeight: CGFloat, extraHeight: CGFloat) -> CGFloat {
        var tableviewRowHeight: CGFloat = 70
        if heightArr.count == index {
            if !isStatusTV && !attachmentPosted {
                tableviewRowHeight = rowHeight
                heightArr.insert(tableviewRowHeight, at: index)
            } else {
                tableviewRowHeight = rowHeight + extraHeight
                heightArr.insert(tableviewRowHeight, at: index)
            }
        } else {
            if !isStatusTV && !attachmentPosted {
                tableviewRowHeight = rowHeight
                if heightArr.count > index {
                    heightArr[index] = tableviewRowHeight
                }
            } else {
                tableviewRowHeight = rowHeight + extraHeight
                if heightArr.count > index {
                    heightArr[index] = tableviewRowHeight
                }
            }
        }
        return tableviewRowHeight
    }

    //MARK: - Delegates
    //Table view delegates
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayForFeed.count != 0 {
            bgImageView?.isHidden = true
            noUpdatesLabel?.isHidden = true
            if bgImageView != nil {
                DispatchQueue.main.async {
                    self.bgImageView?.removeFromSuperview()
                    self.bgImageView = nil
                    self.noUpdatesLabel?.removeFromSuperview()
                    self.noUpdatesLabel = nil
                }
            }
        } else {
            bgImageView?.isHidden = false
            noUpdatesLabel?.isHidden = false
        }
        return arrayForFeed.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.nameOfIdentifiers.feedCellIdentifier) as! StatusFeedTableViewCell
        let listOfChannels = Constants.defaults.value(forKey: "listOfChannels") as? [[String: String]] ?? []
        let selectedChannelList = Constants.defaults.value(forKey: "selectedChannelIds") as? [String] ?? []
        cell.gotoUserDelegate = self
        cell.closeBtn.isHidden = true
        if Constants.clientSpecificConstantsStructName.showBookmarkBtn {
            cell.bookmark.isHidden = false
        }
        cell.bookmarkCellDelegate = self
        cell.showMoreLessDelegate = self
        cell.fullScreenDelegate = self
        cell.updateLikesInDictDelegate = self
        cell.openInWebviewDelegate = self
        cell.openSinglePostVcDelegate = self
        cell.chatVCDelegate = self
        cell.chevronDelegate = self
        cell.bookmarkCellDelegate = self
        if #available(iOS 13.0, *) {
            cell.contentView.backgroundColor = UIColor.systemGray5
        } else {
            cell.contentView.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
        }
        cell.embeddedView?.removeFromSuperview()
        cell.previewView?.removeFromSuperview()
        cell.playerView?.removeFromSuperview()
        cell.attachments = []
        cell.attachmentBackgroundView?.removeFromSuperview()
        cell.attachmentBaseView?.removeFromSuperview()
        cell.attachmentImageView?.removeFromSuperview()
        cell.attachmentNameLabel?.removeFromSuperview()
        cell.playPauseBtn?.removeFromSuperview()
        cell.slider?.removeFromSuperview()
        cell.progressLabel?.removeFromSuperview()
        cell.postImageView?.image = nil
        cell.postImageView?.removeFromSuperview()
        cell.imagesCollectionView?.removeFromSuperview()
        cell.downloadImageView?.removeFromSuperview()
        cell.circularProgressView?.removeFromSuperview()
        cell.statusTextView.textContainer.exclusionPaths.removeAll()
        cell.locationTextView.isScrollEnabled = true
        cell.statusTextView.isScrollEnabled = true
        cell.coloredTextView.isScrollEnabled = true
        cell.locationMarkerTextView.isScrollEnabled = true
        //cell.statusTVBottomConstraint.constant = 0
        //cell.coloredTVBottomConstraint.constant = 0
        //cell.locationTVBottomConstraint.constant = 0
        //cell.viewsCountLabel.isHidden = true
        cell.viewsCountLabel.text = ""
        cell.shareButton.isHidden = false
        cell.indicatorForPost.isHidden = true
        var moreString: NSAttributedString?
        cell.shareButton.isEnabled = !isUnsavedContact
        if isUnsavedContact {
            cell.likesCountButton.setTitleColor(UIColor.lightGray, for: .normal)
        } else {
            if #available(iOS 13.0, *) {
                cell.likesCountButton.setTitleColor(UIColor.label, for: .normal)
            } else {
                cell.likesCountButton.setTitleColor(UIColor.black, for: .normal)
            }
        }
        if arrayForFeed.count > indexPath.row {
            let tempDict = arrayForFeed[indexPath.row] as PostObject
            var str = tempDict.desc
            var rowHeight: CGFloat?
            cell.desc = str
            cell.userID = tempDict.id
            cell.pid = tempDict.pid
            let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 20.0
            paragraphStyle.maximumLineHeight = 20.0
            paragraphStyle.minimumLineHeight = 20.0
            cell.showPreview = tempDict.showPreview
            var hashTagToAppend = ""
            if tempDict.channelList != [] {
                for channelId in tempDict.channelList {
                    if selectedChannelList.contains(channelId) {
                        let index = listOfChannels.firstIndex(where: { (keyValuePair) -> Bool in
                            if keyValuePair.keys.first == channelId {
                                return true
                            }
                            return false
                        })
                        if index != nil {
                            hashTagToAppend = hashTagToAppend + "#" + listOfChannels[index!][channelId]! + " "
                        }
                    }
                }
            }
            cell.hashLabel.text = hashTagToAppend
            cell.hashLabel.sizeToFit()
            cell.hashLabelHeightConstant?.constant = cell.hashLabel.frame.size.height
            let orgStr = str
            if str.count > 320  && tempDict.colorCode == "" {
                cell.statusTextView.addGestureRecognizer(cell.tappedOnStatusTV)
                cell.locationTextView.addGestureRecognizer(cell.tappedOnLocTV)
                var endIndex = 320
                for i in 320...(str.count - 1) {
                    let index = str.index(str.startIndex, offsetBy: i)
                    
                    if str[index] == " " || str[index] == "\n" {
                        endIndex = i
                        break
                    }
                }
                if !self.cellShowMore.keys.contains(tempDict.pid) {
                    str = String(str[..<str.index(str.startIndex, offsetBy: endIndex)])
                    moreString = NSAttributedString(string: Constants.otherConsts.seeMoreText.replacingOccurrences(of: "*", with: "").replacingOccurrences(of: "\n", with: ""), attributes: [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0) , NSAttributedString.Key.foregroundColor: UIColor.lightGray, NSAttributedString.Key.strokeWidth : -4.0])
                    cell.showMore = false
                } else {
                    cell.showMore = true
                }
            } else {
                cell.statusTextView.removeGestureRecognizer(cell.tappedOnStatusTV)
                cell.locationTextView.removeGestureRecognizer(cell.tappedOnLocTV)
                cell.showMore = false
            }
            var attributes = [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0), NSAttributedString.Key.paragraphStyle: paragraphStyle]
            if #available(iOS 13.0, *) {
                attributes[NSAttributedString.Key.foregroundColor] = UIColor.label
            } else {
                attributes[NSAttributedString.Key.foregroundColor] = UIColor.black
            }
            var mutStr = NSMutableAttributedString(string: str, attributes: attributes)
            if moreString != nil {
                mutStr.append(moreString!)
            }
            cell.postImageName = tempDict.postFileName
            cell.likeButton.isSelected = tempDict.liked
            cell.likesArrCount = tempDict.likesArray.count
            if let bookmarkedList = Constants.defaults.value(forKey: "bookmarkedList") as? [String], cell.pid != "", cell.pid != nil, bookmarkedList.contains(cell.pid!) {
                cell.bookmark.isSelected = true
            } else {
                cell.bookmark.isSelected = false
            }
            let attachments = tempDict.attachments
            if attachments.count > 0 {
                cell.attachments = attachments
            }
            if tempDict.colorCode != "" {
                cell.coloredTextView.attributedText = mutStr
                cell.coloredTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
                cell.statusTextView.isHidden = true
                cell.mapView.isHidden = true
                cell.coloredTextView.isHidden = false
                cell.color_code = tempDict.colorCode
                cell.coloredTextView.textAlignment = .center
                cell.coloredTextView.textColor = UIColor.white
                cell.locationMarkerTextView.isHidden = true
                cell.locationTextView.isHidden = true
                cell.coloredTextView.backgroundColor = appDelegate.hexStringToUIColor(hex: cell.color_code!)
                cell.coloredTextView.textContainerInset = UIEdgeInsets.init(top: 0, left: 50.0, bottom: 0, right: 50.0)
                cell.coloredTextView.sizeToFit()
                var gap: CGFloat = 20
                if fixedHeight - cell.coloredTextView.frame.height > 0 {
                    gap = (fixedHeight - cell.coloredTextView.frame.height)/2
                }
                cell.coloredTextView.textContainerInset = UIEdgeInsets.init(top: gap, left: 50.0, bottom: gap, right: 50.0)
                cell.coloredTextView.centerVertically()
                cell.coloredTextView.sizeToFit()
                cell.coloredTextView.frame = CGRect(x: 0, y: cell.coloredTextView.frame.origin.y, width: self.view.frame.size.width, height: cell.coloredTextView.frame.height)
                if fixedHeight - cell.coloredTextView.frame.height > 0 {
                    rowHeight = fixedHeight + totalHeightToLeaveForTitleAndBottomBarInCell + cell.hashLabel.frame.size.height
                } else {
                    rowHeight = cell.coloredTextView.frame.height + totalHeightToLeaveForTitleAndBottomBarInCell + cell.hashLabel.frame.size.height
                }
            } else {
                if tempDict.location != "" && tempDict.location != "null,null" {
                    let latitude = tempDict.location.components(separatedBy: ",")[0]
                    let longitude = tempDict.location.components(separatedBy: ",")[1]
                    if mutStr.string == "" {
                        cell.mapViewToDateLabelConstraint?.isActive = true
                        cell.mapViewToDateLabelConstraint.constant = 10
                        cell.locationTextView.frame = CGRect(x: cell.locationTextView.frame.minX, y: cell.locationTextView.frame.minY, width: cell.locationTextView.frame.width, height: 0)
                    } else {
                        cell.mapViewToDateLabelConstraint?.isActive = true
                    }
                    cell.locationTextView.attributedText = mutStr
                    cell.locationTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
                    let locationCoordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude:  Double(longitude)!)
                    cell.updateLocation(location: locationCoordinate)
                    cell.statusTextView.isHidden = true
                    cell.coloredTextView.isHidden = true
                    cell.color_code = ""
                    cell.mapViewHeightConstraint?.constant = cell.frame.width/2
                    cell.mapView.isHidden = false
                    cell.locationMarkerTextView.isHidden = false
                    cell.locationTextView.isHidden = false
                    
                    var locCellHeight: CGFloat?
                    if tempDict.location.components(separatedBy: ",").count >= 3 {
                        let locationName = tempDict.location.replacingOccurrences(of: (latitude + "," + longitude + ","), with: "")
                        cell.locationName = locationName
                        let locAttachment = NSTextAttachment()
                        let img = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "mapIconGray"))
                        locAttachment.image = img
                        locAttachment.bounds = CGRect(x: 0, y: cell.locationTextView.font!.descender, width: 17, height: 17)
                        let locAttachmentStr = NSMutableAttributedString(attributedString: NSAttributedString(attachment: locAttachment))
                        let locStr = NSAttributedString(string: locationName, attributes: [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0), NSAttributedString.Key.foregroundColor: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))])
                        locAttachmentStr.append(locStr)
                        locAttachmentStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: locStr.string.count))
                        cell.locationMarkerTextView.attributedText = locAttachmentStr
                        cell.locationMarkerTextView.sizeToFit()
                        locCellHeight = cell.locationMarkerTextView.frame.height
                    }
                    
                    cell.locationTextView.sizeToFit()
                    let locationTVHeight = cell.locationTextView.frame.height
                    if str != "" {
                        cell.mapViewToDateLabelConstraint?.constant = cell.locationTextView.frame.height + 10
                        locCellHeight = (locCellHeight ?? 0) + locationTVHeight
                    }
                    rowHeight = cell.frame.width/2 + (locCellHeight ?? 0) + totalHeightToLeaveForTitleAndBottomBarInCell + cell.hashLabel.frame.size.height
                }  else {
                    cell.statusTextView.attributedText = mutStr
                    cell.statusTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
                    cell.statusTextView.isHidden = false
                    cell.coloredTextView.isHidden = true
                    cell.mapView.isHidden = true
                    cell.locationMarkerTextView.isHidden = true
                    cell.locationTextView.isHidden = true
                    cell.statusTextView.sizeToFit()
                    rowHeight = cell.statusTextView.frame.height + totalHeightToLeaveForTitleAndBottomBarInCell + cell.hashLabel.frame.size.height
                    if str == "" {
                        rowHeight = rowHeight! - cell.statusTextView.frame.height
                    }
                }
            }
            if cell.coloredTextView.isHidden {
                cell.previewView?.frame = CGRect(x: 0, y: rowHeight! - (120 + cell.hashLabel.frame.size.height), width: self.view.frame.size.width, height: 120)
                cell.playerView?.frame = CGRect(x: 0, y: rowHeight! - (110 + cell.hashLabel.frame.size.height), width: self.view.frame.size.width, height: fixedHeight)
            }
            var heightFactorForPreview: CGFloat?
            var postImage: UIImage?
            var hasAttachment = false
            if cell.coloredTextView.isHidden {
                var isImage = false
                if cell.attachments.count > 0 {
                    if cell.attachments[0]["type"] as? String == "doc" || cell.attachments[0]["type"] as? String == "aud" || (cell.attachments[0]["type"] as? String != "image" && cell.postImageName != "") {
                        isImage = false
                        hasAttachment = true
                        var isSomeOtherType = false
                        if cell.attachments[0]["type"] as? String != "image" && cell.postImageName != "" && cell.attachments[0]["type"] as? String != "doc" && cell.attachments[0]["type"] as? String != "aud" {
                            isSomeOtherType = true
                        }
                        let docPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("LocalData").appendingPathComponent(cell.attachments[0]["link"] as? String ?? "").path
                        
                        if FileManager.default.fileExists(atPath: docPath) {
                            if cell.pid != nil {
                                cellIsDownloading.removeValue(forKey: cell.pid!)
                            }
                        } else {
                            if !cellIsDownloading.keys.contains(cell.pid ?? "") {
                                cellIsDownloading[cell.pid!] = false
                            }
                        }
                        var thumbnailImage: UIImage?
                        if let thumbnailLink = cell.attachments[0]["thumbnail"] as? String {
                            thumbnailImage = Constants.sharedUtils.getImage(index: indexPath, isProfileImg: false, endPath: thumbnailLink)
                            if thumbnailImage?.images != nil {
                                thumbnailImage = nil
                            }
                        }
                        var attachmentSize: CGSize?
                        if thumbnailImage != nil {
                            attachmentSize = CGSize(width: self.view.frame.size.width - 20, height: 150)
                            cell.attachmentImageView?.frame.size = CGSize(width: attachmentSize!.width, height: 100)
                            cell.attachmentImageView?.contentMode = .top
                            cell.attachmentImageView?.contentMode = .scaleAspectFill
                            cell.downloadImageView?.frame = CGRect(x: (attachmentSize!.width/2 - 30), y: 35, width: 60, height: 30)
                        } else {
                            attachmentSize = CGSize(width: self.view.frame.size.width - 20, height: 80)
                            cell.attachmentImageView?.frame.size = CGSize(width: 80, height: attachmentSize!.height)
                            cell.attachmentImageView?.contentMode = .scaleAspectFit
                            cell.downloadImageView?.frame = CGRect(x: 10, y: 35, width: 60, height: 30)
                        }
                        if isSomeOtherType {
                            cell.attachmentImageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "brokenFileImage"))
                            cell.circularProgressView?.removeFromSuperview()
                            cell.downloadImageView?.removeFromSuperview()
                            cell.attachmentBaseView?.isUserInteractionEnabled = true
                        } else if cellIsDownloading[cell.pid ?? ""] == false {
                            cell.attachmentImageView?.image = thumbnailImage ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfGrayDummyImage"))
                            cell.downloadImageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "downloadImage"))
                            cell.attachmentBaseView?.isUserInteractionEnabled = true
                            cell.attachmentImageView?.addSubview(cell.downloadImageView!)
                        } else if cellIsDownloading[cell.pid ?? ""] == true {
                            cell.attachmentImageView?.image = thumbnailImage ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfGrayDummyImage"))
                            cell.downloadImageView?.image = nil
                            cell.downloadImageView?.removeFromSuperview()
                            if cell.attachmentImageView?.subviews.contains(cell.circularProgressView!) == false {
                                cell.circularProgressView?.center = cell.attachmentImageView!.center
                                cell.attachmentImageView?.addSubview(cell.circularProgressView!)
                            }
                        } else {
                            if cell.attachments[0]["type"] as? String == "aud" {
                                cell.attachmentImageView?.isHidden = true
                                cell.playPauseBtn?.isHidden = false
                                cell.slider?.isHidden = false
                                cell.progressLabel?.isHidden = false
                                cell.downloadImageView?.image = nil
                                cell.downloadImageView?.removeFromSuperview()
                                cell.attachmentImageView?.removeFromSuperview()
                            } else {
                                cell.attachmentImageView?.isHidden = false
                                cell.playPauseBtn?.isHidden = true
                                cell.slider?.isHidden = true
                                cell.progressLabel?.isHidden = true
                                cell.attachmentImageView?.image = thumbnailImage ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfDummyImage"))
                                cell.downloadImageView?.image = nil
                                cell.downloadImageView?.removeFromSuperview()
                            }
                            cell.circularProgressView?.removeFromSuperview()
                            cell.attachmentBaseView?.isUserInteractionEnabled = true
                        }
                        if !cell.mapView.isHidden {
                            cell.attachmentBaseView?.frame = CGRect(x: 10, y: 0, width: attachmentSize!.width, height: attachmentSize!.height)
                        } else {
                            if str == "" {
                                cell.attachmentBaseView?.frame = CGRect(x: 10, y: 0, width: attachmentSize!.width, height: attachmentSize!.height)
                            } else {
                                cell.attachmentBaseView?.frame = CGRect(x: 10, y: cell.statusTextView!.frame.height, width: attachmentSize!.width, height: attachmentSize!.height)
                            }
                        }
                        if isSomeOtherType {
                            cell.attachmentNameLabel?.text = Constants.errorMessages.fileFormatUnknownText
                        } else if let fileSize = cell.attachments[0]["size"] as? Int64 {
                            let countBytes = ByteCountFormatter()
                            countBytes.allowedUnits = [.useMB]
                            countBytes.countStyle = .file
                            var fileSizeString = countBytes.string(fromByteCount: Int64(fileSize))
                            if fileSizeString == "0 MB" {
                                countBytes.allowedUnits = [.useKB]
                                countBytes.countStyle = .file
                                fileSizeString = countBytes.string(fromByteCount: Int64(fileSize))
                            }
                            if thumbnailImage != nil {
                                let pdfAttachment = NSTextAttachment()
                                let pdfImg = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfDummyImage"))
                                pdfAttachment.image = pdfImg
                                pdfAttachment.bounds = CGRect(x: 0, y: cell.attachmentNameLabel!.font!.descender, width: 17, height: 17)
                                let pdfAttachmentStr = NSMutableAttributedString(attributedString: NSAttributedString(attachment: pdfAttachment))
                                pdfAttachmentStr.append(NSAttributedString(string: " " + fileSizeString + "\n" + (cell.attachments[0]["name"] as? String ?? "")))
                                cell.attachmentNameLabel?.attributedText = pdfAttachmentStr
                            } else {
                                cell.attachmentNameLabel?.attributedText = NSAttributedString(string: fileSizeString + "\n" + (cell.attachments[0]["name"] as? String ?? ""))
                            }
                        } else {
                            cell.attachmentNameLabel?.text = cell.attachments[0]["name"] as? String ?? ""
                        }
                        if cell.playPauseBtn?.isHidden == false && cell.attachmentImageView?.isHidden != false {
                            cell.attachmentNameLabel?.frame = CGRect(x: 90, y: 0, width: cell.attachmentBaseView!.frame.width - 100, height: 40)
                            cell.slider?.frame = CGRect(x: 90, y: 50, width: cell.attachmentBaseView!.frame.width - 120, height: 20)
                            cell.progressLabel?.frame = CGRect(x: 15, y: 50, width: cell.playPauseBtn!.frame.width, height: 20)
                        } else {
                            cell.attachmentNameLabel?.frame = CGRect(x: thumbnailImage == nil ? 90: 10, y: thumbnailImage == nil ? 10: 100, width: thumbnailImage == nil ? (cell.attachmentBaseView!.frame.width - 100): cell.attachmentBaseView!.frame.width, height: thumbnailImage == nil ? 60: 40)
                        }
                        cell.attachmentBackgroundView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: cell.attachmentBaseView?.frame.height ?? 120)
                        if cell.playPauseBtn?.isHidden == false && cell.attachmentImageView?.isHidden != false {
                            cell.attachmentBaseView?.addSubview(cell.playPauseBtn!)
                            cell.attachmentBaseView?.addSubview(cell.progressLabel!)
                            cell.attachmentBaseView?.addSubview(cell.slider!)
                            cell.attachmentBaseView?.removeGestureRecognizer(cell.tappedOnAttachment)
                        } else {
                            cell.attachmentBaseView?.addGestureRecognizer(cell.tappedOnAttachment)
                            if cell.playPauseBtn?.isHidden == false && cell.attachmentImageView?.isHidden != false {
                                cell.attachmentBaseView?.addSubview(cell.playPauseBtn!)
                                cell.attachmentBaseView?.addSubview(cell.progressLabel!)
                                cell.attachmentBaseView?.addSubview(cell.slider!)
                                cell.attachmentBaseView?.removeGestureRecognizer(cell.tappedOnAttachment)
                            } else {
                                cell.attachmentBaseView?.addGestureRecognizer(cell.tappedOnAttachment)
                                if cell.attachmentImageView != nil {
                                    cell.attachmentBaseView?.addSubview(cell.attachmentImageView!)
                                }
                            }
                        }
                        cell.attachmentBaseView?.addSubview(cell.attachmentNameLabel!)
                        if !cell.mapView.isHidden {
                            cell.attachmentBackgroundView?.addSubview(cell.attachmentBaseView!)
                            cell.mapView.addSubview(cell.attachmentBackgroundView!)
                            cell.mapView.frame.size = CGSize(width: cell.attachmentBackgroundView!.frame.width, height: cell.attachmentBackgroundView!.frame.height - 3)
                            if mutStr.string != "" {
                                cell.mapViewToDateLabelConstraint?.isActive = true
                                cell.mapViewToDateLabelConstraint?.constant = cell.locationTextView.frame.height + 10
                            }
                            cell.mapViewHeightConstraint?.constant = cell.attachmentBackgroundView!.frame.height - 3
                        } else {
                            cell.statusTextView.addSubview(cell.attachmentBaseView!)
                            let path = UIBezierPath(rect: CGRect(x:0, y: cell.statusTextView!.frame.height, width: cell.attachmentBaseView!.frame.width, height:cell.attachmentBaseView!.frame.height))
                            cell.statusTextView.textContainer.exclusionPaths = [path]
                        }
                    } else if cell.attachments[0]["type"] as? String == "image" {
                        isImage = true
                        cell.imagesCollectionView?.isHidden = false
                        cell.postImageView?.isHidden = true
                    } else if cell.postImageName != "" {
                        isImage = true
                    }
                } else if cell.postImageName != "" {
                    isImage = true
                }
                if isImage {
                    postImage = Constants.sharedUtils.getImage(index: indexPath, isProfileImg: false, endPath: cell.attachments.count > 0 ? (cell.attachments[0]["type"] as? String == "image" ? (cell.attachments[0]["link"] as? String ?? ""): cell.postImageName): cell.postImageName)
                    if cell.attachments.count == 0 && postImage != nil {
                        cell.postImageView?.isHidden = false
                        cell.imagesCollectionView?.isHidden = true
                    } else {
                        cell.postImageView?.isHidden = true
                        cell.imagesCollectionView?.isHidden = false
                        cell.setCollectionView(forRow: indexPath.row)
                    }
                    cell.postImageView?.image = postImage
                    if !cell.mapView.isHidden {
                        cell.postImageView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: fixedHeight)
                    } else {
                        if str == "" {
                            cell.postImageView?.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: fixedHeight)
                        } else {
                            cell.postImageView?.frame = CGRect(x: 0, y: cell.statusTextView!.frame.height, width: self.view.frame.size.width, height: fixedHeight)
                        }
                    }
                    var imgHeight: CGFloat?
                    if cell.imagesCollectionView?.isHidden == true || cell.attachments.count == 1 {
                        if postImage!.images != nil && cell.postImageView != nil {
                            cell.postImageView?.contentMode = .center
                        } else {
                            imgHeight = (cell.postImageView!.frame.width/postImage!.size.width) * postImage!.size.height
                            cell.postImageView?.contentMode = .scaleAspectFit
                        }
                    }
                    cell.postImageView?.frame = CGRect(x: cell.postImageView!.frame.origin.x, y: cell.postImageView!.frame.origin.y, width: cell.postImageView!.frame.width, height: imgHeight ?? fixedHeight)
                    cell.postImageView?.clipsToBounds = true
                    cell.imagesCollectionView?.frame = cell.postImageView!.frame
                    if cell.imagesCollectionView?.isHidden == false {
                        cell.postImageView!.image = nil
                    }
                    if !cell.mapView.isHidden {
                        cell.mapView.addSubview(cell.postImageView!)
                        cell.mapView.addSubview(cell.imagesCollectionView!)
                        cell.postImageView?.backgroundColor = UIColor.white
                        cell.mapView.frame.size = CGSize(width: cell.postImageView!.frame.width, height: cell.postImageView!.frame.height)
                        if mutStr.string != "" {
                            cell.mapViewToDateLabelConstraint?.isActive = true
                            cell.mapViewToDateLabelConstraint?.constant = cell.locationTextView.frame.height + 10
                        }
                        cell.mapViewHeightConstraint?.constant = cell.postImageView!.frame.height
                    } else {
                        cell.statusTextView.addSubview(cell.postImageView!)
                        cell.statusTextView.addSubview(cell.imagesCollectionView!)
                        let path = UIBezierPath(rect: CGRect(x:0, y: cell.statusTextView!.frame.height, width: cell.postImageView!.frame.width, height:cell.postImageView!.frame.height))
                        cell.statusTextView.textContainer.exclusionPaths = [path]
                    }
                }
                let set = Constants.sharedUtils.linkDetector(heightArr: heightArr, attachmentPresent: postImage != nil ? true:(hasAttachment == true ? true:false), str: str, mutStr: mutStr, orgStr: orgStr, cell: cell, index: indexPath.row, tableRowHeight: tableView.rowHeight)
                mutStr = set.mutableStr
                heightArr = set.heightArr
                if hasAttachment {
                    if !cell.mapView.isHidden {
                        rowHeight = rowHeight! - cell.frame.width/2
                        heightFactorForPreview = cell.attachmentBackgroundView!.frame.height
                    } else {
                        heightFactorForPreview = cell.attachmentBaseView!.frame.height
                    }
                } else if postImage != nil {
                    if !cell.mapView.isHidden {
                        rowHeight = rowHeight! - cell.frame.width/2
                    }
                    heightFactorForPreview = cell.postImageView!.frame.height
                } else {
                    if cell.previewView!.isHidden {
                        heightFactorForPreview = fixedHeight
                    } else {
                        heightFactorForPreview = 120
                    }
                    if set.whiteViewFrame != nil {
                        cell.cellBackgroundView.frame = set.whiteViewFrame!
                    }
                    if set.tableRowHeight != nil {
                        tableView.rowHeight = set.tableRowHeight!
                    }
                }
            }
            var attachmentPosted = false
            if postImage != nil {
                attachmentPosted = true
            }
            if attachments.count > 0 {
                attachmentPosted = true
            }
            tableView.rowHeight = heighArrSettingOnCellLoad(isStatusTV: !cell.statusTextView.isHidden, attachmentPosted: attachmentPosted, closeBtnHidden: cell.closeBtn.isHidden, index: indexPath.row, rowHeight: rowHeight!, extraHeight: heightFactorForPreview ?? 120)
            if self.heightArr.count > indexPath.row {
                cell.cellBackgroundView = cellBG(height: self.heightArr[indexPath.row])
            } else {
                cell.cellBackgroundView = cellBG(height: tableView.rowHeight)
            }
            if #available(iOS 13.0, *) {
                cell.backgroundColor = UIColor.systemBackground
            } else {
                cell.backgroundColor = UIColor.white
            }
            if cell.statusTextView.textContainer.exclusionPaths.count == 0 && cell.mapView.isHidden ==  true {
                if self.heightArr.count > indexPath.row  {
                    if self.heightArr[indexPath.row] != rowHeight! {
                        tableView.rowHeight = rowHeight!
                        self.heightArr[indexPath.row] = tableView.rowHeight
                        cell.cellBackgroundView.frame = CGRect(x: 0, y: 8, width: self.view.frame.size.width, height: tableView.rowHeight)
                    }
                }
            }
            cell.sendSubviewToBack(cell.greyedOutOverlay)
            cell.greyedOutOverlay.isHidden = true
            var usersProfileVerified:Bool?
            var name = ""
            var image: UIImage?
            var savedDict: MinimalContactDetails?
            if savedContactRefDict.keys.contains(tempDict.id ?? "") {
                savedDict = savedContactRefDict[tempDict.id ?? ""]?.minimalDetails
                name = savedDict != nil ? (savedDict!.profileVerified ? savedDict!.actualProfileName: savedDict!.name): (savedDict?.name ?? "")
                image = Constants.sharedUtils.getImage(isProfileImg: true, endPath: savedDict?.imageName ?? "")
            }
            if image == nil {
                image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))!
            }
            usersProfileVerified = savedDict?.profileVerified ?? false
            cell.profilePicImgVw.image = appDelegate.cropImage(img: image!)
            cell.nameLabel.text = name
            var time = tempDict.time
            //time = time.padding(toLength: 10, withPad: time, startingAt: 0)
            cell.time = time
            if time != "" {
                let dateFromString = time.dateFromISO8601
                if dateFromString != nil {
                    let calendar = NSCalendar.current
                    let datecomponents = calendar.dateComponents([.day, .month, .year, .hour, .minute, .second], from: Date(), to: dateFromString!)
                    
                    time = appDelegate.timeFromDateComponents(datecomponents: datecomponents, time: time, date: dateFromString!)
                } else if time != "" {
                    time.removeLast(10)
                }
            }
            
            if let verified = usersProfileVerified {
                if verified {
                    cell.contentView.addSubview(cell.verifyImageView)
                    cell.contentView.bringSubviewToFront(cell.verifyImageView)
                } else {
                    cell.verifyImageView.removeFromSuperview()
                }
            } else {
                cell.verifyImageView.removeFromSuperview()
            }
            time = time + " |  "
            cell.privacy = tempDict.privacy
            cell.privacyID = tempDict.privacyId
            cell.dateLabel.text = time

            let dateStr = NSMutableAttributedString(string: time)
            cell.dateLabel.attributedText = settingNameAttributedLabelTextForCell(dateStr: dateStr, time: time, privacy: cell.privacy ?? "Public", userId: tempDict.id ?? "")
            DispatchQueue.main.async {
                cell.dateLabel.frame = CGRect(x: cell.dateLabel.frame.origin.x, y: cell.dateLabel.frame.origin.y, width: UIScreen.main.bounds.width - 40, height: cell.dateLabel.frame.height)
            }
            if arrayForFeed.count <= indexPath.row {
                tableView.rowHeight = 0
                if heightArr.count > indexPath.row {
                    heightArr[indexPath.row] = 0
                }
            }
        } else {
            tableView.rowHeight = 0
            if heightArr.count > indexPath.row {
                heightArr[indexPath.row] = 0
            }
        }
//        for subview in cell.subviews {
//            subview.setNeedsLayout()
//            subview.setNeedsDisplay()
//        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if heightArr.count > indexPath.row {
            //tableView.estimatedRowHeight = heightArr[indexPath.row]
            return heightArr[indexPath.row]
        }
        return 0
    }
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        if heightArr.count > indexPath.row {
//            return heightArr[indexPath.row]
//        }
//        return 250
//    }

    //scroll view delegates
    override func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        if #available(iOS 11.0, *) {
            scrollView.delaysContentTouches = true
        }
        scrollView.canCancelContentTouches = false
        //isScrolling = false
        self.scrollViewLastContentOffset = scrollView.contentOffset.y
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if #available(iOS 11.0, *) {
            scrollView.delaysContentTouches = true
        }
        scrollView.canCancelContentTouches = false
        let currentY = CGFloat(scrollView.contentOffset.y)
        self.scrollViewLastContentOffset = currentY
        if (self.endOfPosts == false || self.endOfPosts == nil) && !self.networkCallOnProgress && appDelegate.reachability.isReachable {
            if arrayForFeed.count > 4 {
                if self.tableView.indexPathsForVisibleRows!.contains(IndexPath(row: self.arrayForFeed.count - 5, section: 0)) {
                    self.networkCall(isRefreshCall: false)
                } else if self.tableView.indexPathsForVisibleRows!.contains(IndexPath(row: self.arrayForFeed.count - 1, section: 0)) {
                    self.networkCall(isRefreshCall: false)
                }
            } else if self.tableView.indexPathsForVisibleRows!.contains(IndexPath(row: self.arrayForFeed.count - 1, section: 0)) {
                self.networkCall(isRefreshCall: false)
            }
            print("more")
        } else {
            if self.endOfPosts == true && !self.networkCallOnProgress && activityIndicator != nil {
                DispatchQueue.main.async {
                    if self.refreshControl?.isRefreshing == true {
                        self.refreshControl?.endRefreshing()
                    }
                }
                self.loaderInFooter(show: false)
            }
        }
    }
    
//    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
//        let nearestIndex = Int(CGFloat(targetContentOffset.pointee.x) / scrollView.bounds.size.width + 0.5)
//        let clampedIndex = max( min( nearestIndex, self.arrayForFeed.count - 1 ), 0 )
//        var xOffset = CGFloat(clampedIndex) * scrollView.bounds.size.width
//        xOffset = xOffset == 0.0 ? 1.0 : xOffset
//        targetContentOffset.pointee.x = xOffset
//    }
    
    func openDoc(cell: StatusFeedTableViewCell, link: String, name: String) {
        if let indexPath = self.tableView.indexPath(for: cell) {
            let docPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("LocalData").appendingPathComponent(link).path
            if FileManager.default.fileExists(atPath: docPath) {
                let url: URL! = URL(fileURLWithPath: docPath)
                if let fullVC = self.storyboard?.instantiateViewController(withIdentifier: "openAttachmentVC") as? OpenAttachmentViewController {
                    fullVC.url = url
                    fullVC.name = name
                    self.navigationController?.pushViewController(fullVC, animated: true)
                }
            } else {
                let urlString = Constants.otherLinks.mediaURL + link
                let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
                let attachment = Attachment(url: url!, index: indexPath.row)
                downloadService.startDownload(attachment)
                self.reloadCellOrTable(cell: cell, isDownloading: true)
            }
        }
    }

    
    //bookmark-down arrow tapped
    func chevronTapped(cell: StatusFeedTableViewCell, desc: String, pid: String, time: String, privacy: String, location: String, locationName: String, color_code: String, postImageName: String, privacyID: String, customContacts: [String], viewsCount: String, isBookmark: Bool, attachments: [[String: Any]]) {
        var hasAttachment = false
        if attachments.count > 0 {
            let type = attachments[0]["type"] as? String ?? ""
            if type == "doc" || type == "aud" {
                hasAttachment = true
            }
        }
        if isBookmark {
            let actionSheetController = UIAlertController()
            let cancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
                print("Cancel")
            }
            actionSheetController.addAction(cancelActionButton)
            
            let externalShareActionButton = UIAlertAction(title: Constants.otherConsts.shareExternalText, style: UIAlertAction.Style.default, handler: { (action) in
                self.callShareToExternal(cell: cell, postImageName: postImageName, location: location, desc: desc, attachments: attachments, isFromCheveron: isBookmark)
            })
            if postImageName != "" || location != "" || desc != "" || hasAttachment {
                actionSheetController.addAction(externalShareActionButton)
            }
            actionSheetController.popoverPresentationController?.sourceView = cell.bookmark.imageView
            self.present(actionSheetController, animated: true, completion: nil)
        } else {
            self.callShareToExternal(cell: cell, postImageName: postImageName, location: location, desc: desc, attachments: attachments, isFromCheveron: isBookmark)
        }
    }
    
    //delegate called to reload cell on tap - readMore/readLess
    func cellExpandOrReduce(pid:String, cell: StatusFeedTableViewCell) {
        if let indexpth = self.tableView.indexPath(for: cell) {
            DispatchQueue.main.async {
                if cell.showMore! {
                    self.cellShowMore[pid] = cell.showMore
                } else {
                    self.cellShowMore.removeValue(forKey: pid)
                }
                self.tableView.reloadRows(at: [indexpth], with: .automatic)
            }
        }
    }
    
    func callShareToExternal(cell: StatusFeedTableViewCell, postImageName: String, location: String, desc: String, attachments: [[String:Any]], isFromCheveron: Bool) {
        var shareItems: [Any]?
        struct type {
            static var isDoc = false
            static var isImage = false
            static var isAud = false
        }
        if attachments.count > 0 {
            type.isImage = attachments[0]["type"] as? String == "image" ? true: false
            type.isAud = attachments[0]["type"] as? String == "aud" ? true: false
            type.isDoc = attachments[0]["type"] as? String == "doc" ? true: false
        }
        if attachments.count > 0 {
            type.isImage = attachments[0]["type"] as? String == "image" ? true: false
            type.isAud = attachments[0]["type"] as? String == "aud" ? true: false
            type.isDoc = attachments[0]["type"] as? String == "doc" ? true: false
        }
        var proceed = true
        if type.isDoc || type.isAud {
            let link = attachments[0]["link"] as? String ?? ""
            if let urlReturned = self.checkIfDocDownoadedElseShowAsDownloading(attachments: attachments, cell: cell, link: link) {
                shareItems = [urlReturned]
            } else {
                proceed = false
            }
        }
        if shareItems == nil && proceed {
            if attachments.count > 0 {
                for attachment in attachments {
                    let link = attachment["link"] as? String ?? ""
                    let image = Constants.sharedUtils.getImage(index: cell.indexPath, isProfileImg: false, endPath: link)
//                    let returnedImageAfterWatermarking = Constants.sharedUtils.getWatermarkLayer(onImage: image)
//                    if shareItems == nil {
//                        shareItems = [returnedImageAfterWatermarking]
//                    } else {
//                        shareItems?.append(returnedImageAfterWatermarking)
//                    }
                    if shareItems == nil {
                        shareItems = [image]
                    } else {
                        shareItems?.append(image)
                    }
                }
            } else if location != "" {
                shareItems = [Constants.otherLinks.googleMapsLink + location] as [Any]
            }
            if desc != "" && postImageName == "" {
                let toShareText = ControllerUtils.shared.shareTextAlongWithDownloadTextAppended(userID: cell.userID ?? "", time: cell.time ?? "", loc: (shareItems != nil && location != "" && shareItems?.isEmpty == false) ? (shareItems![0] as? String ?? ""): nil, desc: desc)
                shareItems = [toShareText]
            }
        }
        if shareItems != nil {
            //Analytics.logEvent("share", parameters: ["id" : cell.userID ?? cell.nameLabel.text ?? ""])
            let activityVC = UIActivityViewController(activityItems: shareItems!, applicationActivities: nil)
            activityVC.view.backgroundColor = UIColor.white
            if isFromCheveron {
                activityVC.popoverPresentationController?.sourceView = cell.bookmark.imageView // so that iPads won't crash
            } else {
                activityVC.popoverPresentationController?.sourceView = cell.shareButton // so that iPads won't crash
            }
            activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
            }
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func checkIfDocDownoadedElseShowAsDownloading(attachments: [[String:Any]], cell: StatusFeedTableViewCell, link: String) -> URL? {
        let docPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("LocalData").appendingPathComponent(link).path
        if FileManager.default.fileExists(atPath: docPath) {
            var name = attachments[0]["name"] as? String ?? ""
            if name.components(separatedBy: ".").contains("pdf") == false && name.components(separatedBy: ".").contains("PDF") == false {
                name = name + ".pdf"
            }
            let url: URL! = URL(fileURLWithPath: docPath)
            let tempURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(name))
            do {
                let data = try Data(contentsOf: url!)
                try data.write(to: tempURL)
                return tempURL
            } catch {
            }
            return url
        } else {
            let urlString = Constants.otherLinks.mediaURL + link
            let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
            let attachment = Attachment(url: url!, index: self.tableView.indexPath(for: cell)?.row ?? 0)
            downloadService.startDownload(attachment)
            return nil
        }
    }
    
    func reloadCellOrTable(cell: StatusFeedTableViewCell, isDownloading: Bool) {
        if cell.pid != nil {
            cellIsDownloading[cell.pid!] = isDownloading
        }
        DispatchQueue.main.async {
            if let indexPath = self.tableView.indexPath(for: cell) {
                self.tableView.reloadRows(at: [indexPath], with: .automatic)
                self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
            } else {
                self.tableView.reloadData()
                self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
            }
        }
    }
    //open image in new vc
    func pushToVC(currentImagePath: String, imagePaths: [String], titleName: String, indexPath: IndexPath?) {
        if let fullVC = self.storyboard?.instantiateViewController(withIdentifier: "openImageVC") as? OpenImageViewController{
            fullVC.imagePaths = imagePaths
            if let index = imagePaths.firstIndex(of: currentImagePath) {
                fullVC.currentTappedImageIndex = index
            }
            fullVC.name = titleName
            fullVC.modalPresentationStyle = .fullScreen
            fullVC.navBarFrame = self.navigationController!.navigationBar.frame
            UIApplication.topViewController()?.present(fullVC, animated: true, completion: nil)
        }
    }
    
    //single post VC
    func openPostInSingleScreen(cell: StatusFeedTableViewCell) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "singlePostVc") as! SinglePostTableViewController
        if let indxpth = self.tableView.indexPath(for: cell), arrayForFeed.count > indxpth.row {
            vc.currentPost = arrayForFeed[indxpth.row]
            vc.indexPath = indxpth
        }
        vc.userDetails = savedContactRefDict[cell.userID ?? ""]?.minimalDetails
        vc.postOwnerName = cell.nameLabel.text ?? ""
        vc.profilePicImage = cell.profilePicImgVw?.image
        vc.savedTabbar = self.tabBarController
        vc.userId = cell.userID
        vc.cellTime = cell.time
        vc.dateAttributedString = cell.dateLabel.attributedText
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //update imgArr when like button state changed
    func updateLikePropertiesInDict(indexPath: IndexPath?, liked: Bool, likesArr: [String], likesArrCount: Int) {
        DispatchQueue.main.async {
            if let indexpath = indexPath, let cell = self.tableView.cellForRow(at: indexpath) as? StatusFeedTableViewCell {
                let dict = self.arrayForFeed[indexpath.row]
                dict.liked = liked
                dict.likesArray = likesArr
                self.arrayForFeed[indexpath.row] = dict
                cell.likeButton.isSelected = liked
                cell.likesArr = likesArr
                cell.likesArrCount = likesArrCount
                if Constants.clientSpecificConstantsStructName.showChatAndLikeBtns {
                    if likesArrCount > 0 {
                        cell.likesCountButton.setTitle("\(likesArrCount)"/* + likeOrLikestext*/, for: .normal)
                        cell.likesCountButton.isHidden = false
                    } else {
                        cell.likesCountButton.isHidden = true
                    }
                }
                Constants.sharedUtils.cacheDataToLocal(with: self.arrayForFeed, to: self.showOnlyBookmarkedItems == true ? "bookmarkedDict": self.id)
            }
        }
    }
    
    //URL delegates
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        guard let url = downloadTask.originalRequest?.url,
            let download = downloadService.activeDownloads[url]  else { return }
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite,
                                                  countStyle: .file)
        DispatchQueue.main.async {
            if let attachment = download.attachment, let cell = self.tableView.cellForRow(at: IndexPath(row: attachment.index, section: 0)) as? StatusFeedTableViewCell {
                cell.updateDisplay(progress: download.progress, totalSize: totalSize)
            }
        }
        
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        let download = downloadService.activeDownloads[sourceURL]
        downloadService.activeDownloads[sourceURL] = nil
        Constants.sharedUtils.saveDownloadedFile(tempLocURL: location, name: sourceURL.path.components(separatedBy: "/").last ?? "", directoryNamesString: sourceURL.path.replacingOccurrences(of: sourceURL.path.components(separatedBy: "/").last ?? "", with: ""), completion: { (success) in
            if success {
                DispatchQueue.main.async {
                    if let index = download?.attachment?.index {
                        if let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? StatusFeedTableViewCell {
                            self.reloadCellOrTable(cell: cell, isDownloading: false)
                        }
                    }
                }
            }
        })
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if sameTabTapped {
            DispatchQueue.main.async {
                self.tableView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
                self.tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .top)
            }
        } else {
            sameTabTapped = true
        }
    }
    
    func openParticularChatVC(userId: String, snippet: String) {
        #if AUTH
        let chatVC = UIStoryboard(name: "AuthRelated", bundle: nil).instantiateViewController(withIdentifier: "ChatingVC") as! ChatingViewController
        chatVC.displayName = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.name ?? Constants.clientSpecificConstantsStructName.appName
        chatVC.profilePicLink = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.imageName ?? ""
        chatVC.usersID = Constants.clientSpecificConstantsStructName.clientId
        chatVC.postSnippet = snippet
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(chatVC, animated: true)
        #endif
    }
    
    @objc func reloadTable(indexPath: IndexPath) {
        if showOnlyBookmarkedItems {
            let bookmarkedList = Constants.defaults.value(forKey: "bookmarkedList") as? [String] ?? []
            let post = arrayForFeed[indexPath.row]
            if arrayForFeed.count > indexPath.row, !bookmarkedList.contains(post.pid) {
                self.heightArr = []
                self.cellShowMore = [:]
                Constants.defaults.set(true, forKey: "shouldReloadQueue")
                arrayForFeed.remove(at: indexPath.row)
                if arrayForFeed.count == 0 {
                    self.navigationController?.popViewController(animated: true)
                } else {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                        self.tableView.setContentOffset(self.tableView.contentOffset, animated: false)
                    }
                }
            }
        }
    }
    //dp or name in cell tapped
    @objc func tappedToOpenProfileFromCell() {
        //for now
//        let imageVC = self.storyboard?.instantiateViewController(withIdentifier: "fullScreenImageVC") as! ImageFullScreenViewController
//        imageVC.image = Constants.sharedUtils.getImage(isProfileImg: true, endPath: Constants.defaults.value(forKey: "clientsProfilePic") as? String ?? "")
//        imageVC.about = self.about ?? Constants.otherConsts.aboutDefaultDescription
//        imageVC.name = "Profile"
//        imageVC.actualProfileName = self.actualProfileName
//        imageVC.userName = self.actualProfileName
//        imageVC.qkopyId = self.qkopyUsername
//        if self.navigationItem.rightBarButtonItems?.count == 2 {
//            imageVC.unknownUser = false
//        } else {
//            imageVC.unknownUser = true
//        }
//        imageVC.location = location
//        imageVC.joinedDateString = createdDateString
//        imageVC.profileVerified = self.profileVerified ?? false
//        imageVC.userPhoneNumber = self.phoneNumber
//        imageVC.fromProfileVC = false
//        imageVC.isPremium = self.isPremium ?? false
//        self.navigationController?.pushViewController(imageVC, animated: true)
    }

    //MARK: - Notifications
    //reachability
    @objc func checkForReachability(note: NSNotification) {
        let reachability = note.object as! Reachability
        if reachability.isReachable && !self.networkCallOnProgress {
            if (self.endOfPosts == false || self.endOfPosts == nil) {
                self.networkCall(isRefreshCall: false)
            } else if changedSelectionOfCategoriesRefreshed == false {
                self.networkCall(isRefreshCall: true)
            }
        }
    }
    
    //MARK: - Targets
    //name of nav tapped to scroll to top
    @objc func titleNameTapped() {
        //tappedToOpenProfileFromCell()
    }
    @objc func sideMenuTapped() {
        let actionSheetController = UIAlertController()
        let cancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)
        let shareActionBtn = UIAlertAction(title: Constants.otherConsts.shareText, style: UIAlertAction.Style.default, handler: { (action) in
            let textToShare = Constants.clientSpecificConstantsStructName.downloadAppText
            //if let myWebsite = NSURL(string: Constants.otherLinks.appstoreLink) {
            let objectsToShare = [textToShare] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.view.backgroundColor = UIColor.white
            activityVC.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
            activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
            }
            self.present(activityVC, animated: true, completion: nil)
        })
        actionSheetController.addAction(shareActionBtn)
        let aboutActionBtn = UIAlertAction(title: Constants.otherConsts.aboutText, style: UIAlertAction.Style.default, handler: { (action) in
            if let aboutVC = self.storyboard?.instantiateViewController(withIdentifier: "AboutVc") as? AboutViewController {
                self.navigationController?.pushViewController(aboutVC, animated: true)
            }
        })
        actionSheetController.addAction(aboutActionBtn)
        actionSheetController.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItems?[0]
        self.present(actionSheetController, animated: true, completion: nil)
    }
    //refreshed by pulling the table
    @objc func pulledToRefresh() {
        if !networkCallOnProgress && appDelegate.reachability.isReachable {
            networkCall(isRefreshCall: true)
        } else {
            refreshControl?.endRefreshing()
        }
    }
    
    @objc func callHelpline() {
        if let url = URL(string: "tel://\(Constants.clientSpecificConstantsStructName.helplineNumber))"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
    @objc func chatBtnTapped() {
        #if AUTH
        openParticularChatVC(userId: self.id, snippet: "")
        #endif
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
