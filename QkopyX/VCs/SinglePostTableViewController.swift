//
//  SinglePostTableViewController.swift
//  Qkopy
//
//  Created by Lincy George on 26/04/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import UIKit
import MisterFusion
import MapKit
//import FirebaseAnalytics
class SinglePostTableViewController: UITableViewController, gotoUserProfileDelegate, singleTvcHeaderDelegate, shareExternalFromFooterDelegate, URLSessionDelegate, goToChatDelegate {
    let downloadService = DownloadService()
    // Create downloadsSession here, to set self as delegate
    lazy var downloadsSession: URLSession = {
        //    let configuration = URLSessionConfiguration.default
        let configuration = URLSessionConfiguration.background(withIdentifier: "profileBgSessionConfiguration")
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()
    let fixedHeight = Constants.defaults.value(forKey: "sizeOfLimitedCharacters") as? CGFloat ?? 200
    let defaults = UserDefaults.standard
    var storyBoard = UIStoryboard(name: "Main", bundle: nil)
    var currentPost: PostObject?
    var postOwnerName: String?
    var dateAttributedString: NSAttributedString?
    var profilePicImage: UIImage?
    var userDetails: MinimalContactDetails?
    var userId: String?
    var savedTabbar: UITabBarController?
    var ownProfile: Bool = false
    var indexPath: IndexPath?
    var cellTime: String?
    var heightArr: [CGFloat] = []
    var cellIsDownloading: [IndexPath: Bool] = [:]
    var cellType: TypeOfCell = TypeOfCell(isDesc: 0, isColored: 0, isLoc: 0, isLocMarkerText: 0, isImage: [], isOtherAttachment: [])
    
    struct TypeOfCell {
        var isDesc: Int
        var isColored: Int
        var isLoc: Int
        var isLocMarkerText: Int
        var isImage: [Int]
        var isOtherAttachment: [Int]
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.value(forKey: "myID") as? String == userId {
            ownProfile = true
        }
        let headerVw = singlePostVcHeaderView().loadNib() as! singlePostVcHeaderView
        headerVw.profilePicImageView.image = profilePicImage
        headerVw.headerDelegate = self
        if userDetails?.profileVerified == true {
            headerVw.verifyImageView.isHidden = false
        } else {
            headerVw.verifyImageView.isHidden = true
        }
        headerVw.nameLabel.text = postOwnerName ?? ""
        headerVw.gotoUserDelegate = self
        headerVw.dateLabel.attributedText = dateAttributedString
        self.tableView.tableHeaderView = headerVw
        
        let footerVw = singlePostVcFooterView(frame: CGRect(x: 0, y: 3, width: self.view.frame.width, height: 50)).loadNib() as! singlePostVcFooterView
        let likesArr = currentPost?.likesArray ?? []
        footerVw.pid = currentPost?.pid
        footerVw.ownProfile = ownProfile
        footerVw.shareExternalDelegate = self
        footerVw.indexPath = indexPath
        footerVw.currentDict = currentPost
        footerVw.chatVCDelegate = self
        footerVw.likesCountBtnOutlet.setTitle("\(likesArr.count)", for: .normal)
        if likesArr.count > 0 {
            footerVw.likesCountBtnOutlet.isHidden = false
        } else {
            footerVw.likesCountBtnOutlet.isHidden = true
        }

        footerVw.likesArr = likesArr
        footerVw.likesArrCount = likesArr.count
        let numberOfViews = currentPost?.viewsCount ?? "0"
        if numberOfViews == "1" {
            footerVw.viewsCountLabel.text = numberOfViews + Constants.otherConsts.viewText
        } else {
            footerVw.viewsCountLabel.text = numberOfViews + Constants.otherConsts.viewsText
        }
        self.tableView.tableFooterView = footerVw
        titleBarSetting()
        tableView.register(UINib(nibName: "singlePostTvcCell", bundle: nil), forCellReuseIdentifier: "singlePostTvcCell")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
    }
    //MARK: - View setup
    //nav bar setting
    func titleBarSetting() {
        self.navigationController?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        } else {
            // Fallback on earlier versions
        }
    }

    func linkDetector(showPreview: String, orgStr: String, mutStr: NSMutableAttributedString, cell: singlePostTvcCell, index: Int, tableRowHeight: CGFloat) -> (mutableStr: NSMutableAttributedString, whiteViewFrame: CGRect?, tableRowHeight: CGFloat?) {
        var lastDetectedURLString: String?
        var httpRemovedString: String?
        var whiteViewframe: CGRect?
        var rowHeight: CGFloat?
        var orgStrVar = orgStr
        var rangeOfEntireString = NSRange(location: 0, length: orgStrVar.count)
        let replacements = ControllerUtils.shared.createHighlightPatterns()
        for (pattern, attributes) in replacements {
            do {
                let regex = try NSRegularExpression(pattern: pattern)
                regex.enumerateMatches(in: orgStrVar, range: rangeOfEntireString) {
                    match, flags, stop in
                    // apply the style
                    if let matchRange = match?.range(at: 1) {
                        print("Matched pattern: \(pattern)")
                        mutStr.addAttributes(attributes, range: matchRange)
                        mutStr.deleteCharacters(in: NSRange(location: matchRange.location - 1, length: 1))
                        mutStr.deleteCharacters(in: NSRange(location: matchRange.location + matchRange.length - 1, length: 1))
                        orgStrVar = mutStr.string
                        rangeOfEntireString = NSRange(location: 0, length: orgStrVar.count)
                    }
                }
            }
            catch {
                print("An error occurred attempting to locate pattern: " +
                    "\(error.localizedDescription)")
            }
        }
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let block = { (result: NSTextCheckingResult?, flags: NSRegularExpression.MatchingFlags, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                if let result = result, result.resultType == NSTextCheckingResult.CheckingType.link {
                    let range = orgStrVar.range(of: result.url!.absoluteString)
                    httpRemovedString = result.url!.absoluteString
                    lastDetectedURLString = nil
                    if range != nil {
                    } else if httpRemovedString!.hasPrefix("mailto:") {
                        httpRemovedString = ""
                    }  else {
                        httpRemovedString = httpRemovedString?.replacingOccurrences(of: "http://", with: "")
                        httpRemovedString = httpRemovedString?.replacingOccurrences(of: "https://", with: "")
                    }
                    if lastDetectedURLString == nil && showPreview == "true" && httpRemovedString != "" {
                        lastDetectedURLString = httpRemovedString
                    }
                }
            }
            detector.enumerateMatches(in: orgStrVar, options: [], range: rangeOfEntireString, using: block)
            if lastDetectedURLString != nil {
                cell.previewLinkURLString = lastDetectedURLString!
                let url = URL(string: cell.previewLinkURLString!)
                if UIApplication.shared.canOpenURL(url!) {
                    if let link = ControllerUtils.shared.extractYoutubeIdFromLink(link: lastDetectedURLString!) {
                        let playerVars = ["loop": 1, "playsinline": 1, "showinfo": 0, "modestbranding": 1, "rel": 0, "origin" : "https://www.qkopy.com"] as [String : Any]
                        cell.playerView?.load(withVideoId: link, playerVars: playerVars)
                        cell.playerView?.isHidden = false
                        cell.previewView?.isHidden = true
                    } else {
                        cell.previewView?.addLayoutSubview( cell.embeddedView!, andConstraints: [ cell.embeddedView!.top |+| 8, cell.embeddedView!.right |-| 2, cell.embeddedView!.left |+| 2, cell.embeddedView!.bottom |-| 0])
                        cell.previewView?.isHidden = false
                        cell.playerView?.isHidden = true
                        mutStr.append(NSAttributedString(string: "\n"))
                        cell.txtVw!.frame = CGRect(x: cell.txtVw!.frame.minX, y: cell.txtVw!.frame.minY, width: cell.txtVw!.frame.width, height: cell.txtVw!.frame.height + 120)
                        whiteViewframe = CGRect(x: 0, y: 8, width: self.view.frame.size.width, height: cell.txtVw!.frame.height)
                        rowHeight = cell.txtVw!.frame.height
                    }
                    var pathView: UIView?
                    if cell.previewView!.isHidden {
                        pathView = cell.playerView
                    } else {
                        pathView = cell.previewView
                    }
                    let path = UIBezierPath(rect: CGRect(x:0, y: cell.txtVw!.frame.height + 20, width: pathView!.frame.width, height:pathView!.frame.height))
                    cell.txtVw.textContainer.exclusionPaths = [path]
                    if cell.previewView!.isHidden {
                        cell.txtVw.addSubview(cell.playerView!)
                        rowHeight = cell.txtVw!.frame.height + pathView!.frame.height
                    } else {
                        cell.embeddedView?.load(urlString: cell.previewLinkURLString!)
                        cell.txtVw.addSubview(cell.previewView!)
                    }
                    cell.txtVw.bringSubviewToFront(cell.txtVw.subviews.last!)
                    cell.embeddedView?.didTapHandler = { embeddedView, URL in
                        guard let URL = URL else { return }
                        UIApplication.shared.openURL(URL)
                    }
                }
            }
            cell.txtVw.dataDetectorTypes = [.link, .phoneNumber]
            //mutStr.addAttribute(NSAttributedString.Key.font, value: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0) , range: NSRange(location: 0, length: (str.count)))
            let linkAttributes: [NSAttributedString.Key : Any] = [
                NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))]
//            if cell.txtVw.isHidden == true {
//                cell.locationTextView.linkTextAttributes = linkAttributes
//                cell.locationTextView.attributedText = mutStr
//                cell.locationTextView.isHidden = false
//            } else {
                cell.txtVw.linkTextAttributes = linkAttributes
                cell.txtVw.attributedText = mutStr
                cell.txtVw.isHidden = false
//            }
        } catch {
            print(error)
        }
        return (mutStr, whiteViewframe, rowHeight)
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        var numberOfRows = 0
        if let nonNilPostObject = currentPost {
            if nonNilPostObject.colorCode == "" {
                if nonNilPostObject.desc != "" {
                    cellType.isDesc = numberOfRows
                    numberOfRows += 1
                } else {
                    cellType.isDesc = -1
                }
                cellType.isColored = -4
            } else {
                if nonNilPostObject.desc != "" {
                    cellType.isColored = numberOfRows
                    numberOfRows += 1
                } else {
                    cellType.isColored = -1
                }
                cellType.isDesc = -4
            }
            if nonNilPostObject.location != "" {
                cellType.isLoc = numberOfRows
                numberOfRows += 1
                cellType.isLocMarkerText = numberOfRows
                numberOfRows += 1
            } else {
                cellType.isLoc = -2
                cellType.isLocMarkerText = -3
            }
            if nonNilPostObject.attachments.count > 0 {
                if nonNilPostObject.attachments[0]["type"] as? String == "image" {
                    for i in 0...(nonNilPostObject.attachments.count - 1) {
                        if !cellType.isImage.contains(i + numberOfRows) {
                            cellType.isImage.append(i + numberOfRows)
                        }
                    }
                } else {
                    for i in 0...(nonNilPostObject.attachments.count - 1) {
                        if !cellType.isOtherAttachment.contains(i + numberOfRows) {
                            cellType.isOtherAttachment.append(i + numberOfRows)
                        }
                    }
                }
                numberOfRows += nonNilPostObject.attachments.count
            }
        }
        return numberOfRows
    }
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "singlePostTvcCell") as! singlePostTvcCell
        cell.imgVw.isHidden = true
        cell.txtVw.isHidden = true
        cell.mapView.isHidden = true
        cell.playerView?.removeFromSuperview()
        if #available(iOS 13.0, *) {
            cell.backgroundColor = UIColor.systemBackground
            cell.imgVw.backgroundColor = UIColor.systemGroupedBackground
        } else {
            cell.backgroundColor = UIColor.white
            cell.imgVw.backgroundColor = UIColor.groupTableViewBackground
        }
        if let tempDict = currentPost {
            let paragraphStyle: NSMutableParagraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = 20.0
            paragraphStyle.maximumLineHeight = 20.0
            paragraphStyle.minimumLineHeight = 20.0
            switch indexPath.row {
            case cellType.isDesc:
                cell.txtVw.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 100)
                let str = tempDict.desc
                var attributes: [NSAttributedString.Key: AnyObject]!
                if #available(iOS 13.0, *) {
                    attributes = [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0), NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.foregroundColor: UIColor.label]
                } else {
                    attributes = [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0), NSAttributedString.Key.paragraphStyle: paragraphStyle, NSAttributedString.Key.foregroundColor: UIColor.black]
                }
                var mutStr = NSMutableAttributedString(string: str, attributes: attributes)
                cell.txtVw.attributedText = mutStr
                cell.txtVw.isScrollEnabled = false
                cell.txtVw.sizeToFit()
                cell.txtVw.isHidden = false
                tableView.rowHeight = cell.txtVw.frame.height
                cell.previewView?.frame = CGRect(x: 0, y: cell.txtVw.frame.height, width: self.view.frame.size.width, height: 120)
                cell.playerView?.frame = CGRect(x: 0, y: cell.txtVw.frame.height, width: self.view.frame.size.width, height: fixedHeight)
                let set = linkDetector(showPreview: tempDict.showPreview, orgStr: str, mutStr: mutStr, cell: cell, index: indexPath.row, tableRowHeight: tableView.rowHeight)
                mutStr = set.mutableStr
                tableView.rowHeight = (set.tableRowHeight ?? tableView.rowHeight)
            case cellType.isColored:
                let str = tempDict.desc
                cell.tag = 0
                let attributes = [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0), NSAttributedString.Key.paragraphStyle: paragraphStyle]
                let mutStr = NSMutableAttributedString(string: str, attributes: attributes)
                cell.txtVw.attributedText = mutStr
                cell.txtVw.isHidden = false
                cell.txtVw.backgroundColor = appDelegate.hexStringToUIColor(hex: tempDict.colorCode)
                cell.txtVw.textAlignment = .center
                cell.txtVw.textColor = UIColor.white
                cell.txtVw.textContainerInset = UIEdgeInsets.init(top: 0, left: 50.0, bottom: 0, right: 50.0)
                cell.txtVw.sizeToFit()
                var gap: CGFloat = 20
                let fixedHeight = Constants.defaults.value(forKey: "sizeOfLimitedCharacters") as? CGFloat ?? 200
                if fixedHeight - cell.txtVw.frame.height > 0 {
                    gap = (fixedHeight - cell.txtVw.frame.height)/2
                }
                cell.txtVw.textContainerInset = UIEdgeInsets.init(top: gap, left: 50.0, bottom: gap, right: 50.0)
                cell.txtVw.centerVertically()
                cell.txtVw.sizeToFit()
                if fixedHeight - cell.txtVw.frame.height > 0 {
                    tableView.rowHeight = fixedHeight
                } else {
                    tableView.rowHeight = cell.txtVw.frame.height
                }
            case cellType.isLocMarkerText:
                cell.txtVw.frame = CGRect(x: 0, y: 10, width: self.tableView.frame.width - 20, height: 100)
                cell.txtVw.tag = 1
                cell.txtVw.textContainerInset = UIEdgeInsets.init(top: 5, left: 0, bottom: 5, right: 0)
                let latitude = tempDict.location.components(separatedBy: ",")[0]
                let longitude = tempDict.location.components(separatedBy: ",")[1]
                let locationName = tempDict.location.replacingOccurrences(of: (latitude + "," + longitude + ","), with: "")
                cell.locationName = locationName
                let locAttachment = NSTextAttachment()
                let img = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "mapIconGray"))
                locAttachment.image = img
                locAttachment.bounds = CGRect(x: 0, y: cell.txtVw.font!.descender, width: 17, height: 17)
                let locAttachmentStr = NSMutableAttributedString(attributedString: NSAttributedString(attachment: locAttachment))
                let locStr = NSAttributedString(string: locationName, attributes: [NSAttributedString.Key.font: UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0), NSAttributedString.Key.foregroundColor: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))])
                locAttachmentStr.append(locStr)
                locAttachmentStr.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: locStr.string.count))
                cell.txtVw.attributedText = locAttachmentStr
                cell.txtVw.isScrollEnabled = false
                cell.txtVw.sizeToFit()
                cell.txtVw.isHidden = false
                cell.addSubview(cell.txtVw)
                tableView.rowHeight = cell.txtVw.frame.height
                
            case cellType.isLoc:
                cell.mapView.isHidden = false
                let latitude = currentPost!.location.components(separatedBy: ",")[0]
                let longitude = currentPost!.location.components(separatedBy: ",")[1]
                let locationName = tempDict.location.replacingOccurrences(of: (latitude + "," + longitude + ","), with: "")
                let locationCoordinate = CLLocationCoordinate2D(latitude: Double(latitude)!, longitude:  Double(longitude)!)
                cell.mapView.frame = CGRect(x: 0, y: 10, width: self.view.frame.width, height: self.view.frame.width/2)
                cell.locationName = locationName
                cell.updateLocation(location: locationCoordinate)
                tableView.rowHeight = cell.mapView.frame.height
            case cellType.isImage.count > 0 ? cellType.isImage.first!...cellType.isImage.last!: -6...(-6):
                cell.imgVw.frame = CGRect(x: 0, y: 3, width: self.tableView.frame.width, height: 100)
                let img = ControllerUtils.shared.getImage(index: indexPath, isProfileImg: false, endPath: tempDict.attachments[indexPath.row -  cellType.isImage.first!]["link"] as! String)
                if img.images == nil {
                    cell.imgVw.image = img
                } else {
                    cell.imgVw.image = nil
                }
                let imgHeight = cell.imgVw.image == nil ? fixedHeight: ((cell.imgVw.frame.width/cell.imgVw.image!.size.width) * cell.imgVw.image!.size.height)
                cell.imgVw.contentMode = .scaleAspectFit
                cell.imgVw.frame = CGRect(x: cell.imgVw.frame.origin.x, y: cell.imgVw.frame.origin.y, width: cell.imgVw.frame.width, height: imgHeight)
                cell.imgVw.clipsToBounds = true
                cell.addSubview(cell.imgVw)
                cell.imgVw.isHidden = false
                if #available(iOS 13.0, *) {
                    cell.backgroundColor = UIColor.systemGray5
                } else {
                    cell.backgroundColor = UIColor(red:0.87, green:0.87, blue:0.87, alpha:1.0)
                }
                tableView.rowHeight = imgHeight + ((indexPath.row -  cellType.isImage.last! == 0) ? 6 : 3)
            case cellType.isOtherAttachment.count > 0 ? (cellType.isOtherAttachment.first!)...(cellType.isOtherAttachment.last!): -7...(-7):
                var thumbnailImage: UIImage?
                let attachment = tempDict.attachments[indexPath.row -  cellType.isOtherAttachment.first!]
                if let thumbnailLink = attachment["thumbnail"] as? String {
                    thumbnailImage = Constants.sharedUtils.getImage(index: indexPath, isProfileImg: false, endPath: thumbnailLink)
                    if thumbnailImage?.images != nil {
                        thumbnailImage = nil
                    }
                }
                var attachmentSize: CGSize?
                if thumbnailImage != nil {
                    attachmentSize = CGSize(width: self.view.frame.size.width - 20, height: 150)
                    cell.attachmentImageView?.frame.size = CGSize(width: attachmentSize!.width, height: 100)
                    cell.attachmentImageView?.contentMode = .top
                    cell.attachmentImageView?.contentMode = .scaleAspectFill
                    cell.downloadImageView?.frame = CGRect(x: (attachmentSize!.width/2 - 30), y: 35, width: 60, height: 30)
                } else {
                    attachmentSize = CGSize(width: self.view.frame.size.width - 20, height: 80)
                    cell.attachmentImageView?.frame.size = CGSize(width: 80, height: attachmentSize!.height)
                    cell.attachmentImageView?.contentMode = .scaleAspectFit
                    cell.downloadImageView?.frame = CGRect(x: 10, y: 35, width: 60, height: 30)
                }
                var isSomeOtherType = false
                if attachment["type"] as? String != "image" && attachment["type"] as? String != "doc" {
                    isSomeOtherType = true
                }
                if isSomeOtherType {
                    cell.attachmentImageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "brokenFileImage"))
                    cell.circularProgressView?.removeFromSuperview()
                    cell.downloadImageView?.removeFromSuperview()
                    cell.attachmentBaseView?.isUserInteractionEnabled = true
                } else if cellIsDownloading[indexPath] == false {
                    cell.attachmentImageView?.image = thumbnailImage ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfGrayDummyImage"))
                    cell.downloadImageView?.image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "downloadImage"))
                    cell.attachmentBaseView?.isUserInteractionEnabled = true
                    cell.attachmentImageView?.addSubview(cell.downloadImageView!)
                } else if cellIsDownloading[indexPath] == true {
                    cell.attachmentImageView?.image = thumbnailImage ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfGrayDummyImage"))
                    cell.downloadImageView?.image = nil
                    cell.downloadImageView?.removeFromSuperview()
                    if cell.attachmentImageView?.subviews.contains(cell.circularProgressView!) == false {
                        cell.circularProgressView?.center = cell.attachmentImageView!.center
                        cell.attachmentImageView?.addSubview(cell.circularProgressView!)
                    }
                } else {
                    cell.attachmentImageView?.isHidden = false
                    cell.attachmentImageView?.image = thumbnailImage ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfDummyImage"))
                    cell.downloadImageView?.image = nil
                    cell.downloadImageView?.removeFromSuperview()
                    cell.circularProgressView?.removeFromSuperview()
                    cell.attachmentBaseView?.isUserInteractionEnabled = true
                }
                if isSomeOtherType {
                    cell.attachmentNameLabel?.text = Constants.errorMessages.fileFormatUnknownText
                } else if let fileSize = attachment["size"] as? Int64 {
                    let countBytes = ByteCountFormatter()
                    countBytes.allowedUnits = [.useMB]
                    countBytes.countStyle = .file
                    var fileSizeString = countBytes.string(fromByteCount: Int64(fileSize))
                    if fileSizeString == "0 MB" {
                        countBytes.allowedUnits = [.useKB]
                        countBytes.countStyle = .file
                        fileSizeString = countBytes.string(fromByteCount: Int64(fileSize))
                    }
                    if thumbnailImage != nil {
                        let pdfAttachment = NSTextAttachment()
                        let pdfImg = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfDummyImage"))
                        pdfAttachment.image = pdfImg
                        pdfAttachment.bounds = CGRect(x: 0, y: cell.attachmentNameLabel!.font!.descender, width: 17, height: 17)
                        let pdfAttachmentStr = NSMutableAttributedString(attributedString: NSAttributedString(attachment: pdfAttachment))
                        pdfAttachmentStr.append(NSAttributedString(string: " " + fileSizeString + "\n" + (attachment["name"] as? String ?? "")))
                        cell.attachmentNameLabel?.attributedText = pdfAttachmentStr
                    } else {
                        cell.attachmentNameLabel?.attributedText = NSAttributedString(string: fileSizeString + "\n" + (attachment["name"] as? String ?? ""))
                    }
                } else {
                    cell.attachmentNameLabel?.text = attachment["name"] as? String ?? ""
                }
                cell.attachmentBaseView?.frame = CGRect(x: 10, y: 10, width: attachmentSize!.width, height: attachmentSize!.height)
                cell.attachmentNameLabel?.frame = CGRect(x: thumbnailImage == nil ? 90: 10, y: thumbnailImage == nil ? 10: 110, width: thumbnailImage == nil ? (cell.attachmentBaseView!.frame.width - 100): cell.attachmentBaseView!.frame.width, height: thumbnailImage == nil ? 60: 40)
                cell.attachmentImageView?.addSubview(cell.downloadImageView!)
                cell.attachmentBaseView?.addSubview(cell.attachmentImageView!)
                cell.attachmentBaseView?.addSubview(cell.attachmentNameLabel!)
                cell.addSubview(cell.attachmentBaseView!)
                tableView.rowHeight = cell.attachmentBaseView!.frame.height + 20
            default: tableView.rowHeight = 0
            }
        }
        // Configure the cell...
        if heightArr.count > indexPath.row {
            heightArr[indexPath.row] = tableView.rowHeight
        }
        return cell
     }

    //MARK: Other Delegates
    func tappedToOpenProfileFromSinglePostView() {
        //for now
//        let imageVC = self.storyboard?.instantiateViewController(withIdentifier: "fullScreenImageVC") as! ImageFullScreenViewController
//        imageVC.image = profilePicImage
//        imageVC.about = Constants.defaults.value(forKey: "clientsAbout") as? String ?? Constants.otherConsts.aboutDefaultDescription
//        imageVC.name = "Profile"
//        imageVC.actualProfileName = self.postOwnerName
//        imageVC.userName = self.postOwnerName
//        imageVC.qkopyId = userDetails?.qkopyId ?? ""
//        if self.navigationItem.rightBarButtonItems?.count == 2 {
//            imageVC.unknownUser = false
//        } else {
//            imageVC.unknownUser = true
//        }
//        imageVC.location = Constants.defaults.value(forKey: "clientsLocation") as? [String: Any]
//        imageVC.joinedDateString = Constants.defaults.value(forKey: "clientsCreatedTime") as? String ?? ""
//        imageVC.profileVerified = userDetails?.profileVerified ?? false
//        imageVC.userPhoneNumber = userDetails?.number
//        imageVC.fromProfileVC = false
//        imageVC.isPremium = userDetails?.isPremium ?? false
//        self.navigationController?.pushViewController(imageVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if cellType.isImage.contains(indexPath.row) {
            var imagePaths: [String] = []
            let currentImagePath = currentPost?.attachments[indexPath.row -  cellType.isImage.first!]["link"] as! String
            for dict in (currentPost?.attachments ?? []) {
                let imgPath = dict["link"] as! String
                if !imagePaths.contains(imgPath) {
                    imagePaths.append(imgPath)
                }
            }
            pushToVC(currentImagePath: currentImagePath, imagePaths: imagePaths, titleName: postOwnerName ?? "", indexPath: indexPath)
        } else if cellType.isOtherAttachment.contains(indexPath.row) {
            if let tempDict = currentPost {
                let attachment = tempDict.attachments[indexPath.row -  cellType.isOtherAttachment.first!]
                let link = attachment["link"] as? String ?? ""
                let name = attachment["name"] as? String ?? ""
                openDoc(indexPath: indexPath, link: link, name: name)
            }
        }
    }
    
    func openDoc(indexPath: IndexPath, link: String, name: String) {
        if let _ = self.tableView.cellForRow(at: indexPath) {
            let docPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("LocalData").appendingPathComponent(link).path
            if FileManager.default.fileExists(atPath: docPath) {
                let url: URL! = URL(fileURLWithPath: docPath)
                if let fullVC = self.storyBoard.instantiateViewController(withIdentifier: "openAttachmentVC") as? OpenAttachmentViewController {
                    fullVC.url = url
                    fullVC.name = name
                    self.navigationController?.pushViewController(fullVC, animated: true)
                }
            } else {
                let urlString = Constants.otherLinks.mediaURL + link
                let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
                let attachment = Attachment(url: url!, index: indexPath.row)
                downloadService.startDownload(attachment)
                self.reloadCellOrTable(indexPath: indexPath, isDownloading: true)
            }
        }
    }

    //open image in new vc
    func pushToVC(currentImagePath: String, imagePaths: [String], titleName: String, indexPath: IndexPath?) {
        if let fullVC = self.storyBoard.instantiateViewController(withIdentifier: "openImageVC") as? OpenImageViewController{
            fullVC.imagePaths = imagePaths
            if let index = imagePaths.firstIndex(of: currentImagePath) {
                fullVC.currentTappedImageIndex = index
            }
            fullVC.name = titleName
            fullVC.modalPresentationStyle = .overCurrentContext
            fullVC.navBarFrame = self.navigationController!.navigationBar.frame
            UIApplication.topViewController()?.present(fullVC, animated: true, completion: nil)
        }
    }
    
    func bookmarkTapped(source: UIView) {
        var bookmarkedList = Constants.defaults.value(forKey: "bookmarkedList") as? [String] ?? []
        if currentPost != nil && currentPost?.pid != "" && currentPost?.pid != nil {
            if let index = bookmarkedList.firstIndex(of: currentPost!.pid) {
                bookmarkedList.remove(at: index)
            } else {
                bookmarkedList.append(currentPost!.pid)
            }
            Constants.defaults.setValue(bookmarkedList, forKey: "bookmarkedList")
        }
//        var hasAttachment = false
//        let attachments = currentPost?.attachments ?? []
//        if attachments.count > 0 {
//            let type = attachments[0]["type"] as? String ?? ""
//            if type == "doc" || type == "aud" {
//                hasAttachment = true
//            }
//        }
//        let actionSheetController = UIAlertController()
//        let cancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
//            print("Cancel")
//        }
//        actionSheetController.addAction(cancelActionButton)
//        let externalShareActionButton = UIAlertAction(title: Constants.otherConsts.shareExternalText, style: UIAlertAction.Style.default, handler: { (action) in
//            self.sharePostToExternal(source: source)
//        })
//        if currentPost?.postFileName != "" || currentPost?.location != "" || currentPost?.desc != "" || hasAttachment {
//            actionSheetController.addAction(externalShareActionButton)
//        }
//        actionSheetController.popoverPresentationController?.sourceView = (tableView.tableHeaderView as? singlePostVcHeaderView)?.bookmarkButtonOutlet ?? tableView.tableHeaderView!
//        self.present(actionSheetController, animated: true, completion: nil)
    }

    func sharePostToExternal(source: UIView) {
        var shareItems: [Any]?
        struct type {
            static var isDoc = false
            static var isImage = false
            static var isAud = false
        }
        var location = ""
        if currentPost?.location.components(separatedBy: ",").count == 2 {
            let latitude = currentPost?.location.components(separatedBy: ",")[0] ?? ""
            let longitude = currentPost?.location.components(separatedBy: ",")[1] ?? ""
            location = "\(latitude),\(longitude)"
        }
        let attachments = currentPost?.attachments ?? []
        if attachments.count > 0 {
            type.isImage = attachments[0]["type"] as? String == "image" ? true: false
            type.isAud = attachments[0]["type"] as? String == "aud" ? true: false
            type.isDoc = attachments[0]["type"] as? String == "doc" ? true: false
        }
        if userDetails?.qkopyId != "" && userDetails?.qkopyId != nil {
            let toShareText = ControllerUtils.shared.shareTextAlongWithDownloadTextAppended(userID: userId ?? "", time: cellTime ?? "", loc: location != "" ? (Constants.otherLinks.googleMapsLink + location): nil, desc: currentPost?.desc ?? "")
            shareItems = [toShareText]
        } else {
            if attachments.count > 0 {
                type.isImage = attachments[0]["type"] as? String == "image" ? true: false
                type.isAud = attachments[0]["type"] as? String == "aud" ? true: false
                type.isDoc = attachments[0]["type"] as? String == "doc" ? true: false
            }
            var proceed = true
            if type.isDoc || type.isAud {
                let link = attachments[0]["link"] as? String ?? ""
                if let urlReturned = self.checkIfDocDownoadedElseShowAsDownloading(attachments: attachments, link: link) {
                    shareItems = [urlReturned]
                } else {
                    proceed = false
                }
            }
            if shareItems == nil && proceed {
                if attachments.count > 0 {
                    for attachment in attachments {
                        let link = attachment["link"] as? String ?? ""
                        let image = Constants.sharedUtils.getImage(isProfileImg: false, endPath: link)
//                        let returnedImageAfterWatermarking = Constants.sharedUtils.getWatermarkLayer(onImage: image)
//                        if shareItems == nil {
//                            shareItems = [returnedImageAfterWatermarking]
//                        } else {
//                            shareItems?.append(returnedImageAfterWatermarking)
//                        }
                        if shareItems == nil {
                            shareItems = [image]
                        } else {
                            shareItems?.append(image)
                        }
                    }
                } else if location != "" {
                    shareItems = [Constants.otherLinks.googleMapsLink + location] as [Any]
                }
                if currentPost?.desc != "" && currentPost?.postFileName == "" {
                    let toShareText = ControllerUtils.shared.shareTextAlongWithDownloadTextAppended(userID: userId ?? "", time: cellTime ?? "", loc: (shareItems != nil && location != "" && shareItems?.isEmpty == false) ? (shareItems![0] as? String ?? ""): nil, desc: currentPost?.desc ?? "")
                    shareItems = [toShareText]
                }
            }
        }
        if shareItems != nil {
            //Analytics.logEvent("share", parameters: ["id" : userId ?? postOwnerName ?? ""])
            let activityVC = UIActivityViewController(activityItems: shareItems!, applicationActivities: nil)
            activityVC.view.backgroundColor = UIColor.white
            activityVC.popoverPresentationController?.sourceView = source // so that iPads won't crash
            activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
            }
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    func reloadCellOrTable(indexPath: IndexPath, isDownloading: Bool) {
        cellIsDownloading[indexPath] = isDownloading
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    //open chat with a particular user
    func openParticularChatVC(userId: String, snippet: String) {
        #if AUTH
        let chatVC = UIStoryboard(name: "AuthRelated", bundle: nil).instantiateViewController(withIdentifier: "ChatingVC") as! ChatingViewController
        chatVC.displayName = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.name ?? Constants.clientSpecificConstantsStructName.appName
        chatVC.profilePicLink = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.imageName ?? ""
        chatVC.usersID = Constants.clientSpecificConstantsStructName.clientId
        chatVC.postSnippet = snippet
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(chatVC, animated: true)
        #endif
    }
    func checkIfDocDownoadedElseShowAsDownloading(attachments: [[String:Any]], link: String) -> URL? {
        let docPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("LocalData").appendingPathComponent(link).path
        if FileManager.default.fileExists(atPath: docPath) {
            var name = attachments[0]["name"] as? String ?? ""
            if name.components(separatedBy: ".").contains("pdf") == false && name.components(separatedBy: ".").contains("PDF") == false {
                name = name + ".pdf"
            }
            let url: URL! = URL(fileURLWithPath: docPath)
            let tempURL = URL(fileURLWithPath: NSTemporaryDirectory().appending(name))
            do {
                let data = try Data(contentsOf: url!)
                try data.write(to: tempURL)
                return tempURL
            } catch {
            }
            return url
        } else {
            let urlString = Constants.otherLinks.mediaURL + link
            let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
            let attachment = Attachment(url: url!, index: -1)
            downloadService.startDownload(attachment)
            return nil
        }
    }
    
    //MARK: - URL delegates
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        guard let url = downloadTask.originalRequest?.url,
            let download = downloadService.activeDownloads[url]  else { return }
        download.progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        let totalSize = ByteCountFormatter.string(fromByteCount: totalBytesExpectedToWrite,
                                                  countStyle: .file)
        DispatchQueue.main.async {
            if let attachment = download.attachment, let cell = self.tableView.cellForRow(at: IndexPath(row: attachment.index, section: 0)) as? singlePostTvcCell {
                cell.updateDisplay(progress: download.progress, totalSize: totalSize)
            }
        }
        
    }
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let sourceURL = downloadTask.originalRequest?.url else { return }
        let download = downloadService.activeDownloads[sourceURL]
        downloadService.activeDownloads[sourceURL] = nil
        Constants.sharedUtils.saveDownloadedFile(tempLocURL: location, name: sourceURL.path.components(separatedBy: "/").last ?? "", directoryNamesString: sourceURL.path.replacingOccurrences(of: sourceURL.path.components(separatedBy: "/").last ?? "", with: ""), completion: { (success) in
            if success {
                DispatchQueue.main.async {
                    if let index = download?.attachment?.index {
                        if index == -1 {
                            self.tableView.reloadData()
                        } else {
                            self.reloadCellOrTable(indexPath: IndexPath(row: index, section: 0), isDownloading: false)
                        }
                    }
                }
            }
        })
    }
}
