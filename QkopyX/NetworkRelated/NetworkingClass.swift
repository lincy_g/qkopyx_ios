//
//  NetworkingClass.swift
//  Qkopy
//
//  Created by Qkopy team on 06/04/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import Foundation
import UIKit
class apiCall {
    
    static let currentEnvironment = Constants.Environment.production
    class func handleApiRequest(endPart: String, params: [String: Any], completion: @escaping (_ json: Any?, _ errorDesc: String, _ timedOut: Bool) -> Void) {
        var timeout = false
        if appDelegate.reachability.isReachable {
            let url = URL(string: currentEnvironment.rawValue + endPart)!
            var request =  URLRequest(url: url)
            request.httpMethod = Constants.urlRequestConstants.requestType
            request.setValue(Constants.urlRequestConstants.contentType, forHTTPHeaderField: "Content-Type")
            request.addValue(Constants.urlRequestConstants.authKey, forHTTPHeaderField: "Auth-Key")
            request.addValue(Constants.urlRequestConstants.clientService, forHTTPHeaderField: "Client-Service")
            let urlconfig = URLSessionConfiguration.default
            urlconfig.urlCache = nil
            urlconfig.timeoutIntervalForRequest = Constants.urlRequestConstants.timeoutIntervalForRequest
            urlconfig.timeoutIntervalForResource = Constants.urlRequestConstants.timeoutIntervalForResource
            let session = URLSession(configuration: urlconfig)
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            let task = session.dataTask(with: request) { (data,response,error) in
                if error == nil {
                    if let httpStatus = response as? HTTPURLResponse {
                        if httpStatus.statusCode == 408 {
                            timeout = true
                        }
                        var httpStatusCodeDecoded = appDelegate.fetchFromPlist(plistName: "errorCodes", key: "\(httpStatus.statusCode)")
                        if httpStatusCodeDecoded == ""  && httpStatus.statusCode != 200 {
                            httpStatusCodeDecoded = Constants.alertMessages.someErrorText
                        }
                        if let resultData = data {
                            do {
                                let json = try JSONSerialization.jsonObject(with: resultData, options: JSONSerialization.ReadingOptions.mutableContainers)
                                if httpStatus.statusCode == 200 {
                                    completion(json, "", timeout)
                                } else {
                                    completion(json, httpStatusCodeDecoded, timeout)
                                }
                            } catch {
                                completion(nil, httpStatusCodeDecoded, timeout)
                            }
                        } else {
                            completion(nil, httpStatusCodeDecoded, timeout)
                        }
                    } else {
                        completion(nil, Constants.alertMessages.someErrorText, timeout)
                    }
                } else {
                    if error!.localizedDescription.contains("timed out") {
                        timeout = true
                    }
                    completion(nil, Constants.errorMessages.requestTimedOutText, timeout)
                }
            }
            task.resume()
            session.finishTasksAndInvalidate()
        } else {
            completion(nil, Constants.errorMessages.connectionLost, timeout)
        }
    }
    
    class func getLinkAfterUploadingFile(thumbnailImage: UIImage? = nil, fileData: Data?, name: String, type: String, id: String, completion: @escaping (_ link: String, _ error: String,_ thumbnailLink: String? ) -> Void) {
        if fileData != nil {
            let supportedFormats = ["jpg", "jpeg", "png", "", "pdf", "m4a"] //gif
            if supportedFormats.contains((name.components(separatedBy: ".").last ?? "").lowercased()) {
                let fileUploadURL = URL(string: currentEnvironment.rawValue + Constants.apiCalls.fileupload)!
                var request = URLRequest(url: fileUploadURL)
                let boundary = generateBoundaryString()
                request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
                request.addValue(Constants.urlRequestConstants.authKey, forHTTPHeaderField: "Auth-Key")
                request.addValue(Constants.urlRequestConstants.clientService, forHTTPHeaderField: "Client-Service")
                request.httpMethod = Constants.urlRequestConstants.requestType
                let urlconfig = URLSessionConfiguration.default
                urlconfig.urlCache = nil
                let countBytes = ByteCountFormatter()
                countBytes.allowedUnits = [.useMB]
                countBytes.countStyle = .file
                if fileData != nil {
                    let fileSize = countBytes.string(fromByteCount: Int64(fileData!.count))
                    if fileSize.components(separatedBy: " ").count > 0, let floatValue = Float(fileSize.components(separatedBy: " ")[0]), floatValue < Float(2.0) {
                        urlconfig.timeoutIntervalForRequest = Constants.urlRequestConstants.timeoutIntervalForRequest
                        urlconfig.timeoutIntervalForResource = Constants.urlRequestConstants.timeoutIntervalForResource
                    }
                } else {
                    urlconfig.timeoutIntervalForRequest = Constants.urlRequestConstants.timeoutIntervalForRequest
                    urlconfig.timeoutIntervalForResource = Constants.urlRequestConstants.timeoutIntervalForResource
                }
                let session = URLSession(configuration: urlconfig)
                var mimetype: String?
                if type == "post" || type == "profile" || type == "msg" {
                    mimetype = "image/" + (name.components(separatedBy: ".").last ?? "jpg")
                } else if type == "doc" {
                    mimetype = "application/pdf"
                } else if type == "aud" {
                    mimetype = "audio/x-m4a"
                    //"audio/mp4"
                }
                var body = Data()
                let params = ["key": type, "id": id]
                for (key,value) in params {
                    body.append(NSString(format:"--\(boundary)\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
                    body.append(NSString(format:"Content-Disposition:form-data; name=\"\(key)\"\r\n\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
                    body.append(NSString(format:"\(value)\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
                }
                body.append(NSString(format:"--\(boundary)\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
                body.append(NSString(format:"Content-Disposition:form-data; name=\"file\"; filename=\"\(name)\"\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
                body.append(NSString(format:"Content-Type: \(mimetype!)\r\n\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
                body.append(fileData!)
                body.append(NSString(format:"\r\n").data(using: String.Encoding.utf8.rawValue)!)
                body.append(NSString(format:"--\(boundary)--\r\n" as NSString).data(using: String.Encoding.utf8.rawValue)!)
                
                request.httpBody = body
                let task = session.dataTask(with: request as URLRequest) { (data,response,error) in
                    if error == nil {
                        if response != nil {
                            let httpStatus = response as? HTTPURLResponse
                            var dataDict: NSDictionary?
                            do {
                                dataDict = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as? NSDictionary
                            } catch {
                                //error in fetching
                            }
                            if httpStatus!.statusCode == 200 {
                                let link = (dataDict!["data"] as! NSDictionary)["value"] as? String ??  ""
                                completion(link, "", "")
                            } else {
                                var decodedMessage = appDelegate.fetchFromPlist(plistName: "errorCodes", key: "\(httpStatus!.statusCode)")
                                if decodedMessage == "" {
                                    decodedMessage = Constants.alertMessages.someErrorText
                                }
                                if dataDict != nil {
//                                    if let msg = dataDict!["message"] as? String, msg != "" {
//                                        completion("", dataDict!["message"] as? String ?? decodedMessage)
//                                    } else {
                                        completion("", decodedMessage, "")
//                                    }
                                } else {
                                    completion("", decodedMessage, "")
                                }
                            }
                        } else {
                            completion("", Constants.alertMessages.someErrorText, "")
                        }
                    } else {
                        completion("", error!.localizedDescription, "")
                    }
                }
                task.resume()
            } else {
                completion("", appDelegate.fetchFromPlist(plistName: "errorCodes", key: "415"), "")
            }
        } else {
            completion("", "", "")
        }
    }
    
    //boundary string generation
    class func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

