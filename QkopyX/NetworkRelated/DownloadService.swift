//
//  DownloadService.swift
//  Qkopy
//
//  Created by Lincy George on 23/07/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import Foundation
class DownloadService {
    
    // SearchViewController creates downloadsSession
    var downloadsSession: URLSession!
    var activeDownloads: [URL: Download] = [:]
    
    // MARK: - Download methods called by TrackCell delegate methods
    
    func startDownload(_ attachment: Attachment) {
        // 1
        let download = Download(attachment: attachment)
        // 2
        download.task = downloadsSession.downloadTask(with: attachment.url)
        // 3
        download.task!.resume()
        // 4
        download.isDownloading = true
        // 5
        if download.attachment != nil {
            activeDownloads[download.attachment!.url] = download
        }
    }
    
    func pauseDownload(_ attachment: Attachment) {
        guard let download = activeDownloads[attachment.url] else { return }
        if download.isDownloading {
            download.task?.cancel(byProducingResumeData: { data in
                download.resumeData = data
            })
            download.isDownloading = false
        }
    }
    
    func cancelDownload(_ attachment: Attachment) {
        if let download = activeDownloads[attachment.url] {
            download.task?.cancel()
            activeDownloads[attachment.url] = nil
        }
    }
    
    func resumeDownload(_ attachment: Attachment) {
        guard let download = activeDownloads[attachment.url] else { return }
        if let resumeData = download.resumeData {
            download.task = downloadsSession.downloadTask(withResumeData: resumeData)
        } else if let attachment = download.attachment {
            download.task = downloadsSession.downloadTask(with: attachment.url)
        }
        download.task!.resume()
        download.isDownloading = true
    }
    
}
