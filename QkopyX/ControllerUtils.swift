//
//  ControllerUtils.swift
//  Qkopy
//
//  Created by Qkopy team on 22/02/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
import MapKit
import MisterFusion

open class ControllerUtils {
    
    var containerView: UIView?
    var progressView: UIView?
    var activityIndicator: UIActivityIndicatorView?
    var randomNumber = 0
    var syncOverlay: UIView?
    var syncOverlayGifImageView: UIImageView?
    var syncOverlayLabel: UILabel?
    var syncOverlayButton: UIButton?
    var syncOverlayDisclaimerLabel: UILabel?
    var initialisingTexts: [String] = []
    var updateTimer = Timer()

    open class var shared: ControllerUtils {
        struct Static {
            static let instance: ControllerUtils = ControllerUtils()
        }
        return Static.instance
    }
    
    //MARK: - Related to Caching
    let LocalServiceCacheDownloadDir = "LocalData"
    // Put your keys here
    
    func getBaseForCacheLocal(with fileName:String) -> String? {
        let appendedFileName = fileName
        let fileManager = FileManager.default

        let filePath = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir).path
        if FileManager.default.fileExists(atPath: filePath) {
            return filePath.appending("/\(appendedFileName)")
        } else {
            do {
                try FileManager.default.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                return filePath.appending("/\(appendedFileName)")

            } catch {
                return nil
            }
        }
    }
    
    func cacheDataToLocal(with Object:[PostObject],to key:String) {
        if #available(iOS 11.0, *) {
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: Object, requiringSecureCoding: false)
                try data.write(to: URL(fileURLWithPath: self.getBaseForCacheLocal(with: key)!))
            } catch {
                print("didn't save \(key)")
            }
        }else {
            let success = NSKeyedArchiver.archiveRootObject(Object, toFile: self.getBaseForCacheLocal(with: key)!)
            print("saved to cache --->\(success)")
        }
    }
    
    func deleteOnDeactivate() {
        let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir).path
        if FileManager.default.fileExists(atPath: filePath) {
            do {
                try FileManager.default.removeItem(atPath: filePath)
            } catch {
                print("Coudn't delete the cached Local data")
            }
        }
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
        
    @objc func newTextInSyncOverlay() {
        self.syncOverlayLabel?.text = Constants.sharedUtils.initialisingTexts[0]
        if Constants.sharedUtils.initialisingTexts.count == 1 {
            DispatchQueue.main.async {
                Constants.sharedUtils.updateTimer.invalidate()
            }
        } else {
            Constants.sharedUtils.initialisingTexts.remove(at: 0)
        }
    }

    func loadCachedDataFromLocal(with key:String ) -> [PostObject]? {
        if let cached = getBaseForCacheLocal(with: key) {
            return NSKeyedUnarchiver.unarchiveObject(withFile: cached) as? [PostObject]
        } else {
            return []
        }
    }
    
    func cacheDictionaryToLocal(with Object:[String: DynamicKeyObject],to key:String) {
        var allValuesNil = false
        for value in Object.values {
            if value.notificationDetails == nil && value.minimalDetails == nil {
                allValuesNil = true
                break
            }
        }
        if let base = self.getBaseForCacheLocal(with: key), !allValuesNil {
            let success = NSKeyedArchiver.archiveRootObject(Object, toFile: base)
            if success {
                //Logger.success(s: "Local Data Cached\(String(describing: getBaseForCacheLocal(with: key.rawValue)))" as AnyObject)
            } else {
                //Logger.error(s: "Sorry Failed")
            }
            print("saved to cache --->\(success)")
        } else {
            print("saved to cache ---> failure")
        }
    }
    
    func loadCachedDictionaryFromLocal(with key:String ) -> [String: DynamicKeyObject]? {
        if let cached = getBaseForCacheLocal(with: key) {
            return NSKeyedUnarchiver.unarchiveObject(withFile: cached) as? [String: DynamicKeyObject]
        } else {
            return [:]
        }
    }
    
    func randomNumberGenerator() -> String {
        return "thread\(Constants.sharedUtils.randomNumber + 1)"
    }
    
    func renameFile(atPath: String, toPath: String) {
        do {
            let originPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir).appendingPathComponent(atPath)
            let destinationPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir).appendingPathComponent(toPath)
            var filePath = toPath
            for directory in toPath.components(separatedBy: "/") {
                if directory != "" {
                    filePath = filePath.appending("/\(directory)")
                    if !FileManager.default.fileExists(atPath: filePath) && directory != "" {
                        do {
                            try FileManager.default.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                        } catch {
                            print("coudn't create profileImages directory")
                        }
                    }
                }
            }
            try FileManager.default.moveItem(at: originPath, to: destinationPath)
        } catch {
            print(error)
        }
    }

    func saveDownloadedFile(tempLocURL: URL, name: String, directoryNamesString: String, completion: @escaping (_ success: Bool) -> Void) {
        do {
            let originalURL = tempLocURL
            var destinationURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir)
            var directoryNames = directoryNamesString.components(separatedBy: "/")
            var i = 0
            for directory in directoryNames {
                if directory != "" {
                    destinationURL = destinationURL.appendingPathComponent(directory)
                    if !FileManager.default.fileExists(atPath: destinationURL.path) && directory != "" {
                        do {
                            try FileManager.default.createDirectory(at: destinationURL, withIntermediateDirectories: true, attributes: nil)
                        } catch {
                            completion(false)
                        }
                    }
                    i += 1
                } else {
                    directoryNames.remove(at: i)
                }
            }
            destinationURL = destinationURL.appendingPathComponent(name)
            try FileManager.default.moveItem(at: originalURL, to: destinationURL)
            completion(true)
        } catch {
            completion(false)
        }
    }

    func saveFileDocumentDirectory(index: IndexPath? = nil, name: String, image: UIImage? = nil, fileData: Data? = nil, directoryNamesString: String, lastUpdatedDate: Date?) {
        DispatchQueue.global(qos: .background).async {
            if name != "" {
                //|| image != UIImage.gif(name: "blueCat") {
                var filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir).path
                var directoryNames = directoryNamesString.components(separatedBy: "/")
                var i = 0
                for directory in directoryNames {
                    if directory != "" {
                        filePath = filePath.appending("/\(directory)")
                        if !FileManager.default.fileExists(atPath: filePath) && directory != "" {
                            do {
                                try FileManager.default.createDirectory(atPath: filePath, withIntermediateDirectories: true, attributes: nil)
                            } catch {
                                print("coudn't create profileImages directory")
                            }
                        }
                        i += 1
                    } else {
                        directoryNames.remove(at: i)
                    }
                }
                filePath = filePath.appending("/\(name)")
                var needToDownloadNew: Bool?
                if directoryNames != [] {
                    needToDownloadNew = true
                    if FileManager.default.fileExists(atPath: filePath) {
                        do {
                            if lastUpdatedDate != nil {
                                let attrs = try FileManager.default.attributesOfItem(atPath: filePath) as NSDictionary
                                let creationDate = attrs.fileCreationDate()
                                if lastUpdatedDate! <= creationDate! {
                                    needToDownloadNew = false
                                }
                            }
                        } catch {
                        }
                    }
                    if needToDownloadNew == true {
                        var fileDataTemp = fileData
                        if image != nil {
                            fileDataTemp = image!.jpegData(compressionQuality: 1)
                        } else if fileDataTemp == nil {
                            var imgName = name
                            if imgName.prefix(1) != "/" {
                                imgName = "/" + imgName
                            }
                            let urlString = Constants.otherLinks.mediaURL + "/" + directoryNames.joined(separator: "/") + imgName
                            let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
                            if url != nil {
                                do {
                                    fileDataTemp = try Data(contentsOf: url!)
                                } catch {
                                    //self.removeFileFromCache(appendName: directoryName + imgName)
                                }
                            }
                        }
                        if image != nil && directoryNames.contains("profile") {
                            fileDataTemp = self.compressImage(image: image!, limit: 300, width: 350)
                        }
                        if fileDataTemp != nil {
                            if FileManager.default.fileExists(atPath: filePath) {
                                do {
                                    try FileManager.default.removeItem(atPath: filePath)
                                } catch {
                                }
                            }
                            FileManager.default.createFile(atPath: filePath, contents: fileDataTemp!, attributes: nil)
                            self.reloadCurrentScreen(index: index, directoryNames: directoryNames)
                        }
                    }
                }
            }
        }
    }
    
    func removeFileFromCache(appendName: String) {
        DispatchQueue.global(qos: .background).async {
            if appendName.replacingOccurrences(of: "/", with: "") != "" {
                let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir).appendingPathComponent(appendName).path
                if FileManager.default.fileExists(atPath: filePath) {
                    do {
                        try FileManager.default.removeItem(atPath: filePath)
                    } catch {
                        
                    }
                }
            }
        }
    }
    
    func compressImage(image: UIImage, limit: Int, width: CGFloat) -> Data {
        var img = image
        if width == 200 {
            if let resized = resized(toWidth: width, img: image) {
                img = resized
            }
        }
        var imageData: Data?
        imageData =  img.jpegData(compressionQuality: 1)
        var imageSize = NSData(data: imageData!).length
        var compression: CGFloat = 0.9
        while (Double(imageSize) / 1024.0) > Double(limit) && compression >= 0 {
            imageData = img.jpegData(compressionQuality: compression)
            imageSize = NSData(data: imageData!).length
            compression -= 0.1
        }
        return imageData!
    }
    
    func resized(toWidth width: CGFloat, img: UIImage) -> UIImage? {
        let imageSize = CGSize(width: img.size.width * img.scale, height: img.size.height * img.scale)
        if imageSize.width > width {
            let canvasSize = CGSize(width: width, height: CGFloat(ceil(width/img.size.width * img.size.height)))
            UIGraphicsBeginImageContextWithOptions(canvasSize, false, img.scale)
            defer { UIGraphicsEndImageContext() }
            img.draw(in: CGRect(origin: .zero, size: canvasSize))
            return UIGraphicsGetImageFromCurrentImageContext()
        }
        return nil
    }

    func getImage(index: IndexPath? = nil, isProfileImg: Bool, endPath: String) -> UIImage {
        let imagePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.LocalServiceCacheDownloadDir).appendingPathComponent(endPath).path
        var image: UIImage?
        if endPath.replacingOccurrences(of: "/", with: "") != "" {
            if FileManager.default.fileExists(atPath: imagePath) {
                image = UIImage(contentsOfFile: imagePath)
            }
        }
        //let position = endPath.components(separatedBy: "/").count - 2
        if image == nil && endPath.replacingOccurrences(of: "/", with: "") != "" {
            DispatchQueue.global(qos: .background).async {
                self.downloadFile(index: index, endPath: endPath)
            }
        }
        if isProfileImg {
            return image ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName"))!
        } else {
            //return UIImage.gif(name: "blueCat")!
            //return image ?? UIImage.gif(name: "blueCat") ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "grayCatImageName"))!
            return image ?? UIImage.gif(name: "grayAnim") ?? UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "grayCatImageName"))!
        }
    }

    func downloadFile(index: IndexPath? = nil, endPath: String) {
        DispatchQueue.global(qos: .background).async {
            let name = endPath.components(separatedBy: "/").last ?? ""
            let urlString = Constants.otherLinks.mediaURL + endPath
            let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "")
            if url != nil {
                do {
                    let data = try Data(contentsOf: url!)
                    let image = UIImage(data: data)
                    let position = endPath.components(separatedBy: "/").count - 2
                    if position >= 0 {
                        self.saveFileDocumentDirectory(index: index, name: name, image: image, directoryNamesString: endPath.replacingOccurrences(of: endPath.components(separatedBy: "/").last ?? "", with: ""), lastUpdatedDate: Date())
                    }
                } catch {
                }
            }
        }
    }
    
    func muteUnmuteContact(userid: String, muted: Bool, vc: UIViewController, bounds: CGRect, completion: @escaping (_ sucees: Bool, _ errorDesc: String,_ mutedTill: String) -> Void) {
        if let myid = Constants.defaults.value(forKey: "myID") as? String {
            let muteArray = Constants.arrays.muteDurationArray
            if muted == false {
                let subActionSheetController = UIAlertController()
                let subCancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
                    completion(false, "", "")
                }
                subActionSheetController.addAction(subCancelActionButton)
                for value in muteArray {
                    let subButton = UIAlertAction(title: value, style: .default) { _ in
                        let calendar = Calendar.current
                        var calenderComponent: Calendar.Component?
                        if value.contains("year") {
                            calenderComponent = .year
                        } else if value.contains("week") {
                            calenderComponent = .weekOfMonth
                        } else if value.contains("day") {
                            calenderComponent = .day
                        } else if value.contains("hour") {
                            calenderComponent = .hour
                        }
                        let date = calendar.date(byAdding: calenderComponent ?? .hour, value: Int(value.components(separatedBy: " ")[0]) ?? 0, to: Date())
                        let params = ["userId": myid, "mUserId": userid, "type": 1, "muteTill":  date?.iso8601 ?? Date().iso8601] as [String : Any]
                        Spinner.start()
                        apiCall.handleApiRequest(endPart: Constants.apiCalls.mute, params: params, completion: {(responseJson, errorDesc, timeout) in
                            if responseJson != nil && errorDesc == "" {
                               completion(true, "", date?.iso8601 ?? Date().iso8601)
                            } else {
                                completion(false, errorDesc, "")
                            }
                        })
                        //api call send with value
                    }
                    subActionSheetController.addAction(subButton)
                }
                if vc.navigationItem.rightBarButtonItem != nil {
                    subActionSheetController.popoverPresentationController?.barButtonItem = vc.navigationItem.rightBarButtonItem
                } else {
                    subActionSheetController.popoverPresentationController?.sourceRect = bounds
                    subActionSheetController.popoverPresentationController?.sourceView = vc.view
                }
                vc.present(subActionSheetController, animated: true, completion: nil)
            } else {
                //call api to unmute
                let params = ["userId": myid, "mUserId": userid] as [String : Any]
                Spinner.start()
                apiCall.handleApiRequest(endPart: Constants.apiCalls.unmute, params: params, completion: {(responseJson, errorDesc, timeout) in
                    if responseJson != nil && errorDesc == "" {
                        completion(true, "", "")
                    } else {
                        completion(false, errorDesc, "")
                    }
                })
            }
        }
    }
    
    func reloadCurrentScreen(index: IndexPath? = nil, directoryNames: [String]) {
        DispatchQueue.main.async {
            if let topVC = UIApplication.topViewController() {
                if topVC.isKind(of: UsersProfileTableViewController.self) {
                    let userVC = topVC as! UsersProfileTableViewController
                    if let header = userVC.tableView.tableHeaderView {
                        if header.subviews.count > 1, let collctnVw = header.subviews[1] as? UICollectionView, let nonNilIndex = index, let _ = collctnVw.cellForItem(at: nonNilIndex) {
                            collctnVw.reloadItems(at: [nonNilIndex])
                        }
                    }
                    if let nonNilIndex = index, let _ = userVC.tableView.cellForRow(at: nonNilIndex) {
                        if let cell = userVC.tableView.cellForRow(at: nonNilIndex) as? StatusFeedTableViewCell, directoryNames.contains("posts") {
                            cell.imagesCollectionView?.reloadData()
                        }
                    } else if directoryNames.contains("profile") {
                        let profileImage = Constants.sharedUtils.getImage(isProfileImg: true, endPath: savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId]?.minimalDetails?.imageName ?? "")
                        userVC.userImageCropped = appDelegate.cropImage(img: profileImage)
                        userVC.tableView.reloadData()
                    }
                }
            }
            if directoryNames.contains("profile") {
                if let rootVC = appDelegate.window?.rootViewController {
                    if let tabbarVC = rootVC as? UITabBarController ?? appDelegate.window?.rootViewController?.presentedViewController as? UITabBarController {
                        if let imgFullVc = tabbarVC.children[0].children[0] as? ImageFullScreenViewController {
                            imgFullVc.setAllUserDetails()
                            imgFullVc.fullScreenImageViewSetup()
                            imgFullVc.baseTable.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    //MARK: Extract Youtube link
    func extractYoutubeIdFromLink(link: String) -> String? {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        guard let regExp = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive) else {
            return nil
        }
        let nsLink = link as NSString
        let options = NSRegularExpression.MatchingOptions(rawValue: 0)
        let range = NSRange(location: 0, length: nsLink.length)
        let matches = regExp.matches(in: link as String, options:options, range:range)
        if let firstMatch = matches.first {
            return nsLink.substring(with: firstMatch.range)
        }
        return nil
    }
    
    //MARK: Fonting
    func createHighlightPatterns() -> [String: [NSAttributedString.Key: Any]] {
        
        // 2
        let boldAttributes = createAttributesForFontStyle(.subheadline,  withTrait:.traitBold)
        let italicAttributes = createAttributesForFontStyle(.subheadline,
                                                            withTrait:.traitItalic)
        let strikeThroughAttributes =  [NSAttributedString.Key.strikethroughStyle: 1]
        
        // 3
        let replacements = [
            " \\*(.*)\\*\\ ": boldAttributes,
            " \\_(.*)\\_\\ ": italicAttributes,
            " \\~(.*)\\~\\ ": strikeThroughAttributes
        ]
        return replacements
    }
    
    func createAttributesForFontStyle(
        _ style: UIFont.TextStyle,
        withTrait trait: UIFontDescriptor.SymbolicTraits
        ) -> [NSAttributedString.Key: Any] {
        let fontDescriptor = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
        let descriptorWithTrait = fontDescriptor.withSymbolicTraits(trait)
        let font = UIFont(descriptor: descriptorWithTrait!, size: 0)
        return [.font: font]
    }
    
    //when tapped on location open MAPS app
    func openMapForPlace(locationName: String?, location: CLLocationCoordinate2D) {
        let latitude: CLLocationDegrees = location.latitude
        let longitude: CLLocationDegrees = location.longitude
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegion.init(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = locationName ?? Constants.otherConsts.sharedLocText as String
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            let locationNameAddedPercentage = locationName!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "\(latitude),\(longitude)"
            UIApplication.shared.openURL(URL(string:
                "comgooglemaps://?q=\(locationNameAddedPercentage),\(latitude),\(longitude)")!)
        } else {
            mapItem.openInMaps(launchOptions: options)
        }
    }
    
    //MARK: - Feed UI related
    //Name Label appended with time and privacy icon. Make the text attributed
    func settingNameAttributedLabelTextForCell(isPremium: Bool?, dateStr: NSMutableAttributedString, time: String, privacy: String) -> NSMutableAttributedString {
        var nameForPrivacyIcon: String?
        switch (privacy) {
        case "Me":
            nameForPrivacyIcon = "mePrivacy.png"
            break
        case "Contacts":
            nameForPrivacyIcon = "contactsPrivacy.png"
            break
        case "Public":
            nameForPrivacyIcon = "publicPrivacy.png"
            break
        case "Custom":
            nameForPrivacyIcon = "custom_contacts.png"
            break
        default:
            nameForPrivacyIcon = "custom_contacts.png"
            break
        }
        let attachmentString = getImageAttachmentString(withImageName: nameForPrivacyIcon!, y: -3)
        dateStr.append(attachmentString)
        if isPremium == true {
            dateStr.append(NSAttributedString(string: "  "))
            let premiumAttachmentString = getImageAttachmentString(withImageName: appDelegate.fetchFromPlist(plistName: "constants", key: "premiumStarImage"), y: -3)
            dateStr.append(premiumAttachmentString)
        }
        return dateStr
    }

    func linkDetector(heightArr: [CGFloat], attachmentPresent: Bool, str: String, mutStr: NSMutableAttributedString, orgStr: String, cell: StatusFeedTableViewCell, index: Int, tableRowHeight: CGFloat) -> (mutableStr: NSMutableAttributedString, whiteViewFrame: CGRect?, tableRowHeight: CGFloat?, heightArr: [CGFloat]) {
        var lastDetectedURLString: String?
        var httpRemovedString: String?
        var whiteViewframe: CGRect?
        var rowHeight: CGFloat?
        var orgStrVar = orgStr
        var rangeOfEntireString = NSRange(location: 0, length: orgStrVar.count)
        let replacements = ControllerUtils.shared.createHighlightPatterns()
        var changingHeightArr = heightArr
        for (pattern, attributes) in replacements {
            do {
                let regex = try NSRegularExpression(pattern: pattern)
                regex.enumerateMatches(in: orgStrVar, range: rangeOfEntireString) {
                    match, flags, stop in
                    // apply the style
                    if let matchRange = match?.range(at: 1) {
                        print("Matched pattern: \(pattern)")
                        mutStr.addAttributes(attributes, range: matchRange)
                        mutStr.deleteCharacters(in: NSRange(location: matchRange.location - 1, length: 1))
                        mutStr.deleteCharacters(in: NSRange(location: matchRange.location + matchRange.length - 1, length: 1))
                        orgStrVar = mutStr.string
                        rangeOfEntireString = NSRange(location: 0, length: orgStrVar.count)
                    }
                }
            }
            catch {
                print("An error occurred attempting to locate pattern: " +
                    "\(error.localizedDescription)")
            }
        }
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let block = { (result: NSTextCheckingResult?, flags: NSRegularExpression.MatchingFlags, stop: UnsafeMutablePointer<ObjCBool>) -> Void in
                if let result = result, result.resultType == NSTextCheckingResult.CheckingType.link {
                    let range = orgStrVar.range(of: result.url!.absoluteString)
                    httpRemovedString = result.url!.absoluteString
                    lastDetectedURLString = nil
                    if range != nil {
                    } else if httpRemovedString!.hasPrefix("mailto:") {
                        httpRemovedString = ""
                    }  else {
                        httpRemovedString = httpRemovedString?.replacingOccurrences(of: "http://", with: "")
                        httpRemovedString = httpRemovedString?.replacingOccurrences(of: "https://", with: "")
                    }
                    if lastDetectedURLString == nil && cell.statusTextView.isHidden == false && cell.showPreview == "true" && httpRemovedString != "" && !attachmentPresent {
                        lastDetectedURLString = httpRemovedString
                    }
                }
            }
            detector.enumerateMatches(in: orgStrVar, options: [], range: rangeOfEntireString, using: block)
            if lastDetectedURLString != nil {
                cell.previewLinkURLString = lastDetectedURLString!
                let url = URL(string: cell.previewLinkURLString!)
                if UIApplication.shared.canOpenURL(url!) {
                    if let link = Constants.sharedUtils.extractYoutubeIdFromLink(link: cell.previewLinkURLString!) {
                        let playerVars = ["loop": 1, "playsinline": 1, "showinfo": 0, "modestbranding": 1, "rel": 0, "origin" : "https://www.qkopy.com"] as [String : Any]
                        cell.playerView?.load(withVideoId: link, playerVars: playerVars)
                        cell.playerView?.isHidden = false
                        cell.previewView?.isHidden = true
                    } else {
                        cell.previewView?.addLayoutSubview( cell.embeddedView!, andConstraints: [ cell.embeddedView!.top |+| 8, cell.embeddedView!.right |-| 2, cell.embeddedView!.left |+| 2, cell.embeddedView!.bottom |-| 0])
                        cell.previewView?.isHidden = false
                        cell.playerView?.isHidden = true
                        mutStr.append(NSAttributedString(string: "\n"))
                        if changingHeightArr.count > index {
                            if changingHeightArr[index] != tableRowHeight {
                                cell.statusTextView?.frame = CGRect(x: cell.statusTextView!.frame.minX, y: cell.statusTextView!.frame.minY, width: cell.statusTextView!.frame.width, height: cell.statusTextView!.frame.height + 130)
                                let heightTemp = cell.statusTextView!.frame.height + 100
                                whiteViewframe = CGRect(x: 0, y: 8, width: UIScreen.main.bounds.width, height: heightTemp - 10)
                                rowHeight = heightTemp
                                changingHeightArr[index] = heightTemp
                            }
                        }
                    }
                    var pathView: UIView?
                    if cell.previewView!.isHidden {
                        pathView = cell.playerView
                    } else {
                        pathView = cell.previewView
                    }
                    let path = UIBezierPath(rect: CGRect(x:0, y: cell.statusTextView!.frame.height + 20, width: pathView!.frame.width, height:pathView!.frame.height))
                    cell.statusTextView.textContainer.exclusionPaths = [path]
                    if cell.previewView!.isHidden {
                        cell.statusTextView.addSubview(cell.playerView!)
                    } else {
                        cell.embeddedView?.load(urlString: cell.previewLinkURLString!)
                        cell.statusTextView.addSubview(cell.previewView!)
                    }
                    cell.statusTextView.bringSubviewToFront(cell.statusTextView.subviews.last!)
                    cell.embeddedView?.didTapHandler = { embeddedView, URL in
                        guard let URL = URL else { return }
                        UIApplication.shared.openURL(URL)
                    }
                }
            }
            cell.statusTextView.dataDetectorTypes = [.link, .phoneNumber]
            let linkAttributes: [String : Any] = [
                NSAttributedString.Key.underlineStyle.rawValue: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor.rawValue: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))]
            if cell.statusTextView.isHidden == true {
                cell.locationTextView.linkTextAttributes = self.convertToOptionalNSAttributedStringKeyDictionary(linkAttributes)
                cell.locationTextView.attributedText = mutStr
                cell.locationTextView.isHidden = false
            } else {
                cell.statusTextView.linkTextAttributes = self.convertToOptionalNSAttributedStringKeyDictionary(linkAttributes)
                cell.statusTextView.attributedText = mutStr
                cell.statusTextView.isHidden = false
            }
        } catch {
            print(error)
        }
        return (mutStr, whiteViewframe, rowHeight, changingHeightArr)
    }
    
    func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
        guard let input = input else { return nil }
        return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
    }
    
    //the heightArr values - either inserted or removed based on updation
    func heighArrSettingOnCellLoad(isStatusTV: Bool, attachmentPosted: Bool, closeBtnHidden: Bool , index : Int, rowHeight: CGFloat, extraHeight: CGFloat, heightArr: [CGFloat]) -> (CGFloat, [CGFloat]) {
        var tableviewRowHeight: CGFloat = 70
        var changedHeightArr = heightArr
        if heightArr.count == index {
            if !isStatusTV && !attachmentPosted {
                tableviewRowHeight = rowHeight
                changedHeightArr.insert(tableviewRowHeight, at: index)
            } else {
                tableviewRowHeight = rowHeight + extraHeight
                changedHeightArr.insert(tableviewRowHeight, at: index)
            }
        } else {
            if !isStatusTV && !attachmentPosted {
                tableviewRowHeight = rowHeight
                if changedHeightArr.count > index {
                    changedHeightArr[index] = tableviewRowHeight
                }
            } else {
                tableviewRowHeight = rowHeight + extraHeight
                if changedHeightArr.count > index {
                    changedHeightArr[index] = tableviewRowHeight
                }
            }
        }
        return (tableviewRowHeight, changedHeightArr)
    }

    func getWatermarkLayer(onImage image: UIImage) -> UIImage {
        let baseView = UIView()
        baseView.backgroundColor = UIColor.white
        let newImageView = UIImageView(image : image)
        let iconImgVw = UIImageView(image: UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "black_white_Qkopy")))
        
        let leftLabel = UILabel(frame: CGRect(x: newImageView.frame.height*3/40, y: newImageView.frame.height, width: 30, height: 30)) //adjust frame to change position of water mark or text
        leftLabel.text = " " + Constants.otherConsts.sharedViaText
        leftLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: newImageView.frame.height/40)
        leftLabel.backgroundColor = UIColor.white
        leftLabel.textColor = UIColor.black
        leftLabel.sizeToFit()
        leftLabel.frame = CGRect(x: leftLabel.frame.origin.x, y: leftLabel.frame.origin.y, width: leftLabel.frame.width + 20, height: newImageView.frame.height/10)
        let websiteLabel = UILabel()
        websiteLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: newImageView.frame.height/40)
        websiteLabel.text = Constants.otherLinks.appstoreLinkWithoutHttp
        iconImgVw.frame = CGRect(x: 5, y: newImageView.frame.height, width: leftLabel.frame.height/2, height: leftLabel.frame.height)
        leftLabel.frame = CGRect(x: iconImgVw.frame.origin.x + iconImgVw.frame.width, y: leftLabel.frame.origin.y, width: leftLabel.frame.width + 20, height: leftLabel.frame.height)
        websiteLabel.frame = CGRect(x: leftLabel.frame.width + leftLabel.frame.origin.x, y: leftLabel.frame.origin.y, width: newImageView.frame.width - (leftLabel.frame.width + leftLabel.frame.origin.x) - leftLabel.frame.height/4, height: leftLabel.frame.height)
        websiteLabel.textAlignment = .right
        leftLabel.textAlignment = .left
        iconImgVw.contentMode = .scaleAspectFit
        baseView.addSubview(newImageView)
        baseView.addSubview(leftLabel)
        baseView.addSubview(websiteLabel)
        baseView.addSubview(iconImgVw)
        baseView.frame = CGRect(x: 0, y: 0, width: newImageView.frame.width, height: leftLabel.frame.origin.y + leftLabel.frame.height)
        UIGraphicsBeginImageContextWithOptions(baseView.frame.size, false, 0.0)
        baseView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let watermarkedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let compressedData = Constants.sharedUtils.compressImage(image: watermarkedImage!, limit: 100, width: 200)
        let compressedImage = UIImage(data: compressedData)
        return compressedImage ?? watermarkedImage!
    }

    func shareTextAlongWithDownloadTextAppended(userID: String, time: String?, loc: String?, desc: String) -> String {
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
        newDateFormatter.locale = NSLocale.system
        let stringFormTime = newDateFormatter.string(from: time?.dateFromISO8601 ?? Date())
        let checkMarkArray = Constants.defaults.value(forKey: "checkMarkArrayForQR") as? [Bool] ?? [true, false]
        let savedDetails = savedContactRefDict[userID]?.minimalDetails
        let actualName = savedDetails?.actualProfileName ?? ""
        let username = savedDetails?.qkopyId ?? ""
        var shareText = "*" + (actualName == "" ? username: actualName) + "*" + "\n"
        shareText = shareText + "(\(stringFormTime))" + "\n\n"
        shareText = shareText + desc + (desc.trimmingCharacters(in: .whitespacesAndNewlines) != "" ? "\n": "") + (loc != nil ? loc!: "") + "\n\n"
        shareText = shareText + Constants.clientSpecificConstantsStructName.verifiedUsersWithoutUsernameDownloadAppText
        return shareText
    }

    func getDate(str: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        dateFormatter.locale = Locale.current
        return dateFormatter.date(from: str) // replace Date String
    }
    
    func getImageAttachmentString(withImageName name: String, y: CGFloat) -> NSMutableAttributedString {
        let locAttachment = NSTextAttachment()
        let img = UIImage(named: name)
        locAttachment.image = img
        locAttachment.bounds = CGRect(x: 0, y: y, width: 17, height: 17)
        return NSMutableAttributedString(attributedString: NSAttributedString(attachment: locAttachment))
    }
   
    #if AUTH
    //to get the country code
    func getCountryPhoneCode (country : String) -> String
    {
        if country.count == 2
        {
            let x : [String] = ["972", "IL","93" , "AF","355", "AL","213", "DZ","1"  , "AS","376", "AD","244", "AO","1"  , "AI","1"  , "AG","54" , "AR","374", "AM","297", "AW","61" , "AU","43" , "AT","994", "AZ","1"  , "BS","973", "BH","880", "BD","1"  , "BB","375", "BY","32" , "BE","501", "BZ","229", "BJ","1"  , "BM","975", "BT","387", "BA","267", "BW","55" , "BR","246", "IO","359", "BG","226", "BF","257", "BI","855", "KH","237", "CM","1"  , "CA","238", "CV","345", "KY","236", "CF","235", "TD","56", "CL","86", "CN","61", "CX","57", "CO","269", "KM","242", "CG","682", "CK","506", "CR","385", "HR","53" , "CU" ,"537", "CY","420", "CZ","45" , "DK" ,"253", "DJ","1"  , "DM","1"  , "DO","593", "EC","20" , "EG" ,"503", "SV","240", "GQ","291", "ER","372", "EE","251", "ET","298", "FO","679", "FJ","358", "FI","33" , "FR","594", "GF","689", "PF","241", "GA","220", "GM","995", "GE","49" , "DE","233", "GH","350", "GI","30" , "GR","299", "GL","1"  , "GD","590", "GP","1"  , "GU","502", "GT","224", "GN","245", "GW","595", "GY","509", "HT","504", "HN","36" , "HU","354", "IS","91" , "IN","62" , "ID","964", "IQ","353", "IE","972", "IL","39" , "IT","1"  , "JM","81", "JP", "962", "JO", "77", "KZ","254", "KE", "686", "KI", "965", "KW", "996", "KG","371", "LV", "961", "LB", "266", "LS", "231", "LR","423", "LI", "370", "LT", "352", "LU", "261", "MG","265", "MW", "60", "MY", "960", "MV", "223", "ML","356", "MT", "692", "MH", "596", "MQ", "222", "MR","230", "MU", "262", "YT", "52","MX", "377", "MC","976", "MN", "382", "ME", "1", "MS", "212", "MA","95", "MM", "264", "NA", "674", "NR", "977", "NP","31", "NL", "599", "AN", "687", "NC", "64", "NZ","505", "NI", "227", "NE", "234", "NG", "683", "NU","672", "NF", "1", "MP", "47", "NO", "968", "OM","92", "PK", "680", "PW", "507", "PA", "675", "PG","595", "PY", "51", "PE", "63", "PH", "48", "PL","351", "PT", "1", "PR", "974", "QA", "40", "RO","250", "RW", "685", "WS", "378", "SM", "966", "SA","221", "SN", "381", "RS", "248", "SC", "232", "SL","65", "SG", "421", "SK", "386", "SI", "677", "SB","27", "ZA", "500", "GS", "34", "ES", "94", "LK","249", "SD", "597", "SR", "268", "SZ", "46", "SE","41", "CH", "992", "TJ", "66", "TH", "228", "TG","690", "TK", "676", "TO", "1", "TT", "216", "TN","90", "TR", "993", "TM", "1", "TC", "688", "TV","256", "UG", "380", "UA", "971", "AE", "44", "GB","1", "US", "598", "UY", "998", "UZ", "678", "VU","681", "WF", "967", "YE", "260", "ZM", "263", "ZW","591", "BO", "673", "BN", "61", "CC", "243", "CD","225", "CI", "500", "FK", "44", "GG", "379", "VA","852", "HK", "98", "IR", "44", "IM", "44", "JE","850", "KP", "82", "KR", "856", "LA", "218", "LY","853", "MO", "389", "MK", "691", "FM", "373", "MD","258", "MZ", "970", "PS", "872", "PN", "262", "RE","7", "RU", "590", "BL", "290", "SH", "1", "KN","1", "LC", "590", "MF", "508", "PM", "1", "VC","239", "ST", "252", "SO", "47", "SJ","963","SY","886","TW", "255","TZ", "670","TL","58","VE","84","VN","284", "VG","340", "VI","678","VU","681","WF","685","WS","967","YE","262","YT","27","ZA","260","ZM","263","ZW"]
            var keys = [String]()
            var values = [String]()
            let whitespace = NSCharacterSet.decimalDigits
            
            for i in x {
                // range will be nil if no whitespace is found
                if  (i.rangeOfCharacter(from: whitespace) != nil) {
                    values.append(i)
                }
                else {
                    keys.append(i)
                }
            }
            let countryCodeListDict = NSDictionary(objects: values as [String], forKeys: keys as [String] as [NSCopying])
            if let _ = countryCodeListDict.value(forKey: country) {
                return "+" + (countryCodeListDict.value(forKey: country) as! String)
            } else
            {
                return ""
            }
        }
        else
        {
            return ""
        }
    }
    //MARK: - Chat Related
    func openParticularChatVC(tvc: UITableViewController, userId: String, snippet: String) {
        let chatVC = UIStoryboard(name: "AuthRelated", bundle: nil).instantiateViewController(withIdentifier: "ChatingVC") as! ChatingViewController
        chatVC.displayName = savedContactRefDict[userId].minimalDetails?.name as? String ?? Constants.clientSpecificConstantsStructName.appName
        chatVC.profilePicLink = savedContactRefDict[userId].minimalDetails?.imageName as? String ?? ""
        chatVC.usersID = userId
        chatVC.postSnippet = snippet
        let backItem = UIBarButtonItem()
        backItem.title = " "
        tvc.navigationItem.backBarButtonItem = backItem
        tvc.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func setFireBaseUserData() {
        if let id = Constants.defaults.value(forKey: "myID") as? String, id != "", appDelegate.firebaseDb != nil {
            let ref = appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(id)
            var userData = ["chatOn": Constants.defaults.value(forKey: "myChatOn") as? Bool ?? true, "contactPrivate": Constants.defaults.value(forKey: "hideNumberInPublicSearch") as? Bool ?? true, "mobile": Constants.defaults.value(forKey: "formattedMyNumber") as? String ?? "", "name": Constants.defaults.value(forKey: "myName") as? String ?? "", "notificationId": Constants.defaults.value(forKey: "fcmToken") as? String ?? "", "notificationMutedList": Constants.defaults.value(forKey: "myNotificationMutedList") as? [String] ?? []]  as [String: Any]
            userData["photoPrivacy"] = Constants.defaults.value(forKey: "profilePicPrivacy") as? String ?? "Public"
            userData["isPremium"] = true
            userData["type"] = "ios"
            userData["profilePic"] = Constants.defaults.value(forKey: "myImageLink") as? String ?? ""
            userData["profileVerified"] = Constants.defaults.value(forKey: "profileVerified") as? Bool ?? false
            userData["userName"] = Constants.defaults.value(forKey: "myUniqueUserName") as? String ?? ""
            ref.setData(userData) { (error) in
                if let _ = error {
                    print("firebase user not set")
                } else {
                    print("firebase user set")
                }
            }
        }
    }
    func setFireBaseConversationData(myUserTask: UserTask?, receiversUserTask: UserTask?, messageObject: MessagesTask?, receiverId: String, lastUpdatedTime: Int64, documentID: String, blockedBy: String?, last_deleted_time_a: Int64, last_deleted_time_b: Int64, member_a: String, member_b: String, completion: @escaping (_ success: Bool,_ documentID: String) -> Void) {
        if let id = Constants.defaults.value(forKey: "myID") as? String, id != "", appDelegate.firebaseDb != nil {
            let ref = appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.conversations)
            var conversationData: [String: Any?] = ["lastUpdatedTime": lastUpdatedTime, "blockedBy": blockedBy, "last_deleted_time_a": last_deleted_time_a, "last_deleted_time_b": last_deleted_time_b, "typing": ["receiver": false, "sender": false]]
            var chatDocumentData: [String: Any] = [:]
            if documentID == "" {
                let createdDocument = ref.document()
                conversationData["member_a"] = id
                conversationData["member_b"] = receiverId
                conversationData["id"] = createdDocument.documentID
                if let object = messageObject {
                    let chatRef = createdDocument.collection("chats")
                    let createdChatDocument = chatRef.document()
                    chatDocumentData = ["_id": createdChatDocument.documentID, "conversationId": createdDocument.documentID, "deleted": false, "messageBody": object.messageBody, "senderId": object.senderId, "receiverId": object.receiverId, "item": object.item, "messageType": object.messageType, "postSnippet": object.postSnippet, "time": object.time] as [String : Any]
                    createdChatDocument.setData(chatDocumentData)
                }
                createdDocument.setData(conversationData, merge: true) { (error) in
                    if let _ = error {
                        print("firebase user not set")
                        completion(false, "")
                    } else {
                        print("firebase user set")
                        self.updateRecentChats(myUserTask: myUserTask, receiversUserTask: receiversUserTask, id: id, receiverId: receiverId, chatDocumentData: chatDocumentData, conversationId: conversationData["id"] as? String ?? "", updatedTime: lastUpdatedTime, last_deleted_time_a: last_deleted_time_a, last_deleted_time_b: last_deleted_time_b, member_a: id, member_b: receiverId, completion: { (success) in
                            completion(success, createdDocument.documentID)
                        })
                    }
                }
            } else {
                conversationData["id"] = documentID
                if let object = messageObject {
                    let chatRef = ref.document(documentID).collection("chats")
                    let createdChatDocument = chatRef.document()
                    chatDocumentData = ["_id": createdChatDocument.documentID, "conversationId": documentID, "deleted": false, "messageBody": object.messageBody, "senderId": object.senderId, "receiverId": object.receiverId, "item": object.item, "messageType": object.messageType, "postSnippet": object.postSnippet, "time": object.time] as [String : Any]
                    createdChatDocument.setData(chatDocumentData)
                }
                ref.document(documentID).updateData(conversationData) { (error) in
                    if let _ = error {
                        print("firebase user not set")
                        completion(false, documentID)
                    } else {
                        if myUserTask == nil {
                            self.getUserTaskForId(id, completion: { (task) in
                                self.updateRecentChats(myUserTask: task, receiversUserTask: receiversUserTask, id: id, receiverId: receiverId, chatDocumentData: chatDocumentData, conversationId: conversationData["id"] as? String ?? "", updatedTime: lastUpdatedTime, last_deleted_time_a: last_deleted_time_a, last_deleted_time_b: last_deleted_time_b, member_a: member_a, member_b: member_b, completion: { (success) in
                                    completion(success, documentID)
                                })
                            })
                        } else {
                            self.updateRecentChats(myUserTask: myUserTask, receiversUserTask: receiversUserTask, id: id, receiverId: receiverId, chatDocumentData: chatDocumentData, conversationId: conversationData["id"] as? String ?? "", updatedTime: lastUpdatedTime, last_deleted_time_a: last_deleted_time_a, last_deleted_time_b: last_deleted_time_b, member_a: member_a, member_b: member_b, completion: { (success) in
                                completion(success, documentID)
                            })
                        }
                    }
                }
            }
        }
    }
    func updateByDeletingDoc(conversationId: String, chatID: String, myID: String, usersID: String) {
        appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.conversations).document(conversationId).collection("chats").document(chatID).updateData(["deleted" : true]) { err in
            if let err = err {
                print("Error removing document: \(err)")
            } else {
                appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(myID).collection("recent_chats").document(conversationId).getDocument(completion: { (document, error) in
                    guard let nonNilDocument = document else {
                        return
                    }
                    var lastMessageDict = nonNilDocument.data()?["lastMessage"] as? [String: Any] ?? [:]
                    if lastMessageDict["messageId"] as? String == chatID {
                        lastMessageDict["deleted"] = true
                        appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(myID).collection("recent_chats").document(conversationId).updateData(["lastMessage": lastMessageDict])
                    }
                })
                appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(usersID).collection("recent_chats").document(conversationId).getDocument(completion: { (document, error) in
                    guard let nonNilDocument = document else {
                        print("Error fetching documents results: \(error!)")
                        return
                    }
                    var lastMessageDict = nonNilDocument.data()?["lastMessage"] as? [String: Any] ?? [:]
                    if lastMessageDict["messageId"] as? String == chatID {
                        lastMessageDict["deleted"] = true
                    appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(usersID).collection("recent_chats").document(conversationId).updateData(["lastMessage": lastMessageDict])
                    }
                    print("Document successfully removed!")
                })
            }
        }
    }
    func getUserTaskForId(_ id: String, completion: @escaping(_ userTask: UserTask?) -> Void) {
        let ref = appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(id)
        ref.getDocument(source: .default) { (document, error) in
            if let document = document {
                let task = UserTask(dictionary: document.data() ?? [:], id: document.documentID)
                completion(task)
            } else {
                completion(nil)
            }
        }
    }
    
    func updateRecentChats(myUserTask: UserTask?, receiversUserTask: UserTask?, id: String, receiverId: String, chatDocumentData: [String: Any], conversationId: String, updatedTime: Int64, last_deleted_time_a: Int64, last_deleted_time_b: Int64, member_a: String, member_b: String, completion: @escaping(_ success: Bool) -> Void) {
        var dict: [String: Any] = chatDocumentData
        dict["messageId"] = chatDocumentData["_id"]
        dict.removeValue(forKey: "_id")
        dict["read"] = false
        let recent_chatsDataUpdateInSender = ["lastMessage": dict, "chatOn": receiversUserTask?.chatOn, "contactPrivate": receiversUserTask?.contactPrivate, "mobile": receiversUserTask?.mobile, "name": receiversUserTask?.name, "photoPrivacy": receiversUserTask?.photoPrivacy, "premium": receiversUserTask?.isPremium, "profilePic": receiversUserTask?.profilePic, "profileVerified": receiversUserTask?.profileVerified, "typing": false, "userId": receiverId, "userName": receiversUserTask?.userName, "updated_time": updatedTime, "member_a": member_a, "member_b": member_b, "last_deleted_time_a": last_deleted_time_a, "last_deleted_time_b": last_deleted_time_b] as [String: Any?]
        let recent_chatsDataUpdateInReceiver = ["lastMessage": dict, "chatOn": myUserTask?.chatOn, "contactPrivate": myUserTask?.contactPrivate, "mobile": myUserTask?.mobile, "name": myUserTask?.name, "photoPrivacy": myUserTask?.photoPrivacy, "premium": myUserTask?.isPremium, "profilePic": myUserTask?.profilePic, "profileVerified": myUserTask?.profileVerified, "typing": false, "userId": id, "userName": myUserTask?.userName, "updated_time": updatedTime, "member_a": member_a, "member_b": member_b, "last_deleted_time_a": last_deleted_time_a, "last_deleted_time_b": last_deleted_time_b] as [String: Any?]
        appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(id).collection("recent_chats").document(conversationId).setData(recent_chatsDataUpdateInSender) { (error) in
            if let _ = error {
                completion(false)
            } else {
                appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.users).document(receiverId).collection("recent_chats").document(conversationId).setData(recent_chatsDataUpdateInReceiver) { (error) in
                    if let _ = error {
                        completion(false)
                    } else {
                        completion(true)
                    }
                }
            }
        }
    }
    
    func authenticateMobileNumber() {
        DispatchQueue.main.async(execute: { () -> Void in
            let vc = UIStoryboard(name: "AuthRelated", bundle: nil).instantiateViewController(withIdentifier: "numberVC") as! PhoneNumberVerificationViewController
            let navController = UINavigationController(rootViewController: vc)
            navController.modalPresentationStyle = .fullScreen
            navController.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))
            UIApplication.topViewController()?.present(navController, animated:true, completion: nil)
        })
    }
    #endif
}

extension UITextView {
    
    func centerVertically() {
        let fittingSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fittingSize)
        let topOffset = (bounds.size.height - size.height * zoomScale) / 2
        let positiveTopOffset = max(1, topOffset)
        contentOffset.y = -positiveTopOffset
    }
    
}

extension UIColor {
    
    func lighter(by percentage:CGFloat=30.0) -> UIColor? {
        return self.adjust(by: abs(percentage) )
    }
    
    func darker(by percentage:CGFloat=30.0) -> UIColor? {
        return self.adjust(by: -1 * abs(percentage) )
    }
    
    func adjust(by percentage:CGFloat=30.0) -> UIColor? {
        var r:CGFloat=0, g:CGFloat=0, b:CGFloat=0, a:CGFloat=0;
        if(self.getRed(&r, green: &g, blue: &b, alpha: &a)){
            return UIColor(red: min(r + percentage/100, 1.0),
                           green: min(g + percentage/100, 1.0),
                           blue: min(b + percentage/100, 1.0),
                           alpha: a)
        }else{
            return nil
        }
    }
}
extension UINavigationController {
    func setDefaults(largeTitlesReq: Bool, title: String? = nil) {
        self.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .systemBackground
            appearance.titleTextAttributes = [.foregroundColor: UIColor.label]
            appearance.largeTitleTextAttributes = [.foregroundColor: UIColor.label]

            UINavigationBar.appearance().tintColor = UIColor.systemBlue
            UINavigationBar.appearance().standardAppearance = appearance
            UINavigationBar.appearance().compactAppearance = appearance
            UINavigationBar.appearance().scrollEdgeAppearance = appearance
            self.navigationBar.barTintColor = .systemBackground
            self.navigationBar.backgroundColor = .systemBackground
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.label]
        } else {
            UINavigationBar.appearance().tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
            UINavigationBar.appearance().barTintColor = .white
            UINavigationBar.appearance().isTranslucent = false
            self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        }
        if title != nil {
            self.viewControllers.last?.navigationItem.title = title
        }
        if #available(iOS 11.0, *) {
            if largeTitlesReq {
                self.navigationBar.prefersLargeTitles = largeTitlesReq
            } else {
                self.viewControllers.last?.navigationItem.largeTitleDisplayMode = .never
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

extension UIColor {
    
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}

extension UIFontDescriptor {
    
    private struct SubStruct {
        static var preferredFontName: String = "Avenir-medium"
    }
    
    static let fontSizeTable: [UIFont.TextStyle: [UIContentSizeCategory: CGFloat]] = [
        .headline: [
            .accessibilityExtraExtraExtraLarge: 28,
            .accessibilityExtraExtraLarge: 28,
            .accessibilityExtraLarge: 28,
            .accessibilityLarge: 28,
            .accessibilityMedium: 27,
            .extraExtraExtraLarge: 27,
            .extraExtraLarge: 26,
            .extraLarge: 25,
            .large: 24,
            .medium: 23,
            .small: 22,
            .extraSmall: 22
        ],
        .subheadline: [
            .accessibilityExtraExtraExtraLarge: 21,
            .accessibilityExtraExtraLarge: 21,
            .accessibilityExtraLarge: 21,
            .accessibilityLarge: 21,
            .accessibilityMedium: 21,
            .extraExtraExtraLarge: 21,
            .extraExtraLarge: 19,
            .extraLarge: 17,
            .large: 15,
            .medium: 14,
            .small: 13,
            .extraSmall: 12
        ],
        .body: [
            .accessibilityExtraExtraExtraLarge: 22,
            .accessibilityExtraExtraLarge: 22,
            .accessibilityExtraLarge: 22,
            .accessibilityLarge: 22,
            .accessibilityMedium: 22,
            .extraExtraExtraLarge: 22,
            .extraExtraLarge: 21,
            .extraLarge: 18,
            .large: 16,
            .medium: 15,
            .small: 14,
            .extraSmall: 13
        ],
        .caption1: [
            .accessibilityExtraExtraExtraLarge: 22,
            .accessibilityExtraExtraLarge: 22,
            .accessibilityExtraLarge: 22,
            .accessibilityLarge: 22,
            .accessibilityMedium: 22,
            .extraExtraExtraLarge: 22,
            .extraExtraLarge: 20,
            .extraLarge: 18,
            .large: 16,
            .medium: 15,
            .small: 14,
            .extraSmall: 13
        ],
        .caption2: [
            .accessibilityExtraExtraExtraLarge: 17,
            .accessibilityExtraExtraLarge: 17,
            .accessibilityExtraLarge: 17,
            .accessibilityLarge: 17,
            .accessibilityMedium: 17,
            .extraExtraExtraLarge: 17,
            .extraExtraLarge: 15,
            .extraLarge: 13,
            .large: 11,
            .medium: 11,
            .small: 11,
            .extraSmall: 11
        ],
        .footnote: [
            .accessibilityExtraExtraExtraLarge: 19,
            .accessibilityExtraExtraLarge: 19,
            .accessibilityExtraLarge: 19,
            .accessibilityLarge: 19,
            .accessibilityMedium: 19,
            .extraExtraExtraLarge: 19,
            .extraExtraLarge: 17,
            .extraLarge: 15,
            .large: 13,
            .medium: 12,
            .small: 12,
            .extraSmall: 12
        ]
    ]
    
    final class func preferredDescriptor(textStyle: UIFont.TextStyle) -> UIFontDescriptor {
        let contentSize = UIApplication.shared.preferredContentSizeCategory
        let style = fontSizeTable[textStyle]!
        var descriptor: UIFontDescriptor?
        if textStyle == .caption1 || textStyle == .headline {
            descriptor = UIFont.systemFont(ofSize: style[contentSize]!, weight: .semibold).fontDescriptor
        } else {
            descriptor = UIFont.systemFont(ofSize: style[contentSize]!).fontDescriptor
        }
        return descriptor!
    }
}
extension Date {
    var iso8601: String {
        return Formatter.iso8601.string(from: self)
    }
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
@available(iOS 10.0, *)
extension ISO8601DateFormatter {
    convenience init(_ formatOptions: Options, timeZone: TimeZone = TimeZone(secondsFromGMT: 0)!) {
        self.init()
        self.formatOptions = formatOptions
        self.timeZone = timeZone
    }
}

extension Formatter {
    @available(iOS 11.0, *)
    static let iso8601Latest = ISO8601DateFormatter([.withInternetDateTime, .withFractionalSeconds])
    
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSXXXXX"
        return formatter
    }()
}

extension String {
    var dateFromISO8601: Date? {
        if #available(iOS 11.0, *) {
            return Formatter.iso8601Latest.date(from: self)
        } else {
            return Formatter.iso8601.date(from: self)
        }
    }
}
