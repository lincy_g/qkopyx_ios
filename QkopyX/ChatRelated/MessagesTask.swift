//
//  MessagesTask.swift
//  Qkopy
//
//  Created by Qkopy team on 01/10/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import Foundation
struct MessagesTask{
    var _id: String
    var conversationId: String
    var deleted: Bool
    var messageBody: String
    var senderId: String
    var receiverId: String
    var item: String
    var messageType: String
    var postSnippet: String
    var time: Int64
    
    var dictionary: [String: Any] {
        return [
            "_id": _id,
            "conversationId": conversationId,
            "deleted": deleted,
            "messageBody": messageBody,
            "senderId": senderId,
            "receiverId": receiverId,
            "item": item,
            "messageType": messageType,
            "postSnippet": postSnippet,
            "time": time
        ]
    }
}

extension MessagesTask {
    init?(dictionary: [String : Any], id: String) {
        let _id = dictionary["_id"] as? String ?? ""
        let conversationId = dictionary["conversationId"] as? String ?? ""
        let deleted = dictionary["deleted"] as? Bool ?? false
        let messageBody = dictionary["messageBody"] as? String ?? ""
        let senderId = dictionary["senderId"] as? String ?? ""
        let receiverId = dictionary["receiverId"] as? String ?? ""
        let item = dictionary["item"] as? String ?? ""
        let messageType = dictionary["messageType"] as? String ?? ""
        let time = dictionary["time"] as? Int64 ?? 0
        let postSnippet = dictionary["postSnippet"] as? String ?? ""
        self.init(_id: _id, conversationId: conversationId, deleted: deleted, messageBody: messageBody, senderId: senderId, receiverId: receiverId, item: item, messageType: messageType, postSnippet: postSnippet, time: time)
    }
}
