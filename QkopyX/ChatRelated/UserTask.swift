//
//  UserTask.swift
//  Qkopy
//
//  Created by Qkopy team on 02/10/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import Foundation
struct UserTask{
    var chatOn: Bool
    var contactPrivate: Bool
    var mobile: String
    var name: String
    var notificationId: String
    var notificationMutedList: [String]
    var photoPrivacy: String
    var isPremium: Bool
    var profilePic: String
    var profileVerified: Bool
    var userName: String
    var type: String
    
    var dictionary: [String: Any] {
        return [
            "chatOn": chatOn,
            "contactPrivate": contactPrivate,
            "mobile": mobile,
            "name": name,
            "notificationId": notificationId,
            "notificationMutedList": notificationMutedList,
            "photoPrivacy": photoPrivacy,
            "isPremium": isPremium,
            "profilePic": profilePic,
            "profileVerified": profileVerified,
            "userName": userName,
            "type": type
        ]
    }
}

extension UserTask {
    init?(dictionary: [String : Any], id: String) {
        let chatOn = dictionary["chatOn"] as? Bool ?? true
        let contactPrivate = dictionary["contactPrivate"] as? Bool ?? true
        let mobile = dictionary["mobile"] as? String ?? ""
        let name = dictionary["name"] as? String ?? ""
        let notificationId = dictionary["notificationId"] as? String ?? ""
        let photoPrivacy = dictionary["photoPrivacy"] as? String ?? ""
        let profilePic = dictionary["profilePic"] as? String ?? ""
        let userName = dictionary["userName"] as? String ?? ""
        let profileVerified = dictionary["profileVerified"] as? Bool ?? false
        let notificationMutedList = dictionary["notificationMutedList"] as? [String] ?? []
        let isPremium = dictionary["isPremium"] as? Bool ?? false
        let type = dictionary["type"] as? String ?? ""
        self.init(chatOn: chatOn, contactPrivate: contactPrivate, mobile: mobile, name: name, notificationId: notificationId, notificationMutedList: notificationMutedList, photoPrivacy: photoPrivacy, isPremium: isPremium, profilePic: profilePic, profileVerified: profileVerified, userName: userName, type: type)
    }
}
