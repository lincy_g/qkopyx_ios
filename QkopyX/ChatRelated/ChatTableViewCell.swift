//
//  ChatTableViewCell.swift
//  Qkopy
//
//  Created by Qkopy team on 26/09/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import UIKit

@objc protocol longPressOnMessageDelegate {
    func showOptionsOnMessageLongPress(indexPath: IndexPath)
}
class ChatTableViewCell: UITableViewCell {

    var contentDict: [String: Any] = [:]
    var bezierPath: UIBezierPath?
    var shapeLayer: CAShapeLayer?
    var imagePath: String?
    let lowerBorder = CALayer()
    
    lazy var longPressGestureOnImageView: UILongPressGestureRecognizer = {
        UILongPressGestureRecognizer(target: self, action: #selector(self.longPressAction))
    }()
    lazy var longPressGestureOnTextView: UILongPressGestureRecognizer = {
        UILongPressGestureRecognizer(target: self, action: #selector(self.longPressAction))
    }()
    weak var longPressedMessageDelegate: longPressOnMessageDelegate?
    weak var fullScreenDelegate: openImageInFullScreenDelegate?

    @IBOutlet weak var bubbleTextView: UITextView!
    @IBOutlet weak var snippetLabel: UILabel!
    @IBOutlet weak var chatTimeLabel: UILabel!
    @IBOutlet weak var bubbleImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        bubbleTextView.backgroundColor = UIColor.clear
        shapeLayer = CAShapeLayer()
        chatTimeLabel.textColor = UIColor.lightGray
        bubbleImageView.isUserInteractionEnabled = true
        let makeFullScreenTap = UITapGestureRecognizer(target: self, action: #selector(self.makeFullScreen))
        makeFullScreenTap.numberOfTapsRequired = 1
        bubbleTextView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        bubbleTextView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: -5)
        bubbleImageView.addGestureRecognizer(makeFullScreenTap)
        bubbleImageView.addGestureRecognizer(longPressGestureOnImageView)
        bubbleTextView.addGestureRecognizer(longPressGestureOnTextView)
        settingFonts()
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
        // Initialization code
    }

    override func layoutSubviews() {
        addLowerlayerToSnippet()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: - General methods
    func settingFonts() {
        snippetLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 0)
        chatTimeLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        bubbleTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 0)
    }
    
    func addLowerlayerToSnippet() {
        lowerBorder.backgroundColor = bubbleTextView.textColor == UIColor.black ? UIColor.gray.cgColor: UIColor.white.cgColor
        lowerBorder.frame = CGRect(x: 0, y: snippetLabel!.frame.height + 5, width: snippetLabel.frame.width, height: 0.5)
        snippetLabel?.layer.addSublayer(lowerBorder)
    }

    //MARK: - Targets
    //tappedOnImage
    @objc func makeFullScreen() {
        fullScreenDelegate?.pushToVC(currentImagePath: imagePath ?? "", imagePaths: [imagePath ?? ""], titleName: "", indexPath: indexPath)
    }
    
    @objc func longPressAction() {
        if indexPath != nil {
            longPressedMessageDelegate?.showOptionsOnMessageLongPress(indexPath: indexPath!)
        }
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
    }
}
