//
//  ChatingViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 03/10/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import UIKit
import Firebase
import Photos
import CropViewController
import libPhoneNumber_iOS

class ChatingViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate, openImageInFullScreenDelegate, longPressOnMessageDelegate {
    
    var myID: String = ""
    var displayName: String = ""
    var usersID: String = ""
    var profilePicLink: String = ""
    var listOfMessages: [[String: Any]] = []
    var galleryButton: UIButton?
    var replyTextView: UITextView?
    var attachedImageView: UIImageView?
    var attachedImageLink: String?
    var sendBtn: UIButton?
    var heightDict: [Int: CGFloat] = [:]
    var documentID: String = ""
    var imagePicker: UIImagePickerController?
    var setImageName: String = "savedImage.JPG"
    var labelOnChatArea: UILabel?
    var usersChatOn: Bool?
    var myChatOn: Bool?
    var userBlockedMe: Bool?
    var iBlockedUser: Bool?
    var iBlockedUserFromContacts: Bool?
    var navigationTitle: String?
    var profilePicPath: String?
    var postSnippet: String?
    var isContactPrivate: Bool?
    var replyBarOnVC: UIView?
    var myMutedList: [String] = []
    var usersMutedList: [String] = []
    var postSnippetLabelOnReplyBar: UILabel?
    var deviceType: String = ""
    var notificationId: String = ""
    var nameLabel: UILabel?
    var initialLoad: Bool = true
    var titleBaseView: UIView?
    var utilsClass = ControllerUtils()
    var copiedImageUrlString: String = ""
    var copiedText: String = ""
    var myUserTask: UserTask?
    var receiversUserTask: UserTask?
    var fetchingMore = false
    var documentReference: DocumentReference?
    var queryForChats: Query?
    var lastVelocityYSign = 0
    var last_deleted_time_a: Int64 = 0
    var last_deleted_time_b: Int64 = 0
    var member_a: String = ""
    var member_b: String = ""
    var loginView: loginToQkopyView?
    lazy var tapForKeyboardDismissal: UITapGestureRecognizer = {
        UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
    }()

    //variables for userDocumen
    @IBOutlet weak var chatTableView: UITableView!
    
    @IBOutlet weak var tableBottomConstraint: NSLayoutConstraint!
    //chat ref
    private var documents: [DocumentSnapshot] = []
    public var tasks: [MessagesTask] = []
    //public var userTask: UserTask = [:]
    private var userListener: ListenerRegistration!
    private var chatListenerArr: [ListenerRegistration] = []
    
    fileprivate func baseQuery() -> [Query] {
        return [appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.conversations)
            .whereField("member_a", isEqualTo: myID)
            .whereField("member_b", isEqualTo: usersID), appDelegate.firebaseDb!.collection(Constants.firebasePathConstants.conversations)
                .whereField("member_a", isEqualTo: usersID)
                .whereField("member_b", isEqualTo: myID)]
    }
    
    private var queryArr: [Query]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.chatTableView.isHidden = true
        chatTableView.register(UINib(nibName: "chatCell", bundle: nil), forCellReuseIdentifier: Constants.nameOfIdentifiers.chatCellIdentifier)
        titleBarSetting()
        replyBarOnVC = getCustomView()
        self.myMutedList = Constants.defaults.value(forKey: "myNotificationMutedList") as? [String] ?? []
        chatTableView.frame = CGRect(x: chatTableView.frame.origin.x, y: self.navigationController!.navigationBar.frame.origin.y + self.navigationController!.navigationBar.frame.height, width: chatTableView.frame.width, height: 300)
        if #available(iOS 13.0, *) {
            chatTableView.backgroundColor = .systemBackground
            self.view.backgroundColor = .systemBackground
        } else {
            chatTableView.backgroundColor = .white
            self.view.backgroundColor = .white
        }
        self.replyTextView?.isUserInteractionEnabled = false
        self.galleryButton?.isUserInteractionEnabled = false
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        heightDict = [:]
        myChatOn = Constants.defaults.value(forKey: "myChatOn") as? Bool
        myID = Constants.defaults.value(forKey: "myID") as? String ?? ""
        if myID != "" && Constants.defaults.value(forKey: "LoggedIn") as? Bool == true && appDelegate.firebaseDb != nil {
            Spinner.start()
            self.queryArr = baseQuery()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)

        //        settingValuesFromFireBase { (success, errorDesc) in
        //            if success {
        //                DispatchQueue.main.async {
        //                    self.chatTableView.reloadData()
        //                }
        //            }
        //        }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.view.inputAccessoryView?.isHidden = true
        for item in chatListenerArr {
            item.remove()
        }
        self.userListener?.remove()
        self.tabBarController?.tabBarItem.badgeValue = nil
        self.titleBaseView?.removeFromSuperview()
        self.enableViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        myID = Constants.defaults.value(forKey: "myID") as? String ?? ""
        if myID != "" && Constants.defaults.value(forKey: "LoggedIn") as? Bool == true && appDelegate.firebaseDb != nil {
            DispatchQueue.main.async {
                self.loginView?.removeFromSuperview()
                self.loginView = nil
                Spinner.start()
            }
            self.queryArr = baseQuery()
           
            if self.myUserTask == nil { appDelegate.firebaseDb?.collection(Constants.firebasePathConstants.users).document(myID).getDocument { (snapshot, err) in
                    guard let snapshot = snapshot else {
                        print("Error fetching documents results: \(err?.localizedDescription ?? "")")
                        return
                    }
                    if let snapShotData = snapshot.data() {
                        if let task = UserTask(dictionary: snapShotData , id: snapshot.documentID) {
                            self.myUserTask = task
                        } else {
                            fatalError("Unable to initialize type \(UserTask.self) with dictionary \(snapShotData)")
                        }
                    }
                }
            }
        }
        self.view.inputAccessoryView?.isHidden = false
        replyPlaceholderAndSendBtnSetting()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if titleBaseView != nil {
            if self.navigationController?.navigationBar.subviews.contains(titleBaseView!) == false {
                self.navigationController?.navigationBar.addSubview(titleBaseView!)
            }
        }
        self.tableBottomConstraint.constant = replyBarOnVC?.frame.height == nil ? 60: replyBarOnVC!.frame.height
        self.tabBarController?.tabBarItem.badgeValue = nil
        if replyTextView != nil {
            if replyTextView!.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
                self.sendBtn?.isEnabled = true
            } else {
                self.sendBtn?.isEnabled = false
            }
        }
        var documentExists = false
        if queryArr != nil {
            var i = 1
            for query in queryArr! {
                self.chatListenerArr.append(query.addSnapshotListener { (snapshot, error) in
                    guard let nonNilSnapshot = snapshot else {
                        print("Error fetching documents results: \(error!)")
                        return
                    }
                    let _ = nonNilSnapshot.documents.map { (document) in
                        documentExists = true
                        self.documentID = document.documentID
                        if let blockedByString = document.data()["blockedBy"] as? String {
                            if blockedByString == self.myID {
                                self.iBlockedUser = true
                            } else {
                                self.iBlockedUser = false
                            }
                            if blockedByString == self.usersID {
                                self.userBlockedMe = true
                            } else {
                                self.userBlockedMe = false
                            }
                        } else {
                            self.userBlockedMe = false
                            self.iBlockedUser = false
                        }
                        self.chatOnOffView()
                        var showAfterThisTime: Int64 = 0
                        self.member_a = document.data()["member_a"] as? String ?? ""
                        self.member_b = document.data()["member_b"] as? String ?? ""
                        self.last_deleted_time_a = document.data()["last_deleted_time_a"] as? Int64 ?? 0
                        self.last_deleted_time_b = document.data()["last_deleted_time_b"] as? Int64 ?? 0
                        if self.member_a == self.myID {
                            showAfterThisTime = self.last_deleted_time_a
                        }
                        if self.member_b == self.myID {
                            showAfterThisTime = self.last_deleted_time_b
                        }
                        self.queryForChats = document.reference.collection("chats").whereField("time", isGreaterThanOrEqualTo: showAfterThisTime).order(by: "time", descending: true)
                        self.chatListenerArr.append(self.queryForChats!.limit(to: 20).addSnapshotListener { (chatDocuments, err) in
                            guard let snapshotChat = chatDocuments else {
                                print("Error fetching documents results: \(err!)")
                                return
                            }
                            if snapshotChat.count > 0 {
                                self.enableViews()
                            }
                            if !documentExists && self.postSnippet != "" && self.postSnippet != nil {
                                let alertController = UIAlertController(title: Constants.chatRelated.messageText, message: Constants.alertMessages.profileWillBeVisibleAlertText + (savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId].minimalDetails?.name as? String ?? Constants.clientSpecificConstantsStructName.appName), preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: Constants.otherConsts.cancelText, style: .default, handler: {(action) -> Void in
                                self.navigationController?.popViewController(animated: true)
                                })
                                alertController.addAction(cancelAction)
                                let okAction = UIAlertAction(title: Constants.otherConsts.okText, style: .default, handler: nil)
                                alertController.addAction(okAction)
                                self.present(alertController, animated: true, completion: { [weak self] in
                                    self?.enableViews()
                                })
                            } else {
                                let _ = snapshotChat.documents.map { (document) in
                                    if let newTask = MessagesTask(dictionary: document.data() , id: document.documentID) {
                                        if let index = self.tasks.firstIndex(where: { $0._id == document.documentID }) {
                                            self.tasks[index] = newTask
                                            self.documents[index] = document
                                        } else {
                                            if let lastTask = self.tasks.last, newTask.time > lastTask.time {
                                                self.tasks.append(newTask)
                                                self.documents.append(document)
                                            } else {
                                                self.tasks.insert(newTask, at: 0)
                                                self.documents.insert(document, at: 0)
                                            }
                                        }
                                    }
                                }
                                if i >= self.queryArr!.count {
                                    DispatchQueue.main.async {
                                        self.chatTableView.reloadData()
                                        self.chatTableView.layoutIfNeeded()
                                        if self.tasks.count > 0 {
                                            self.chatTableView.scrollToRow(at: IndexPath(row: self.tasks.count - 1, section: 0), at: .top, animated: false)
                                        }
                                    }
                                }
                            }
                        })
                    }
                    i += 1
                })
            }
        } else {
            loginView = loginToQkopyView(frame: CGRect(x: 0, y: 0, width: 200, height: 100)).loadNib() as? loginToQkopyView
            self.view.addSubview(loginView!)
            loginView?.center = self.view.center
        }
        self.userListener = appDelegate.firebaseDb?.collection(Constants.firebasePathConstants.users).document(usersID).addSnapshotListener { (document, error) in
            guard let snapshot = document else {
                print("Error fetching documents results: \(error!)")
                return
            }
            if let snapShotData = snapshot.data() {
                if !documentExists && self.postSnippet != "" && self.postSnippet != nil {
                    let alertController = UIAlertController(title: Constants.chatRelated.messageText, message: Constants.alertMessages.profileWillBeVisibleAlertText + (savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId].minimalDetails?.name as? String ?? Constants.clientSpecificConstantsStructName.appName), preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: Constants.otherConsts.cancelText, style: .default, handler: {(action) -> Void in
                    self.navigationController?.popViewController(animated: true)
                    })
                    alertController.addAction(cancelAction)
                    let okAction = UIAlertAction(title: Constants.otherConsts.okText, style: .default, handler: nil)
                    alertController.addAction(okAction)
                    self.present(alertController, animated: true, completion: { [weak self] in
                        self?.enableViews()
                    })
                } else {
                    if let task = UserTask(dictionary: snapShotData , id: snapshot.documentID) {
                        self.receiversUserTask = task
                        self.usersChatOn = task.chatOn
                        self.myChatOn = Constants.defaults.value(forKey: "myChatOn") as? Bool
                        self.chatOnOffView()
                        if self.tasks.count == 0 {
                            self.enableViews()
                        }
                        self.deviceType = task.type
                        self.notificationId = task.notificationId
                    } else {
                        fatalError("Unable to initialize type \(UserTask.self) with dictionary \(snapShotData)")
                    }
                }
            } else {
                let alertController = UIAlertController(title: Constants.chatRelated.messageText, message: appDelegate.reachability.isReachable == true ? ((savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId].minimalDetails?.name as? String ?? Constants.clientSpecificConstantsStructName.appName) + Constants.alertMessages.oldVersionUsageText): Constants.errorMessages.connectionLost, preferredStyle: .alert)
                let okAction = UIAlertAction(title: Constants.otherConsts.okText, style: .default, handler: {(action) -> Void in
                    self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: { [weak self] in
                    self?.enableViews()
                })
            }
        }
    }
    
    override var inputAccessoryView: UIView? {
        return replyBarOnVC
    }
    

        
    func chatOnOffView() {
//        var userBlockedMeOrIsDeactivated = false
        if let arr = Constants.defaults.value(forKey: "blockedIDsStore") as? [String], arr.contains(self.usersID) {
            iBlockedUserFromContacts = true
        }
//        if !savedContactRefDict.keys.contains(self.usersID) {
//            userBlockedMeOrIsDeactivated = true
//        }
        if myChatOn == false || usersChatOn == false || userBlockedMe == true || iBlockedUser == true || iBlockedUserFromContacts == true /*|| userBlockedMeOrIsDeactivated == true*/ {
            if self.labelOnChatArea == nil {
                self.labelOnChatArea = UILabel(frame: CGRect(x: 0, y: UIScreen.main.bounds.height - 60, width: self.replyBarOnVC!.frame.width, height: 60))
            }
            if self.myChatOn == false {
                self.labelOnChatArea?.text = Constants.chatRelated.iTurnedOffMessageText
            } else if self.iBlockedUser == true || self.iBlockedUserFromContacts == true {
                self.labelOnChatArea?.text = Constants.chatRelated.iBlockedUserText.replacingOccurrences(of: "**", with: nameLabel?.text ?? "the user")
            } else if self.userBlockedMe == true || self.usersChatOn == false /*|| userBlockedMeOrIsDeactivated*/ {
                self.labelOnChatArea?.text = Constants.chatRelated.userTurnedOffMessage.replacingOccurrences(of: "**", with: nameLabel?.text ?? "User")
            }
            self.labelOnChatArea?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 0)
            self.labelOnChatArea?.numberOfLines = 0
            self.labelOnChatArea?.textAlignment = .center
            if #available(iOS 13.0, *) {
                self.labelOnChatArea?.backgroundColor = UIColor.systemGroupedBackground
            } else {
                self.labelOnChatArea?.backgroundColor = UIColor.groupTableViewBackground
            }
            self.labelOnChatArea?.isEnabled = false
            self.view.addSubview(self.labelOnChatArea!)
            self.inputAccessoryView?.isHidden = true
        } else {
            self.labelOnChatArea?.removeFromSuperview()
            self.labelOnChatArea = nil
            self.inputAccessoryView?.isHidden = false
        }
        replyPlaceholderAndSendBtnSetting()
    }
    
    //MARK: - General Methods
    //nav bar setting
    func titleBarSetting() {
        if titleBaseView == nil {
            titleBaseView = UIView(frame: CGRect(x: 40, y: 0, width: self.navigationController!.navigationBar.frame.width - 80, height: 40))
        }
        let settingsImage = UIImage(named: "Menu_Dot")!.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        let profileImageView = UIImageView(image: appDelegate.cropImage(img: self.utilsClass.getImage(isProfileImg: true, endPath: savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId].minimalDetails?.imageName as? String ?? "")))
        profileImageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        profileImageView.layer.cornerRadius = 20
        profileImageView.clipsToBounds = true
        nameLabel = UILabel(frame: CGRect(x: 50, y: 0, width: titleBaseView!.frame.width - 60, height: 40))
        nameLabel?.text = (savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId].minimalDetails?.name as? String ?? Constants.clientSpecificConstantsStructName.appName)
        nameLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        titleBaseView?.addSubview(profileImageView)
        titleBaseView?.addSubview(nameLabel!)
        self.navigationController?.navigationBar.addSubview(titleBaseView!)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: settingsImage, style: .done, target: self, action: #selector(self.moreBtnTapped))
            //UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(self.moreBtnTapped))
        self.navigationController?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        if #available(iOS 11.0, *) {
            navigationItem.largeTitleDisplayMode = .never
        } else {
            // Fallback on earlier versions
        }
        DispatchQueue.main.async {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        }
    }
    
    func enableViews() {
        DispatchQueue.main.async {
            Spinner.stop()
            self.replyTextView?.isUserInteractionEnabled = true
            self.galleryButton?.isUserInteractionEnabled = true
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }

    func chatNotify(textToSend: String) {
        let params = ["notificationId": self.notificationId, "data": ["senderId": self.myID, "messageBody": textToSend, "item": "", "time": "\(Date().millisecondsSince1970)", "messageType": "text"], "type": self.deviceType] as [String: Any]
        apiCall.handleApiRequest(endPart: Constants.apiCalls.chatNotify, params: params, completion: {(responseJson, errorDesc, timeout) in
            if responseJson != nil && errorDesc == "" {
                print("notification sent")
            } else {
                print(errorDesc)
            }
        })
    }
    
    func replyPlaceholderAndSendBtnSetting() {
        if replyTextView!.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            (replyTextView?.viewWithTag(100) as? UILabel)?.isHidden = true
        } else {
            replyTextView?.placeholder = Constants.otherConsts.initialStatus
            replyTextView?.textViewChanged()
        }
        if replyTextView!.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            self.sendBtn?.isEnabled = true
        } else {
            self.sendBtn?.isEnabled = false
        }
    }
    
    //MARK: - Setting views
    //creating view with post and location btns
    func getCustomView() -> UIView {
        let customView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60))
        if #available(iOS 13.0, *) {
            customView.backgroundColor = UIColor.systemGroupedBackground
        } else {
            customView.backgroundColor = UIColor.groupTableViewBackground
        }
        let upperBorder = CALayer()
        upperBorder.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode")).cgColor
        upperBorder.frame = CGRect(x: 0, y: 0, width: customView.frame.width, height: 0.5)
        customView.layer.addSublayer(upperBorder)
        
        if postSnippet != nil && postSnippet != "" {
            postSnippetLabelOnReplyBar = UILabel(frame: CGRect(x: 10, y: 5, width: self.view.frame.width - 20, height: 40))
            postSnippetLabelOnReplyBar?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
            postSnippetLabelOnReplyBar?.textColor = UIColor.gray
            postSnippetLabelOnReplyBar?.textAlignment = .justified
            let lowerBorder = CALayer()
            lowerBorder.backgroundColor = UIColor.gray.cgColor
            lowerBorder.frame = CGRect(x: 0, y: postSnippetLabelOnReplyBar!.frame.height + 5, width: postSnippetLabelOnReplyBar!.frame.width, height: 0.5)
            postSnippetLabelOnReplyBar?.layer.addSublayer(lowerBorder)
            postSnippetLabelOnReplyBar?.text = postSnippet!
            postSnippetLabelOnReplyBar?.numberOfLines = 0
            postSnippetLabelOnReplyBar?.adjustsFontSizeToFitWidth = true
            customView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 110)
            customView.addSubview(postSnippetLabelOnReplyBar!)
        } else {
            postSnippetLabelOnReplyBar = nil
            postSnippetLabelOnReplyBar?.removeFromSuperview()
            customView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 60)
        }
        
        
        galleryButton = UIButton(frame: CGRect(x: 5, y: postSnippetLabelOnReplyBar == nil ? 5: 55, width: 50, height: 50))
        galleryButton?.addTarget(self, action: #selector(self.galleryBtnTapped), for: .touchUpInside)
        //galleryButton?.imageView?.contentMode = .scaleAspectFit
        galleryButton?.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "galleryIcon"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        galleryButton?.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        customView.addSubview(galleryButton!)
        
        replyTextView = UITextView()
        replyTextView?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 0)
        replyTextView?.layer.cornerRadius = 20
        replyTextView?.textContainerInset = UIEdgeInsets.init(top: 10.0, left: 10.0, bottom: 10, right: 10.0)
        replyTextView?.clipsToBounds = true
        replyTextView?.delegate = self
        replyTextView?.placeholder = Constants.otherConsts.initialStatus
        replyTextView?.textViewChanged()
        if #available(iOS 13.0, *) {
            replyTextView?.backgroundColor = UIColor.systemGray5
            replyTextView?.textColor = .label
        } else {
            replyTextView?.backgroundColor = UIColor.white
            replyTextView?.textColor = .black
        }
        //        docButton = UIButton(frame: CGRect(x: galleryButton!.frame.origin.x - 55, y: 5, width: 50, height: 50))
        //        docButton?.addTarget(self, action: #selector(StatusPreviewViewController.docBtnTapped), for: .touchUpInside)
        //        docButton?.imageView?.contentMode = .scaleAspectFit
        //        docButton?.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "pdfIcon")), for: .normal)
        //        customView.addSubview(docButton!)
        
        sendBtn = UIButton(frame: CGRect(x: customView.frame.width - 55, y: postSnippetLabelOnReplyBar == nil ? 5: 55, width: 50, height: 50))
        sendBtn?.addTarget(self, action: #selector(self.sendBtnTapped), for: .touchUpInside)
        sendBtn?.imageView?.contentMode = .scaleAspectFit
        sendBtn?.setImage(UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "sendIcon"))?.withRenderingMode(.alwaysTemplate), for: .normal)
        sendBtn?.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        replyTextView?.frame = CGRect(x: 55, y: postSnippetLabelOnReplyBar == nil ? 10: 60, width: sendBtn!.frame.origin.x - 55, height: 40)
        customView.addSubview(replyTextView!)
        customView.addSubview(sendBtn!)
        return customView
    }
    
    //MARK: - Targets
    @objc func galleryBtnTapped() {
        self.dismissKeyboard()
        let actionSheetController = UIAlertController()
        if actionSheetController.popoverPresentationController == nil {
            self.inputAccessoryView?.isHidden = true
        }
        let cancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
            print("Cancel")
            self.inputAccessoryView?.isHidden = false
        }
        actionSheetController.addAction(cancelActionButton)
        
        let takePhotoActionButton = UIAlertAction(title: "Take Photo", style: UIAlertAction.Style.default, handler: { (action) in
            self.imagePicker = UIImagePickerController()
            self.inputAccessoryView?.isHidden = false
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                self.imagePicker?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
                self.imagePicker?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
                self.imagePicker?.delegate = self
                self.imagePicker?.mediaTypes = ["public.image"]
                self.imagePicker?.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker?.allowsEditing = false
                if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
                    DispatchQueue.main.async {
                        self.present(self.imagePicker!, animated: true, completion: nil)
                    }
                } else {
                    AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                        if granted == true {
                            DispatchQueue.main.async {
                                self.present(self.imagePicker!, animated: true, completion: nil)
                            }
                        } else {
                            DispatchQueue.main.async(execute: { () -> Void in
                                self.dismiss(animated: true, completion: nil)
                                let message = Constants.alertMessages.cameraAccessDenied
                                let alertController = UIAlertController(title: Constants.alertTitles.accessDeniedText, message: message, preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: Constants.otherConsts.cancelText, style: .default, handler: nil)
                                alertController.addAction(cancelAction)
                                let settingsAction = UIAlertAction(title: Constants.otherConsts.settings, style: .default) { (_) -> Void in
                                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                        return
                                    }
                                    
                                    if UIApplication.shared.canOpenURL(settingsUrl) {
                                        UIApplication.shared.openURL(settingsUrl)
                                    }
                                }
                                alertController.addAction(settingsAction)
                                self.present(alertController, animated: true, completion: nil)
                            })
                        }
                    })
                }
            }
        })
        actionSheetController.addAction(takePhotoActionButton)
        
        let pickFromGalleryActionButton = UIAlertAction(title: "Choose Photo", style: .default) { (action) in
            self.imagePicker = UIImagePickerController()
            self.inputAccessoryView?.isHidden = false
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                self.imagePicker?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
                self.imagePicker?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
                self.imagePicker?.delegate = self
                self.imagePicker?.mediaTypes = ["public.image"]
                self.imagePicker?.sourceType = UIImagePickerController.SourceType.photoLibrary
                self.imagePicker?.allowsEditing = false
                PHPhotoLibrary.requestAuthorization({ (status) in
                    if status == .authorized {
                        DispatchQueue.main.async {
                            self.present(self.imagePicker!, animated: true, completion: nil)
                        }
                    } else {
                        DispatchQueue.main.async(execute: { () -> Void in
                            let message = Constants.alertMessages.photosAccessDenied
                            let alertController = UIAlertController(title: Constants.alertTitles.accessDeniedText, message: message, preferredStyle: .alert)
                            let cancelAction = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel, handler: nil)
                            alertController.addAction(cancelAction)
                            let settingsAction = UIAlertAction(title: Constants.otherConsts.settings, style: .default) { (_) -> Void in
                                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                    return
                                }
                                
                                if UIApplication.shared.canOpenURL(settingsUrl) {
                                    UIApplication.shared.openURL(settingsUrl)
                                }
                            }
                            alertController.addAction(settingsAction)
                            self.present(alertController, animated: true, completion: nil)
                        })
                    }
                })
            }
        }
        actionSheetController.popoverPresentationController?.sourceRect = self.galleryButton?.bounds ?? self.view.bounds
        actionSheetController.popoverPresentationController?.sourceView = self.galleryButton ?? self.view
        actionSheetController.addAction(pickFromGalleryActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
        
    }
    
    //image picker : after picking image
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        if var postImage  = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            if picker.sourceType == .photoLibrary {
                //self.dismiss(animated: true, completion: nil)
                let imageURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.referenceURL)] as? NSURL
                setImageName = imageURL?.path ?? "savedImage.JPG"
                imagePicker = picker
            } else if picker.sourceType == .camera {
                //UIImageWriteToSavedPhotosAlbum(postImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                self.dismiss(animated: true, completion: nil)
                setImageName = "cameraClick.JPG"
                imagePicker = nil
            }
            if setImageName.components(separatedBy: ".").last!.lowercased() == "heic" {
                setImageName = setImageName.lowercased().replacingOccurrences(of: ".heic", with: ".png")
                postImage = UIImage(data: postImage.pngData()!)!
            }
            passSelectedImageToEditVC(image: postImage)
        }
    }
    
    func passSelectedImageToEditVC(image: UIImage) {
        let croppingStyle = CropViewCroppingStyle.default
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        cropController.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropController.toolbar.doneTextButton.setTitleColor(appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode")), for: .normal)
        cropController.modalPresentationStyle = .fullScreen
        if imagePicker == nil {
            self.present(cropController, animated: true, completion: nil)
        } else {
            imagePicker?.present(cropController, animated: true, completion: nil)
        }
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //let endPart = appDelegate.fetchFromPlist(plistName: "constants", key: "updateUserDetailsURL")
        let imageData = self.utilsClass.compressImage(image: image, limit: 300, width: 350)
        Spinner.start()
        apiCall.getLinkAfterUploadingFile(fileData: imageData, name: setImageName, type: "msg", id: "", completion: {[weak self] (imageLink, errorDesc, thumbnailLink) in
            guard let strongSelf = self else {
                return
            }
            if errorDesc == "" {
                let object = MessagesTask(_id: "", conversationId: "", deleted: false, messageBody: "", senderId: strongSelf.myID, receiverId: strongSelf.usersID, item: imageLink, messageType: "image", postSnippet: "", time: Date().millisecondsSince1970)
                var blockedBy: String?
                if self?.iBlockedUser == true {
                    blockedBy = self?.myID
                }
                if self?.userBlockedMe == true {
                    blockedBy = self?.usersID
                }
                self?.utilsClass.setFireBaseConversationData(myUserTask: self?.myUserTask, receiversUserTask: self?.receiversUserTask, messageObject: object, receiverId: strongSelf.usersID, lastUpdatedTime: Date().millisecondsSince1970, documentID: strongSelf.documentID, blockedBy: blockedBy, last_deleted_time_a: self?.last_deleted_time_a ?? 0, last_deleted_time_b: self?.last_deleted_time_b ?? 0, member_a: self?.member_a ?? "", member_b: self?.member_b ?? "", completion: {[weak self] (success, documentID) in
                    if success {
                        self?.documentID = documentID
                        cropViewController.dismiss(animated: true, completion: { [weak self] in
                            if strongSelf.tasks.count > 0 {
                                self?.chatTableView.scrollToRow(at: IndexPath(row: self?.tasks.count == 1 ? 0:  strongSelf.tasks.count - 1, section: 0), at: .top, animated: true)
                            }
                        })
                        self?.imagePicker?.dismiss(animated: true, completion: nil)
                        if strongSelf.tasks.count > 0 {
                            self?.chatTableView.scrollToRow(at: IndexPath(row: self?.tasks.count == 1 ? 0:  strongSelf.tasks.count - 1, section: 0), at: .top, animated: true)
                        }
                        self?.enableViews()
                        let params = ["notificationId": strongSelf.notificationId, "data": ["senderId": strongSelf.myID, "messageBody": "image", "item": "", "time": "\(Date().millisecondsSince1970)", "messageType": "text"], "type": strongSelf.deviceType] as [String: Any]
                        apiCall.handleApiRequest(endPart: Constants.apiCalls.chatNotify, params: params, completion: {(responseJson, errorDesc, timeout) in
                            if responseJson != nil && errorDesc == "" {
                                print("notification sent")
                            } else {
                                print(errorDesc)
                            }
                        })
                    } else {
                        let viewController = cropViewController.children.first!
                        viewController.modalTransitionStyle = .coverVertical
                        viewController.presentingViewController?.dismiss(animated: true, completion: nil)
                        self?.imagePicker?.dismiss(animated: true, completion: nil)
                        self?.enableViews()
                    }
                })
            } else {
                let viewController = cropViewController.children.first!
                viewController.modalTransitionStyle = .coverVertical
                viewController.presentingViewController?.dismiss(animated: true, completion: nil)
                self?.imagePicker?.dismiss(animated: true, completion: nil)
                self?.enableViews()
            }
        })
    }
    
    @objc func sendBtnTapped() {
        let textToSend = self.replyTextView?.text ?? ""
        var object: MessagesTask? = MessagesTask(_id: "", conversationId: "", deleted: false, messageBody: textToSend, senderId: myID, receiverId: self.usersID, item: "", messageType: "text", postSnippet: postSnippet ?? "", time: Date().millisecondsSince1970)
        var blockedBy: String?
        if iBlockedUser == true {
            blockedBy = myID
        }
        if userBlockedMe == true {
            blockedBy = usersID
        }
        if (attachedImageLink == "" || attachedImageLink == nil) && (replyTextView?.text == "" || replyTextView?.text == nil) {
            object = nil
        }
        self.replyTextView?.text = ""
        postSnippet = nil
        self.dismissKeyboard()
        self.utilsClass.setFireBaseConversationData(myUserTask: self.myUserTask, receiversUserTask: self.receiversUserTask, messageObject: object, receiverId: usersID, lastUpdatedTime: Date().millisecondsSince1970, documentID: documentID, blockedBy: blockedBy, last_deleted_time_a: last_deleted_time_a, last_deleted_time_b: last_deleted_time_b, member_a: member_a, member_b: member_b, completion: {[weak self] (success, documentID) in
            guard let strongSelf = self else {
                return
            }
            if success {
                self?.chatTableView.layoutIfNeeded()
                if strongSelf.tasks.count > 0 {
                    self?.chatTableView.scrollToRow(at: IndexPath(row: self?.tasks.count == 1 ? 0:  strongSelf.tasks.count - 1, section: 0), at: .top, animated: true)
                }
                self?.documentID = documentID
                if object != nil {
                    if self?.notificationId == "" {
                        if let docRef = appDelegate.firebaseDb?.collection(Constants.firebasePathConstants.users).document(strongSelf.usersID) {
                        
                        // Force the SDK to fetch the document from the cache. Could also specify
                        // FirestoreSource.server or FirestoreSource.default.
                            docRef.getDocument(source: .cache) { (document, error) in
                                if let document = document {
                                    self?.notificationId = document.data()?["notificationId"] as? String ?? ""
                                    if self?.deviceType == "" {
                                        self?.deviceType = document.data()?["type"] as? String ?? ""
                                    }
                                    self?.chatNotify(textToSend: textToSend)
                                }
                            }
                        }
                    } else {
                        self?.chatNotify(textToSend: textToSend)
                    }
                }
                //chatNotify
            }
        })
        //appDelegate.firebaseDb
        
    }
    
    //to dismiss keyboard
    @objc func dismissKeyboard() {
        self.replyTextView?.resignFirstResponder()
        self.tableBottomConstraint.constant = replyBarOnVC?.frame.height == nil ? 60: replyBarOnVC!.frame.height
    }
    
    @objc func moreBtnTapped() {
        self.dismissKeyboard()
        let actionSheetController = UIAlertController()
        if actionSheetController.popoverPresentationController == nil {
            self.inputAccessoryView?.isHidden = true
        }
        let cancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
            print("Cancel")
            if self.myChatOn == false || self.usersChatOn == false || self.userBlockedMe == true || self.iBlockedUser == true || self.iBlockedUserFromContacts == true /*|| userBlockedMeOrIsDeactivated == true*/ {
                self.inputAccessoryView?.isHidden = true
            } else {
                self.inputAccessoryView?.isHidden = false
            }
        }
        actionSheetController.addAction(cancelActionButton)
        if !self.myMutedList.contains(self.usersID) {
            let muteChatAction = UIAlertAction(title: "Mute Notifications", style: UIAlertAction.Style.default, handler: { (action) in
                if self.myChatOn == false || self.usersChatOn == false || self.userBlockedMe == true || self.iBlockedUser == true || self.iBlockedUserFromContacts == true /*|| userBlockedMeOrIsDeactivated == true*/ {
                    self.inputAccessoryView?.isHidden = true
                } else {
                    self.inputAccessoryView?.isHidden = false
                }
                self.myMutedList.append(self.usersID)
                Constants.defaults.set(self.myMutedList, forKey: "myNotificationMutedList")
                DispatchQueue.global(qos: .background).async {
                    self.utilsClass.setFireBaseUserData()
                }
            })
            actionSheetController.addAction(muteChatAction)
        } else {
            let unmuteChatAction = UIAlertAction(title: "Unmute Notifications", style: UIAlertAction.Style.default, handler: { (action) in
                if self.myChatOn == false || self.usersChatOn == false || self.userBlockedMe == true || self.iBlockedUser == true || self.iBlockedUserFromContacts == true /*|| userBlockedMeOrIsDeactivated == true*/ {
                    self.inputAccessoryView?.isHidden = true
                } else {
                    self.inputAccessoryView?.isHidden = false
                }
                let index = self.myMutedList.firstIndex(of: self.usersID)
                self.myMutedList.remove(at: index!)
                Constants.defaults.set(self.myMutedList, forKey: "myNotificationMutedList")
                DispatchQueue.global(qos: .background).async {
                    self.utilsClass.setFireBaseUserData()
                }
            })
            actionSheetController.addAction(unmuteChatAction)
        }
        actionSheetController.popoverPresentationController?.barButtonItem = self.navigationItem.rightBarButtonItem
        self.present(actionSheetController, animated: true, completion: nil)
    }
    
    //other delegates
    
    //MARK: - Delegates
    func textViewDidBeginEditing(_ textView: UITextView) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow), name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        self.tapForKeyboardDismissal.cancelsTouchesInView = false
        self.tapForKeyboardDismissal.delegate = self
        self.view.addGestureRecognizer(tapForKeyboardDismissal)
        self.replyTextView?.becomeFirstResponder()
        replyPlaceholderAndSendBtnSetting()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        replyPlaceholderAndSendBtnSetting()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        self.view.removeGestureRecognizer(tapForKeyboardDismissal)
        replyPlaceholderAndSendBtnSetting()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view!.description.contains("UIButton") {
            return false
        } else {
            return true
        }
    }
    
    //open image in new vc
    func pushToVC(currentImagePath: String, imagePaths: [String], titleName: String, indexPath: IndexPath?) {
        if let fullVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "openImageVC") as? OpenImageViewController{
            fullVC.imagePaths = imagePaths
            if let index = imagePaths.firstIndex(of: currentImagePath) {
                fullVC.currentTappedImageIndex = index
            }
            self.copiedImageUrlString = ""
            fullVC.name = titleName
            //fullVC.chatingVcParentClass = self
            if indexPath != nil {
                let messageObject = tasks[indexPath!.row]
                if messageObject.messageType == "image" {
                    self.copiedImageUrlString = messageObject.item
                }
            }
            //fullVC.modalPresentationStyle = .overCurrentContext
            fullVC.navBarFrame = self.navigationController!.navigationBar.frame
            fullVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(fullVC, animated: true)
            //tabBarController?.present(fullVC, animated: true, completion: nil)
        }
    }
    
    func showOptionsOnMessageLongPress(indexPath: IndexPath) {
        self.copiedImageUrlString = ""
        self.copiedText = ""
        if tasks.count > indexPath.row {
            let messageObject = tasks[indexPath.row]
            if messageObject.deleted == false {
                let incomingMessage = messageObject.senderId == myID ? false: true
                if messageObject.messageType == "text" || incomingMessage == false || messageObject.messageType == "image" {
                    let actionSheetController = UIAlertController()
                    if actionSheetController.popoverPresentationController == nil {
                        self.inputAccessoryView?.isHidden = true
                    }
                    let cancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
                        print("Cancel")
                        self.inputAccessoryView?.isHidden = false
                    }
                    actionSheetController.addAction(cancelActionButton)
                    if incomingMessage == false {
                        let deleteMessageAction = UIAlertAction(title: Constants.chatRelated.deleteMessageText, style: UIAlertAction.Style.default, handler: { (action) in
                            self.utilsClass.updateByDeletingDoc(conversationId: self.documentID, chatID: self.tasks[indexPath.row]._id, myID
                                : self.myID, usersID: self.usersID)
                            self.inputAccessoryView?.isHidden = false
                        })
                        actionSheetController.addAction(deleteMessageAction)
                    }
                    if messageObject.messageType == "text" {
                        let copyMessageAction = UIAlertAction(title: Constants.chatRelated.copyMessageText, style: UIAlertAction.Style.default, handler: { (action) in
                            UIPasteboard.general.string = messageObject.messageBody
                            self.inputAccessoryView?.isHidden = false
                        })
                        actionSheetController.addAction(copyMessageAction)
                    }
                    let cell = self.chatTableView.cellForRow(at: indexPath) as? ChatTableViewCell
                    //actionSheetController.popoverPresentationController?.permittedArrowDirections = .down
                    //actionSheetController.popoverPresentationController?.sourceRect = CGRect(x: x, y: y, width: width, height: height)
                    actionSheetController.popoverPresentationController?.sourceView = cell?.bubbleImageView ?? self.view
                    self.present(actionSheetController, animated: true, completion: nil)
                }
            }
        }
    }

    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if tasks.count == 0 {
            //addBgToTab
        }
        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = chatTableView.dequeueReusableCell(withIdentifier: Constants.nameOfIdentifiers.chatCellIdentifier)! as! ChatTableViewCell
        //cell.contentView.backgroundColor = UIColor.gray
        cell.snippetLabel?.isHidden = true
        cell.lowerBorder.removeFromSuperlayer()
        cell.bubbleImageView?.mask = nil
        cell.fullScreenDelegate = nil
        cell.longPressedMessageDelegate = self
        cell.bubbleImageView?.contentMode = .scaleToFill
        cell.bubbleImageView?.clipsToBounds = false
        cell.bubbleImageView?.backgroundColor = UIColor.clear
        
        if tasks.count > indexPath.row {
            let messageObject = tasks[indexPath.row]
            let dateString = appDelegate.chatTimeFromDateComponents(date: Date(milliseconds: messageObject.time))
            var previousDateString = ""
            if indexPath.row - 1 >= 0 {
                previousDateString = appDelegate.chatTimeFromDateComponents(date: Date(milliseconds: tasks[indexPath.row - 1].time))
            }
            let incomingMessage = messageObject.senderId == myID ? false: true
            //var bubbleText = messageObject.messageBody + "\n\n\(dateString.components(separatedBy: " ")[2] + dateString.components(separatedBy: " ")[3])"
            let actualTextWithSpacing = (messageObject.deleted == true ? Constants.chatRelated.deletedMessageText:  (messageObject.messageBody)) + ((messageObject.messageType == "text" || messageObject.deleted == true) ? "\n":"  ")
            var timeText = dateString
            if dateString.components(separatedBy: " ").count > 3 {
                timeText = dateString.components(separatedBy: " ")[2] + " " + dateString.components(separatedBy: " ")[3]
            }
            let bubbleText = NSMutableAttributedString(string: actualTextWithSpacing + timeText)
            let remainingLeftOutLength = bubbleText.length - timeText.count
            bubbleText.addAttribute(NSAttributedString.Key.font, value: UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 0) , range: NSRange(location: 0, length: remainingLeftOutLength))
            bubbleText.addAttribute(NSAttributedString.Key.font, value: UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0) , range: NSRange(location:remainingLeftOutLength,length:timeText.count))
            if messageObject.deleted == true {
                let descriptor = UIFont.italicSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .subheadline).pointSize).fontDescriptor
                bubbleText.addAttribute(NSAttributedString.Key.font, value: UIFont(descriptor: descriptor, size: 0), range: NSRange(location: 0, length: actualTextWithSpacing.count - 1))

            }
            let imagePath = messageObject.deleted == true ? "": messageObject.item
            cell.imagePath = imagePath
            cell.bubbleImageView.image = nil
            cell.bubbleTextView.isScrollEnabled = false
            cell.bubbleTextView.dataDetectorTypes = [.link, .phoneNumber]
            cell.bubbleTextView.isEditable = false
            let constraintRect = CGSize(width: 0.66 * view.frame.width,
                                        height: .greatestFiniteMagnitude)
            cell.bubbleTextView.attributedText = bubbleText
            cell.bubbleTextView.sizeToFit()
            var bubbleImageSize: CGSize?
            if messageObject.postSnippet != "" {
                cell.snippetLabel.isHidden = false
                cell.snippetLabel.text = messageObject.postSnippet
                cell.snippetLabel.numberOfLines = 0
                cell.snippetLabel.sizeToFit()
                let boundingBox = messageObject.postSnippet.boundingRect(with: constraintRect,options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [.font: cell.snippetLabel.font], context: nil)
                cell.snippetLabel.frame.size = CGSize(width: ceil(boundingBox.width), height: ceil(boundingBox.height))
                cell.addSubview(cell.snippetLabel)
            }
            if messageObject.messageBody.count > 0 || messageObject.deleted == true {
                let boundingBoxMain = bubbleText.string.boundingRect(with: constraintRect,options: [.usesLineFragmentOrigin, .usesFontLeading],attributes: [.font: UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 19)],context: nil)
                let textviewWidth = ceil(messageObject.postSnippet == "" ? boundingBoxMain.width: cell.snippetLabel.frame.size.width)
                let newSize = cell.bubbleTextView.sizeThatFits(CGSize(width: textviewWidth, height: CGFloat.greatestFiniteMagnitude))

                cell.bubbleTextView.frame.size = CGSize(width: textviewWidth, height: ceil(newSize.height))
                
                
                bubbleImageSize = CGSize(width: cell.bubbleTextView.frame.width + 20,
                                         height: messageObject.postSnippet == "" ? (cell.bubbleTextView.frame.height + 16) : (cell.snippetLabel.frame.height + cell.bubbleTextView.frame.height + 32))
            } else {
                let halfScreen = view.frame.width / 2
                bubbleImageSize = CGSize(width: halfScreen,
                                         height: halfScreen / 0.75)
                cell.bubbleImageView.frame = CGRect(x: 20,
                                                    y: 10,
                                                    width: bubbleImageSize!.width,
                                                    height: bubbleImageSize!.height)
                
            }
            
            var bubbleImage: UIImage?
            cell.chatTimeLabel.text = dateString.components(separatedBy: " ")[0]
            cell.chatTimeLabel.frame = CGRect(x: 20, y: 5, width: tableView.frame.width - 40, height: previousDateString.components(separatedBy: " ")[0] == dateString.components(separatedBy: " ")[0] ? 0:25)
            if incomingMessage {
                bubbleImage = UIImage(named: "incoming-message-bubble")!.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
                cell.snippetLabel.layer.borderColor = UIColor.black.cgColor
                cell.snippetLabel.textColor = .black
                cell.bubbleTextView.textColor = .black
                let linkAttributes: [String : Any] = [
                    NSAttributedString.Key.underlineStyle.rawValue: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor.rawValue: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))]
                cell.bubbleTextView.linkTextAttributes = Constants.sharedUtils.convertToOptionalNSAttributedStringKeyDictionary(linkAttributes)
                cell.bubbleImageView.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "bubbleGrayColorCode"))
                cell.bubbleImageView.frame = CGRect(x: 20,
                                                    y: cell.chatTimeLabel.frame.origin.y + cell.chatTimeLabel.frame.height,
                                                    width: bubbleImageSize!.width,
                                                    height: bubbleImageSize!.height)
            } else {
                bubbleImage = UIImage(named: "outgoing-message-bubble")!.resizableImage(withCapInsets: UIEdgeInsets(top: 17, left: 21, bottom: 17, right: 21), resizingMode: .stretch).withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
                cell.snippetLabel.layer.borderColor = UIColor.white.cgColor
                cell.snippetLabel.textColor = .white
                cell.bubbleTextView.textColor = .white
                let linkAttributes: [String : Any] = [
                    NSAttributedString.Key.underlineStyle.rawValue: NSUnderlineStyle.single.rawValue, NSAttributedString.Key.foregroundColor.rawValue: UIColor.white]
                cell.bubbleTextView.linkTextAttributes = Constants.sharedUtils.convertToOptionalNSAttributedStringKeyDictionary(linkAttributes)
                cell.bubbleImageView.frame =
                    CGRect(x: view.frame.width - bubbleImageSize!.width - 20,
                           y: cell.chatTimeLabel.frame.origin.y + cell.chatTimeLabel.frame.height,
                           width: bubbleImageSize!.width,
                           height: bubbleImageSize!.height)
            }
            cell.chatTimeLabel.textAlignment = .center
            cell.bubbleImageView.image = bubbleImage
            if messageObject.messageBody.count > 0 || messageObject.deleted == true {
                if messageObject.postSnippet != "" && messageObject.deleted != true {
                    cell.snippetLabel.center = CGPoint(x: cell.snippetLabel.frame.width/2 + (cell.bubbleImageView.frame.origin.x) + 10, y: cell.snippetLabel.frame.height/2 + (cell.bubbleImageView.frame.origin.y) + 8)
                    cell.bubbleTextView.center = CGPoint(x: cell.snippetLabel.frame.width/2 + (cell.bubbleImageView.frame.origin.x) + 10, y: cell.snippetLabel.frame.height + cell.snippetLabel.frame.origin.y + (cell.bubbleTextView.frame.height/2) + 8)
                } else {
                    cell.bubbleTextView.center = cell.bubbleImageView.center
                }
                cell.bubbleTextView.backgroundColor = UIColor.clear.withAlphaComponent(0.0)
                cell.addSubview(cell.bubbleTextView)
            } else {
                cell.fullScreenDelegate = self
                let maskView = UIImageView(image: bubbleImage)
                maskView.frame = cell.bubbleImageView.bounds
                cell.bubbleImageView.image = self.utilsClass.getImage(index: indexPath, isProfileImg: false, endPath: imagePath)
                if cell.bubbleImageView.image?.images != nil {
                    cell.bubbleImageView.contentMode = .center
                    if #available(iOS 13.0, *) {
                        cell.bubbleImageView.backgroundColor = UIColor.label.withAlphaComponent(0.05)
                    } else {
                        cell.bubbleImageView.backgroundColor = UIColor.black.withAlphaComponent(0.05)
                    }
                } else {
                    cell.bubbleImageView?.backgroundColor = UIColor.clear
                    cell.bubbleImageView.contentMode = .scaleAspectFill
                }
                cell.bubbleImageView.clipsToBounds = true
                cell.bubbleTextView.frame = CGRect(x: 0, y: cell.bubbleImageView.frame.height - 30, width: cell.bubbleImageView.frame.width, height: 30)
                cell.bubbleTextView.backgroundColor = UIColor.black.withAlphaComponent(0.1)
                cell.bubbleTextView.textColor = UIColor.white
                cell.bubbleImageView.addSubview(cell.bubbleTextView)
                cell.bubbleImageView.mask = maskView
            }
            tableView.rowHeight = cell.bubbleImageView.frame.height + 20 + cell.chatTimeLabel.frame.height
            heightDict[indexPath.row] = tableView.rowHeight
//            for subview in cell.subviews {
//                subview.setNeedsLayout()
//                subview.setNeedsDisplay()
//            }
        } else {
            tableView.rowHeight = 0
        }
        if myID != "" && Constants.defaults.value(forKey: "LoggedIn") as? Bool == true {
            if initialLoad {
                DispatchQueue.main.async {
                    if indexPath.row == self.tasks.count - 2 {
                        self.chatTableView.setContentOffset(CGPoint(x: 0, y: self.chatTableView.contentSize.height - self.chatTableView.frame.height), animated: true)
                    }
                    if indexPath.row == self.tasks.count - 1 {
                        self.chatTableView.isHidden = false
                        self.chatTableView.scrollToRow(at: indexPath, at: .top, animated: false)
                        self.initialLoad = false
                        self.enableViews()
                    }
                }
            }
        }
        // Configure the cell...
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height = heightDict[indexPath.row] {
            //tableView.estimatedRowHeight = height
            return height
        }
        return 100
    }
    
    //MARK: Notifications for Keyboard
    @objc func keyboardDidShow(notification: NSNotification) {
        if let frameY = notification.userInfo?["UIKeyboardBoundsUserInfoKey"] as? NSValue {
            if frameY.cgRectValue.height > CGFloat(100) {
                self.tableBottomConstraint.constant = frameY.cgRectValue.height
                if self.tasks.count > 0 {
                    DispatchQueue.main.async {
                        self.chatTableView.layoutIfNeeded()
                        self.chatTableView.scrollToRow(at: IndexPath(row: self.tasks.count == 1 ? 0:  self.tasks.count - 1, section: 0), at: .top, animated: true)
                    }
                }
            }
        }
    }
    @objc func keyboardWillHide(notification: NSNotification) {
        self.tableBottomConstraint.constant = replyBarOnVC?.frame.height == nil ? 60: replyBarOnVC!.frame.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    //MARK: - Scroll delegates
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let currentVelocityY =  scrollView.panGestureRecognizer.velocity(in: scrollView.superview).y
        let currentVelocityYSign = Int(currentVelocityY).signum()
        if currentVelocityYSign != lastVelocityYSign &&
            currentVelocityYSign != 0 {
            lastVelocityYSign = currentVelocityYSign
        }
        //let contentHeight = scrollView.contentSize.height
        if offsetY < 50 && lastVelocityYSign > 0 {
        //if offsetY > contentHeight - scrollView.frame.height - 50 {
            // Bottom of the screen is reached
            if !fetchingMore && queryForChats != nil {
                paginateData()
            }
        }
    }
    
    // Paginates data
    func paginateData() {
        
        fetchingMore = true
        
        var query: Query!
        
        if self.tasks.isEmpty {
            query = self.queryForChats!.limit(to: 30)
            print("First 3 rides loaded")
        } else if self.documents.first != nil {
            query = self.queryForChats!.start(afterDocument: self.documents.first!).limit(to: 15)
            print("Next 1 rides loaded")
        }
        self.chatListenerArr.append(query.addSnapshotListener { (chatDocuments, err) in
            guard let snapshot = chatDocuments else {
                print("Error fetching documents results: \(err?.localizedDescription ?? "")")
                return
            }
            if let err = err {
                print("\(err.localizedDescription)")
            } else if snapshot.isEmpty {
                self.fetchingMore = false
                return
            } else {
                let _ = snapshot.documents.map { (document) in
                    if let newTask = MessagesTask(dictionary: document.data() , id: document.documentID) {
                        if let index = self.tasks.firstIndex(where: { $0._id == document.documentID }) {
                            self.tasks[index] = newTask
                            self.documents[index] = document
                        } else {
                            if let lastTask = self.tasks.last, newTask.time > lastTask.time {
                                self.tasks.append(newTask)
                                self.documents.append(document)
                            } else {
                                self.tasks.insert(newTask, at: 0)
                                self.documents.insert(document, at: 0)
                            }
                        }
                    }
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                    self.chatTableView.reloadData()
                    self.fetchingMore = false
                })
                
            }
        })
    }
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        self.labelOnChatArea?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 0)
        nameLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        postSnippetLabelOnReplyBar?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        replyTextView?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .body), size: 0)
    }

    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension UITextView {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    func textViewChanged() {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
            if self.tag == 101 {
                self.textColor = UIColor.white
                placeholderLabel.textColor = UIColor.white.withAlphaComponent(0.5)
                if placeholderLabel.isHidden {
                    self.textAlignment = .center
                } else {
                    self.textAlignment = .left
                }
            } else {
                if #available(iOS 13.0, *) {
                    self.textColor = UIColor.label
                    placeholderLabel.textColor = UIColor.secondaryLabel
                } else {
                    self.textColor = UIColor.black
                    placeholderLabel.textColor = UIColor.lightGray
                }
            }
            if !placeholderLabel.isHidden {
                resizePlaceholder()
            }
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainerInset.left + 5
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2) - 10
            placeholderLabel.sizeToFit()
            let labelHeight = placeholderLabel.frame.height
            placeholderLabel.numberOfLines = 0
            if labelX < 50 {
                placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: UIScreen.main.bounds.width - 20, height: labelHeight)
            } else {
                placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
            }
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        if self.tag == 101 {
           placeholderLabel.textColor = UIColor.white.withAlphaComponent(0.5)
        } else {
            placeholderLabel.textColor = UIColor.lightGray
        }
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
    }
    
}
// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
