//
//  MinimalContactDetails.swift
//  Qkopy
//
//  Created by Qkopy team on 17/09/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import Foundation
class MinimalContactDetails: NSObject, NSCoding {
    var name: String
    var imageName: String
    var profileVerified: Bool
    var isPremium: Bool
    var muted: Bool
    var mutedTill: String
    var number: String
    var qkopyId: String
    var actualProfileName: String
    var about: String
    var createdDate: String?
    var location: [String: Any]?

    init(name: String, imageName: String, profileVerified: Bool, isPremium: Bool, muted: Bool, mutedTill: String, number: String, qkopyId: String, actualProfileName: String, about: String, createdDate: String? = nil, location: [String: Any]? = nil) {
        self.name = name
        self.imageName = imageName
        self.profileVerified = profileVerified
        self.isPremium = isPremium
        self.muted = muted
        self.mutedTill = mutedTill
        self.number = number
        self.qkopyId = qkopyId
        self.actualProfileName = actualProfileName
        self.about = about
        self.createdDate = createdDate
        self.location = location
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(imageName, forKey: "imageName")
        aCoder.encode(profileVerified, forKey: "profileVerified")
        aCoder.encode(isPremium, forKey: "isPremium")
        aCoder.encode(muted, forKey: "muted")
        aCoder.encode(mutedTill, forKey: "mutedTill")
        aCoder.encode(number, forKey: "number")
        aCoder.encode(qkopyId, forKey: "qkopyId")
        aCoder.encode(actualProfileName, forKey: "actualProfileName")
        aCoder.encode(about, forKey: "about")
        aCoder.encode(createdDate, forKey: "createdDate")
        aCoder.encode(location, forKey: "location")
    }
    
    required init(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        imageName = aDecoder.decodeObject(forKey: "imageName") as? String ?? ""
        profileVerified = aDecoder.decodeObject(forKey: "profileVerified") as? Bool ?? false
        isPremium = aDecoder.decodeObject(forKey: "isPremium") as? Bool ?? false
        muted = aDecoder.decodeObject(forKey: "muted") as? Bool ?? false
        mutedTill = aDecoder.decodeObject(forKey: "mutedTill") as? String ?? ""
        number = aDecoder.decodeObject(forKey: "number") as? String ?? ""
        qkopyId = aDecoder.decodeObject(forKey: "qkopyId") as? String ?? ""
        actualProfileName = aDecoder.decodeObject(forKey: "actualProfileName") as? String ?? ""
        about = aDecoder.decodeObject(forKey: "about") as? String ?? ""
        createdDate = aDecoder.decodeObject(forKey: "createdDate") as? String
        location = aDecoder.decodeObject(forKey: "location") as? [String: Any]
    }
}
