//
//  DynamicKeyObject.swift
//  Qkopy
//
//  Created by Qkopy team on 01/08/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import Foundation
class DynamicKeyObject: NSObject, NSCoding {
    var notificationDetails: [String]?
    var minimalDetails: MinimalContactDetails?
    init(notificationDetails:[String]? = nil, minimalDetails: MinimalContactDetails? = nil) {
        self.notificationDetails = notificationDetails
        self.minimalDetails = minimalDetails
    }
    func encode(with aCoder: NSCoder) {
        aCoder.encode(notificationDetails, forKey: "notificationDetails")
        aCoder.encode(minimalDetails, forKey: "minimalDetails")
    }
    required init(coder aDecoder: NSCoder) {
        notificationDetails = aDecoder.decodeObject(forKey: "notificationDetails") as? [String] ?? []
        minimalDetails = aDecoder.decodeObject(forKey: "minimalDetails") as? MinimalContactDetails ?? MinimalContactDetails(name: "", imageName: "", profileVerified: false, isPremium: false, muted: false, mutedTill: "", number: "", qkopyId: "", actualProfileName: "", about: "")
    }
}
