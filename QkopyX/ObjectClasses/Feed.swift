//
//  Feed.swift
//  Qkopy
//
//  Created by Lincy George on 11/07/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//
import Foundation

struct Feed {
    var desc: String
    var location: String
    var time: String
    var pid: String
    var privacy: String
    var showPreview: String
    var colorCode: String
    var likesArray: [String]
    var liked: Bool
    var attachments: [[String: Any]]
    var privacyId: String
    var customContacts: [String]
    var viewsCount: String
    var id: String
    var lastUpdatedDate: Date
    var isPremium: Bool
    var singlePostImageName: String
    var channelList: [String]
    
    var dictionary: [String: Any?] {
        return [
            "desc": desc,
            "location": location,
            "time": time,
            "pid": pid,
            "privacy": privacy,
            "showPreview": showPreview,
            "colorCode": colorCode,
            "likesArray": likesArray,
            "liked": liked,
            "attachments": attachments,
            "privacyId": privacyId,
            "customContacts": customContacts,
            "viewsCount": viewsCount,
            "id": id,
            "lastUpdatedDate": lastUpdatedDate,
            "isPremium": isPremium,
            "singlePostImageName": singlePostImageName,
            "channelList": channelList
        ]
    }
}

extension Feed {
    init(feed: NSDictionary) {
        let id = feed.value(forKey: "userId") as? String ?? ""
        let pid = feed.value(forKey: "_id") as? String ?? ""
        let desc = feed.value(forKey: "post_desc") as? String ?? ""
        let dataCode = desc.data(using: String.Encoding.utf8)!
        var messageString = desc
        if let msg = String(data: dataCode, encoding: String.Encoding.nonLossyASCII), msg != "" {
            messageString = msg
        }
        let loc = feed["post_loc"] as? String ?? ""
        let color_code = feed["bg_colour"] as? String ?? ""
        let time = feed["createdDate"] as? String ?? ""
        let privacy = feed["privacy"] as? String ?? "Public"
        let showPreview = feed["showPreview"] as? String ?? "true"
        let lastUpdatedDate = (feed["updatedDate"] as? String ?? "").dateFromISO8601 ?? Date()
        let likesArr = feed["postLike"] as? [String] ?? []
        let isPremium = savedContactRefDict[id]?.minimalDetails?.isPremium ?? false
        let privacyId = feed["privacyId"] as? String ?? ""
        let viewsCount = "\(String(describing: feed["postView"] ?? ""))"
        let customContacts = feed["custom_contacts"] as? [String] ?? []
        let channelList = feed["channelList"] as? [String] ?? []
        var liked = false
        if let myId = Constants.defaults.value(forKey: "myID") as? String {
            liked = likesArr.contains(myId)
        }
        let singlePostImageName = feed["post_image"] as? String ?? ""
        let attachments = feed["attachments"] as? [[String: Any]] ?? []
        self.init(desc: messageString, location: loc, time: time, pid: pid, privacy: privacy, showPreview: showPreview, colorCode: color_code, likesArray: likesArr, liked: liked, attachments: attachments, privacyId: privacyId, customContacts: customContacts, viewsCount: viewsCount, id: id, lastUpdatedDate: lastUpdatedDate, isPremium: isPremium, singlePostImageName: singlePostImageName, channelList: channelList)
    }
}

