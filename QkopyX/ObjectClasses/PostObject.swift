//
//  PostObject.swift
//  Qkopy
//
//  Created by Qkopy team on 16/07/18.
//  Copyright © 2018 Qkopy team. All rights reserved.
//

import Foundation
class PostObject: NSObject, NSCoding {
    var desc: String
    var location: String
    var time: String
    var pid: String
    var privacy: String
    var showPreview: String
    var colorCode: String
    var postFileName: String
    var likesArray: [String]
    var liked: Bool
    var attachments: [[String: Any]]
    var privacyId: String?
    var success: Bool?
    var customContacts: [String]?
    var viewsCount: String?
    var id: String?
    var params: [String: Any]?
    var originalTime: String?
    var newlyEdited: Bool?
    var isPremium: Bool?
    var channelList: [String]
    
    init(desc: String, location: String, time: String, pid: String, privacy: String, showPreview: String, colorCode: String, postFileName: String, likesArray: [String], liked: Bool, attachments: [[String: Any]], privacyId: String? = nil, success: Bool? = nil, customContacts: [String]? = nil, viewsCount: String? = nil, id: String? = nil, params: [String: Any]? = nil, originalTime: String? = nil, newlyEdited: Bool? = nil, isPremium: Bool? = nil, channelList: [String]) {
        self.desc = desc
        self.location = location
        self.time = time
        self.pid = pid
        self.privacy = privacy
        self.showPreview = showPreview
        self.colorCode = colorCode
        self.postFileName = postFileName
        self.likesArray = likesArray
        self.liked = liked
        self.attachments = attachments
        self.privacyId = privacyId
        self.success = success
        self.customContacts = customContacts
        self.viewsCount = viewsCount
        self.id = id
        self.params = params
        self.originalTime = originalTime
        self.newlyEdited = newlyEdited
        self.isPremium = isPremium
        self.channelList = channelList
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(desc, forKey: "desc")
        aCoder.encode(location, forKey: "location")
        aCoder.encode(time, forKey: "time")
        aCoder.encode(pid, forKey: "pid")
        aCoder.encode(privacy, forKey: "privacy")
        aCoder.encode(showPreview, forKey: "showPreview")
        aCoder.encode(colorCode, forKey: "colorCode")
        aCoder.encode(postFileName, forKey: "postFileName")
        aCoder.encode(likesArray, forKey: "likesArray")
        aCoder.encode(liked, forKey: "liked")
        aCoder.encode(attachments, forKey: "attachments")
        aCoder.encode(privacyId, forKey: "privacyId")
        aCoder.encode(success, forKey: "success")
        aCoder.encode(customContacts, forKey: "customContacts")
        aCoder.encode(viewsCount, forKey: "viewsCount")
        aCoder.encode(id, forKey: "id")
        aCoder.encode(params, forKey: "params")
        aCoder.encode(originalTime, forKey: "originalTime")
        aCoder.encode(newlyEdited, forKey: "newlyEdited")
        aCoder.encode(isPremium, forKey: "isPremium")
        aCoder.encode(channelList, forKey: "channelList")
    }
    
    required init(coder aDecoder: NSCoder) {
        desc = aDecoder.decodeObject(forKey: "desc") as? String ?? ""
        location = aDecoder.decodeObject(forKey: "location") as? String ?? ""
        time = aDecoder.decodeObject(forKey: "time") as? String ?? ""
        pid = aDecoder.decodeObject(forKey: "pid") as? String ?? ""
        privacy = aDecoder.decodeObject(forKey: "privacy") as? String ?? ""
        showPreview = aDecoder.decodeObject(forKey: "showPreview") as? String ?? ""
        colorCode = aDecoder.decodeObject(forKey: "colorCode") as? String ?? ""
        postFileName = aDecoder.decodeObject(forKey: "postFileName") as? String ?? ""
        likesArray = aDecoder.decodeObject(forKey: "likesArray") as? [String] ?? []
        liked = aDecoder.decodeObject(forKey: "liked") as? Bool ?? false
        attachments = aDecoder.decodeObject(forKey: "attachments") as? [[String: Any]] ?? []
        privacyId = aDecoder.decodeObject(forKey: "privacyId") as? String
        success = aDecoder.decodeObject(forKey: "success") as? Bool
        customContacts = aDecoder.decodeObject(forKey: "customContacts") as? [String]
        viewsCount = aDecoder.decodeObject(forKey: "viewsCount") as? String
        id = aDecoder.decodeObject(forKey: "id") as? String
        params = aDecoder.decodeObject(forKey: "params") as? [String: Any]
        originalTime = aDecoder.decodeObject(forKey: "originalTime") as? String
        newlyEdited = aDecoder.decodeObject(forKey: "newlyEdited") as? Bool
        isPremium = aDecoder.decodeObject(forKey: "isPremium") as? Bool
        channelList = aDecoder.decodeObject(forKey: "channelList") as? [String] ?? []
    }
}
