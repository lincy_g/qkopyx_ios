//
//  attachment.swift
//  Qkopy
//
//  Created by Lincy George on 23/07/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import Foundation.NSURL

// Query service creates Track objects
class Attachment {
    
    let url: URL
    let index: Int
    var downloaded = false
    
    init(url: URL, index: Int) {
        self.url = url
        self.index = index
    }
    
}
