//
//  Download.swift
//  Qkopy
//
//  Created by Lincy George on 23/07/19.
//  Copyright © 2019 Qkopy team. All rights reserved.
//

import Foundation
// Download service creates Download objects
class Download {
    
    var attachment: Attachment?
    init(attachment: Attachment) {
        self.attachment = attachment
    }
    
    // Download service sets these values:
    var task: URLSessionDownloadTask?
    var isDownloading = false
    var resumeData: Data?
    
    // Download delegate sets this value:
    var progress: Float = 0
    
}
