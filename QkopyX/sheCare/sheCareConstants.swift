//
//  sheCareConstants.swift
//  QkopyX
//
//  Created by Lincy George on 05/05/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import Foundation
struct sheCareConstants {
    static let showPinnedPosts = true
    static let pickFromPincode = false
    static let hasChannels = false
    static let hasRemoteConfig = false
    static let showBookmarkBtn = true
    static let showChatAndLikeBtns = false
    static let clientId = "5a9ab296b41fa605802be6b9" //"5a9ab296b41fa605802be5e9"
    static let appName = "She Care"
    static let iTunesItemIdentifier = "1511702101"
    static let downloadAppText = "Hi,\nI am using She Care App, to get instant messages from She Care. Download She Care App from http://x.qkopy.com/shecare"
    static let verifiedUsersWithoutUsernameDownloadAppText = "Shared via She Care App. Download app from http://x.qkopy.com/shecare"
    static let gmsApiKey = "AIzaSyDa-816e6orCRPjclimdICJVeEKuCvZdlE"
    static let clientLogoName = "shecareClientLogo.png"
    static let fromWhomText = "Qkopy"
    static let helplineNumber = ""
    static let showQkopyXLogoInAbout = true
    static let aboutScreenDescription = "Qkopy X is powered by Qkopy\nDesigned and Developed by Qkopy Services Private Limited"
    static let healthAlertTitle = ""
    static let healthAlertMessage = ""
    static let whoLinkToWhatsap = ""
    
    struct remoteConfigIdentifiers {
        static let configKeyName = ""
        static let labelNameKey = ""
        static let urlToHitKey = ""
        static let enableKey = ""
    }
}

