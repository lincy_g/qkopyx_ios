//
//  PhoneNumberVerificationViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 11/08/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
import Firebase

@objc protocol phoneNumberScreenDelegate {
    func setCountryCode(code: String)
}

class PhoneNumberVerificationViewController: UIViewController, phoneNumberScreenDelegate, UITextFieldDelegate {

    var indicator: UIActivityIndicatorView?

    @IBOutlet weak var smsVerificationTextView: UITextView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberField: UITextField!
    @IBOutlet weak var countryCodeButtonOutlet: UIButton!
    @IBAction func countryCodeButtonAction(_ sender: Any) {
        let vc = UIStoryboard(name: "AuthRelated", bundle: nil).instantiateViewController(withIdentifier: "countryPickerVC") as! CountryPickerTableViewController
        vc.delegate = self
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet weak var goButtonOutlet: UIButton!
    @IBAction func goButtonAction(_ sender: Any) {
        errorMessageLabel.text = ""
        indicator = UIActivityIndicatorView(frame: CGRect(x: goButtonOutlet.frame.width - 40, y: 0, width: 40, height: 40))
        indicator!.color = UIColor.white
        DispatchQueue.main.async {
            self.goButtonOutlet.isUserInteractionEnabled = false
            self.countryCodeButtonOutlet.isUserInteractionEnabled = false
            self.numberField.isUserInteractionEnabled = false
            self.navigationController?.navigationBar.isUserInteractionEnabled = false
            self.goButtonOutlet.addSubview(self.indicator!)
            self.indicator!.startAnimating()
        }

        do {
            try Auth.auth().signOut()
        } catch {
            print("something wrong")
        }
        let number = countryCodeButtonOutlet.titleLabel!.text! + numberField!.text!
        PhoneAuthProvider.provider().verifyPhoneNumber(number, uiDelegate: nil) {(verificationID, error) in
            if let nserror = error as NSError? {
                switch nserror.code {
                case 17010: self.errorMessageLabel.text = Constants.errorsWhileAuthenticating.heavyLoadAtServer
                case 17020: self.errorMessageLabel.text = Constants.errorMessages.connectionLost
                case 17041: self.errorMessageLabel.text = Constants.errorsWhileAuthenticating.numberMissing
                case 17042: self.errorMessageLabel.text = Constants.errorsWhileAuthenticating.invalidNumber
                case 17054, 17053: self.errorMessageLabel.text = Constants.errorsWhileAuthenticating.notificationError
                case 17059: self.errorMessageLabel.text = Constants.errorsWhileAuthenticating.verificationError
                case 17995: self.errorMessageLabel.text = Constants.errorsWhileAuthenticating.keychainError
                case 17999: self.errorMessageLabel.text = Constants.errorsWhileAuthenticating.internalError
                default: self.errorMessageLabel.text = nserror.localizedDescription
                }
                self.errorMessageLabel.sizeToFit()
                self.settingFrames()
                DispatchQueue.main.async {
                    self.goButtonOutlet.isUserInteractionEnabled = true
                    self.countryCodeButtonOutlet.isUserInteractionEnabled = true
                    self.navigationController?.navigationBar.isUserInteractionEnabled = true
                    self.numberField.isUserInteractionEnabled = true
                    self.indicator?.stopAnimating()
                    self.indicator?.removeFromSuperview()
                }
                //self.showMessagePrompt(error.localizedDescription)
                return
            } else {
                Constants.defaults.set(verificationID, forKey: "authVerificationID")
                let vc = UIStoryboard(name: "AuthRelated", bundle: nil).instantiateViewController(withIdentifier: "otpVC") as! OTPViewController
                vc.phonenumber = number
                Constants.defaults.set(self.countryCodeButtonOutlet.titleLabel!.text!, forKey: "countryCode")
                Constants.defaults.set(self.numberField!.text!, forKey: "myNumber")
                DispatchQueue.main.async {
                    self.goButtonOutlet.isUserInteractionEnabled = true
                    self.countryCodeButtonOutlet.isUserInteractionEnabled = true
                    self.navigationController?.navigationBar.isUserInteractionEnabled = true
                    self.numberField.isUserInteractionEnabled = true
                    self.indicator?.stopAnimating()
                    self.indicator?.removeFromSuperview()
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
             //Sign in using the verificationID and the code sent to the user
             //...
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        errorMessageLabel.text = ""
        errorMessageLabel.textAlignment = .justified
        errorMessageLabel.numberOfLines = 0
        titleBarSetting()
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
            countryCodeButtonOutlet.setTitleColor(.label, for: .normal)
        } else {
            self.view.backgroundColor = .white
            countryCodeButtonOutlet.setTitleColor(.black, for: .normal)
        }
        let lowerBorder = CALayer()
        lowerBorder.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode")).cgColor
        lowerBorder.frame = CGRect(x: 0, y: countryCodeButtonOutlet!.frame.height - 0.3, width: countryCodeButtonOutlet!.frame.width, height: 1.0)
        countryCodeButtonOutlet.layer.addSublayer(lowerBorder)
        smsVerificationTextView.text = Constants.otherConsts.smsVerificationDescription
        smsVerificationTextView.textColor = UIColor.lightGray
        smsVerificationTextView.textAlignment = .justified
        
        let currentLocale = NSLocale.current
        let countryCode = Constants.sharedUtils.getCountryPhoneCode(country: currentLocale.regionCode ?? "")
        smsVerificationTextView.isScrollEnabled = false
        countryCodeButtonOutlet.setTitle(countryCode, for: .normal)
        self.navigationController?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))

        settingFonts()
        settingFrames()
        numberFieldSetting()
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        numberField.becomeFirstResponder()
    }

    override func viewWillDisappear(_ animated: Bool) {
        numberField.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberFieldSetting() {
        numberField.borderStyle = UITextField.BorderStyle.none
        let border = CALayer()
        let width = CGFloat(1.0)
        numberField.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        border.borderColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode")).cgColor
        border.frame = CGRect(x: 0, y: numberField.frame.size.height - width, width:  numberField.frame.size.width, height: numberField.frame.size.height)
        border.borderWidth = width
        numberField.addTarget(self, action: #selector(PhoneNumberVerificationViewController.editingChanged), for: .editingChanged)
        numberField.layer.addSublayer(border)
        numberField.layer.masksToBounds = true
    }
    
    func titleBarSetting() {
        self.navigationItem.title = Constants.loginToText
        self.navigationController?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(PhoneNumberVerificationViewController.cancelBtnTapped))
    }
    
    func settingFonts() {
        //setting fonts
        errorMessageLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0)
        smsVerificationTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0)
        titleLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption1), size: 0)
        smsVerificationTextView.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0)
        countryCodeButtonOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
        numberField.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
        goButtonOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
    }
    
    func settingFrames() {
        titleLabel.frame = CGRect(x: 0, y: 80, width: self.view.frame.width, height: 40)
        countryCodeButtonOutlet.frame = CGRect(x: 40, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 70, height: 30)
        numberField.frame = CGRect(x: countryCodeButtonOutlet.frame.minX + countryCodeButtonOutlet.frame.width + 10, y: countryCodeButtonOutlet.frame.minY, width: self.view.frame.width - (100 + countryCodeButtonOutlet.frame.width), height: 30)
        errorMessageLabel.sizeToFit()
        var height = errorMessageLabel.frame.height
        if errorMessageLabel.frame.height == 0 {
            height = 10
        }
        errorMessageLabel.frame = CGRect(x: 20, y: countryCodeButtonOutlet.frame.minY + countryCodeButtonOutlet.frame.height + 10, width: self.view.frame.width - 40, height: height)
        goButtonOutlet.frame = CGRect(x: 20, y: errorMessageLabel.frame.minY + errorMessageLabel.frame.height + 10, width: self.view.frame.width - 40, height: 40)
        smsVerificationTextView.frame = CGRect(x: 20, y: goButtonOutlet.frame.minY + goButtonOutlet.frame.height, width: goButtonOutlet.frame.width, height: 70)
        smsVerificationTextView.sizeToFit()
    }
    
    func setCountryCode(code: String) {
        if code != "" {
            countryCodeButtonOutlet.setTitle(code, for: .normal)
        }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        errorMessageLabel.text = ""
    }
    
    
    //Cancel tapped
    @objc func cancelBtnTapped() {
        self.view.endEditing(true)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func editingChanged() {
        errorMessageLabel.text = ""
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
        settingFrames()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
