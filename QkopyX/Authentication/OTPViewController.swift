//
//  OTPViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 11/08/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
import Firebase
import libPhoneNumber_iOS
class OTPViewController: UIViewController, UITextFieldDelegate {
    
    //variables
    var indicator: UIActivityIndicatorView?
    var phonenumber: String?
    var otpEntered = ["","","","","",""]
    var movingToTabbarVC = false
    var userID: String?
    //outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var errorMessagelabel: UILabel!
    @IBOutlet weak var resendOutlet: UIButton!
    @IBOutlet weak var sendCodeOutlet: UIButton!
    @IBOutlet weak var otpFieldOne: UITextField!
    @IBOutlet weak var otpFieldTwo: UITextField!
    @IBOutlet weak var otpFieldThree: UITextField!
    @IBOutlet weak var otpFieldFour: UITextField!
    @IBOutlet weak var otpFieldFive: UITextField!
    @IBOutlet weak var otpFieldSix: UITextField!
    
    //MARK: - Button Actions
    //actions
    @IBAction func sendCodeAction(_ sender: Any) {
        var otp = ""
        errorMessagelabel.text = ""
        for value in otpEntered {
            otp = otp + value
        }
        if otp.count == 6 {
            indicator = UIActivityIndicatorView(frame: CGRect(x: sendCodeOutlet.frame.width - 30, y: 5, width: 30, height: 30))
            indicator!.color = UIColor.white
            DispatchQueue.main.async {
                self.sendCodeOutlet.addSubview(self.indicator!)
                self.sendCodeOutlet.isUserInteractionEnabled = false
                self.resendOutlet.isUserInteractionEnabled = false
                for sub in self.view.subviews {
                    sub.isUserInteractionEnabled = false
                }
                self.navigationController?.navigationBar.isUserInteractionEnabled = false
                self.indicator!.startAnimating()
            }
            let verificationID = Constants.defaults.string(forKey: "authVerificationID")
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: otp)
            Auth.auth().signIn(with: credential) { (user, error) in
                if let nserror = error as NSError? {
                    DispatchQueue.main.async {
                        self.sendCodeOutlet.isUserInteractionEnabled = true
                        self.resendOutlet.isUserInteractionEnabled = true
                        for sub in self.view.subviews {
                            sub.isUserInteractionEnabled = true
                        }
                        self.navigationController?.navigationBar.isUserInteractionEnabled = true
                        self.indicator?.stopAnimating()
                        self.indicator?.removeFromSuperview()
                    }
                    switch nserror.code {
                    case 17044 : self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.codeInvalid
                    case 17043 : self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.codeNotCreated
                    case 17051 : self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.codeExpired
                    case 17052 : self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.codeNotSend
                    case 17054, 17053: self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.notificationError
                    case 17059: self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.verificationError
                    case 17995: self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.keychainError
                    case 17999: self.errorMessagelabel.text = Constants.errorsWhileAuthenticating.internalError
                    default: self.errorMessagelabel.text = nserror.localizedDescription
                    }
                    self.errorMessagelabel.numberOfLines = 0
                    self.errorMessagelabel.sizeToFit()
                    self.settingFrames()
                    // ...
                    return
                } else {
                    self.callApiAfterVerification()
                }
            }
        } else {
            self.refreshAfterError(message: Constants.errorsWhileAuthenticating.otpDigitMissingError)
            return
        }
    }
    //MARK: - Network Calls
    func callApiAfterVerification() {
        if appDelegate.reachability.isReachable {
            let phoneUtil = NBPhoneNumberUtil()
            let code = Constants.defaults.value(forKey: "countryCode") as! String
            let unformattedNumber = code + (Constants.defaults.value(forKey: "myNumber") as! String)
            let country_code_Alpha = phoneUtil.getRegionCode(forCountryCode: Int(code.replacingOccurrences(of: "+", with: ""))! as NSNumber)
            var formattedPhoneNumber: String?
            do {
                let tempPhoneNumber: NBPhoneNumber = try phoneUtil.parse(unformattedNumber, defaultRegion: country_code_Alpha)
                //to check for validity of landline
                formattedPhoneNumber = try phoneUtil.format(tempPhoneNumber, numberFormat: NBEPhoneNumberFormat.INTERNATIONAL)
                Constants.defaults.set(formattedPhoneNumber, forKey: "formattedMyNumber")
            } catch let error as NSError {
                formattedPhoneNumber = unformattedNumber
                Constants.defaults.set(formattedPhoneNumber, forKey: "formattedMyNumber")
                print(error.localizedDescription)
            }
            //Constants.defaults.synchronize()
            let params = ["mobile": formattedPhoneNumber ?? "", "country_code": (country_code_Alpha ?? "").uppercased()]
            apiCall.handleApiRequest(endPart: Constants.apiCalls.checkUserExistance, params: params, completion: {[weak self] (responseJson, errorDesc, timeout) in
                guard let strongSelf = self else {
                    return
                }
                if let dataDict = responseJson as? NSDictionary, errorDesc == "" {
                    let apiReturnedID = (dataDict["data"]! as! NSDictionary)["code"]! as? Int
                    if apiReturnedID != nil {
                        Constants.defaults.set(apiReturnedID!, forKey: "apiReturnedID")
                    }
                    if apiReturnedID == 1 || apiReturnedID == 2 {
                        self?.userID = ((dataDict["data"]! as! NSDictionary)["value"]! as! NSDictionary)["_id"] as? String
                        Constants.defaults.set(self?.userID, forKey: "myID")
                        
                    }
                    if apiReturnedID! == 0 {
                        DispatchQueue.main.async {
                            self?.performSegue(withIdentifier: Constants.segueIdentifiers.splashSegue, sender: strongSelf)
                        }
                    } else {
                        Constants.defaults.set("Public", forKey: "defaultPostPrivacy")
                        if let privacy = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["photo_privacy"] as? String {
                            Constants.defaults.set(privacy, forKey: "profilePicPrivacy")
                        }
                        
                        if let teamsAvailable = ((dataDict["data"] as! NSDictionary)["value"] as! NSDictionary)["teamList"] as? [NSDictionary] {
                            var teams: [[String: Any]] = []
                            for team in teamsAvailable {
                                let modifiedTeam = ["name": team["name"] as? String ?? "", "members": team["members"] as? [String] ?? [], "teamId": team["_id"] as? String ?? ""] as [String : Any]
                                teams.append(modifiedTeam)
                            }
                            Constants.defaults.set(teams, forKey: "savedTeams")
                        }
                        if let blockedIDToSave = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["BlockedId"] as? String {
                            Constants.defaults.set(blockedIDToSave, forKey: "myBlockedID")
                        }
                        if let contactsIDToSave = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["ContactsId"] as? String {
                            Constants.defaults.set(contactsIDToSave, forKey: "myContactID")
                        }
                        if let uniqueName = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["userName"] as? String, ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["userName"] as? String != "" {
                            Constants.defaults.set(uniqueName, forKey: "myUniqueUserName")
                        }
                        if let partialImageUrlString = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["profile_pic"] as? String {
                            if partialImageUrlString != "" {
                                let position = partialImageUrlString.components(separatedBy: "/").count - 2
                                if position >= 0 {
                                    Constants.sharedUtils.saveFileDocumentDirectory(name: partialImageUrlString.components(separatedBy: "/").last ?? "", directoryNamesString: partialImageUrlString.replacingOccurrences(of: partialImageUrlString.components(separatedBy: "/").last ?? "", with: ""), lastUpdatedDate: Date())
                                }
                                Constants.defaults.set(partialImageUrlString, forKey: "myImageLink")
                            }
                        }
                        if let publicProfile = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["publicProfile"] as? Bool {
                            Constants.defaults.set(publicProfile, forKey: "isPublicProfile")
                        }
                        if let showNearBy = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["showNearBy"] as? Bool {
                            Constants.defaults.set(showNearBy, forKey: "showNearBy")
                        }
                        if let contactPrivate = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["contactPrivate"] as? Bool {
                            Constants.defaults.set(contactPrivate, forKey: "hideNumberInPublicSearch")
                        }
                        if let profileVerified = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["profileVerified"] as? Bool {
                            Constants.defaults.set(profileVerified, forKey: "profileVerified")
                        }
                        if let prefferedLocation = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["location"] as? [String: Any] {
                            if let coordinates = prefferedLocation["coordinates"] as? [Double] {
                                let locArr = ["\(coordinates[1])", "\(coordinates[0])", prefferedLocation["name"] as? String ?? ""]
                                Constants.defaults.set(locArr, forKey: "location")
                            }
                        }
                        if var createdDate = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["createdDate"] as? String {
                            let dateFromString = createdDate.dateFromISO8601
                            if dateFromString != nil {
                                createdDate = appDelegate.joinedDateString(date: dateFromString!)
                                Constants.defaults.set(createdDate, forKey: "createdDateString")
                            }
                        }
                        if let isPremium = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["isPremium"] as? Bool {
                            Constants.defaults.set(isPremium, forKey: "isPremium")
                        }
                        if let arr = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["blockList"] as? [String] {
                            Constants.defaults.setValue(arr, forKey: "blockedIDs")
                            Constants.defaults.setValue(arr, forKey: "blockedIDsStore")
                        }
                        if Constants.defaults.value(forKey: "myID") != nil {
                            if let _ = Constants.defaults.value(forKey: "countryCodes") as? [NSDictionary] {
                                Constants.defaults.removeObject(forKey: "countryCodes")
                            }
                            self?.movingToTabbarVC = true
                            if let myName = ((dataDict["data"]! as! NSDictionary)["value"] as! NSDictionary)["name"] as? String, myName != "" {
                                Constants.defaults.set(myName, forKey: "myName")
                                Constants.defaults.set(true, forKey: "LoggedIn")
                                appDelegate.loggedInSoEnable()
                            }
                            DispatchQueue.main.async {
                                Spinner.stop()
                                //Constants.defaults.synchronize()
                                if apiReturnedID! == 1 || Constants.defaults.value(forKey: "myID") == nil || Constants.defaults.value(forKey: "myName") == nil || Constants.defaults.value(forKey: "myName") as? String == "" {
                                    self?.performSegue(withIdentifier: Constants.segueIdentifiers.splashSegue, sender: self)
                                } else {
                                    self?.navigationController?.dismiss(animated: true, completion: nil)
                                }
                            }
                        } else {
                            self?.refreshAfterError(message: Constants.errorMessages.unknownErrorText)
                            return
                        }
                    }
                } else {
                    if errorDesc == "" {
                        self?.refreshAfterError(message: Constants.errorMessages.unknownErrorText)
                    } else {
                        self?.refreshAfterError(message: errorDesc)
                    }
                }
            })
        } else {
            self.refreshAfterError(message: Constants.errorMessages.connectionLost)
            return
        }
    }
    
    func refreshAfterError(message: String) {
        DispatchQueue.main.async {
            self.sendCodeOutlet.isUserInteractionEnabled = true
            self.resendOutlet.isUserInteractionEnabled = true
            for sub in self.view.subviews {
                sub.isUserInteractionEnabled = true
            }
            self.navigationController?.navigationBar.isUserInteractionEnabled = true
            self.indicator?.stopAnimating()
            self.indicator?.removeFromSuperview()
        }
        self.errorMessagelabel.text = message
        self.errorMessagelabel.numberOfLines = 0
        self.errorMessagelabel.sizeToFit()
        self.settingFrames()
    }
    
    @IBAction func resendCodeAction(_ sender: Any) {
        if phonenumber != nil {
            errorMessagelabel.text = ""
            errorMessagelabel.sizeToFit()
            indicator = UIActivityIndicatorView(frame: CGRect(x: resendOutlet.frame.width - 30, y: 5, width: 30, height: 30))
            indicator!.color = UIColor.white
            DispatchQueue.main.async {
                self.resendOutlet.addSubview(self.indicator!)
                self.sendCodeOutlet.isUserInteractionEnabled = false
                self.resendOutlet.isUserInteractionEnabled = false
                for sub in self.view.subviews {
                    sub.isUserInteractionEnabled = false
                }
                self.navigationController?.navigationBar.isUserInteractionEnabled = false
                self.indicator!.startAnimating()
            }
           // otpField.isEnabled = false
            PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber!, uiDelegate: nil) { (verificationID, error) in
                if let error = error {
                    self.errorMessagelabel.text = error.localizedDescription
                    self.errorMessagelabel.sizeToFit()
                    self.settingFrames()
                    DispatchQueue.main.async {
                        self.sendCodeOutlet.isUserInteractionEnabled = true
                        self.resendOutlet.isUserInteractionEnabled = true
                        for sub in self.view.subviews {
                            sub.isUserInteractionEnabled = true
                        }
                        self.navigationController?.navigationBar.isUserInteractionEnabled = true
                        self.indicator?.stopAnimating()
                        self.indicator?.removeFromSuperview()
                    }
                   // self.otpField.isEnabled = true
                    //self.showMessagePrompt(error.localizedDescription)
                    return
                } else {
                    Constants.defaults.set(verificationID, forKey: "authVerificationID")
                    DispatchQueue.main.async {
                        self.sendCodeOutlet.isUserInteractionEnabled = true
                        self.resendOutlet.isUserInteractionEnabled = true
                        for sub in self.view.subviews {
                            sub.isUserInteractionEnabled = true
                        }
                        self.navigationController?.navigationBar.isUserInteractionEnabled = true
                        self.indicator?.stopAnimating()
                        self.indicator?.removeFromSuperview()
                    }
                    //self.otpField.isEnabled = true
                    self.otpFieldOne.becomeFirstResponder()
                }
            }
        }
    }
    
    //MARK: - Default Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        errorMessagelabel.text = ""
        errorMessagelabel.sizeToFit()
        errorMessagelabel.textAlignment = .justified
        self.navigationItem.title = phonenumber ?? ""
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = .systemBackground
        } else {
            self.view.backgroundColor = .white
        }
        otpFieldOne.delegate = self
        otpFieldTwo.delegate = self
        otpFieldThree.delegate = self
        otpFieldFour.delegate = self
        otpFieldFive.delegate = self
        otpFieldSix.delegate = self
        
        otpFieldOne.addTarget(self, action: #selector(OTPViewController.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpFieldTwo.addTarget(self, action: #selector(OTPViewController.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpFieldThree.addTarget(self, action: #selector(OTPViewController.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpFieldFour.addTarget(self, action: #selector(OTPViewController.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpFieldFive.addTarget(self, action: #selector(OTPViewController.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        otpFieldSix.addTarget(self, action: #selector(OTPViewController.textFieldDidChange(textField:)), for: UIControl.Event.editingChanged)
        
        otpFieldOne.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        otpFieldTwo.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        otpFieldThree.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        otpFieldFour.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        otpFieldFive.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        otpFieldSix.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "colorCode"))
        settingFonts()
        settingFrames()
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        otpFieldOne.resignFirstResponder()
        otpFieldTwo.resignFirstResponder()
        otpFieldThree.resignFirstResponder()
        otpFieldFour.resignFirstResponder()
        otpFieldFive.resignFirstResponder()
        otpFieldSix.resignFirstResponder()
        //*** to uncomment
        /*
        if !movingToTabbarVC {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            movingToTabbarVC = false
        }
     */
    }
    
    override func viewDidAppear(_ animated: Bool) {
        otpFieldOne.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Setting Views
    func settingFrames() {
        titleLabel.frame = CGRect(x: 0, y: 80, width: self.view.frame.width, height: 30)
        otpFieldOne.frame = CGRect(x: self.view.frame.width/2 - 145 , y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        otpFieldTwo.frame = CGRect(x: otpFieldOne.frame.minX + otpFieldOne.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        otpFieldThree.frame = CGRect(x: otpFieldTwo.frame.minX + otpFieldTwo.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        otpFieldFour.frame = CGRect(x: otpFieldThree.frame.minX + otpFieldThree.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        otpFieldFive.frame = CGRect(x: otpFieldFour.frame.minX + otpFieldFour.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        otpFieldSix.frame = CGRect(x: otpFieldFive.frame.minX + otpFieldFive.frame.width + 10, y: titleLabel.frame.minY + titleLabel.frame.height + 20, width: 40, height: 40)
        
        errorMessagelabel.sizeToFit()
        var height = errorMessagelabel.frame.height
        if errorMessagelabel.frame.height == 0 {
            height = 10
        }
        errorMessagelabel.frame = CGRect(x: otpFieldOne.frame.minX, y:  otpFieldOne.frame.minY + otpFieldOne.frame.height + 10, width: otpFieldSix.frame.minX + otpFieldSix.frame.width - otpFieldOne.frame.minX , height: height)
        resendOutlet.frame = CGRect(x: errorMessagelabel.frame.minX, y:  errorMessagelabel.frame.minY + errorMessagelabel.frame.height + 10, width: (errorMessagelabel.frame.width/2) - 5, height: 40)
        sendCodeOutlet.frame = CGRect(x: resendOutlet.frame.minX + resendOutlet.frame.width + 10, y:  errorMessagelabel.frame.minY + errorMessagelabel.frame.height + 10, width: (errorMessagelabel.frame.width/2) - 5, height: 40)
    }
    
    func settingFonts() {
        titleLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        errorMessagelabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0)
        resendOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        sendCodeOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        for sub in self.view.subviews {
            if let field = sub as? UITextField {
                field.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
            }
        }
    }
    
    //MARK: - General
    //boundary string generation
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
            
    //MARK: - Delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == "" {
            textField.text = "\u{200B}"
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 2
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if !newString.contains("\u{200B}") {
            maxLength = 1
        }
        return newString.length <= maxLength
    }
    
    //segue call
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constants.segueIdentifiers.splashSegue {
            let vc = segue.destination as! SplashViewController
            vc.modalPresentationStyle = .fullScreen
            if self.userID != nil {
                vc.userID = self.userID!
            }
        }
    }

    //MARK: - Targets
    @objc func textFieldDidChange(textField: UITextField){
        let text = textField.text
        if text!.count == 2 {
            switch textField{
            case otpFieldOne:
                otpEntered[0] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                otpFieldTwo.becomeFirstResponder()
            case otpFieldTwo:
                otpEntered[1] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                otpFieldThree.becomeFirstResponder()
            case otpFieldThree:
                otpEntered[2] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                otpFieldFour.becomeFirstResponder()
            case otpFieldFour:
                otpEntered[3] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                otpFieldFive.becomeFirstResponder()
            case otpFieldFive:
                otpEntered[4] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                otpFieldSix.becomeFirstResponder()
            case otpFieldSix:
                otpEntered[5] = text!.replacingOccurrences(of: "\u{200B}", with: "")
                otpFieldSix.becomeFirstResponder()
            default:
                break
            }
        } else if (text!.count == 0 || text!.count == 1) {
            switch textField{
            case otpFieldOne:
                otpEntered[0] = ""
                otpFieldOne.text = "\u{200B}"
                otpFieldOne.becomeFirstResponder()
            case otpFieldTwo:
                otpEntered[1] = ""
                otpFieldOne.becomeFirstResponder()
            case otpFieldThree:
                otpEntered[2] = ""
                otpFieldTwo.becomeFirstResponder()
            case otpFieldFour:
                otpEntered[3] = ""
                otpFieldThree.becomeFirstResponder()
            case otpFieldFive:
                otpEntered[4] = ""
                otpFieldFour.becomeFirstResponder()
            case otpFieldSix:
                otpEntered[5] = ""
                otpFieldFive.becomeFirstResponder()
            default:
                break
            }
        }
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
        settingFrames()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
