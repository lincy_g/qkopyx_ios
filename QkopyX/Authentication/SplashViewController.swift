//
//  SplashViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 15/02/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit
import AVFoundation
import Photos
import libPhoneNumber_iOS
import CropViewController
class SplashViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CropViewControllerDelegate {
    
    //variables
    var backgroundImageView : UIImageView?
    var usernameField : UITextField?
    var profilePicImageView : UIImageView?
    var nextButton: UIButton?
    var profileImageName: String?
    var imageSet = false
    var movingToTabbarVC: Bool = false
    var originalImage: UIImage?
    var descLabel: UILabel?
    var titleLabel: UILabel?
    var border: CALayer?
    var userID: String?
    var imagePicker: UIImagePickerController?
    //MARK:- Default view methods
    override func viewDidLoad() {
        super.viewDidLoad()
        //setting UIViews
        if #available(iOS 13.0, *) {
            self.view.backgroundColor = UIColor.systemBackground
        } else {
            self.view.backgroundColor = UIColor.white
        }
        profilePicImageViewCreation()
        titlesAboveImageSetting()
        userNameField()
        nextButtonCreation()
        settingFonts()
        settingFrames()
        
        //adding views
        self.view.addSubview(titleLabel!)
        self.view.addSubview(descLabel!)
        self.view.addSubview(profilePicImageView!)
        self.view.addSubview(usernameField!)
        self.view.addSubview(nextButton!)
        
        //tap for entire view - to dismiss keyboard when tapped anywhere else
        let tapForKeyboardDismissal: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SplashViewController.dismissKeyboard))
        self.view.addGestureRecognizer(tapForKeyboardDismissal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        usernameField?.becomeFirstResponder()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if !movingToTabbarVC {
            self.navigationController?.setNavigationBarHidden(false, animated: true)
        } else {
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            movingToTabbarVC = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - View set up
    
    //profile pic imageView
    func profilePicImageViewCreation() {
        profilePicImageView = UIImageView()
        profilePicImageView?.layer.cornerRadius = self.view.frame.width/9
        profilePicImageView?.clipsToBounds = true
        profilePicImageView?.image = appDelegate.cropImage(img: UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "profileDummyImageName"))!)
        //adding tap gesture to the view
        let tap = UITapGestureRecognizer(target: self, action: #selector(SplashViewController.handleTapForProfilePic))
        profilePicImageView?.isUserInteractionEnabled = true
        profilePicImageView?.addGestureRecognizer(tap)
    }
    
    //UserName TextField Setting
    func userNameField() {
        usernameField = UITextField()
        //text field styling
        usernameField?.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))
        usernameField?.borderStyle = UITextField.BorderStyle.none
        border = CALayer()
        border?.borderColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode")).cgColor
        border?.borderWidth = CGFloat(1.0)
        usernameField?.layer.addSublayer(border!)
        usernameField?.layer.masksToBounds = true
        //setting placeholder
        let placeholderText = Constants.otherConsts.usernamePlaceholder
        usernameField?.placeholder = placeholderText
        usernameField?.attributedPlaceholder = NSAttributedString(string: placeholderText,attributes: [NSAttributedString.Key.foregroundColor: appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode"))])
    }
    
    //next button
    func nextButtonCreation() {
        nextButton = UIButton()
        nextButton?.addTarget(self, action: #selector(SplashViewController.nextButtonTarget), for: .touchUpInside)
        nextButton?.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))
        if #available(iOS 13.0, *) {
            nextButton?.setTitleColor(UIColor.systemBackground, for: .normal)
        } else {
            nextButton?.setTitleColor(UIColor.white, for: .normal)
        }
        nextButton?.setTitle(Constants.otherConsts.next, for: .normal)
    }
    
    //name and desc for page
    func titlesAboveImageSetting() {
        descLabel = UILabel()
        descLabel?.textColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "greyColorCode"))
        descLabel?.numberOfLines = 0
        descLabel?.text = Constants.otherConsts.descTextForProfileUpload
        
        titleLabel = UILabel()
        titleLabel?.textColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))
        titleLabel?.textAlignment = .center
        let descriptor = UIFont.systemFont(ofSize: UIFont.preferredFont(forTextStyle: .subheadline).pointSize + 7).fontDescriptor
        titleLabel?.font = UIFont(descriptor: descriptor, size: 0)
        titleLabel?.text = Constants.otherConsts.aboutMe
    }
    
    func settingFrames() {
        titleLabel?.frame = CGRect(x: 0, y: self.view.frame.height/9, width: self.view.frame.width, height: 30)
        descLabel?.frame = CGRect(x: 20, y: titleLabel!.frame.minY + titleLabel!.frame.height + 10, width: self.view.frame.width - 40, height: 40)
        descLabel?.sizeToFit()
        descLabel?.frame = CGRect(x: 20, y: titleLabel!.frame.minY + titleLabel!.frame.height + 10, width: self.view.frame.width - 40, height: descLabel!.frame.height)
        descLabel?.textAlignment = .center
        profilePicImageView?.frame = CGRect(x: self.view.frame.width/2 - self.view.frame.width/9 , y: descLabel!.frame.minY + descLabel!.frame.height + 10, width: (self.view.frame.width/9) * 2, height: (self.view.frame.width/9) * 2)
        usernameField?.frame = CGRect(x: self.view.frame.width/2 - (self.view.frame.width/4 - 15), y: profilePicImageView!.frame.minY + profilePicImageView!.frame.height + 20, width: self.view.frame.width/2 - 30, height: 20)
        border?.frame = CGRect(x: 0, y: usernameField!.frame.size.height - border!.borderWidth, width:  usernameField!.frame.size.width, height: usernameField!.frame.size.height)
        nextButton?.frame = CGRect(x: self.view.frame.width/2 - ((self.view.frame.width/7) * 2.5) , y: usernameField!.frame.minY + usernameField!.frame.height + 30, width: (self.view.frame.width/7) * 5, height: 50)
    }
    
    func settingFonts() {
        descLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .caption2), size: 0)
        titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .headline), size: 0)
        usernameField?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        nextButton?.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
    }
    
    //MARK: - Delegates
    //for image picker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
            self.imageSet = true
            if picker.sourceType == .photoLibrary {
                let imageURL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.referenceURL)] as! NSURL
                self.profileImageName = imageURL.path
                imagePicker = picker
            } else {
                picker.dismiss(animated: true, completion: nil)
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                self.profileImageName = "cameraClickForProfile.JPG"
                imagePicker = nil
            }
            //picker.dismiss(animated: true, completion: nil)
            passSelectedImageToEditVC(image: image)
        }
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        let viewController = cropViewController.children.first!
        viewController.modalTransitionStyle = .coverVertical
        viewController.presentingViewController?.dismiss(animated: true, completion: nil)
        self.imagePicker?.dismiss(animated: true, completion: nil)
        originalImage = image
        self.profilePicImageView?.image = appDelegate.cropImage(img: image)
    }
    //image saved to gallery
    @objc func image(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            print("Not saved:\(error.localizedDescription)")
        }
    }

    
    //MARK: - TARGETS
    //next button action
    @objc func nextButtonTarget() {
        self.dismissKeyboard()
        let phoneNumber = (Constants.defaults.value(forKey: "myNumber") as! String)
        let country_code = (Constants.defaults.value(forKey: "countryCode") as! String)
        self.uploadUserData(phoneNumber: phoneNumber, country_code: country_code)
    }
    
    //handling tap recognizer for image picki@objc ng
    @objc func handleTapForProfilePic() {
        DispatchQueue.main.async {
            self.usernameField?.endEditing(true)
            let actionSheetController = UIAlertController()
            let cancelActionButton = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel) { _ in
                print("Cancel")
            }
            actionSheetController.addAction(cancelActionButton)
            
            let deletePhotoActionButton = UIAlertAction(title: Constants.otherConsts.deletePhotoText, style: .destructive) { (action) in
                let image = UIImage(named: appDelegate.fetchFromPlist(plistName: "constants", key: "profileDummyImageName"))
                self.originalImage = nil
                self.profilePicImageView?.image = appDelegate.cropImage(img: image!)
            }
            actionSheetController.addAction(deletePhotoActionButton)
            
            let takePhotoActionButton = UIAlertAction(title: "Take Photo", style: UIAlertAction.Style.default, handler: { (action) in
                self.imagePicker = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
                    self.imagePicker?.navigationBar.barTintColor = UIColor.white
                    self.imagePicker?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))
                    self.imagePicker?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
                    self.imagePicker?.delegate = self
                    self.imagePicker?.mediaTypes = ["public.image"]
                    self.imagePicker?.sourceType = UIImagePickerController.SourceType.camera
                    self.imagePicker?.allowsEditing = false
                    self.imagePicker?.modalPresentationStyle = .fullScreen
                    if AVCaptureDevice.authorizationStatus(for: AVMediaType.video) ==  AVAuthorizationStatus.authorized {
                        self.present(self.imagePicker!, animated: true, completion: nil)
                    } else {
                        AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { (granted :Bool) -> Void in
                            if granted == true {
                                self.present(self.imagePicker!, animated: true, completion: nil)
                            } else {
                                let message = Constants.alertMessages.cameraAccessDenied
                                let alertController = UIAlertController(title: Constants.alertTitles.accessDeniedText, message: message, preferredStyle: .alert)
                                let cancelAction = UIAlertAction(title: Constants.otherConsts.cancelText, style: .default, handler: nil)
                                alertController.addAction(cancelAction)
                                let settingsAction = UIAlertAction(title: Constants.otherConsts.settings, style: .default) { (_) -> Void in
                                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                        return
                                    }
                                    if UIApplication.shared.canOpenURL(settingsUrl) {
                                        UIApplication.shared.openURL(settingsUrl)
                                    }
                                }
                                alertController.addAction(settingsAction)
                                self.present(alertController, animated: true, completion: nil)
                            }
                        })
                    }
                }
            })
            actionSheetController.addAction(takePhotoActionButton)
            
            let pickFromGalleryActionButton = UIAlertAction(title: "Choose Photo", style: .default) { (action) in
                self.imagePicker = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                    self.imagePicker?.navigationBar.barTintColor = UIColor.white
                    self.imagePicker?.navigationBar.tintColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode"))
                    self.imagePicker?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
                    self.imagePicker?.delegate = self
                    self.imagePicker?.mediaTypes = ["public.image"]
                    self.imagePicker?.sourceType = UIImagePickerController.SourceType.photoLibrary
                    self.imagePicker?.allowsEditing = false
                    self.imagePicker?.modalPresentationStyle = .fullScreen
                    PHPhotoLibrary.requestAuthorization({ (status) in
                        if status == .authorized {
                            self.present(self.imagePicker!, animated: true, completion: nil)
                        } else {
                            let message = Constants.alertMessages.photosAccessDenied
                            let alertController = UIAlertController(title: Constants.alertTitles.accessDeniedText, message: message, preferredStyle: .alert)
                            let cancelAction = UIAlertAction(title: Constants.otherConsts.cancelText, style: .cancel, handler: nil)
                            alertController.addAction(cancelAction)
                            let settingsAction = UIAlertAction(title: Constants.otherConsts.settings, style: .default) { (_) -> Void in
                                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                                    return
                                }
                                
                                if UIApplication.shared.canOpenURL(settingsUrl) {
                                    UIApplication.shared.openURL(settingsUrl)
                                }
                            }
                            alertController.addAction(settingsAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    })
                }
            }
            actionSheetController.addAction(pickFromGalleryActionButton)
            actionSheetController.popoverPresentationController?.sourceRect = self.profilePicImageView?.bounds ?? self.view.bounds
            actionSheetController.popoverPresentationController?.sourceView = self.profilePicImageView ?? self.view
            self.present(actionSheetController, animated: true, completion: nil)
        }
    }
    
    
    //MARK: - General methods
    
    //to dismiss keyboard
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    //to enable and disable user interaction of views
    func viewsUserInteraction(value: Bool) {
        self.profilePicImageView?.isUserInteractionEnabled = value
        self.nextButton?.isUserInteractionEnabled = value
        self.usernameField?.isUserInteractionEnabled = value
    }

    func passSelectedImageToEditVC(image: UIImage) {
        let croppingStyle = CropViewCroppingStyle.default
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        //cropController.customAspectRatio = CGSize(width: self.view.bounds.width, height: (self.view.frame.width/1.4) + 10)
        cropController.setAspectRatioPreset(CropViewControllerAspectRatioPreset.preset4x3, animated: true)
        cropController.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropController.toolbar.doneTextButton.setTitleColor(appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "themeColorCode")), for: .normal)
        cropController.aspectRatioLockEnabled = true
        cropController.aspectRatioPickerButtonHidden = true
        cropController.toolbar.rotateClockwiseButtonHidden = true
        cropController.toolbar.rotateCounterclockwiseButtonHidden = true
        cropController.toolbar.resetButton.isHidden = true
        cropController.modalPresentationStyle = .fullScreen
        if imagePicker == nil {
            self.present(cropController, animated: true, completion: nil)
        } else {
            imagePicker!.present(cropController, animated: true, completion: nil)
        }
    }

    //MARK: - Network Related
    func uploadUserData(phoneNumber : String, country_code: String) {
        if usernameField?.text != "" {
            if appDelegate.reachability.isReachable {
                Spinner.start()
                var endPart: String?
                let apiReturnedID = Constants.defaults.value(forKey: "apiReturnedID") as? Int
                if apiReturnedID == 1 || apiReturnedID == 2 {
                    endPart = Constants.apiCalls.updateUser
                } else {
                    endPart = Constants.apiCalls.signup
                }
                let userName = self.usernameField?.text ?? ""
                let imageName = profileImageName ?? appDelegate.fetchFromPlist(plistName: "constants", key: "dummyContactImageName")
                var img: UIImage?
                if self.imageSet == true {
                    img = self.originalImage
                }
                var imageData: Data?
                if img != nil {
                    imageData = Constants.sharedUtils.compressImage(image: img!, limit: 300, width: 350)
                }
                apiCall.getLinkAfterUploadingFile(fileData: imageData, name: imageName, type: "profile", id: "", completion: {[weak self] (imageLink, errorDesc, thumbnailLink) in
                    guard let strongSelf = self else {
                        return
                    }
                    if errorDesc == "" {
                        let data1 = userName.data(using: String.Encoding.nonLossyASCII)!
                        var nameToSend: String = userName
                        if let name = String(data: data1, encoding: String.Encoding.utf8) {
                            if name == "" {
                                nameToSend = userName
                            } else {
                                nameToSend = name
                            }
                        }
                        let phoneUtil = NBPhoneNumberUtil()
                        let unformattedNumber = country_code + phoneNumber
                        let country_code_Alpha = phoneUtil.getRegionCode(forCountryCode: Int(country_code.replacingOccurrences(of: "+", with: ""))! as NSNumber)
                        var phoneNumberFormatted: String?
                        do {
                            let tempPhoneNumber: NBPhoneNumber = try phoneUtil.parse(unformattedNumber, defaultRegion: country_code_Alpha)
                            //to check for validity of landline
                            phoneNumberFormatted = try phoneUtil.format(tempPhoneNumber, numberFormat: NBEPhoneNumberFormat.INTERNATIONAL)
                            Constants.defaults.set(phoneNumberFormatted, forKey: "formattedMyNumber")
                        } catch let error as NSError {
                            phoneNumberFormatted = unformattedNumber
                            Constants.defaults.set(phoneNumberFormatted, forKey: "formattedMyNumber")
                            print(error.localizedDescription)
                        }
                        //Constants.defaults.synchronize()
                        var params: [String: Any]?
                        if apiReturnedID == 0 {
                            params = ["name" : nameToSend, "mobile" : phoneNumberFormatted ?? "", "pic_privacy": "Public", "country_code": (country_code_Alpha ?? "").uppercased(), "profile_pic": imageLink]
                        } else {
                            params = ["userid": strongSelf.userID ?? "","updateObj": ["name": nameToSend, "profile_pic": imageLink]]
                        }
                        //["content_type": imageExtension, "filename": imageName, "file_data": base64String ?? ""]] as [String : Any]
                        Constants.defaults.setValue(true, forKey: "showTrendingScreenFirst")
                        self?.profileImageName = nil
                        print(params!)
                        apiCall.handleApiRequest(endPart: endPart!, params: params!, completion: {[weak self] (responseJson, errorDesc, timeout) in
                            Constants.defaults.set("Public", forKey: "defaultPostPrivacy")
                            if responseJson != nil && errorDesc == "" {
                                let dataDict = responseJson as? NSDictionary
                                if apiReturnedID == 0 {
                                    if self?.userID == nil {
                                        let id = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["_id"] as? String
                                        Constants.defaults.set(id, forKey: "myID")
                                    } else {
                                        Constants.defaults.set(self?.userID!, forKey: "myID")
                                    }
                                    if let blockedIDToSave = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["BlockedId"] as? String {
                                        Constants.defaults.set(blockedIDToSave, forKey: "myBlockedID")
                                    }
                                    if let contactsIDToSave = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["ContactsId"] as? String {
                                        Constants.defaults.set(contactsIDToSave, forKey: "myContactID")
                                    }
                                    if let privacy = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["photo_privacy"] as? String {
                                        Constants.defaults.set(privacy, forKey: "profilePicPrivacy")
                                    }
                                    if let publicProfile = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["publicProfile"] as? Bool {
                                        Constants.defaults.set(publicProfile, forKey: "isPublicProfile")
                                    }
                                    if let showNearBy = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["showNearBy"] as? Bool {
                                        Constants.defaults.set(showNearBy, forKey: "showNearBy")
                                    }
                                    if let profileVerified = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["profileVerified"] as? Bool {
                                        Constants.defaults.set(profileVerified, forKey: "profileVerified")
                                    }
                                    if let prefferedLocation = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["location"] as? [String: Any] {
                                        if let coordinates = prefferedLocation["coordinates"] as? [Double] {
                                            let locArr = ["\(coordinates[1])", "\(coordinates[0])", prefferedLocation["name"] as? String ?? ""]
                                            Constants.defaults.set(locArr, forKey: "location")
                                        }
                                    }
                                    if var createdDate = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["createdDate"] as? String {
                                        let dateFromString = createdDate.dateFromISO8601
                                        if dateFromString != nil {
                                            createdDate = appDelegate.joinedDateString(date: dateFromString!)
                                            Constants.defaults.set(createdDate, forKey: "createdDateString")
                                        }
                                    }
                                    if let contactPrivate = ((dataDict!["data"]! as! NSDictionary)["value"] as! NSDictionary)["contactPrivate"] as? Bool {
                                        Constants.defaults.set(contactPrivate, forKey: "hideNumberInPublicSearch")
                                    }
                                }
                                if nameToSend == "" {
                                    Constants.defaults.set(userName, forKey: "myName")
                                }
                                Constants.defaults.set(nameToSend, forKey: "myName")
                                let position = imageLink.components(separatedBy: "/").count - 2
                                if position >= 0 {
                                    Constants.sharedUtils.saveFileDocumentDirectory(name: imageLink.components(separatedBy: "/").last ?? "", directoryNamesString: imageLink.replacingOccurrences(of: imageLink.components(separatedBy: "/").last ?? "", with: ""), lastUpdatedDate: Date())
                                }
                                Constants.defaults.set(imageLink, forKey: "myImageLink")
                                if Constants.defaults.value(forKey: "myID") != nil {
                                    Constants.defaults.set(true, forKey: "LoggedIn")
                                    appDelegate.loggedInSoEnable()
                                    DispatchQueue.main.async {
                                        self?.viewsUserInteraction(value: true)
                                        Spinner.stop()
                                        self?.navigationController?.dismiss(animated: true, completion: nil)
                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        let message = Constants.errorMessages.unknownErrorText
                                        let alertController = UIAlertController(title: Constants.alertTitles.error, message: message, preferredStyle: .alert)
                                        alertController.addAction(UIAlertAction(title: Constants.otherConsts.okText, style: .cancel, handler : nil))
                                        self?.present(alertController, animated: true, completion: { [weak self] in
                                            self?.viewsUserInteraction(value: true)
                                            Spinner.stop()
                                        })
                                    }
                                }
                            } else {
                                if errorDesc == "" {
                                    DispatchQueue.main.async {
                                        let message = Constants.errorMessages.unknownErrorText
                                        let alertController = UIAlertController(title: Constants.alertTitles.error, message: message, preferredStyle: .alert)
                                        alertController.addAction(UIAlertAction(title: Constants.otherConsts.okText, style: .cancel, handler : nil))
                                        self?.present(alertController, animated: true, completion: { [weak self] in
                                            self?.viewsUserInteraction(value: true)
                                            Spinner.stop()
                                        })                                    }
                                } else {
                                    DispatchQueue.main.async {
                                        let alertController = UIAlertController(title: errorDesc, message: "", preferredStyle: .alert)
                                        alertController.addAction(UIAlertAction(title: Constants.otherConsts.okText, style: .cancel, handler : nil))
                                        self?.present(alertController, animated: true, completion: { [weak self] in
                                            self?.viewsUserInteraction(value: true)
                                            Spinner.stop()
                                        })
                                    }
                                }
                            }
                        })
                    } else {
                        DispatchQueue.main.async {
                            let message = errorDesc
                            let alertController = UIAlertController(title: message, message: "", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: Constants.otherConsts.okText, style: .cancel, handler : nil))
                            self?.present(alertController, animated: true, completion: { [weak self] in
                                self?.viewsUserInteraction(value: true)
                                Spinner.stop()
                            })
                        }
                    }
                })
            } else {
                DispatchQueue.main.async {
                    let alertController = UIAlertController(title: Constants.alertTitles.error, message: Constants.errorMessages.connectionLost, preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: Constants.otherConsts.okText, style: .cancel, handler: nil))
                    self.present(alertController, animated: true, completion: { [weak self] in
                        self?.viewsUserInteraction(value: true)
                        Spinner.stop()
                    })
                }
            }
        } else {
            DispatchQueue.main.async {
                let alertController = UIAlertController(title: Constants.alertTitles.error, message: Constants.splashScreenConsts.usernameMissing, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: Constants.otherConsts.okText, style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: { [weak self] in
                    self?.viewsUserInteraction(value: true)
                    Spinner.stop()
                })
            }
        }
    }
    
    //MARK: - Notifications
    @objc func userChangedTextSize(notification: NSNotification) {
        settingFonts()
        settingFrames()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
