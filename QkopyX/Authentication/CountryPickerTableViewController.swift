//
//  CountryPickerTableViewController.swift
//  Qkopy
//
//  Created by Qkopy team on 11/08/17.
//  Copyright © 2017 Qkopy team. All rights reserved.
//

import UIKit

class CountryPickerTableViewController: UITableViewController, UISearchResultsUpdating, UISearchControllerDelegate {
    var countries: [NSDictionary] = []
    var filtered : [NSDictionary] = []
    weak var delegate: phoneNumberScreenDelegate?
    var resultSearchController:UISearchController!
    var selectedCountryCode: String?
    var searchActive : Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        let countryCodeArray = NSLocale.isoCountryCodes
        searchBarUISetUp()
        selectedCountryCode = ""
        if let arr = Constants.defaults.value(forKey: "countryCodes") as? [NSDictionary] {
            countries = arr
        } else {
            self.tableView.separatorStyle = .none
            Spinner.start()
            DispatchQueue.main.async {
                for code in countryCodeArray as [String] {
                    let id = NSLocale.localeIdentifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
                    let name = NSLocale(localeIdentifier: "en_UK").displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
                    let number = Constants.sharedUtils.getCountryPhoneCode(country: code)
                    let dict = [name : number]
                    if name != "" && number != "" {
                        self.countries.append(dict as NSDictionary)
                    }
                    if countryCodeArray.last == code {
                        Spinner.stop()
                        self.tableView.reloadData()
                    }
                }
                Constants.defaults.setValue(self.countries, forKey: "countryCodes")
            }
        }
        self.navigationItem.title = "Pick your Country"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func viewWillDisappear(_ animated: Bool) {
        delegate?.setCountryCode(code: selectedCountryCode!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filtered = []
        if (searchController.searchBar.text?.count)! > 0 {
            filtered.removeAll(keepingCapacity: false)
            filtered = countries.filter({ country in
                if let countryName = country.allKeys[0] as? String, let query = searchController.searchBar.text {
                    return countryName.range(of: query) != nil
                }
                return false
            })
            self.searchActive = true
            tableView.reloadData()
        } else {
            self.searchActive = false
            filtered.removeAll(keepingCapacity: false)
            filtered = []
            tableView.reloadData()
        }
    }

    func searchBarUISetUp() {
        resultSearchController = UISearchController(searchResultsController: nil)
        resultSearchController.searchResultsUpdater = self
        resultSearchController.delegate = self
        resultSearchController.hidesNavigationBarDuringPresentation = true
        resultSearchController.searchBar.searchBarStyle = UISearchBar.Style.prominent
        self.definesPresentationContext = true
        resultSearchController.dimsBackgroundDuringPresentation = false
        resultSearchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = resultSearchController.searchBar
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if searchActive {
            return filtered.count
        }
        return countries.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "countryCodeCell", for: indexPath)
        let accessoryLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 30, height: 35))
        accessoryLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .footnote), size: 0)
        accessoryLabel.textColor = UIColor.gray
        if searchActive {
            cell.textLabel?.text = filtered[indexPath.row].allKeys[0] as? String
            accessoryLabel.text = filtered[indexPath.row].allValues[0] as? String
        } else {
            cell.textLabel?.text = countries[indexPath.row].allKeys[0] as? String
            accessoryLabel.text = countries[indexPath.row].allValues[0] as? String
        }
        if #available(iOS 10.0, *) {
            cell.textLabel?.adjustsFontForContentSizeCategory = true
            accessoryLabel.adjustsFontForContentSizeCategory = true
        } else {
            // Fallback on earlier versions
        }
        accessoryLabel.sizeToFit()
        cell.accessoryView = accessoryLabel
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if searchActive {
            selectedCountryCode = filtered[indexPath.row].allValues[0] as? String
        } else {
            selectedCountryCode = countries[indexPath.row].allValues[0] as? String
        }
        self.navigationController?.popViewController(animated: true)
        //tableView.reloadData()
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
