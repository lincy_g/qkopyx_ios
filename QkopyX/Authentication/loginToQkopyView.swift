//
//  loginToQkopyView.swift
//  K-Spk Kerala Space Park
//
//  Created by Lincy George on 09/04/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import UIKit

class loginToQkopyView: UIView {
    
    @IBOutlet weak var loginDescLabel: UILabel!
    
    @IBOutlet weak var loginButtonOutlet: UIButton!
    @IBAction func loginButtonAction(_ sender: Any) {
        Constants.sharedUtils.authenticateMobileNumber()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        loginDescLabel.text = savedContactRefDict[Constants.clientSpecificConstantsStructName.clientId].minimalDetails?.name as? String ?? Constants.clientSpecificConstantsStructName.appName)
        loginButtonOutlet.setTitle(Constants.loginToText, for: .normal)
        loginDescLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        loginButtonOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        loginButtonOutlet.backgroundColor = appDelegate.hexStringToUIColor(hex: appDelegate.fetchFromPlist(plistName: "constants", key: "defaultBlueColorCode"))
        loginButtonOutlet.setTitleColor(.white, for: .normal)
        if #available(iOS 13.0, *) {
            self.backgroundColor = UIColor.systemBackground
            loginDescLabel.textColor = UIColor.secondaryLabel
        } else {
            self.backgroundColor = .white
            loginDescLabel.textColor = UIColor.gray
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(userChangedTextSize(notification:)), name: UIContentSizeCategory.didChangeNotification, object: nil)
    }
    @objc func userChangedTextSize(notification: NSNotification) {
        loginDescLabel.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
        loginButtonOutlet.titleLabel?.font = UIFont(descriptor: .preferredDescriptor(textStyle: .subheadline), size: 0)
    }
}
