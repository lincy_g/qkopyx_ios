//
//  File.swift
//  kerala-spk
//
//  Created by Lincy George on 20/03/20.
//  Copyright © 2020 Lincy George. All rights reserved.
//

import Foundation
struct kspkConstants {
    static let showPinnedPosts = false
    static let pickFromPincode = false
    static let hasChannels = false
    static let hasRemoteConfig = false
    static let showBookmarkBtn = false
    static let showChatAndLikeBtns = true
    static let clientId = "5b0a500d903f544f7cf93fa1"
    static let appName = "K-Spk Kerala Space Park"
    static let iTunesItemIdentifier = "1496768405"
    static let downloadAppText = "Hi,\nI am using K-Spk(Kerala Space Park) App, to get instant messages from Kerala Space Park. Download K-Spk(Kerala Space Park) App from http://x.qkopy.com/keralaspacepark"
    static let verifiedUsersWithoutUsernameDownloadAppText = "Shared via K-Spk App. Download app from http://x.qkopy.com/keralaspacepark"
    static let gmsApiKey = "AIzaSyBjMzblrfBeFaPoF-TCtM4u0Wi9LvpXDXQ"
    static let clientLogoName = "kspkClientLogo.png"
    static let fromWhomText = "Qkopy"
    static let helplineNumber = ""
    static let showQkopyXLogoInAbout = true
    static let aboutScreenDescription = "Qkopy X is powered by Qkopy\nDesigned and Developed by Qkopy Services Private Limited"
    static let healthAlertTitle = ""
    static let healthAlertMessage = ""
    static let whoLinkToWhatsap = ""
    
    struct remoteConfigIdentifiers {
        static let configKeyName = ""
        static let labelNameKey = ""
        static let urlToHitKey = ""
        static let enableKey = ""
    }
}
